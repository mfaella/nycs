#!/bin/bash

cat $1 | sed 's/&amp;/\&/g' | sed 's/&lt;/</g' | sed 's/&gt;/>/g'
