#!/bin/bash

cat $1 | sed -r 's!((</)|(<nycs)|(<spaceex)|(<guard)|(<invariant)|(<assignment)|(<transition)|(<location)|(<contr)|(<flow)|(<param)|(<component)|(<scenario)|(<outer-operator)|(<inner-operator)|(<safe)|(<region)|(<reach)|(<max)|(<timeout)|(<over))[^>]*>!<<&>>!g' | sed -r 's!&!\&amp;!g' | sed -r 's!([^<])<([^<])!\1\&lt;\2!g' | sed -r 's!([^>])>([^>])!\1\&gt;\2!g' | sed -r 's!<<<!<!g' | sed -r 's!>>>!>!g'
