#!/bin/bash

# nycs output filter: splits result in different files for each location

#if (( $# != 5 )); then
#    echo "Illegal number of parameters"
#    echo "Usage: $0 <file.spaceex>"
#    exit 1
#fi

temp=$(mktemp)

# Leaves the location name and its winning region on each line
# Each polyhedron is in the format { constraint & ... & constraint }
(grep "Location:" | sed 's/Location: \([^{]*\){/\1 {/g') < "${1:-/dev/stdin}" > $temp

# write separate files
while read -r line || [[ -n "$line" ]]; do
    loc_name=$(echo $line | sed 's/ {.*//')
    polyhedron=$(echo $line | sed 's/[^{]*{/{/')
    echo "Location name:" $loc_name
    echo "Polyhedron:" $polyhedron
    filename="${loc_name}.poly"
    echo "Saving" $filename
    echo $loc_name "(" $polyhedron ")" > $filename
    # append filename to list
    args="$args -f $filename"
    # collect the variables from all polyhedra
    var_list=$var_list$'\n'$(echo $polyhedron | sed 's/[+-]/ /g' | sed 's/[^ [:alpha:]]//g' | sed 's/  */\n/g')
done < $temp

rm $temp

# echo -e $var_list

vars=$(echo $var_list | sed 's/  */\n/g' | sort | uniq | tr '\n' ' ')

#echo "You may now call:   poly2tex -crop $2 $3 $4 $5 $args $vars"

