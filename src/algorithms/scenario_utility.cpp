/*
 *  File: scenario_utility.cpp
 *  Created on: September 5, 2017
 *  Author: Marco Urbano
*/

#include "scenario_utility.h"
#include "pairwise_reduce.h"


using namespace std;

namespace NaPoly{


/*
AGGIUNGE AL POWERSET FIRST IL POWERSET SECOND
*/
void add_powerset(PS &first, PS &second) {
//Aggiunge al powerset first tutti i disgiunti del powerset second
  PS::iterator it;

  for (it=second.begin(); it!=second.end(); it++)
    if (!(it->pointset().is_empty()))
      first.add_disjunct(it->pointset());
}



// Funzione che fa le veci della vecchia continuous_set_PPL_NNC::negate(), serve per negare il PS.
void negate(PS& to_negate){

	using IO_Operators::operator<<;

	PS p_neg(to_negate.space_dimension(), Parma_Polyhedra_Library::UNIVERSE);
	//cout << "to_negate = " << to_negate << "p_neg = " << p_neg << endl;
	p_neg.difference_assign(to_negate);
	//cout << "to_negate = " << to_negate << "p_neg = " << p_neg << endl;
	fast_pairwise_reduce(p_neg);
	//cout << "to_negate = " << to_negate << "p_neg = " << p_neg << endl;
	to_negate = p_neg;
	//cout << "to_negate = " << to_negate << "p_neg = " << p_neg << endl;
}

/* GIA' CONTENUTA IN rwa_operations.h
void simplify(PS& to_simplify){

  for(PS::const_iterator it=to_simplify.begin(); it!=to_simplify.end(); it++)
		 it->pointset().minimized_constraints();
}
*/



//fa l'inverso rispetto all'origine
void get_prepoly(Poly& my_poly)
{
    // maps all points componentwise by x'=-x
    for (unsigned int i=0; i<my_poly.space_dimension(); ++i)
    {
      Linear_Expression lexp=-Variable(i);
      my_poly.affine_image(Variable(i),lexp);
    }
}


// Metodo negate trovato in cpre_reach.cpp, probabilmente scritto da Benerecetti.
/*
  PS negate_bene(PS p){

  Pointset_Powerset<NNC_Polyhedron> p_neg(p.space_dimension(),UNIVERSE);
  p_neg.difference_assign(p);
  simplify(p_neg);
  fast_pairwise_reduce(p_neg);
  return p_neg;
  }*/




}
