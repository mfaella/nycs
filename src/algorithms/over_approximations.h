/*
 *  File: scenario_algo.h
 *  Created on: September 10, 2017
 *  Author: Marco Urbano
 */


#ifndef _SCENARIO_ALGO_NAPOLY__
#define _SCENARIO_ALGO_NAPOLY__

#include "../automaton/preprocessor.h"
#include "../automaton/Location.h"

namespace NaPoly{
  std::pair<PS, PS> over_small_approx(PS& U, PS& V, const Location& loc);
  std::pair<PS, PS> over_large_approx(PS& U, PS& V, const Location& loc);
}

#endif
