/*
 * sorm_localplus.h
 *
 *  Created on: 11/set/2014
 *      Author: donatella
 */

#ifndef SORM_LOCALPLUS_H_
#define SORM_LOCALPLUS_H_


#include "../rwa/rwa_operations.h"
#include "../rwa/rwa_map_definition.h"
// #include "../rwa/rwa_maps.h"

#include "../../automaton/Location.h"
#include "../../scenario/scenario_bundle.h"
#include "../../scenario/Scenario_location_bundle.h"


#include <ext/hash_map>

namespace NaPoly{
using namespace Parma_Polyhedra_Library;

typedef Pointset_Powerset<NNC_Polyhedron> PS;
typedef NNC_Polyhedron Poly;

// Initialize the two maps related to the internal adjaciencies [ruadj],[rradj]
//and the external map related to the external adjaciencies [extadj] (Used in Local+ Approach)
list_nnc_type init_maps(PS &risky_ps, PS &under_ps, Poly postflow_nnc, Poly preflow_nnc, hash_map_type& rradj,
		hash_map_u_type& ruadj, hash_map_type& extadj, list_nnc_type& queue);

// Initialize the maps for successive calls to sorM (allows to reuse the maps).
// static list_nnc_type align_maps(PS under_ps, PS under_eff, Poly postflow_nnc, Poly preflow_nnc, hash_map_type& rradj,		hash_map_u_type& ruadj, hash_map_type& extadj, list_nnc_type& queue);

// Update the internal adj only by the adj of new polyhedron [pnew_ps].
//Called when the original polyhedron has no internal adjs.(Local+ approach)
void update_int(PS under_approx, PS &pnew_ps, Poly *p_point, hash_map_type &rradj, hash_map_u_type &ruadj);

// Update internal and external maps (when [*p1_point] has a non empty adjlist). (Local+).
void update_int_ext(PS under_approx, PS &pnew_ps, PS &cut_ps, Poly *p1_point, Poly &postflow_nnc, Poly &preflow_nnc,
		hash_map_type &rradj, hash_map_u_type &ruadj, hash_map_type &extadj, list_nnc_type &queue);

// The main loop. Refines internal and external maps. (Local+).
// (Output: pair<risky_ps, totalcut_ps>)
static std::pair<PS, PS> refine_maps(PS &under_approx_ps, Poly postflow, Poly flow_nnc, hash_map_type& rradj,
		hash_map_u_type& ruadj, hash_map_type& extadj, list_nnc_type& queue);


// Compute sorM by using the LOCAL+ APPROACH (Reuse of the Maps and the only relevant portion of NOT Z)
// (Output: pair<risky_ps, totalcut_ps>)
std::pair<PS, PS> sor_local_recycle(PS &over_approx, PS &under_approx, const Location& loc, ScenarioLocationBundle& bundle_loc);


}

#endif /* SORM_LOCALPLUS_H_ */
