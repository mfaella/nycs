/*
 * sorm_basic.h
 *
 *  Created on: 29/set/2014
 *      Author: donatella
 *
 * Modified on: 06 September 2017.
 *      Author: Marco Urbano.
 */

#ifndef SORM_BASIC_H_
#define SORM_BASIC_H_

// Per utilizzare i metodi sui PS. (aggiunta di un PS ad un altro).
#include "../rwa/rwa_operations.h"
#include "../rwa/rwa_map_definition.h"
#include <ext/hash_map>


// Libreria che include tutti i metodi rinnovati.
#include "../scenario_utility.h"


namespace NaPoly {
using namespace Parma_Polyhedra_Library;

typedef Pointset_Powerset<NNC_Polyhedron> PS;

std::pair<PS, PS> sor_basic(PS over_approx_ps, PS under_approx_ps, Poly flow);
}




#endif /* SORM_BASIC_H_ */
