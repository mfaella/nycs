#include "sor_local.h"
#include "sor_global.h"
#include "../pairwise_reduce.h"
#include <cstring>

namespace NaPoly {
list_nnc_type init_maps(PS &risky_ps, PS &under_ps, Poly postflow_nnc, Poly preflow_nnc, hash_map_type& intadj,
		hash_map_type& extadj, list_nnc_type& queue)
{
  	dimension_type dim=risky_ps.space_dimension();
  	hash_map_type::iterator it1_intadj, it2_intadj;
	PS::iterator it_risky, it_under;
	Poly *p1_point, *p2_point, p1close_nnc(dim), pprime_nnc(dim), b_nnc(dim), *b_point;
	list_nnc_type empty_list;
	list_nnc_type under_nnc;

	// Insert all (newly generated) elements of risky into intadj
	for (it_risky=risky_ps.begin(); it_risky!=risky_ps.end(); it_risky++)
	{
		p1_point = new Poly(it_risky->pointset());
		intadj[p1_point] = empty_list;
	}
	// Insert all (newly generated) elements of under in a list
	for (it_under=under_ps.begin(); it_under!=under_ps.end(); it_under++)
	{
		p1_point = new Poly(it_under->pointset());
		under_nnc.push_back(p1_point);
	}
	// For each P1 in intadj (in risky)
	for (it1_intadj=intadj.begin(); it1_intadj!=intadj.end(); it1_intadj++)
	{
		p1_point = it1_intadj->first;
		p1close_nnc = *p1_point;
		p1close_nnc.topological_closure_assign();
		PS adj_ps(*p1_point);
		for (list_nnc_type::iterator it_adj=intadj[p1_point].begin(); it_adj!=intadj[p1_point].end(); it_adj++)
		{
			pprime_nnc = **it_adj;
			adj_ps.add_disjunct(pprime_nnc);
		}
		// For each P2<>P1 in intadj (in risky)
		it2_intadj=it1_intadj;
		it2_intadj++;
		while (it2_intadj!=intadj.end())
		{
			p2_point = it2_intadj->first;
			// If boundary is not empty, P1 and P2 are adjacents
			if (!simple_border_opt(*p1_point, p1close_nnc, *p2_point).is_empty())
			{
			  // so, put each of them in the adjacency list of the other
			  intadj[p1_point].push_back(p2_point);
			  intadj[p2_point].push_back(p1_point);
			  // and add P2 to the powerset of adjacents of P1
			  adj_ps.add_disjunct(*p2_point);
			}
			++it2_intadj;
		} // ENDFOR P2

		// For each P2 in under, update adjlist of the polyhedron P1 (in risky)
		for (list_nnc_type::iterator it_listunder=under_nnc.begin(); it_listunder!=under_nnc.end(); it_listunder++)
		{
			p2_point = *it_listunder;
			if (!simple_border_opt(*p1_point, p1close_nnc, *p2_point).is_empty())
				adj_ps.add_disjunct(*p2_point);
		}

		// Compute exit borders of P1
		Poly p1_post(*p1_point);
		p1_post.time_elapse_assign(postflow_nnc);
		PS p1_exit_ps(p1_post);
		p1_exit_ps.difference_assign(adj_ps);

//		assert (empty_list.empty());

		bool first_ext=true;
		for (PS::iterator iter_exit=p1_exit_ps.begin(); iter_exit!=p1_exit_ps.end(); iter_exit++)
		{
			pprime_nnc = iter_exit->pointset();
			b_nnc = exit_border_opt(*p1_point, p1close_nnc, pprime_nnc, preflow_nnc);

			// If P1 is exposed, insert P1 and its border in extadj
			if (!b_nnc.is_empty())
			{
			  if (first_ext) // we just realized that P1 is exposed
			    {
			      extadj[p1_point] = empty_list;
			      queue.push_back(p1_point);
			      first_ext=false;
			    }
			  b_point = new Poly(b_nnc);
			  extadj[p1_point].push_back(b_point);
			}
		} // ENDFOR P'
	} // ENDFOR P1
	return under_nnc;
}


// Update of the internal adj of [p2_point] in under (adjacents to the original P):
// checks borders only between [p2_point] and Pnew (no check for (Pnew, Pnew))
static void update_int_under(list_nnc_type &mylist_nnc, Poly *p2_point, hash_map_type &intadj)
{
	Poly *q_point;
	list_nnc_type:: iterator it_list;

	// For each Q in mylist_nnc (for each Q in Pnew)
	for (it_list=mylist_nnc.begin(); it_list!=mylist_nnc.end(); it_list++)
	{
		q_point = *it_list;
		Poly qclose_nnc(*q_point);
		qclose_nnc.topological_closure_assign();

		// If P2 and Q are adjacent: insert P2 in adjlist of Q
		if (!simple_border_opt(*q_point, qclose_nnc, *p2_point).is_empty())
			// update only the adjlist of Q (as P2 is in under)
			intadj[q_point].push_back(p2_point);
	}
}

// Update external adj of [p2_point] (adjacents to the original P):
//checks borders among [*p2_point] and the parts removed from original P
void update_external(PS &cut_ps, Poly *p2_point, Poly &postflow_nnc, Poly &preflow_nnc, hash_map_type &extadj,
		list_nnc_type &queue)
{
	dimension_type dim = preflow_nnc.space_dimension();
	list_nnc_type list_nnc;
	Poly q1_nnc(dim), *b_point, p2close_nnc(*p2_point);
	Poly p2_post(*p2_point);
	p2_post.time_elapse_assign(postflow_nnc);
	PS external_ps(p2_post);
	PS:: iterator it_cut;

	p2close_nnc.topological_closure_assign();
	external_ps.intersection_assign(cut_ps);
	// For each Q1 in cnew
	for (it_cut=external_ps.begin(); it_cut!=external_ps.end(); it_cut++)
	{
		q1_nnc = it_cut->pointset();
		Poly b_nnc(exit_border_opt(*p2_point, p2close_nnc, q1_nnc, preflow_nnc));
		if (!b_nnc.is_empty())
		{
			b_point = new Poly(b_nnc);
			if (extadj.find(p2_point)==extadj.end())
				extadj[p2_point]=list_nnc;
			extadj[p2_point].push_back(b_point);
			bool find=false;
			// Scan queue
			list_nnc_type::iterator it_queue=queue.begin();
			while (it_queue!=queue.end() && (!find))
			{
				if (*it_queue==p2_point)
					find=true;
				it_queue++;
			}
			// Insert P' in queue only if P' not in already queue
			if (!find)
				queue.push_back(p2_point);
		}
	} // ENDFOR cut
}

// Update internal and external maps (when [*p1_point] has a non-empty adjlist).
void update_int_ext(PS &pnew_ps, PS &cut_ps, Poly *p1_point, Poly &postflow_nnc, Poly &preflow_nnc,
		    hash_map_type &intadj, hash_map_type &extadj, list_nnc_type &queue)
{
	bool first=true;
	Poly *p2_point, *q_point;
	list_nnc_type pnew_list, empty_list;

	// Insert all convex Q in Pnew in intadj
	for (PS::iterator it_pnew=pnew_ps.begin(); it_pnew!=pnew_ps.end(); it_pnew++)
	{
		q_point=new Poly(it_pnew->pointset());
		intadj[q_point] = empty_list;
		pnew_list.push_back(q_point);
	}

	update_internal_of_list(pnew_list, intadj);

	// For each P2 adj to P1
	for (list_nnc_type::iterator it_list=intadj[p1_point].begin(); it_list!=intadj[p1_point].end(); it_list++)
	{
		p2_point=*it_list;
		if (intadj.find(p2_point) != intadj.end())
		{
			// P2 belongs to risky
			// Remove P1 from the list of the adjacents of P2
			intadj[p2_point].remove(p1_point);

			update_internal_of_poly_and_list(p2_point, pnew_list, intadj);
			update_external(cut_ps, p2_point, postflow_nnc, preflow_nnc, extadj, queue);
    		} else {
		  // P2 belongs to under_approx
		  update_int_under(pnew_list, p2_point, intadj);
		}
	}
}


// The main loop. Refines internal and external maps.
// (Output: pair<risky_ps, totalcut_ps>)
std::pair<PS, PS> refine_maps(PS &under_approx_ps, Poly postflow, Poly flow_nnc,
	       hash_map_type& intadj, hash_map_type& extadj, list_nnc_type& queue)
{
	dimension_type dim = flow_nnc.space_dimension();
	Poly *p_point, p_nnc(dim);
	PS cut_ps(dim), risky_ps(dim,EMPTY), totalcut_ps(dim, EMPTY);
	PS::iterator it_under;
	hash_map_type::iterator it_int;
	list_nnc_type::iterator it_list;

	// Scan queue
	while (!queue.empty())
	{
		// Take first exposed Convex Polyhedron P from the queue
		p_point=queue.front();
		// Delete first element
		queue.pop_front();
		p_nnc = *p_point;

		// Cut from P the preflow of its exit borders
		cut_ps = cut_polyhedra_with_exit_borders(p_point, flow_nnc, extadj);
		// if cut is not empty update the maps accordingly
		if (!cut_ps.empty())
		{
			//simplify(cut_ps);
			//cut_ps.pairwise_reduce();
			totalcut_ps.upper_bound_assign(cut_ps);
			// compute Pnew
			PS pnew_ps(p_nnc);
			pnew_ps.difference_assign(cut_ps);
			simplify(pnew_ps);
			//pnew_ps.pairwise_reduce();

			// Check if P has adjacents
			if (!intadj[p_point].empty())
				// Updates internal and external borders (in case P has adjs)
				update_int_ext(pnew_ps, cut_ps, p_point, postflow, flow_nnc, intadj, extadj, queue);
			else
				// Updates only internal borders (because P has no adjs)
				update_int(pnew_ps, intadj);

			// Erase P from intadj map
			intadj.erase(p_point);

			// FREE MEMORY: Delete all external objects
			for (it_list=extadj[p_point].begin(); it_list!=extadj[p_point].end(); it_list++)
				delete (*it_list);
			extadj.erase(p_point);

			// Delete processed P
			delete p_point;
		} // END IF
	} // END WHILE

	// Rebuild risky
	for (it_int=intadj.begin(); it_int!=intadj.end(); it_int++) {

		risky_ps.add_disjunct(*(it_int->first));
	}
/*
	// Add each convex of under approximation to risky
	for (it_under=under_approx_ps.begin();it_under!=under_approx_ps.end();it_under++)
		risky_ps.add_disjunct(it_under->pointset());


*/
	//simplify(risky_ps);
	simplify(totalcut_ps);
	//	totalcut_ps.pairwise_reduce();
	fast_pairwise_reduce(totalcut_ps);
	return std::make_pair(risky_ps, totalcut_ps);
}

/*
std::pair<PS, PS> get_sorM_Local(PS over_approx_ps, PS under_approx_ps, location_synthesis* loc){
	dimension_type dim=under_approx_ps.space_dimension();
	hash_map_type intadj, extadj;
	list_nnc_type under_nnc, queue;
	PS risky_ps(dim,EMPTY);
	std::pair<PS, PS> result;

	//Calcolo del flow e preflow
	time_constraints tcons = loc->get_time_constraints();
	const continuous::relation_dynamics* dp=
			dynamic_cast<const continuous::relation_dynamics*>(tcons.get_dynamics().get());
	const ppl_polyhedron::continuous_set_PPL_Powerset* tmp=
			dynamic_cast<const ppl_polyhedron::continuous_set_PPL_Powerset*> (dp->get_relation().get());

	ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr postflow(tmp->clone());
	postflow->decrease_primedness();
	PS postflow_ps = postflow->get_powerset();

	ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr preflow(postflow->clone());
	preflow->get_prepoly();
	PS preflow_ps = preflow->get_powerset();

	// risky = over \ under
	risky_ps=over_approx_ps;
	//simplify(under_approx_ps);
	//	fast_difference_assign(risky_ps,under_approx_ps);
	risky_ps.difference_assign(under_approx_ps);
	//simplify(risky_ps);
	//	risky_ps.pairwise_reduce(); // Seems to speed up init_tab a little
	fast_pairwise_reduce(risky_ps);
	under_nnc = init_maps(risky_ps, under_approx_ps, postflow_ps.begin()->pointset(),
			preflow_ps.begin()->pointset(), intadj, extadj, queue);

	result = refine_maps(under_approx_ps, postflow_ps.begin()->pointset(),preflow_ps.begin()->pointset(),
			intadj, extadj, queue);
	//std::cout<<"\ndimensione intadj: "<<intadj.size()<<std::endl;

	risky_ps = result.first;
	// ADD under_approx TO risky
	for (list_nnc_type::iterator it_list= under_nnc.begin() ; it_list!=under_nnc.end() ; it_list++)
	{
		risky_ps.add_disjunct(**it_list);
		// FREE UNDER LIST
		delete *it_list;
	}
	//simplify(risky_ps);
	fast_pairwise_reduce(risky_ps);
	return std::make_pair(risky_ps, result.second);
}
*/

/* SOR^Must(A, B) */
std::pair<PS, PS> sor_local(PS over_approx_ps, PS under_approx_ps, Poly flow){

	dimension_type dim=under_approx_ps.space_dimension();
	hash_map_type intadj, extadj;
	list_nnc_type under_nnc, queue;
	PS risky_ps(dim,EMPTY);
	std::pair<PS, PS> result;

	//Calcolo del flow e preflow
	//time_constraints tcons = loc->get_time_constraints();
	//const continuous::relation_dynamics* dp=
			//dynamic_cast<const continuous::relation_dynamics*>(tcons.get_dynamics().get());
	//const ppl_polyhedron::continuous_set_PPL_Powerset* tmp=
			//dynamic_cast<const ppl_polyhedron::continuous_set_PPL_Powerset*> (dp->get_relation().get());

  //Poly flow = loc.get_flow();

	//ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr postflow(tmp->clone());
  Poly postflow(flow);

	// Non è più necessario.
	//postflow->decrease_primedness();
  // Non utilizziamo più powerset, ma direttamente il poly.
	//PS postflow_ps = postflow->get_powerset();

	//ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr preflow(postflow->clone());
  Poly preflow(postflow);

	//preflow->get_prepoly();
  get_prepoly(preflow);

	// Non utilizziamo più powerset, ma direttamente il poly.
	//PS preflow_ps = preflow->get_powerset();

	// risky = over \ under
	risky_ps=over_approx_ps;
	//simplify(under_approx_ps);
	//	fast_difference_assign(risky_ps,under_approx_ps);
	risky_ps.difference_assign(under_approx_ps);
	//simplify(risky_ps);
	//	risky_ps.pairwise_reduce(); // Seems to speed up init_tab a little
	fast_pairwise_reduce(risky_ps);


	//under_nnc = init_maps(risky_ps, under_approx_ps, postflow_ps.begin()->pointset(),
	//                       preflow_ps.begin()->pointset(), intadj, extadj, queue);
  // Rispetto al vecchio metodo lavoro direttamente con i poly.
	under_nnc = init_maps(risky_ps, under_approx_ps, postflow, preflow, intadj, extadj, queue);

  //result = refine_maps(under_approx_ps, postflow_ps.begin()->pointset(),preflow_ps.begin()->pointset(),
	//		                   intadj, extadj, queue);
  // Rispetto al vecchio metodo lavoro direttamente con i poly.
	result = refine_maps(under_approx_ps, postflow,preflow, intadj, extadj, queue);

	//std::cout<<"\ndimensione intadj: "<<intadj.size()<<std::endl;

	risky_ps = result.first;
	// ADD under_approx TO risky
	for (list_nnc_type::iterator it_list= under_nnc.begin() ; it_list!=under_nnc.end() ; it_list++)
	{
		risky_ps.add_disjunct(**it_list);
		// FREE UNDER LIST
		delete *it_list;
	}
	//simplify(risky_ps);
	fast_pairwise_reduce(risky_ps);
	return std::make_pair(risky_ps, result.second);
}



}
