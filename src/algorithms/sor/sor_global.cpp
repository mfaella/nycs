#include "sor_global.h"
#include "../pairwise_reduce.h"

#include <cstring>

namespace NaPoly {
// Initialize internal and external adj maps
static void init_maps(PS &risky_ps, PS &notover_ps, Poly flow_nnc, hash_map_type& intadj, hash_map_type& extadj,
		list_nnc_type& queue)
{
	dimension_type dim=risky_ps.space_dimension();
	hash_map_type::iterator it1_intadj, it2_intadj;
	PS::iterator it_risky, it_notover;
	Poly *p1_point, *p2_point, p1close_nnc(dim), pprime_nnc(dim), b_nnc(dim), *b_point;
	list_nnc_type list_nnc;
	bool first_ext;

	// Insert all element of risky in intadj hash_map
	for (it_risky=risky_ps.begin(); it_risky!=risky_ps.end(); it_risky++)
    {
		p1_point = new Poly(it_risky->pointset());
		intadj[p1_point]=list_nnc;
    }

	// For each P1 intadj (same of risky)
	for (it1_intadj=intadj.begin(); it1_intadj!=intadj.end(); it1_intadj++)
    {
		p1_point = it1_intadj->first;
		p1close_nnc=*p1_point;
		p1close_nnc.topological_closure_assign();

		// For each P2<>P1 in intadj (same of risky)
		it2_intadj=it1_intadj;
		it2_intadj++;
		while (it2_intadj!=intadj.end())
		{
			p2_point = it2_intadj->first;
			b_nnc = simple_border_opt(*p1_point, p1close_nnc, *p2_point);

			// If boundary exists, P1 and P2 are adjacents
			if (!b_nnc.is_empty())
			{
				intadj[p1_point].push_back(p2_point);
				intadj[p2_point].push_back(p1_point);
			}
			++it2_intadj;
		} // ENDFOR P2

		// For each P' in notover
		first_ext=true;
		for (it_notover=notover_ps.begin(); it_notover!=notover_ps.end(); it_notover++)
		{
			pprime_nnc = it_notover->pointset();
			b_nnc = exit_border_opt(*p1_point, p1close_nnc, pprime_nnc, flow_nnc);

			// If P1 is exposed, insert P1 and its border in extadj
			if (!b_nnc.is_empty())
			{
				if (first_ext)
				{
					extadj[p1_point]=list_nnc;
					queue.push_back(p1_point);
					first_ext=false;
				}
				b_point = new Poly(b_nnc);
				extadj[p1_point].push_back(b_point);
			}
		} // ENDFOR P'
    } // ENDFOR P1
}

/*
// Update the internal adj only by the adj of new polyhedron [pnew_ps].
//Called when the original polyhedron has no internal adjs.
void update_int(PS &pnew_ps, hash_map_type &intadj)
{
	list_nnc_type list_nnc, mylist_nnc;
	list_nnc_type::iterator it1_list, it2_list;
	Poly *q1_point, *q2_point;
	PS::iterator it_pnew;

	// Insert all convex of Pnew in the intadj map
	// For Each Q1 in Pnew
	for (it_pnew=pnew_ps.begin(); it_pnew!=pnew_ps.end(); it_pnew++)
	{
		q1_point = new Poly(it_pnew->pointset());
		// initialize adj list of Q1
		intadj[q1_point]=list_nnc;
		// copy Q1 in a new list
		mylist_nnc.push_back(q1_point);
	}

	// For each Q1 in mylist_nnc (Q1 in Pnew)
	for (it1_list=mylist_nnc.begin(); it1_list!=mylist_nnc.end(); it1_list++)
	{
		q1_point = *it1_list;
	    	Poly q1close_nnc(*q1_point);
		q1close_nnc.topological_closure_assign();

		// For each Q2<>Q1 in mylist_nnc (Q1, Q2 in Pnew)
		it2_list=it1_list;
		it2_list++;
		while (it2_list!=mylist_nnc.end())
		{
			q2_point = *it2_list;
			Poly b_nnc(simple_border_opt(*q1_point, q1close_nnc, *q2_point));

			// If Q1 and Q2 are adjacent, insert Q1 and Q2 in intadj map
			if (!b_nnc.is_empty())
			{
				intadj[q1_point].push_back(q2_point);
				intadj[q2_point].push_back(q1_point);
			}
			it2_list++;
		} // ENDFOR Q2
	} // ENDFOR Q1
}
*/

// Update intadj of a list of polyhedra,
// by checking adjacency among all pairs of polyhedra in the list.
void update_internal_of_list(list_nnc_type &mylist_nnc, hash_map_type &intadj)
{
	list_nnc_type::iterator it1_list, it2_list;
	Poly *q1_point, *q2_point;

	// For each Q1 in mylist_nnc (for each Q1 in Pnew)
	for (it1_list=mylist_nnc.begin(); it1_list!=mylist_nnc.end(); it1_list++)
	{
		q1_point = *it1_list;
		Poly q1close_nnc(*q1_point);
		q1close_nnc.topological_closure_assign();

		// For each Q2<>Q1 in pnew
		it2_list=it1_list;
		it2_list++;
		while (it2_list!=mylist_nnc.end())
		{
			q2_point = *it2_list;
			const Poly &b_nnc = simple_border_opt(*q1_point, q1close_nnc, *q2_point);

			// If Q1 and Q2 are adjacent, insert Q1 and Q2 in intadj map
			if (!b_nnc.is_empty())
			{
				intadj[q1_point].push_back(q2_point);
				intadj[q2_point].push_back(q1_point);
			}
			++it2_list;
		} // ENDFOR Q2
	} // ENDFOR Q1
}


// Checks adjacency between [*p2_point] and the old ajacents in the list.
void update_internal_of_poly_and_list(Poly *p2_point, list_nnc_type &mylist_nnc, hash_map_type &intadj)
{
	Poly *q_point;
	list_nnc_type:: iterator it_list;

	// For each Q in mylist_nnc (for each Q in Pnew)
	for (it_list=mylist_nnc.begin(); it_list!=mylist_nnc.end(); it_list++)
	{
		q_point = *it_list;
		Poly qclose_nnc(*q_point);
		qclose_nnc.topological_closure_assign();

		Poly b_nnc(simple_border_opt(*q_point, qclose_nnc, *p2_point));
		// If P2 and Q are adjacent
		if (!b_nnc.is_empty())
		{
			intadj[q_point].push_back(p2_point);
			intadj[p2_point].push_back(q_point);
		}
	}
}


// Update external adj of [p2_point] (adjacents to the original P): checks borders among [*p2_point]
//and the parts removed from original P
void update_external(PS &cut_ps, Poly *p2_point, Poly &flow_nnc, hash_map_type &extadj, list_nnc_type &queue)
{
	dimension_type dim = flow_nnc.space_dimension();
	list_nnc_type list_nnc;
	Poly q1_nnc(dim), *b_point, p2close_nnc(*p2_point);
	PS *extadj_ps;
	PS:: iterator it_cut;

	p2close_nnc.topological_closure_assign();

	// For each Q1 in cnew
	for (it_cut=cut_ps.begin(); it_cut!=cut_ps.end(); it_cut++)
    {
		q1_nnc = it_cut->pointset();
		Poly b_nnc(exit_border_opt(*p2_point, p2close_nnc, q1_nnc, flow_nnc));

		if (!b_nnc.is_empty())
		{
			b_point = new Poly(b_nnc);
			if (extadj.find(p2_point)==extadj.end())
				extadj[p2_point]=list_nnc;

			extadj[p2_point].push_back(b_point);

			bool find=false;

			// Scan queue
			list_nnc_type::iterator it_queue=queue.begin();
			while (it_queue!=queue.end() && (!find))
			{
				if (*it_queue==p2_point)
					find=true;
				it_queue++;
			}
			// Insert P' in queue only if P' not in already queue
			if (!find)
				queue.push_back(p2_point);
		}
	} // ENDFOR cut
}

// Update internal and external maps (when [*p1_point] has a non empty adjlist).
void update_int_ext(PS &pnew_ps, PS &cut_ps, Poly *p1_point, Poly &flow_nnc, hash_map_type &intadj,
		hash_map_type &extadj, list_nnc_type &queue)
{
  Poly *p2_point,*q_point;
  list_nnc_type pnew_list, empty_list;

  // Insert all convex Q in Pnew in intadj
  for (PS::iterator it_pnew=pnew_ps.begin(); it_pnew!=pnew_ps.end(); it_pnew++) {
    q_point = new Poly(it_pnew->pointset());
    // Initialize its internal adjacency list
    intadj[q_point] = empty_list;
    pnew_list.push_back(q_point);
  }

  update_internal_of_list(pnew_list, intadj);

  // For each P2 adj to P1
  for (list_nnc_type::iterator it_list=intadj[p1_point].begin(); it_list!=intadj[p1_point].end(); it_list++) {
    p2_point=*it_list;

    // Remove P1 from the list of the adjacents of P2
    intadj[p2_point].remove(p1_point);

    update_internal_of_poly_and_list(p2_point, pnew_list, intadj);
    update_external(cut_ps, p2_point, flow_nnc, extadj, queue);
  }
}

// The main loop: Refines internal and external maps.
// (Output: pair<risky_ps, totalcut_ps>).
static std::pair<PS, PS> refine_maps(PS &over_approx_ps, PS &under_approx_ps, Poly flow_nnc, hash_map_type& intadj,
		hash_map_type& extadj, list_nnc_type& queue)
{
	dimension_type dim=flow_nnc.space_dimension();
	Poly *p_point, p_nnc(dim), *b_point, b_nnc(dim), pprime_nnc(dim), *pprime_point, pprime_close_nnc(dim);
	PS *pnew_point, *cut_point, cut_ps(dim), risky_ps(dim), totalcut_ps(dim,EMPTY);
	PS::iterator it_cut, it_pnew, it_under;

	hash_map_type::iterator it_int, it_ext, it_findint, it_findext;
	list_nnc_type::iterator it_list, it1_list;

	// Scan queue
	while (!queue.empty())
    {
		// Take first exposed Convex Polyhedron P from the queue
		p_point=queue.front();
		// Delete first element
		queue.pop_front();
		p_nnc = *p_point;

	    // Cut from P the preflow of its exit borders
		cut_ps = cut_polyhedra_with_exit_borders(p_point, flow_nnc, extadj);
		// if cut is not empty update the maps accordingly
		if (!cut_ps.empty())
		{
			totalcut_ps.upper_bound_assign(cut_ps);
			PS pnew_ps(dim,EMPTY);
			pnew_ps.add_disjunct(p_nnc);
			// compute Pnew
			pnew_ps.difference_assign(cut_ps);
			//simplify(pnew_ps);
			//pnew_ps.pairwise_reduce();

			// Check if P has adjacents
			if (!intadj[p_point].empty())
				// Updates internal and external borders (in case P has adjs)
				update_int_ext(pnew_ps, cut_ps, p_point, flow_nnc, intadj, extadj, queue);
			else
				// Updates only internal borders (because P has no adjs)
				update_int(pnew_ps, intadj);

			// Erase P from intadj map
			intadj.erase(p_point);

			// FREE MEMORY: Delete all external objects
			for (it_list=extadj[p_point].begin(); it_list!=extadj[p_point].end(); it_list++)
				delete (*it_list);
			extadj.erase(p_point);

			// Delete original P
			delete p_point;
		} // END IF
    } // END WHILE


	// Compute output
	risky_ps.clear();
	for (it_int=intadj.begin(); it_int!=intadj.end(); it_int++)
		risky_ps.add_disjunct(*(it_int->first));
	// Add each convex of under approsimation to risky
	for (it_under=under_approx_ps.begin();it_under!=under_approx_ps.end();it_under++)
		risky_ps.add_disjunct(it_under->pointset());

	fast_pairwise_reduce(risky_ps);
	fast_pairwise_reduce(totalcut_ps);

	/////PROVA
	/*
	PS result = over_approx_ps;
	result.upper_bound_assign(under_approx_ps);
	result.difference_assign(totalcut_ps);

	if(!ppl_polyhedron::comp_powerset(result, risky_ps))
		std::cout<<"result OPT E NONOPT DIVERSI"<<std::endl;
	else std::cout<<"result OPT E NONOPT UGUALI"<<std::endl;
	//////FINE PROVA */

  return std::make_pair(risky_ps, totalcut_ps);
}

/*

std::pair<PS, PS> get_sorM_Global(PS over_approx_ps, PS under_approx_ps, location_synthesis* loc){
	dimension_type dim=under_approx_ps.space_dimension();
	hash_map_type intadj, extadj;
	list_nnc_type queue;
	PS notover_ps(dim,UNIVERSE);

	//Calcolo del preflow
	time_constraints tcons = loc->get_time_constraints();
	const continuous::relation_dynamics* dp=
			dynamic_cast<const continuous::relation_dynamics*>(tcons.get_dynamics().get());
	const ppl_polyhedron::continuous_set_PPL_Powerset* tmp=
			dynamic_cast<const ppl_polyhedron::continuous_set_PPL_Powerset*> (dp->get_relation().get());

	ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr preflow(tmp->clone());
	preflow->decrease_primedness();
	preflow->get_prepoly();
	PS preflow_ps = preflow->get_powerset();

	// Compute NOT{Over Approximation}
	notover_ps.difference_assign(over_approx_ps);

	PS risky_ps(dim,EMPTY);
	risky_ps=over_approx_ps;
	// Compute risky
	//simplify(under_approx_ps);
	risky_ps.difference_assign(under_approx_ps);
	//simplify(risky_ps);
	risky_ps.pairwise_reduce(); // Seems to speed up init_tab a little

	init_maps(risky_ps, notover_ps, preflow_ps.begin()->pointset(), intadj, extadj, queue);
	return refine_maps(over_approx_ps, under_approx_ps, preflow_ps.begin()->pointset(), intadj, extadj,
			queue);

}

*/


std::pair<PS, PS> sor_global(PS over_approx_ps, PS under_approx_ps, Poly flow){
	dimension_type dim=under_approx_ps.space_dimension();
	hash_map_type intadj, extadj;
	list_nnc_type queue;
	PS notover_ps(dim,UNIVERSE);

	//Calcolo del preflow
	//time_constraints tcons = loc->get_time_constraints();
	//const continuous::relation_dynamics* dp=
	//dynamic_cast<const continuous::relation_dynamics*>(tcons.get_dynamics().get());

	//Poly flow = loc.get_flow();
	//const ppl_polyhedron::continuous_set_PPL_Powerset* tmp=
			//dynamic_cast<const ppl_polyhedron::continuous_set_PPL_Powerset*> (dp->get_relation().get());

	//ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr preflow(tmp->clone());
  Poly preflow(flow);

	// NON SERVE PIU.
	//preflow->decrease_primedness();

	//preflow->get_prepoly();
  get_prepoly(preflow);

	// NON SERVE CREARE IL PS E POI ESTRARRE IL POLY. PASSO DIRETTAMENTE IL POLY.
	// PS preflow_ps = preflow->get_powerset();

	// Compute NOT{Over Approximation}
	notover_ps.difference_assign(over_approx_ps);

	PS risky_ps(dim,EMPTY);
	risky_ps=over_approx_ps;
	// Compute risky
	//simplify(under_approx_ps);
	risky_ps.difference_assign(under_approx_ps);
	//simplify(risky_ps);
	fast_pairwise_reduce(risky_ps); // Seems to speed up init_tab a little

	// La vecchia chiamata a metodo init_maps estrae il Poly da PS. La nuova usa direttamente il Poly.
	//init_maps(risky_ps, notover_ps, preflow_ps.begin()->pointset(), intadj, extadj, queue);
	init_maps(risky_ps, notover_ps, preflow, intadj, extadj, queue);

	// La vecchia chiamata a metodo init_maps estrae il Poly da PS. La nuova usa direttamente il Poly.
	//return refine_maps(over_approx_ps, under_approx_ps, preflow_ps.begin()->pointset(), intadj, extadj, queue);
	return refine_maps(over_approx_ps, under_approx_ps, preflow, intadj, extadj, queue);

}

}
