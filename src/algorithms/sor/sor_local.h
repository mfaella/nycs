/*
 * sorm_local.h
 *
 *  Created on: 11/set/2014
 *      Author: donatella
 *  Modified on: Semptember 06, 2017
 *	Author: Marco Urbano
 */

#ifndef SORM_LOCAL_H_
#define SORM_LOCAL_H_

#include "../rwa/rwa_operations.h"
#include "../rwa/rwa_map_definition.h"
//#include "../rwa/rwa_ce_map.h"
//#include "../../automaton/Location.h"

#include <ext/hash_map>

namespace NaPoly {
using namespace Parma_Polyhedra_Library;


typedef Pointset_Powerset<NNC_Polyhedron> PS;
typedef NNC_Polyhedron Poly;

// Initialize the map related to the internal adjaciencies [intadj] and the external map
//related to the external adjaciencies [extadj] (Used in Global Local)
list_nnc_type init_maps(PS &risky_ps, PS &under_ps, Poly postflow_nnc, Poly preflow_nnc, hash_map_type& intadj,
		hash_map_type& extadj, list_nnc_type& queue);

// Update of the internal adj of [p2_point] in under (adjacents to the original P):
// checks borders only between [p2_point] and Pnew (no check for (Pnew, Pnew))
// static void update_int_under(list_nnc_type &mylist_nnc, Poly *p2_point, hash_map_type &intadj);

// Update external adj of [p2_point] (adjacents to the original P): checks borders among [*p2_point]
//and the parts removed from original P. (Local and Local+ Approaches)
void update_external(PS &cut_ps, Poly *p2_point, Poly &postflow_nnc, Poly &preflow_nnc, hash_map_type &extadj,
		list_nnc_type &queue);

// Update internal and external maps (when [*p1_point] has a non empty adjlist). (Local).
void update_int_ext(PS &pnew_ps, PS &cut_ps, Poly *p1_point, Poly &postflow_nnc,Poly &preflow_nnc,
		hash_map_type &intadj, hash_map_type &extadj, list_nnc_type &queue);

// The main loop. Refines internal and external maps. (Local).
// (Output: pair<risky_ps, totalcut_ps>)
std::pair<PS, PS> refine_maps(PS &under_approx_ps, Poly postflow, Poly flow_nnc, hash_map_type& intadj, hash_map_type& extadj,
		list_nnc_type& queue);

// Compute sorM by using the LOCAL APPROACH (Maps and the only relevant portion of NOT Z)
// (Output: pair<risky_ps, totalcut_ps>)
std::pair<PS, PS> sor_local(PS over_approx, PS under_approx, Poly flow);
}

#endif /* SORM_LOCAL_H_ */
