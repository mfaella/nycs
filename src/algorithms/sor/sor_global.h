#ifndef SORM_GLOBAL_H_
#define SORM_GLOBAL_H_


#include "../rwa/rwa_operations.h"
#include "../rwa/rwa_map_definition.h"
#include "../rwa/rwa_maps.h"

//#include "../../automaton/Location.h"

// Libreria che include tutti i metodi rinnovati.
#include "../scenario_utility.h"

#include <ext/hash_map>

namespace NaPoly{
using namespace Parma_Polyhedra_Library;

typedef Pointset_Powerset<NNC_Polyhedron> PS;
typedef NNC_Polyhedron Poly;

// Initialize the map related to the internal adjaciencies [intadj]
//and the external map related to the external adjaciencies [extadj] (Used in Global Approach)
void init_maps(PS &risky_ps, PS &notover_ps, Poly flow_nnc, hash_map_type& intadj, hash_map_type& extadj,
		list_nnc_type& queue);

/*
// Update the internal adj only by the adj of new polyhedron [pnew_ps].
//Called when the original polyhedron has no internal adjs. (Global and Local approaches)
void update_int(PS &pnew_ps, hash_map_type &intadj);
*/

// First step of the update for the internal adj. Checks borders among polyhedra of [*p2_point]
//and among polyhedra of [*p2_point] and old ajacents.
void update_internal_of_list(list_nnc_type &mylist_nnc, hash_map_type &intadj);

// First step of the update for the internal adj. Checks borders among of [*p2_point]
//and old ajacents. (no check among [*p2_point]).
void update_internal_of_poly_and_list(Poly *p, list_nnc_type &list, hash_map_type &intadj);

// Update external adj of [p2_point] (adjacents to the original P): checks borders among [*p2_point]
//and the parts removed from original P. (Global Approach)
void update_external(PS &cut_ps, Poly *p2_point, Poly &flow_nnc, hash_map_type &extadj, list_nnc_type &queue);

// Update internal and external maps (when [*p1_point] has a non empty adjlist). (Global).
void update_int_ext(PS &pnew_ps, PS &cut_ps, Poly *p1_point, Poly &flow_nnc, hash_map_type &intadj,
		 hash_map_type &extadj, list_nnc_type &queue);

// The main loop: Refines internal and external maps. (Global).
// (Output: pair<risky_ps, totalcut_ps>).
std::pair<PS, PS> refine_maps(PS &over_approx_ps, PS &under_approx_ps, Poly flow_nnc, hash_map_type& intadj,
		hash_map_type& extadj,list_nnc_type& queue);

// Compute sorM by using the GLOBAL APPROACH (Maps and the Whole NOT Z)
// (Output: pair<risky_ps, totalcut_ps>).
std::pair<PS, PS> sor_global(PS over_approx, PS under_approx, Poly flow);

}

#endif /*SORM_GLOBAL_H_*/
