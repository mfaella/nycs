#include "sor_basic.h"
#include "../pairwise_reduce.h"

#include <cstring>

/*
namespace NaPoly{
  class Location;
} */

namespace NaPoly {

/*

std::pair<PS, PS> get_sorM_Basic(PS over_approx_ps, PS under_approx_ps, location_synthesis* loc)
{
	dimension_type dim = over_approx_ps.space_dimension();
	PS risky_ps(dim), notover_ps(dim, UNIVERSE), pnew_ps(dim), b_ps(dim), preflow_ps(dim, EMPTY), cut_ps(dim);
	PS totalcut_ps(dim,EMPTY);
	Poly p_nnc(dim), p_close_nnc(dim), p_prime_nnc(dim), p_prime_close_nnc(dim), b_nnc(dim), checkfix_nnc(dim);

	PS::const_iterator it_notover, it_pnew, it_cut, it_under;

	PS::iterator it_risky_ps;

	int num_nnc;

	// Compute {over_approx_ps\under_approx_ps}
	risky_ps=over_approx_ps;
	risky_ps.difference_assign(under_approx_ps);
	//risky_ps.pairwise_reduce();

	// Compute NOT_over_approx
	notover_ps.difference_assign(over_approx_ps);

	//Calcolo del preflow
	time_constraints tcons = loc->get_time_constraints();
	const continuous::relation_dynamics* dp=
			dynamic_cast<const continuous::relation_dynamics*>(tcons.get_dynamics().get());
	const ppl_polyhedron::continuous_set_PPL_Powerset* tmp=
			dynamic_cast<const ppl_polyhedron::continuous_set_PPL_Powerset*> (dp->get_relation().get());
	ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr preflow(tmp->clone());
	preflow->get_prepoly();
	preflow->decrease_primedness();
	preflow_ps = preflow->get_powerset();

	bool fixpoint = false;
	while (!fixpoint)
	{
		fixpoint = true;

		// FOR EACH P belongs to risky_ps
		it_risky_ps = risky_ps.begin();
		for (num_nnc=1; num_nnc<=risky_ps.size(); num_nnc++)
		{
			p_nnc=it_risky_ps->pointset();
			p_close_nnc=p_nnc;
			// Take topological closure of P --> [P]
			p_close_nnc.topological_closure_assign();

			// Initialize Powerset of new P
			pnew_ps.clear();
			pnew_ps.add_disjunct(p_nnc);
			// Initialize Powerset of part to cut from P
			cut_ps.clear();
			cut_ps.add_disjunct(p_nnc);

			// Initialize Powerset of the border between P and P'
			b_ps.clear();

			// For each P' do not belong to over_approx_ps
			for (it_notover=notover_ps.begin(); it_notover!=notover_ps.end(); it_notover++)
			{
				p_prime_nnc=it_notover->pointset();
				p_prime_close_nnc=p_prime_nnc;
				// Take topological closure of P' --> [P']
				p_prime_close_nnc.topological_closure_assign();

				// Compute ([P] Intersection P') U (P Intersection [P']): BORDER
				b_nnc=p_close_nnc;
				b_nnc.intersection_assign(p_prime_nnc);

				if (b_nnc.is_empty())
				{
					b_nnc=p_nnc;
					b_nnc.intersection_assign(p_prime_close_nnc);
				}

				// If P and P' are adjacent
				if (!b_nnc.is_empty())
				{
					// Compute exist_preflow(B inters. exist_preflow(P'))
					p_prime_nnc.time_elapse_assign(preflow_ps.begin()->pointset());
					b_nnc.intersection_assign(p_prime_nnc);

					b_ps.add_disjunct(b_nnc);
					if (fixpoint)
					{
						b_nnc.time_elapse_assign(preflow_ps.begin()->pointset());
						// Compute slice to cut from P
						checkfix_nnc=p_nnc;
						checkfix_nnc.intersection_assign(b_nnc);
						// Add slice to cut from P in the powerset
						if (!checkfix_nnc.is_empty())
							fixpoint=false;
					}
				}
			} // ENDFOR p_prime
			b_ps.time_elapse_assign(preflow_ps);
			b_ps.pairwise_reduce();

			// Compute parts to cut from P
			cut_ps.intersection_assign(b_ps);

			// Compute the remains parts of P
			pnew_ps.difference_assign(cut_ps);
			//pnew_ps.pairwise_reduce();

			// Add the remains parts of P to risky_ps
			for (it_pnew=pnew_ps.begin();it_pnew!=pnew_ps.end();it_pnew++)
				if (!it_pnew->pointset().is_empty())
					risky_ps.add_disjunct(it_pnew->pointset());

			// Drop P from risky_ps and increment the iterator
			it_risky_ps=risky_ps.drop_disjunct(it_risky_ps);

			// Add the slices to cut from P in NOTOVER
			for (it_cut=cut_ps.begin();it_cut!=cut_ps.end();it_cut++)
				if (!(it_cut->pointset().is_empty())) {
					notover_ps.add_disjunct(it_cut->pointset());
					totalcut_ps.add_disjunct(it_cut->pointset());
				}
		} // ENDFOR p

	} // END WHILE (FIXPOINT)
	totalcut_ps.pairwise_reduce();

	// Computes new over_approx_ps=union between over_approx_ps\under_approx_ps and under_approx_ps
	//under_approx_ps.intersection_assign(over_approx_ps);
	for (it_under=under_approx_ps.begin();it_under!=under_approx_ps.end();it_under++)
		risky_ps.add_disjunct(it_under->pointset());
	risky_ps.pairwise_reduce();

	return std::make_pair(risky_ps, totalcut_ps);
}

*/

std::pair<PS, PS> sor_basic(PS over_approx_ps, PS under_approx_ps, Poly flow)
{
	dimension_type dim = over_approx_ps.space_dimension();
	PS risky_ps(dim), notover_ps(dim, UNIVERSE), pnew_ps(dim), b_ps(dim), cut_ps(dim);
	PS totalcut_ps(dim,EMPTY);
	Poly p_nnc(dim), p_close_nnc(dim), p_prime_nnc(dim), p_prime_close_nnc(dim), b_nnc(dim), checkfix_nnc(dim);

	PS::const_iterator it_notover, it_pnew, it_cut, it_under;

	PS::iterator it_risky_ps;

	int num_nnc;

	// Compute {over_approx_ps\under_approx_ps}
	risky_ps=over_approx_ps;
	risky_ps.difference_assign(under_approx_ps);
	//risky_ps.pairwise_reduce();

	// Compute NOT_over_approx
	notover_ps.difference_assign(over_approx_ps);

	//Calcolo del preflow
	//time_constraints tcons = loc->get_time_constraints();
	//const continuous::relation_dynamics* dp=
			//dynamic_cast<const continuous::relation_dynamics*>(tcons.get_dynamics().get());


	//const ppl_polyhedron::continuous_set_PPL_Powerset* tmp=
	//dynamic_cast<const ppl_polyhedron::continuous_set_PPL_Powerset*> (dp->get_relation().get());
  // Modificato da Urbano visto che era inutile passare Location e creare questa dipendenza per il test.
  //Poly flow = loc.get_flow();

	//ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr preflow(tmp->clone());
  Poly preflow(flow);

	//preflow->get_prepoly();
  NaPoly::get_prepoly(preflow);

	// NON SERVE PIU' POICHE' IL POLY PER FLOW E' GIA COSTRUITO CON LE VARIABILI NON PRIMATE.
	//preflow->decrease_primedness();

  // Visto che preflow_ps è un PS e il flow è un Poly, allora aggiungo il Poly.



	bool fixpoint = false;
	while (!fixpoint)
	{
		//cout << "SONO NEL CICLO ESTERNO DI SORM_BASIC" << endl;
		fixpoint = true;

		// FOR EACH P belongs to risky_ps
		it_risky_ps = risky_ps.begin();
		for (num_nnc=1; num_nnc<=risky_ps.size(); num_nnc++)
		{
			p_nnc=it_risky_ps->pointset();
			p_close_nnc=p_nnc;
			// Take topological closure of P --> [P]
			p_close_nnc.topological_closure_assign();

			// Initialize Powerset of new P
			pnew_ps.clear();
			pnew_ps.add_disjunct(p_nnc);
			// Initialize Powerset of part to cut from P
			cut_ps.clear();
			cut_ps.add_disjunct(p_nnc);

			// Initialize Powerset of the border between P and P'
			b_ps.clear();

			// For each P' do not belong to over_approx_ps
			for (it_notover=notover_ps.begin(); it_notover!=notover_ps.end(); it_notover++)
			{
				p_prime_nnc=it_notover->pointset();
				p_prime_close_nnc=p_prime_nnc;
				// Take topological closure of P' --> [P']
				p_prime_close_nnc.topological_closure_assign();

				// Compute ([P] Intersection P') U (P Intersection [P']): BORDER
				b_nnc=p_close_nnc;
				b_nnc.intersection_assign(p_prime_nnc);

				if (b_nnc.is_empty())
				{
					b_nnc=p_nnc;
					b_nnc.intersection_assign(p_prime_close_nnc);
				}

				// If P and P' are adjacent
				if (!b_nnc.is_empty())
				{
					// Compute exist_preflow(B inters. exist_preflow(P'))
					p_prime_nnc.time_elapse_assign(preflow);
					b_nnc.intersection_assign(p_prime_nnc);

					b_ps.add_disjunct(b_nnc);
					if (fixpoint)
					{
						b_nnc.time_elapse_assign(preflow);
						// Compute slice to cut from P
						checkfix_nnc=p_nnc;
						checkfix_nnc.intersection_assign(b_nnc);
						// Add slice to cut from P in the powerset
						if (!checkfix_nnc.is_empty())
							fixpoint=false;
					}
				}
			} // ENDFOR p_prime

			PS preflow_ps(preflow);

			b_ps.time_elapse_assign(preflow_ps);
			fast_pairwise_reduce(b_ps);

			// Compute parts to cut from P
			cut_ps.intersection_assign(b_ps);

			// Compute the remains parts of P
			pnew_ps.difference_assign(cut_ps);
			//pnew_ps.pairwise_reduce();

			// Add the remains parts of P to risky_ps
			for (it_pnew=pnew_ps.begin();it_pnew!=pnew_ps.end();it_pnew++)
				if (!it_pnew->pointset().is_empty())
					risky_ps.add_disjunct(it_pnew->pointset());

			// Drop P from risky_ps and increment the iterator
			it_risky_ps=risky_ps.drop_disjunct(it_risky_ps);

			// Add the slices to cut from P in NOTOVER
			for (it_cut=cut_ps.begin();it_cut!=cut_ps.end();it_cut++)
				if (!(it_cut->pointset().is_empty())) {
					notover_ps.add_disjunct(it_cut->pointset());
					totalcut_ps.add_disjunct(it_cut->pointset());
				}
		} // ENDFOR p
	} // END WHILE (FIXPOINT)
	fast_pairwise_reduce(totalcut_ps);

	// Computes new over_approx_ps=union between over_approx_ps\under_approx_ps and under_approx_ps
	//under_approx_ps.intersection_assign(over_approx_ps);
	for (it_under=under_approx_ps.begin();it_under!=under_approx_ps.end();it_under++)
		risky_ps.add_disjunct(it_under->pointset());
	fast_pairwise_reduce(risky_ps);

	return std::make_pair(risky_ps, totalcut_ps);
}


}
