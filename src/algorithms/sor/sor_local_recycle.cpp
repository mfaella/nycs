#include "sor_local_recycle.h"
#include "sor_local.h"
#include "sor_global.h"
#include "../pairwise_reduce.h"

#include <cstring>

namespace NaPoly{
  
// init_map version for Local+ Approach
list_nnc_type init_maps(PS &risky_ps, PS &under_ps, Poly postflow_nnc,Poly preflow_nnc,
		hash_map_type& rradj, hash_map_u_type& ruadj, hash_map_type& extadj, list_nnc_type& queue)
{
	LOGGER(DEBUG, "init_maps", "Inizio");
	dimension_type dim=risky_ps.space_dimension();
	hash_map_type::iterator it1_rradj, it2_rradj;
	PS::iterator it_risky, it_under;
	Poly *p1_point, *p2_point, p1close_nnc(dim), pprime_nnc(dim);
	Poly b_nnc(dim), *b_point;
	list_nnc_type list_nnc, list1_nnc;
	list_nnc_type under_nnc;
	bool first_ext;

	// Insert all (newly generated) elements of risky in intadj hash_map
	for (it_risky=risky_ps.begin(); it_risky!=risky_ps.end(); it_risky++)
	{
		p1_point = new Poly(it_risky->pointset());
		rradj[p1_point]=list_nnc;
		ruadj[p1_point]=1;
	}

	// Insert all (newly generated) elements of under in a list
	for (it_under=under_ps.begin(); it_under!=under_ps.end(); it_under++)
	{
		p1_point = new Poly(it_under->pointset());
		under_nnc.push_back(p1_point);
	}

	// For each P1 intadj (in risky)
	for (it1_rradj=rradj.begin(); it1_rradj!=rradj.end(); it1_rradj++)
	{
		p1_point = it1_rradj->first;
		p1close_nnc=*p1_point;
		p1close_nnc.topological_closure_assign();
		PS adj_ps(*p1_point);
		for (list_nnc_type::iterator it_adj=rradj[p1_point].begin(); it_adj!=rradj[p1_point].end(); it_adj++)
		{
			pprime_nnc = **it_adj;
			adj_ps.add_disjunct(pprime_nnc);
		}
		// For each P2<>P1 in intadj (in risky)
		it2_rradj=it1_rradj;
		it2_rradj++;
		while (it2_rradj!=rradj.end())
		{
			p2_point = it2_rradj->first;
			// If boundary exists, P1 and P2 are adjacents
			if (!simple_border_opt(*p1_point, p1close_nnc, *p2_point).is_empty())
			{
				rradj[p1_point].push_back(p2_point);
				rradj[p2_point].push_back(p1_point);
				adj_ps.add_disjunct(*p2_point);
			}
			++it2_rradj;
		} // ENDFOR P2

		//For each P2 (in under), update adjlist of the polyhedron P1 (in risky)
		bool first_ru=true;
		for (list_nnc_type::iterator it_listunder=under_nnc.begin(); it_listunder!=under_nnc.end(); it_listunder++)
		{
			p2_point = *it_listunder;
			if (!simple_border_opt(*p1_point, p1close_nnc, *p2_point).is_empty())
			{
				if (first_ru)
				{
				ruadj[p1_point]=2;
				first_ru=false;
				}
				adj_ps.add_disjunct(*p2_point);
			}
		}
		fast_pairwise_reduce(adj_ps);
		// Compute exit borders of P1
		Poly p1_post(*p1_point);
		p1_post.time_elapse_assign(postflow_nnc);
		PS p1_exit_ps(p1_post);
		p1_exit_ps.difference_assign(adj_ps);
		fast_pairwise_reduce(p1_exit_ps);

		first_ext=true;
		for (PS::iterator iter_exit=p1_exit_ps.begin();iter_exit!=p1_exit_ps.end();iter_exit++)
		{
			Poly pprime_nnc = iter_exit->pointset();
			b_nnc = exit_border_opt(*p1_point, p1close_nnc, pprime_nnc, preflow_nnc);
			// If P1 is exposed, insert P1 and its border in extadj
			if (!b_nnc.is_empty())
			{
				if (first_ext)
				{
					extadj[p1_point]=list1_nnc;
					queue.push_back(p1_point);
					first_ext=false;
				}
				b_point = new Poly(b_nnc);
				extadj[p1_point].push_back(b_point);
			}
		}
	} // ENDFOR P'
	return under_nnc;
}

// Initialize the maps for successive calls to sorM (allows to reuse the maps).
static list_nnc_type align_maps(PS under_ps, PS under_eff, Poly postflow_nnc, Poly preflow_nnc,
		hash_map_type& rradj, hash_map_u_type& ruadj, hash_map_type& extadj, list_nnc_type& queue)
{
	LOGGER(DEBUG, "align_maps", "Inizio");
	dimension_type dim=under_ps.space_dimension();
	list_nnc_type list_nnc, list1_nnc;
	list_nnc_type under_nnc, under_eff_nnc;
	Poly *p1_point,*p2_point, *b_point, *u1_point, *u2_point;
	Poly p1close_nnc(dim), pprime_nnc(dim), b_nnc(dim), u1close_nnc(dim);
	PS::iterator it_under;
	hash_map_type::iterator it_ruadj, it_rradj;
	list_nnc_type::iterator it_listunder, it2_listunder;
	bool first_ext, first_ru;

	for (it_under=under_eff.begin(); it_under!=under_eff.end(); it_under++)
	{
		u1_point = new Poly(it_under->pointset());
		under_eff_nnc.push_back(u1_point);
		rradj[u1_point]=list_nnc;
		ruadj[u1_point]=3;
	}

	for (it_listunder=under_eff_nnc.begin(); it_listunder!=under_eff_nnc.end(); it_listunder++)
	{
		u1_point = *it_listunder;
		u1close_nnc=*u1_point;
		u1close_nnc.topological_closure_assign();

		it2_listunder=it_listunder;
		it2_listunder++;
		while (it2_listunder!=under_eff_nnc.end())
		{
			u2_point = *it2_listunder;
			if (!simple_border_opt(*u1_point, u1close_nnc, *u2_point).is_empty())
			{
				rradj[u1_point].push_back(u2_point);
				rradj[u2_point].push_back(u1_point);
			}
			it2_listunder++;
		}

		for (it_rradj=rradj.begin(); it_rradj!=rradj.end(); it_rradj++)
		{
			p1_point = it_rradj->first;
			if (ruadj[p1_point]==2)
			{
				if (!simple_border_opt(*u1_point, u1close_nnc, *p1_point).is_empty())
				{
					rradj[p1_point].push_back(u1_point);
					rradj[u1_point].push_back(p1_point);
				}
			}
		}
	}

	// Insert all (newly generated) elements of under in a list
	for (it_under=under_ps.begin(); it_under!=under_ps.end(); it_under++)
	{
		p1_point = new Poly(it_under->pointset());
		under_nnc.push_back(p1_point);
	}
	// For each P1 intadj (in risky)
	for (it_rradj=rradj.begin(); it_rradj!=rradj.end(); it_rradj++)
	{
		p1_point = it_rradj->first;
		PS adj_ps(*p1_point);

		for (list_nnc_type::iterator it_adj=rradj[p1_point].begin(); it_adj!=rradj[p1_point].end(); it_adj++)
		{
			pprime_nnc = **it_adj;
			adj_ps.add_disjunct(pprime_nnc);
		}
		p1close_nnc=*p1_point;
		p1close_nnc.topological_closure_assign();

		if (ruadj[p1_point]==2 || ruadj[p1_point]==3)
		{
			first_ru=true;
			for (list_nnc_type::iterator it_listunder=under_nnc.begin(); it_listunder!=under_nnc.end();
					it_listunder++)
			{
				p2_point = *it_listunder;
				if (!simple_border_opt(*p1_point, p1close_nnc, *p2_point).is_empty())
				{
					if (first_ru)
					{
						ruadj[p1_point]=2;
						first_ru=false;
					}
					adj_ps.add_disjunct(*p2_point);
				}
			}
			if (first_ru)
				ruadj[p1_point]=1;
		}
		fast_pairwise_reduce(adj_ps);
		// Compute exit borders of P1
		Poly p1_post(*p1_point);
		p1_post.time_elapse_assign(postflow_nnc);
		PS p1_exit_ps(p1_post);
		p1_exit_ps.difference_assign(adj_ps);
		fast_pairwise_reduce(p1_exit_ps);
		first_ext=true;
		for (PS::iterator iter_exit=p1_exit_ps.begin();iter_exit!=p1_exit_ps.end();iter_exit++)
		{
			Poly pprime_nnc = iter_exit->pointset();
			b_nnc = exit_border_opt(*p1_point, p1close_nnc, pprime_nnc, preflow_nnc);
			// If P1 is exposed, insert P1 and its border in extadj
			if (!b_nnc.is_empty())
			{
				if (first_ext)
				{
					extadj[p1_point]=list1_nnc;
					queue.push_back(p1_point);
					first_ext=false;
				}
				b_point = new Poly(b_nnc);
				extadj[p1_point].push_back(b_point);
			}
		}
	}
	return under_nnc;
}


// Update the internal adj only by the adj of new polyhedron [pnew_ps].
//Called when the original polyhedron has no internal adjs.(Local+ approach)
void update_int(PS under_approx, PS &pnew_ps, Poly *p_point, hash_map_type &rradj, hash_map_u_type &ruadj)
{
	list_nnc_type list_nnc, mylist_nnc;
	list_nnc_type::iterator it1_list, it2_list;
	Poly *q1_point, *q2_point;
	PS::iterator it_pnew;

	// Insert all convex of Pnew in the intadj map
	// For Each Q1 in Pnew
	for (it_pnew=pnew_ps.begin(); it_pnew!=pnew_ps.end(); it_pnew++)
    {
		q1_point = new Poly(it_pnew->pointset());
		// initialize adj list of Q1
		rradj[q1_point]=list_nnc;
		// copy Q1 in a new list (WHY?)
		mylist_nnc.push_back(q1_point);

		ruadj[q1_point]=1;

		if (ruadj[p_point]==2)
		{
			PS::iterator it_under=under_approx.begin();
			while ((ruadj[q1_point]!=2) && it_under!=under_approx.end())
			{
				Poly u(it_under->pointset());
				Poly uclose(u);
				uclose.topological_closure_assign();
				if (!simple_border_opt(u, uclose, *q1_point).is_empty())
					ruadj[q1_point]=2;
				it_under++;
			}
		}
    }

	// For each Q1 in mylist_nnc (Q1 in Pnew)
	for (it1_list=mylist_nnc.begin(); it1_list!=mylist_nnc.end(); it1_list++)
	{
		q1_point = *it1_list;
		Poly q1close_nnc(*q1_point);
		q1close_nnc.topological_closure_assign();

		// For each Q2<>Q1 in mylist_nnc (Q1, Q2 in Pnew)
		it2_list=it1_list;
		it2_list++;
		while (it2_list!=mylist_nnc.end())
		{
			q2_point = *it2_list;
			Poly b_nnc(simple_border_opt(*q1_point, q1close_nnc, *q2_point));

			// If Q1 and Q2 are adjacent, insert Q1 and Q2 in intadj map
			if (!b_nnc.is_empty())
			{
				rradj[q1_point].push_back(q2_point);
				rradj[q2_point].push_back(q1_point);
			}
			it2_list++;
		} // ENDFOR Q2
	} // ENDFOR Q1
}

// Update internal and external maps (when [*p1_point] has a non empty adjlist).
void update_int_ext(PS under_approx, PS &pnew_ps, PS &cut_ps, Poly *p1_point, Poly &postflow_nnc, Poly &preflow_nnc,
		hash_map_type &rradj, hash_map_u_type &ruadj, hash_map_type &extadj, list_nnc_type &queue)
{
  // bool first;
	Poly *p2_point,*q_point;
	list_nnc_type mylist_nnc, list_nnc;

	// Insert all convex Q in Pnew in intadj
	for (PS::iterator it_pnew=pnew_ps.begin(); it_pnew!=pnew_ps.end(); it_pnew++)
	{
		q_point=new Poly(it_pnew->pointset());
		rradj[q_point]=list_nnc;
		mylist_nnc.push_back(q_point);
		ruadj[q_point]=1;

		if (ruadj[p1_point]==2)
		{
			PS::iterator it_under=under_approx.begin();
			while ((ruadj[q_point]!=2) && it_under!=under_approx.end())
			{
				Poly u(it_under->pointset());
				Poly uclose(u);
				uclose.topological_closure_assign();
				if (!simple_border_opt(u, uclose, *q_point).is_empty())
					ruadj[q_point]=2;
				it_under++;
			}
		}
	}

	update_internal_of_list(mylist_nnc, rradj);

	// For each P2 adj to P1
	for (list_nnc_type::iterator it_list=rradj[p1_point].begin(); it_list!=rradj[p1_point].end(); it_list++)
	{
		p2_point=*it_list;
		// Remove P1 from the list of the adjacents of P2
		rradj[p2_point].remove(p1_point);

		update_internal_of_poly_and_list(p2_point, mylist_nnc, rradj);
		update_external(cut_ps, p2_point, postflow_nnc, preflow_nnc, extadj, queue);

	}
}

// The main loop. Refines internal and external maps.
static std::pair<PS, PS> refine_maps(PS &under_approx_ps, Poly postflow, Poly flow_nnc, hash_map_type& rradj,
		hash_map_u_type& ruadj, hash_map_type& extadj, list_nnc_type& queue)
{
	LOGGER(DEBUG, "refine_maps", "Inizio");
	dimension_type dim=flow_nnc.space_dimension();
	Poly *p_point, p_nnc(dim);
	PS cut_ps(dim), risky_ps(dim,EMPTY), totalcut_ps(dim, EMPTY);
	PS:: iterator it_under;
	hash_map_type::iterator it_int;
	list_nnc_type::iterator it_list;

	// Scan queue
	while (!queue.empty())
	{
		// Take first exposed Convex Polyhedron P from the queue
		p_point=queue.front();
		// Delete first element
		queue.pop_front();
		p_nnc=*p_point;
		// Cut from P the preflow of its exit borders
		cut_ps = cut_polyhedra_with_exit_borders(p_point, flow_nnc, extadj);

		// if cut is not empty update the maps accordingly
		if (!cut_ps.empty())
		{
			totalcut_ps.upper_bound_assign(cut_ps);
			PS pnew_ps(p_nnc);
			// compute Pnew
			pnew_ps.difference_assign(cut_ps);
			fast_pairwise_reduce(pnew_ps); // ??? ///

			// Check if P has adjacents
			if (!rradj[p_point].empty())
				// Updates internal and external borders (in case P has adjs)
				update_int_ext(under_approx_ps, pnew_ps, cut_ps, p_point, postflow, flow_nnc, rradj, ruadj,
						extadj, queue);
			else
				// Updates only internal borders (because P has no adjs)
				update_int(under_approx_ps, pnew_ps, p_point, rradj, ruadj);

			// Erase P from rradj map
			rradj.erase(p_point);
			ruadj.erase(p_point);
			// FREE MEMORY: Delete all external object
			for (it_list=extadj[p_point].begin(); it_list!=extadj[p_point].end(); it_list++)
				delete (*it_list);
			extadj.erase(p_point);
			// Delete processed P
			delete p_point;
		} // END IF
	} // END WHILE

	// Rebuild risky
	for (it_int=rradj.begin(); it_int!=rradj.end(); it_int++)
	{
		risky_ps.add_disjunct(*(it_int->first));
		// FREE RISKY LIST
		//delete it_int->first;
	}
	fast_pairwise_reduce(risky_ps);
	return std::make_pair(risky_ps, totalcut_ps);
}


/*   SOSTITUITO DAL SUCCESSIVO get_sorM_LocalPlus_new convertito da Urbano.

std::pair<PS, PS> get_sorM_LocalPlus(PS &over_approx_ps, PS &under_approx_ps, location_synthesis* loc)
{
	LOGGER(DEBUG, "get_sorM_LocalPlus", "Inizio");
	dimension_type dim=under_approx_ps.space_dimension();
	hash_map_type extadj;
	list_nnc_type under_nnc, queue;
	PS risky_ps(dim,EMPTY);
	std::pair<PS, PS> result;
	ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr under_new(loc->get_Under_eff()->create_empty());
	under_new->set_powerset(under_approx_ps);

	//Calcolo del preflow
	time_constraints tcons = loc->get_time_constraints();
	const continuous::relation_dynamics* dp=
			dynamic_cast<const continuous::relation_dynamics*>(tcons.get_dynamics().get());
	const ppl_polyhedron::continuous_set_PPL_Powerset* tmp=
			dynamic_cast<const ppl_polyhedron::continuous_set_PPL_Powerset*> (dp->get_relation().get());

	ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr postflow(tmp->clone());
	postflow->decrease_primedness();
	PS postflow_ps = postflow->get_powerset();
	Poly postflow_nnc = postflow_ps.begin()->pointset();

	ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr preflow(postflow->clone());
	preflow->get_prepoly();
	PS preflow_ps = preflow->get_powerset();
	Poly preflow_nnc = preflow_ps.begin()->pointset();

	fast_pairwise_reduce(under_approx_ps);
	if (loc->is_initialized_map_sorm())
	{
		PS under_eff(loc->get_Under_eff()->get_powerset());
		under_eff.difference_assign(under_approx_ps);
		fast_pairwise_reduce(under_eff);

		under_nnc = align_maps(under_approx_ps, under_eff, postflow_nnc, preflow_nnc, loc->rradj, loc->ruadj,
				extadj, queue);
	} else {
		risky_ps=over_approx_ps;
		risky_ps.difference_assign(under_approx_ps);
		fast_pairwise_reduce(risky_ps); // Seems to speed up init_tab a little

		under_nnc = init_maps(risky_ps, under_approx_ps, postflow_nnc, preflow_nnc, loc->rradj, loc->ruadj,
				extadj, queue);
		loc->set_initialized_map_sorm(true);

	}

	result = refine_maps(under_approx_ps, postflow_nnc, preflow_nnc, loc->rradj, loc->ruadj, extadj, queue);

	loc->set_Under_eff(under_new);
	risky_ps = result.first;

	for (list_nnc_type::iterator it_list=under_nnc.begin() ; it_list!=under_nnc.end() ; it_list++) {
		risky_ps.add_disjunct(**it_list);
		// FREE UNDER LIST
		delete *it_list;
	}
	fast_pairwise_reduce(risky_ps);
	return std::make_pair(risky_ps, result.second);
}

*/

std::pair<PS, PS> sor_local_recycle(PS &over_approx_ps, PS &under_approx_ps,
				    const Location& loc, ScenarioLocationBundle& bundle_loc)
{
	LOGGER(DEBUG, "get_sorM_LocalPlus", "Inizio");
	dimension_type dim=under_approx_ps.space_dimension();
	hash_map_type extadj;
	list_nnc_type under_nnc, queue;
	PS risky_ps(dim,EMPTY);
	std::pair<PS, PS> result;

	// Riferimenti alle mappe da aggiornare di bundle_loc che è passato per riferimento.
	hash_map_type& loc_rradj = bundle_loc.get_rradj();
	hash_map_u_type& loc_ruadj = bundle_loc.get_ruadj();

	//ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr under_new(loc->get_Under_eff()->create_empty());
	//under_new->set_powerset(under_approx_ps);

	PS under_new(under_approx_ps);

	//Calcolo del preflow
	//time_constraints tcons = loc->get_time_constraints();
	//const continuous::relation_dynamics* dp=
			//dynamic_cast<const continuous::relation_dynamics*>(tcons.get_dynamics().get());
	//const ppl_polyhedron::continuous_set_PPL_Powerset* tmp=
			//dynamic_cast<const ppl_polyhedron::continuous_set_PPL_Powerset*> (dp->get_relation().get());

	 Poly flow(loc.get_flow());


	 //ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr postflow(tmp->clone());
	 //postflow->decrease_primedness();
	 Poly postflow(flow);
	 
	 //PS postflow_ps = postflow->get_powerset();
	 //Poly postflow_nnc = postflow_ps.begin()->pointset();
	 
	 //ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr preflow(postflow->clone());
	 Poly preflow(postflow);
	 
	 //preflow->get_prepoly();
	 get_prepoly(preflow);

	//PS preflow_ps = preflow->get_powerset();
	//Poly preflow_nnc = preflow_ps.begin()->pointset();

	fast_pairwise_reduce(under_approx_ps);

	//if (loc->is_initialized_map_sorm())
	if (bundle_loc.get_my_initialized_map_sorm())
	{
	  //PS under_eff(loc->get_Under_eff()->get_powerset());
	  PS under_eff(bundle_loc.get_my_Under_eff());
	  
	  under_eff.difference_assign(under_approx_ps);
	  fast_pairwise_reduce(under_eff);
	  
	  //under_nnc = align_maps(under_approx_ps, under_eff, postflow_nnc, preflow_nnc, loc->rradj, loc->ruadj,
	  //extadj, queue);
	  
	  under_nnc = align_maps(under_approx_ps, under_eff, postflow, preflow, loc_rradj, loc_ruadj, extadj, queue);
	  
	} else {
	  risky_ps = over_approx_ps;
	  risky_ps.difference_assign(under_approx_ps);
	  fast_pairwise_reduce(risky_ps); // Seems to speed up init_tab a little
	  
	  //under_nnc = init_maps(risky_ps, under_approx_ps, postflow_nnc, preflow_nnc, loc->rradj, loc->ruadj,
	  //                      extadj, queue);
	  // Sostituito con la versione senza postflow_nnc e preflow_nnc che sono stati sostituiti con postflow e preflow.
	  under_nnc = init_maps(risky_ps, under_approx_ps, postflow, preflow, loc_rradj, loc_ruadj, extadj, queue);
	  
	  
	  //loc->set_initialized_map_sorm(true);
	  bundle_loc.set_my_initialized_map_sorm(true);
	  
	}

	//result = refine_maps(under_approx_ps, postflow_nnc, preflow_nnc, loc->rradj, loc->ruadj, extadj, queue);
  // Sostituito con la versione senza postflow_nnc e preflow_nnc che sono stati sostituiti con postflow e prefow.
	result = refine_maps(under_approx_ps, postflow, preflow, loc_rradj, loc_ruadj, extadj, queue);


	//loc->set_Under_eff(under_new);
	bundle_loc.set_my_Under_eff(under_new);

	risky_ps = result.first;

	for (list_nnc_type::iterator it_list=under_nnc.begin() ; it_list!=under_nnc.end() ; it_list++) {
		risky_ps.add_disjunct(**it_list);
		// FREE UNDER LIST
		delete *it_list;
	}

	fast_pairwise_reduce(risky_ps);
	return std::make_pair(risky_ps, result.second);
}






}
