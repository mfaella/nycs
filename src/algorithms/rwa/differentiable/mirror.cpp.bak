#include "mirror.h"
 
/* 
 * mirror_by_GS restituisce la riflessione di poly rispetto l'affine hull di set
 * 
 *  INPUT:	poly 	 (NNC_Polyhedron)	-> poliedro da riflettere;
 * 	        set 	 (NNC_Polyhedron)	-> poliedro rispetto al quale riflettere
 *  OUTPUT:     poly_copy (NNC_Polyhedron)     	-> poliedro riflesso
 * 
 * Utilizza l'algoritmo di Gram Schmidt per ortogonalizzare una base dell'affine set che 
 * si ottiene traslando l'affine hull di set in modo che passi per l'origine 
 */
Poly mirror_by_GS(const Poly &poly, const Poly &set)
{ 
  
  //se poly è vuoto oppure set è vuoto
  if ((poly.is_empty())||(set.is_empty()))
  {
    //il mirror di poly rispetto a set è poly stesso
    return poly;
  }
   
  //affine hull di set
  Poly affine_set(affine_hull(set));
  
  //dimensione affine di affine_set
  int s=affine_set.affine_dimension();
  
  //dimensione dello spazio ambiente
  int n=affine_set.space_dimension();
  //se affine_set è tutto lo spazio ambiente 
  //la riflessione di poly è poly stesso
  if (s==n) return poly;
 if (affine_set.contains(poly)) return poly;
  //copia di poly
  Poly poly_copy(poly);
  //aggiunge n nuove dimensioni spaziali ed 
  //incorpora poly_copy nel nuovo spazio vettoriale 
  poly_copy.add_space_dimensions_and_embed(n);
  
  //p è l'unico punto dell'affine set: dà la traslazione rispetto l'origine
  Generator p(Generator::point());
  Linear_Expression *v = new Linear_Expression[s];
  int lines = 0; // line counter

  // Collects the (unique) point and the lines of affine_set.
  // The lines form a basis of the subspace affine_set-{p}
  const Generator_System& gs=affine_set.generators();
  for (Generator_System::const_iterator i = gs.begin(),gs_end = gs.end(); i != gs_end; ++i)
  {
    Generator x=*i;
    //viene salvato in p l'unico point di affine_set
    if (x.is_point() && !x.is_closure_point())
      p=x;
    else if (i->is_line())
      v[lines++] = Linear_Expression(*i);
  }
   //stampa le coordinate del punto p
   //cout<<"p="<<endl<<p<<endl;
  
  //se affine_set ha dimensione affine 0 e non è vuoto => affine set è dato dal sol punto p
  if (s==0)
  {
    Poly reflection(n);
    reflection=point_mirror(poly,p);
    return reflection;
  }
  
  // Build an orthogonal basis for the subspace
  Linear_Expression *w=new Linear_Expression[s];

  //ortogonalizzazione della base v di affine_set-{p}
  w=Gram_Schmidt(s,v);
  
  delete[] v;
    
  //cout << "base ortogonale di affine_set-{p}" << endl;
   //for (int f=0;f<s;++f)
    //cout << "w[" << f << "]:" << w[f] << endl;
      
  GMP_Integer d_p=p.divisor();
  //denominator è il prodotto di tutti i prodotti scalari
  //inizializzazione denominator
  GMP_Integer denominator=1;
  //ww_scalar_product[i]=<w^i,w^i> dove w^i è l'i-esimo vettore della base w
  GMP_Integer *ww_scalar_product=new GMP_Integer[s];
  //product_product[i] = prodotto di tutti i prodotti scalari eccetto l'iesimo
  GMP_Integer *product_product=new GMP_Integer[s];
  
  //inizializzazione product_product[i]
  for (int i=0;i<s;++i)
    product_product[i]=1;
  
  //ww_scalar_product[i]=<w^i,w^i> dove w^i è l'i-esimo vettore della base w
  for (int i=0;i<s;++i)
  {
    GMP_Integer scalar_product=0;
    
    for (int j=0;j<n;++j)
      scalar_product=scalar_product+((w[i]).coefficient(Variable(j)))*((w[i]).coefficient(Variable(j)));
    ww_scalar_product[i]=scalar_product;
     //cout<<"ww_scalar_product["<<i<<"]:"<<endl<<ww_scalar_product[i]<<endl;
    //aggiornamento di denominator col prodotto scalare corrente
    denominator*=scalar_product;
    
  }
 
  
  //product_product[i] = prodotto di tutti i prodotti scalari eccetto l'iesimo
  for (int j=0;j<s;++j)
    { 
      GMP_Integer product_product_j=1;
      for (int h=0;h<s;++h)
      {
	if (h!=j)
	  product_product_j*=ww_scalar_product[h]; 
      }
      product_product[j]=product_product_j;
      //cout<<"product_product["<<j<<"]:"<<endl<<product_product[j]<<endl;
    }
     delete[] ww_scalar_product;
/*-----------------------------------------------------------------------------------------------  *
 * calcola il risultato x' della funzione di trasformazione x'=f(x,w)                              * 
 * di un generico punto x del poliedro poly.                                                       *
 * La funzione di trasformazione f riflette x in x'                                                *
 *-------------------------------------------------------------------------------------------------*/

for (int i=0;i<n;i++)
  {
    //cout <<"i: "<<i<<endl;
    Linear_Expression e;
    Linear_Expression e_temp2;
    
    for (int j=0;j<s;j++)
    { 
      for (int k=0;k<n;k++)
	e_temp2 +=(d_p*Variable(k)-p.coefficient(Variable(k))) * (w[j]).coefficient(Variable(k))*(w[j]).coefficient(Variable(i))*product_product[j];
      //cout << "e_temp2: "<< e_temp2<< endl;
       // Strano: controllare  
       //cout << "e_temp2: "<< e_temp2<< endl;
    }   
    e+=2*e_temp2;
     //cout << "e: " << e << endl;
    
    e+=(2*p.coefficient(Variable(i))-d_p*Variable(i))*denominator;
    
    //cout<<endl<<Variable(i)<<" -> "<<"("<<e<<") / "<<denominator*d_p<<endl;
   //cout<<"denominator"<<endl<<denominator<<endl;
    
    poly_copy.affine_image(Variable(n+i),e,denominator*d_p);
    //cout<<"poly_copy.affine_image"<<endl<<poly_copy.constraints()<<endl;
  }
  
   delete[] w;
   delete[] product_product;
  
  //rimuove le dimensioni appartenenti all'insieme di variabili {Variable(0),...,Variable(n-1)} 
  poly_copy.remove_space_dimensions(Variables_Set(Variable(0),Variable(n-1)));
  
  //cout <<"poliedro riflesso"<<endl<<poly_copy.constraints()<<endl;
  return poly_copy;    
}


