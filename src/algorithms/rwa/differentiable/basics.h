#include "ralib_private.h"
#include "Gram_Schmidt.h"
/*  
 * affine_hull restituisce l'affine hull del poliedro ph preso in input
 * 
 *  INPUT:	ph 	(NNC_Polyhedron)	-> poliedro a partire dal quale calcolare l'affine hull;
 *  OUTPUT:	ahull	(NNC_Polyhedron)	-> affine hull di ph;
 * 
 */
Poly affine_hull(const Poly &ph);


/* 
 * relative_interior restituisce il relative interior del poliedro preso in input
 * 
 *  INPUT:   ph 			(NNC_Polyhedron)	-> poliedro a partire dal quale calcolare 
 * 					   	   il relative interior;
 *  OUTPUT:  ph_relative_interior	(NNC_Polyhedron)	-> relative interior di ph;
 * 
 */
Poly relative_interior(const Poly &ph); 


/* 
 * zero_mirror_assign trasforma il poliedro passato nella sua riflessione rispetto l'origine
 * 
 * 	INPUT:   ph 	(NNC_Polyhedron)	-> poliedro da riflettere;					   	  
 * 	OUTPUT:  ph	(NNC_Polyhedron)	-> riflessione di ph rispetto l'origine;
 * 
 */
void zero_mirror_assign(Poly &ph);


/* 
 * point_mirror restituisce la riflessione di ph rispetto al punto x, 
 * cioé 2*x-y, con y appartenente a ph
 * 
 * 	INPUT:	ph 	(NNC_Polyhedron)	-> poliedro da riflettere;
 *		x	(Generator)	-> punto rispetto al quale riflettere;					   	  
 * 	OUTPUT:	my_ph 	(NNC_Polyhedron)	-> riflessione di ph rispetto a x
 * 
 */
Poly point_mirror(const Poly &ph, const Generator &x);


/* 
 * mirror restituisce la riflessione di poly rispetto l'affine hull di set
 * 
 *  INPUT:	poly 	 (NNC_Polyhedron)	-> poliedro da riflettere;
 * 		set 	 (NNC_Polyhedron)	-> poliedro rispetto al quale riflettere
 *  OUTPUT:	poly_copy(NNC_Polyhedron)	-> risultato della riflessione di poly
 * 
 */
Poly mirror(const Poly &poly, const Poly &set);


/*
 * bndry(ph1,ph2) = (ph1/\cl(ph2))\/(ph2/\cl(ph1))
 * 
 *   INPUT:   ph1, ph2  (NNC_Polyhedron)
 *   OUTPUT:  output    (NNC_Polyhedron) -> output = (ph1/\cl(ph2))\/(ph2/\cl(ph1))
 * 
 */
Poly bndry(const Poly &ph1,const Poly &ph2);
