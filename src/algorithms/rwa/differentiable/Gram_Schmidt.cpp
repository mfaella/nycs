#include "Gram_Schmidt.h"

/*
 * scalar_product restituisce il prodotto scalare dei due vettori (a elementi interi) presi in input.
 *
 *  INPUT:	x 	(Linear_Expression) -> vettore da moltiplicare;
 *              	y	(Linear_Expression) -> vettore da moltiplicare;
 *  OUTPUT:	e	(GMP_Integer)	-> prodotto scalare di x per y.
 *
 */
GMP_Integer scalar_product(const Linear_Expression &x, const Linear_Expression &y)
  {
    GMP_Integer e;
    for (dimension_type i = x.space_dimension(); i-- > 0; )
      e += x.coefficient(Variable(i))*y.coefficient(Variable(i));
    return e;
  }


/*
 * proj effettua la proiezione ortogonale del vettore v (a elementi interi)
 * sul vettore u (a elementi interi)
 *
 *  INPUT:	v 	(Linear_Expression) -> vettore da proiettare;
 *              	u	(Linear_Expression) -> vettore su cui proiettare;
 *  OUTPUT:	point(e,u_u)    (Generator) -> vettore risultante dalla proiezione
 *					      dato dal vettore e di denominatore u_u.
 */
Generator proj(const Linear_Expression &v, const Linear_Expression &u)
  {
      //prodotto scalare di v per u: <v,u>
      GMP_Integer v_u=scalar_product(v,u);
      //prodotto scalare di u per se stesso <u,u>
      GMP_Integer u_u=scalar_product(u,u);
      Linear_Expression e;
      for (dimension_type i = u.space_dimension(); i-- > 0; )
	 //prodotto vettore per uno scalare -> e=<v,u>u
	 e+=v_u*u.coefficient(Variable(i))*Variable(i);
      //vettore risultato -> (<v,u>u)/<u,u>
      return point(e,u_u);
  }


/*
 * Gram_Schmidt implementa una variante dell'algoritmo di Gram_Schmidt
 *
 *  INPUT:	s 	(int) -> numero di vettori da ortogonalizzare
 *              	v	(Linear_Expression []) -> array contenente gli
 * 						 s vettori (di tipo Linear_Expression)
 * 						 da ortogonalizzare
 *  OUTPUT:	u    	(Linear_Expression []) -> array contenente gli s vettori ortogonalizzati
 *
 */
Linear_Expression *Gram_Schmidt(const int &s, Linear_Expression v[])
{
  Linear_Expression *u=new Linear_Expression[s];
  u[0]=v[0];
  for (int k=1;k<s;k++)
  {
    u[k]=v[k];
    //prodotto dei vecchi <u_j,u_j>
    GMP_Integer d_old=1;
    for (int j=0;j<k;j++)
    {
      //proiezione ortogonale del vettore v[k] su vettore u[j]
      Generator p_kj=proj(v[k],u[j]);
      //Linear_Expression del numeratore di p_kj
      //Linear_Expression e_kj(p_kj);

      // Utilizzata la nuova firma di PPL, Urbano.
      Linear_Expression e_kj(p_kj.expression());

      u[k]+=-e_kj;
      u[k]*=p_kj.divisor();
      u[k]+=e_kj*p_kj.divisor()-e_kj*d_old;
      d_old*=p_kj.divisor();
    }
  }
  return u;
}
