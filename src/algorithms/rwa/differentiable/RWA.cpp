#include "cross.h"
 
PS RWA_GS(const Poly &g,const PS &a, const Poly &f, const Poly &f_neg, int *conta)
{  
  int n=g.space_dimension();
  //insieme universo R^n con n=g.space_dimension()
  PS b(a.space_dimension(),UNIVERSE);

  //b=R^n-a => b è il complemento di a
  b.difference_assign(a);
  b.add_space_dimensions_and_embed(n-b.space_dimension());
  //w_old è un PPS inizializzato al vuoto
  PS w_old(n,EMPTY);
  
  //w_new è un PPS inizializzato al vuoto
  PS w_new(n,EMPTY);
  
  //w_new unito g
  w_new.add_disjunct(g);
  
  //per ogni ph appartenente a [[b]]
  for (PS::const_iterator i = b.begin(), in_end = b.end(); i != in_end; ++i)
    { 
      Poly ph=i->pointset();
      //se reach(ph,g,f,f_neg) non è vuoto
      if (!(reach(ph,g,f,f_neg)).is_empty())
	//w_new unito reach(ph,g,f,f_neg)
	w_new.least_upper_bound_assign(reach(ph,g,f,f_neg)); 
    }
  *conta=0;
  //finché w_old != w_new
  while (!w_old.geometrically_equals(w_new))
  {
    *conta=*conta+1;
    //eliminazine da w_new di tutti gli elementi ridondanti
    w_new.omega_reduce();
    w_old=w_new;

    //per ogni ph appartenente a [[b]] ovvero [[not A]]
    for (PS::const_iterator i = b.begin(), in_end = b.end(); i != in_end; ++i)
    {
      Poly ph=i->pointset(); 

      //per ogni ph1 appartenente a [[w_old]] ovvero [[w_new]]
      for (PS::const_iterator j = w_old.begin(), in_end = w_old.end(); j != in_end; ++j)
      { 
	Poly ph1=j->pointset();

	Poly boundary = bndry(ph,ph1);

	if (!boundary.is_empty()) {
	  PS e = cross_GS(ph, boundary, ph1, f, f_neg);
	  if (!e.is_empty()) {
	    // DEBUG
	    /* cout << endl << "P1: " << ph;
	    cout << endl << "boundary: " << boundary;
	    cout << endl << "P2: " << ph1;
	    cout << endl << "cross: " << e; */

	    w_new.least_upper_bound_assign(e);     
	  }
	  continue; // skip the rest of this j-iteration
	}

	//copia di ph
	Poly c(ph);
	//chiusura topologica di ph
	c.topological_closure_assign();
	//copia di ph1
	Poly cl_ph1(ph1);
	//chiusura topologica ph1
	cl_ph1.topological_closure_assign();
	//intersezione tra la chiusura di ph e quella di ph1
	c.intersection_assign(cl_ph1);
	//se c non è vuoto
	if (!c.is_empty())
	{
	 //promozione di c da Poly a PS
	 PS d(c);
	 // d è l'intersezione tra c e w
	 d.intersection_assign(w_old);
	 // per ogni ph_hat appartenente a [[d]], 
	 // ovvero nell'intersezione tra la chiusura di ph, la chiusura di ph1 e w
	 for (PS::const_iterator k = d.begin(), in_end = d.end(); k != in_end; ++k)
	 {
	    Poly ph_hat(k->pointset());
	    PS e = cross_GS(ph, ph_hat, ph1, f, f_neg);
	    //se cross(ph,ph_hat,ph1,f,f_neg) non è vuoto,...
	    if (!e.is_empty())
	    {
	      // DEBUG
	      /* cout << endl << "P1: " << ph;
	      cout << endl << "P^: " << ph_hat;
	      cout << endl << "P2: " << ph1;
	      cout << endl << "cross: " << e; */

	      //...allora ci sono altri elementi da aggiungere a w_new 
	      //w_new unito cross(ph,ph_hat,ph1,f,f_neg)
	      w_new.least_upper_bound_assign(e);     
	      //cout<<"end_if !e.is_empty"<<endl; 
	    }//end if
	   //cout<<"end_for ph_hat"<<endl; 
	  }//end for (per ogni ph_hat)	  
	  //cout<<"end_if c non è vuoto"<<endl; 
	}//end if (c non è vuoto) 
	//cout<<"end_per ogni ph1"<<endl; 
      }//end for (per ogni ph1)
    //cout<<"end_for (per ogni ph)"<<endl; 
    }//end for (per ogni ph)   
   //cout<<"end_while"<<endl;  
   // w_old.clear();
  }//end while
 
  //cout<<"end_RWA"<<endl;
 w_new.omega_reduce();
 b.clear();
 w_old.clear();
 
 //cout<<"n° itarazioni RWA_GS: "<<conta<<endl;
 
 
 return w_new;
}

