#include "cross.h"

//extern int conta_mirror_GS;
//extern int conta_mirror_cof;

//extern int conta_reach; COMMENTATO DA MARCO.
//extern int conta_mir_cross; COMMENTATO DA MARCO.

/* PS exact_poly_diff restituisce in output la differenza "esatta"
 * tra i poliedri convessi presi in input
 *   INPUT:   ph1, ph2  (NNC_Polyhedron) -> poliedri tra i quali calcolare la differenza;
 *   OUTPUT:  ph_diff   (Pointset_Powerset<NNC_Polyhedron>) -> differenza esatta tra ph1 e ph2
 */
static inline PS exact_poly_diff(const Poly &ph1, const Poly &ph2)
{
  PS ph_diff(ph1);
  PS PS_ph2(ph2);
  ph_diff.difference_assign(PS_ph2);
  return ph_diff;
}


/*
 * mir_cross(ph1,q,ph2) = ( ph1 \/ (cl(ph1) /\ positive_post_flow(ph1)) ) /\ positive_pre_flow(q) /\
 *                        ( mirror(ph2,q) \/ mirror(cl(ph2)/\ positive_pre_flow(ph2), q) )
 *
 *   INPUT:   ph1, ph2, q (NNC_Polyhedron)
 *            f, f_neg    (NNC_Polyhedron) -> flusso positivo e negativo
 *   OUTPUT:  output      (NNC_Polyhedron) -> output=mir_cross(ph1,q,ph2)
 *
 */
Poly mir_cross_GS(const Poly &ph1, const Poly &q, const Poly &ph2, const Poly &f, const Poly &f_neg)
{
   //conta_mir_cross++; COMMENTATO DA MARCO.
   
  //positive_post_flow(ph1) (post-flusso positivo di ph1)
  Poly positive_post_flow_ph1=positive_time_elapse(ph1,f);

  //copia di ph1
  Poly output(ph1);
  //cl(ph1) (chiusura topologica di ph1)
  output.topological_closure_assign();

  //cl(ph1) /\ positive_post_flow(ph1)
  output.intersection_assign(positive_post_flow_ph1);

  //(cl(ph1) /\ positive_post_flow(ph1)) \/ ph1
  // In this context, this is guaranteed to be exact
  output.upper_bound_assign_if_exact(ph1);

  //positive_pre_flow(q) (pre-flusso positivo di q)
  Poly positive_pre_flow_q=positive_time_elapse(q,f_neg);
  // output = ((cl(ph1) /\ positive_post_flow(ph1)) \/ ph1) /\ positive_pre_flow(q)
  output.intersection_assign(positive_pre_flow_q);

  // Now compute the mirror
  Poly to_be_mirrored(ph2);
  // cl(ph2)
  to_be_mirrored.topological_closure_assign();
  Poly positive_pre_flow_ph2 = positive_time_elapse(ph2, f_neg);

  // cl(ph2) /\ positive_pre_flow(ph2)
  to_be_mirrored.intersection_assign(positive_pre_flow_ph2);
  // (cl(ph2) /\ positive_pre_flow(ph2)) \/ ph2
  // In this context, this is guaranteed to be exact
  to_be_mirrored.upper_bound_assign_if_exact(ph2);

  // reflection of ph2 w.r.t. the affine hull of q
  Poly mir = mirror_by_GS(to_be_mirrored, q);

  // mirror(cl(ph2) /\ positive_pre_flow(ph2),q)
  // Poly mir2=mirror_by_GS(ph2_copy,q);
  // mirror(ph2,q)\/mirror(cl(ph2) /\ positive_pre_flow(ph2),q)
  //  conta_mirror_GS=conta_mirror_GS+1;
  // mir.upper_bound_assign_if_exact(mir2);

  //(((cl(ph1) /\ positive_post_flow(ph1)) \/ ph1) /\ positive_pre_flow(q)) /\
  // mirror(to_be_mirrored, q)
  output.intersection_assign(mir);

  return output;
}


/*
 * r_mir_cross(ph, ph_hat, ph1) = mir_cross(ph, rint(ph_hat), ph1) \/
 *                                \/_(sui q di [[p_hat\rint(p_hat)]]) (di r_mir_cross(ph,q,ph1))
 *
 *   INPUT:   ph1, ph_hat, ph2 (NNC_Polyhedron)
 *            f, f_neg         (NNC_Polyhedron) -> flusso positivo e negativo
 *   OUTPUT:  m_cross          (NNC_Polyhedron) -> m_cross=r_mir_cross(ph, ph_hat, ph1)
 *
 */
Poly r_mir_cross_GS(const Poly &ph1, const Poly &ph_hat, const Poly &ph2,const Poly &f,const Poly &f_neg)
{
  //rint(ph_hat) (relative interior di ph_hat)
  Poly rint_ph_hat=relative_interior(ph_hat);
  //mir_cross(ph1,rint(ph_hat),ph2,f,f_neg);
  Poly m_cross=mir_cross_GS(ph1,rint_ph_hat,ph2,f,f_neg);

  //ph_hat \ rint(ph_hat) (differenza esatta tra ph_hat e il rint(ph_hat)
  PS rbndry=exact_poly_diff(ph_hat,rint_ph_hat);

  // per ogni q appartenente a rbndry
  for (PS::const_iterator i = rbndry.begin(), in_end = rbndry.end(); i != in_end; ++i)
  {
    Poly q=i->pointset();
    //mir_cross(ph1,rint(ph_hat),ph2,f,f_neg) \/ r_mir_cross(ph1,q,ph2,f,f_neg)
    m_cross.upper_bound_assign_if_exact(r_mir_cross_GS(ph1,q,ph2,f,f_neg));
  }
  return m_cross;
}


/*
 * cross_plus(ph1,ph_hat,ph2) = ph1 /\ pre_flow(r_mir_cross(ph1,ph_hat,ph2)) =
 * = ph1 /\ (r_mir_cross(ph1,ph_hat,ph2) \/ positive_pre_flow(r_mir_cross(ph1,ph_hat,ph2)))=
 * = (ph1 /\ r_mir_cross(ph1,ph_hat,ph2)) \/ (ph1 /\positive_pre_flow(r_mir_cross(ph1,ph_hat,ph2)))
 *
 *   INPUT:   ph1, ph_hat, ph2 (NNC_Polyhedron)
 *            f, f_neg         (NNC_Polyhedron) -> flusso positivo e negativo
 *   OUTPUT:  output           (Pointset_Powerset<NNC_Polyhedron<) -> output=cross_plus(ph1,ph_hat,ph2)
 *
 */
PS cross_plus_GS(const Poly &ph1,const Poly &ph_hat,const Poly &ph2,const Poly &f,const Poly &f_neg)
{
  //r_mir_cross(ph1,ph_hat,ph2)
  Poly r_m_cross=r_mir_cross_GS(ph1,ph_hat,ph2,f,f_neg);
 if(!r_m_cross.is_empty())
	{
	  //positive_pre_flow(r_mir_cross(ph1,ph_hat,ph2))
	  Poly positive_pre_flow=positive_time_elapse(r_m_cross,f_neg);

	  //r_mir_cross(ph1,ph_hat,ph2)/\ph1
	  r_m_cross.intersection_assign(ph1);
	  //positive_pre_flow(r_mir_cross(ph1,ph_hat,ph2))/\ph1
	  positive_pre_flow.intersection_assign(ph1);

	  PS output(r_m_cross);
	  //(ph1 /\ r_mir_cross(ph1,ph_hat,ph2)) \/ (positive_pre_flow(r_mir_cross(ph1,ph_hat,ph2))/\ph1)
	  output.add_disjunct(positive_pre_flow);

	  //rimozione di eventuali elementi ridondanti
	  output.omega_reduce();
	  return output;
	}
else return PS(ph1.space_dimension(), EMPTY);
}


/*
 * cross_plus(ph1,ph2) = cross_plus(ph1,bndry(ph1,ph2),ph2)
 *
 *   INPUT:   ph1, ph2  (NNC_Polyhedron)
 *            f, f_neg  (NNC_Polyhedron) -> flusso positivo e negativo
 *   OUTPUT:  cross_p   (Pointset_Powerset<NNC_Polyhedron>) -> cross_p=cross_plus(ph1,bndry(ph1,ph2),ph2)
 *
 */
PS cross_plus_GS(const Poly &ph1,const Poly &ph2,const Poly &f,const Poly &f_neg)
{

  //bndry(ph1,ph2) ( (ph1/\cl(ph2))\/(ph2/\cl(ph1)) )
  Poly ph_hat=bndry(ph1,ph2);
  if(ph_hat.is_empty()){
	return PS(ph1.space_dimension(), EMPTY);
	}
  //cross_plus(ph1,bndry(ph1,ph2),ph2,f,f_neg)
  PS cross_p=cross_plus_GS(ph1,ph_hat,ph2,f,f_neg);
  return cross_p;
}

/*
 * cross_zero(ph1,ph2) = ph1 /\ cl(ph2) /\ post_flow(ph2) =
 *                       ph1 /\ (ph2 \/ (cl(ph2)/\positive_pre_flow(ph2)))
 *
 *   INPUT:   ph1, ph2  (NNC_Polyhedron)
 *            f, f_neg  (NNC_Polyhedron) -> flusso positivo e negativo
 *   OUTPUT:  output    (NNC_Polyhedron) -> output=
 *                                          ph1 /\ (ph2 \/ (cl(ph2)/\positive_pre_flow(ph2)))
 */
Poly cross_zero(const Poly &ph1,const Poly &ph2,const Poly &f,const Poly &f_neg)
{
  //copia di ph2
  Poly output(ph2);
  //cl(ph2)
  output.topological_closure_assign();
  //positive_pre_flow(ph2)
  Poly positive_pre_flow_ph2=positive_time_elapse(ph2,f_neg);
  //cl(ph2)/\positive_pre_flow(ph2)
  output.intersection_assign(positive_pre_flow_ph2);
  //ph2 \/ (cl(ph2)/\positive_pre_flow(ph2))
  output.upper_bound_assign_if_exact(ph2);
  //ph1 /\ (ph2 \/ (cl(ph2)/\positive_pre_flow(ph2)))
  output.intersection_assign(ph1);
  return output;
}

/*
 * cross(ph1,p_hat,ph2) = cross_zero(ph1,ph2)\/cross_plus(ph1,ph2)\/cross_plus(ph1,ph_hat,ph2)
 *
 *   INPUT:   ph1, ph_hat, ph2 (NNC_Polyhedron)
 *            f, f_neg         (NNC_Polyhedron) -> flusso positivo e negativo
 *   OUTPUT:  c                (Pointset_Powerset<NNC_Polyhedron>) -> c=cross_zero(ph1,ph2) \/
 *                                                                      cross_plus(ph1,ph2) \/
 *                                                                      cross_plus(ph1,ph_hat,ph2)
 */
PS cross_GS(const Poly &ph1,const Poly &ph_hat,const Poly &ph2,const Poly &f,const Poly &f_neg)
{
  //cross_zero(ph1,ph2)
  PS c((cross_zero(ph1,ph2,f,f_neg)));
  Poly b=bndry(ph1,ph2);
  //cross_zero(ph1,ph2)\/cross_plus(ph1,ph2)
 if(!b.is_empty()){
	  c.least_upper_bound_assign(cross_plus_GS(ph1,ph2,f,f_neg));
	}
  else {
	  //cross_zero(ph1,ph2)\/cross_plus(ph1,ph2)\/cross_plus(ph1,ph_hat,ph2)
 	 c.least_upper_bound_assign(cross_plus_GS(ph1,ph_hat,ph2,f,f_neg));
	}
  //rimozione di eventuali elementi ridondanti
  c.omega_reduce();
  return c;
}

/**************************************************************************************************************/
/*
 * reach(p,g) = p /\ pre_flow(entry(p,g)) =
 *              p /\ pre_flow(bndry(p,g)/\pre_flow(g)) =
 *              (p /\ bndry(p,g)/\pre_flow(g)) \/ (p /\ positive_pre_flow(bndry(p,g)/\pre_flow(g)))
 *
 * Points of p that can reach g without exiting from p \/ g.
 *
 *   INPUT:   p, g     (NNC_Polyhedron)
 *            f, f_neg (NNC_Polyhedron) -> flusso positivo e negativo
 *   OUTPUT:  output   (Pointset_Powerset<NNC_Polyhedron>) -> output=reach(p,g)
 */
PS reach(const Poly &p, const Poly &g, const Poly &f,const Poly &f_neg)
{
  //conta_reach++; COMMENTATO DA MARCO.

   //bndry(p,g)
  Poly b=bndry(p,g);
  if(b.is_empty()){
	return PS(p.space_dimension(), EMPTY);
	}
  //copia di g
  Poly pre_flow_g(g);
  //pre_flow(g)
  pre_flow_g.time_elapse_assign(f_neg);
  //bndry(p,g)/\pre_flow(g)
  b.intersection_assign(pre_flow_g);
  //positive_pre_flow(bndry(p,g)/\pre_flow(g))
  Poly positive_pre_flow=positive_time_elapse(b,f_neg);
  //bndry(p,g)/\pre_flow(g)/\p
  b.intersection_assign(p);
  PS output(b);
  if(!positive_pre_flow.is_empty()){
  	//positive_pre_flow(bndry(p,g)/\pre_flow(g))/\p
  	positive_pre_flow.intersection_assign(p);
	output.add_disjunct(positive_pre_flow);
  }
  output.pairwise_reduce();
  return output;
}
