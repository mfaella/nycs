#include "basics.h"
 
extern int conta_bndry;
/* 
 * affine_hull restituisce l'affine hull del poliedro ph preso in input
 * 
 *  INPUT:	ph 	(NNC_Polyhedron)	-> poliedro a partire dal quale calcolare l'affine hull;
 *  OUTPUT:	ahull	(NNC_Polyhedron)	-> affine hull di ph;
 * 
 */
Poly affine_hull(const Poly &ph) 
{
  //dimensione spaziale del poliedro in input
  dimension_type dim_space=ph.space_dimension();
  
  //dimensione affine del poliedro in input 
  dimension_type dim_affine=ph.affine_dimension();
  
  //se ph è  vuoto restituisce l'insieme vuoto, ossia ph stesso
  if((ph.is_empty()))
  {
    //poliedro vuoto 
    return ph;
  }
  else
  {
    Poly ahull(dim_space,UNIVERSE); 
    //se invece è uguale alla dimensione spaziale restituisce l'insieme universo
    if (dim_affine==dim_space)
    {
      return ahull;
    }
    else
    //altrimenti
    //restituisce l'insieme dei vincoli di uguaglianza di ph poiché 
    //la loro intersezione è uguale a un poliedro avente stessa 
    //dimensione affine di ph
    {
	const Constraint_System& cs=ph.constraints();
	for (Constraint_System::const_iterator i = cs.begin(),cs_end = cs.end();i != cs_end; ++i)     
	  if (i->is_equality())
		 ahull.add_constraint(*i);
	return ahull;     
    }    
  }
}


/* 
 * relative_interior restituisce il relative interior del poliedro preso in input
 * 
 *  INPUT:   ph 			(NNC_Polyhedron)	-> poliedro a partire dal quale calcolare 
 * 					   	   il relative interior;
 *  OUTPUT:  ph_relative_interior	(NNC_Polyhedron)	-> relative interior di ph;
 * 
 */
Poly relative_interior(const Poly &ph) 
{
  //sistema di vincoli di ph
  const Constraint_System& cs = ph.constraints();
  //sistema di vincoli del relative interior di ph
  Constraint_System new_cs;
  //cs.set_space_dimension(ph.space_dimension());
  
  for (Constraint_System::const_iterator i = cs.begin(), cs_end = cs.end(); i != cs_end; ++i)
  {
    Constraint c=*i;
    //se sono vincoli > o = vengono aggiunti così come sono a new_cs
    if (c.is_strict_inequality()||c.is_equality())
      new_cs.insert(c);
    else
    //altrimenti  
    //si tratta di un vincolo >= che viene convertito in >
    {
      Linear_Expression e(c);
      Constraint strict_ineq_c=(e>0);
      new_cs.insert(strict_ineq_c);
    }	 
  }
  
  Poly ph_relative_interior(new_cs);
  return ph_relative_interior;
}


/* 
 * zero_mirror_assign trasforma il poliedro passato nella sua riflessione rispetto l'origine
 * INPUT:   ph 	(NNC_Polyhedron)	-> poliedro da riflettere;					   	  
 * OUTPUT:  ph	(NNC_Polyhedron)	-> riflessione di ph rispetto l'origine;
 * 
 */
void zero_mirror_assign(Poly &ph)
{
  Linear_Expression lexp;
  for (unsigned int i=0; i<ph.space_dimension(); ++i)
  {
    lexp=-Variable(i);
    //trasforma ogni x appartenente a ph in -x
    ph.affine_image(Variable(i),lexp);
  }
  
}


/* 
 * point_mirror restituisce la riflessione di ph rispetto al punto x, 
 * cioé 2*x-y, con y appartenente a ph
 * 
 * INPUT:	ph 	(NNC_Polyhedron)	-> poliedro da riflettere;
 *		x	(Generator)	-> punto rispetto al quale riflettere;					   	  
 * OUTPUT:	my_ph 	(NNC_Polyhedron)	-> riflessione di ph rispetto a x
 * 
 */
Poly point_mirror(const Poly &ph, const Generator &x) 
{
  Linear_Expression lexp;
  GMP_Integer denominator;
  Poly my_ph(ph); 
  //per ogni punto y appartenete a ph...
  for (unsigned int i=0; i<ph.space_dimension(); ++i)
  {
    //...y -> 2*x-y
    lexp=2*x.coefficient(Variable(i))-Variable(i)*x.divisor();
    //denominatore del punto x
    denominator=x.divisor();
    my_ph.affine_image(Variable(i),lexp,denominator);
  }
  return my_ph;
}



/*
 * bndry(ph1,ph2) = (ph1/\cl(ph2))\/(ph2/\cl(ph1))
 * 
 *   INPUT:   ph1, ph2  (NNC_Polyhedron)
 *   OUTPUT:  output    (NNC_Polyhedron) -> output = (ph1/\cl(ph2))\/(ph2/\cl(ph1))
 * 
 */
Poly bndry(const Poly &ph1,const Poly &ph2)
{
  conta_bndry++;
  //copia ph1
  Poly output(ph1);
  //copia ph2
  Poly cl_ph2(ph2);
  
  //cl(ph1) (chiusura topologica di ph1)
  output.topological_closure_assign();
  
  //cl(ph2)
  cl_ph2.topological_closure_assign();
  
  //cl(ph1) /\ ph2 (cl(ph1) intersecato ph2)
  output.intersection_assign(ph2);
  
  //cl(ph2) /\ ph1
  cl_ph2.intersection_assign(ph1);
  
  //(cl(ph1) /\ ph2)\/(cl(ph2) /\ ph1) ((cl(ph1) /\ ph2) unito (cl(ph2) /\ ph1))
  output.upper_bound_assign_if_exact(cl_ph2);
  
  return output;    
} 
