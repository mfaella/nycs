#include "ralib_private.h"
 
/* 
 * scalar_product restituisce il prodotto scalare dei due vettori (a elementi interi) presi in input.
 * 
 *  INPUT:	x 	(Linear_Expression) -> vettore da moltiplicare;
 *              	y	(Linear_Expression) -> vettore da moltiplicare;
 *  OUTPUT:	e	(GMP_Integer)	-> prodotto scalare di x per y.
 * 
 */
GMP_Integer scalar_product(const Linear_Expression &x, const Linear_Expression &y);


/* 
 * proj effettua la proiezione ortogonale del vettore v (a elementi interi) 
 * sul vettore u (a elementi interi)
 * 
 *  INPUT:	v 	(Linear_Expression) -> vettore da proiettare;
 *              	u	(Linear_Expression) -> vettore su cui proiettare;
 *  OUTPUT:	point(e,u_u)    (Generator) -> vettore risultante dalla proiezione  
 *					      dato dal vettore e di denominatore u_u.
 */
Generator proj(const Linear_Expression &v, const Linear_Expression &u);


/* 
 * Gram_Schmidt implementa una variante dell'algoritmo di Gram_Schmidt 
 * 
 *  INPUT:	s 	(int) -> numero di vettori da ortogonalizzare
 *              	v	(Linear_Expression []) -> array contenente gli 
 * 						 s vettori (di tipo Linear_Expression)
 * 						 da ortogonalizzare
 *  OUTPUT:	u    	(Linear_Expression []) -> array contenente gli s vettori ortogonalizzati
 * 					     
 */
Linear_Expression *Gram_Schmidt(const int &n,Linear_Expression v[]);
