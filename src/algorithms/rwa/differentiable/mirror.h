#include "ralib_private.h"
#include "basics.h"
#include "Gram_Schmidt.h"
#include "matrix_operation.h"
 
/* 
 * mirror_by_GS restituisce la riflessione di poly rispetto l'affine hull di set
 * 
 *  INPUT:	poly 	 (NNC_Polyhedron)	-> poliedro da riflettere;
 * 	        set 	 (NNC_Polyhedron)	-> poliedro rispetto al quale riflettere
 *  OUTPUT:     poly_copy (NNC_Polyhedron)     	-> poliedro riflesso
 * 
 * Utilizza l'algoritmo di Gram Schmidt per ortogonalizzare una base dell'affine set che 
 * si ottiene traslando l'affine hull di set in modo che passi per l'origine 
 */
Poly mirror_by_GS(const Poly &poly, const Poly &set);



