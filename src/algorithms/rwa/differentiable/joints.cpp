#include "basics.h"
#include "time_elapse.h"
 
bool is_joint(const Generator &x, const Poly &ph1,const Poly &ph2, const Poly &f,const Poly &f_neg)
{
  
  Generator_System gen_sys_x;
  gen_sys_x.insert(x);
  Poly poly_x(gen_sys_x); //poliedro dato dal solo punto x: {x}
  Poly pos_time_elapse_x=positive_time_elapse(poly_x,f);
  
  Poly my_ph1(ph1);//copia ph1
  Poly my_ph2(ph2);//copia ph2
  
  my_ph2.topological_closure_assign();//my_ph2 ora è la chiusura topologica di ph2
  Poly neg_time_elapse_ph2=positive_time_elapse(ph2,f_neg);
  my_ph2.intersection_assign(neg_time_elapse_ph2);//my_ph2:=my_ph2 intersez neg_time_elapse_ph2
  my_ph2.intersection_assign(pos_time_elapse_x);//my_ph2:=my_ph2 intersez pos_time_elapse_x
  
  
  my_ph1.topological_closure_assign();//my_ph1 ora è la chiusura topologica di ph1
  Poly pos_time_elapse_ph1=positive_time_elapse(ph1,f);
  my_ph1.intersection_assign(pos_time_elapse_ph1);//my_ph1:=my_ph1 intersez pos_time_elapse_ph1
  Poly diff_2x_ph1=point_mirror(my_ph1,x);//diff_2x_ph1:=2x-my_ph1
  my_ph2.intersection_assign(diff_2x_ph1);//my_ph2:=my_ph2 intersez diff_2x_ph1
  
  return !my_ph2.is_empty();
}

Poly internal_joints(const Poly ph1, const Poly ph2, const Poly e, const Poly &f,const Poly &f_neg)
{
  Poly rint_e=relative_interior(e);
  Generator x(Generator::point());
  const Generator_System& gs = rint_e.generators();
  for (Generator_System::const_iterator i = gs.begin(),gs_end = gs.end(); i != gs_end; ++i)
  {
    Generator p=*i;
    if (p.is_point() && !p.is_closure_point())
    {
      x=p;
      break;
    }
  }
  
  if (is_joint(x,ph1,ph2,f,f_neg)==1)//se dato un punto x di rint_e is_joint(x,ph1,ph2) è true
      return rint_e;
  else 
  {
    //return rint_e.add_constraint(Constraint::zero_dim_false());
    Poly empty(ph1.space_dimension(),EMPTY);//viene restituito l'insieme vuoto
    return empty;
  }
}


//restituisce in output la differenza "esatta" tra i Poly ph1 e ph2
static inline PS exact_poly_diff(const Poly ph1, const Poly ph2) 
{
  PS ph_diff(ph1);
  PS PS_ph2(ph2);
  ph_diff.difference_assign(PS_ph2);
  return ph_diff;     
}


Poly joints(const Poly ph1, const Poly ph2, const Poly e,const Poly &f,const Poly &f_neg)
{
  Poly rel_int_e=relative_interior(e);
  PS rbndry=exact_poly_diff(e,rel_int_e);
  Poly i_j=internal_joints(ph1,ph2,e,f,f_neg);
  if (!rbndry.is_empty())
    for (PS::const_iterator i = rbndry.begin(), in_end = rbndry.end(); i != in_end; ++i)
    { 
      Poly e1=i->pointset();
      i_j.upper_bound_assign_if_exact(joints(ph1,ph2,e1,f,f_neg));
    }
    return i_j;   
}

Poly pass(const Poly &ph1, const Poly &q, const Poly &ph2, const Poly &f, const Poly &f_neg)
{
  Poly my_ph1(ph1);//copia di ph1
  my_ph1.topological_closure_assign();//my_ph1 è la chiusura topologica di ph1
  
  Poly te_ph1(ph1); //copia di ph1
  te_ph1.time_elapse_assign(f);//te_ph1 è il time elapse tra ph1 e f
  
  //neg_time_elapse_q è il positive time elapse tra q e f_neg
  Poly neg_time_elapse_q=positive_time_elapse(q,f_neg);
  
  //my_ph1 è l'intersezione tra la chiusura topologica di ph1 e il time elapse di ph1
  my_ph1.intersection_assign(te_ph1);
  //my_ph1 è l'intersezione tra la chiusura topologica di ph1, il time elapse di ph1 e il
  //positive time elapse tra q e f_neg
  my_ph1.intersection_assign(neg_time_elapse_q);
  
  
  Poly my_ph2(ph2);//copia di ph2
  my_ph2.topological_closure_assign();//my_ph2 è la chiusura topologica di ph2
  
  //neg_time_elapse_ph2 è il positive time elapse tra ph2 e f_neg
  Poly neg_time_elapse_ph2=positive_time_elapse(ph2,f_neg);
  
  //my_ph2 è l'intersezione fra la chiusura topologica di ph2 e il positive time elapse tra ph2 e f_neg
  my_ph2.intersection_assign(neg_time_elapse_ph2);     
  
  //m è la riflessione di my_ph2 rispetto l'affine hull di q
  //Poly m=mirror(my_ph2,q); da rivedere: manca la funzione mirror!!
  
  //my_ph1 è l'intersezione fra la chiusura topologica di ph1, il time elapse di ph1 e f, il
  //positive time elapse tra q e f_neg e m
  //my_ph1.intersection_assign(m); da rivedere: manca la funzione mirror!!
  return my_ph1;
}   
