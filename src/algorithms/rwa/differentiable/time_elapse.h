#include <ppl.hh>
#include "ralib_private.h"
// #include<ext/hash_map>
 
using namespace std;

/* Returns the post-flow of p w.r.t. flow, for t>0 */
NNC_Polyhedron positive_time_elapse(const NNC_Polyhedron& p,
				    const NNC_Polyhedron& flow);

/* Returns the exact post-flow of p w.r.t. flow */
Parma_Polyhedra_Library::Powerset<NNC_Polyhedron> time_elapse(const NNC_Polyhedron& p, 
      	       const NNC_Polyhedron& flow);
