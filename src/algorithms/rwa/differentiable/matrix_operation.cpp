#include "matrix_operation.h"
/*  
 * exstract_matrix_form_assign estrae i coefficienti e i termini noti 
 * dei vincoli del poliedro preso in input
 * 
 *  INPUT:	ph	(NNC_Polyhedron)	-> poliedro da esprimere in forma matriciale
 *  OUTPUT:	a	(GMP_Integer [])		-> array dei coefficienti;
 *  OUTPUT	b	(GMP_Integer [])	-> array dei termini noti
 *
 *  ph può essere espresso nella forma matricile Ax=B 
 *  (A € R^((n-k)Xn), x € R^(n) e B € R^(n-k)) con A matrice dei coefficienti 
 *  (la riga i-esima contiene i coeff. del vincolo i-esimo), B vettore di termini noti, 
 *  n dim. spaziale di ph e k dim. affine di ph.
 *  Gli array a e b restituiti dalla funzione sono tali per cui A[i][j]=a[n*i+j] e B[i]=b[i]
 *  per i=0,...,n-k-1 e j=0,...,n-1.
 *
 */
void exstract_matrix_form_assign(Poly ph, GMP_Integer *a, GMP_Integer *b)
{
  int j_a=0,j_b=0;
  int n=ph.space_dimension();
  Constraint_System cs=ph.constraints();
  for (Constraint_System::const_iterator i=cs.begin(),cs_end=cs.end();i!=cs_end;++i)
  {
    Constraint vincolo=*i;
    for (int i=0;i<n;++i)
      {
        a[j_a]=vincolo.coefficient(Variable(i));
        j_a=j_a+1;
      }
    b[j_b]=-1*vincolo.inhomogeneous_term();
    j_b=j_b+1;
   }
}


/* 
 * get_sub_matrix estrae dalla matrice presa in input la sotto-matrice che si ottiene 
 * cancellando la riga i-esima e la colonna j-esima  
 * 
 *  INPUT:	a	(GMP_Integer [])	-> matrice a partire dalla quale calcolare la sottomatrice;
 *  INPUT:	i	(intero)		-> indice della riga da cancellare
 *  INPUT:	j	(intero)		-> indice della colonna da cancellare
 *  INPUT	dim_a	(intero)		-> dimensione della matrice a
 *  OUTPUT      aij	(GMP_Integer [])	-> sotto-matrice risultante
 *
 */
static inline void get_sub_matrix(GMP_Integer *a,int i,int j, int dim_a, GMP_Integer *aij)
{
  
  int h,k;
  
  h=0;
  for (int r=0;r<dim_a;r++)
  {
    if (r!=i)
    {
        k=0;
        for (int c=0;c<dim_a;c++)
            if (c!=j)
            {
                aij[(dim_a-1)*h+k]=a[dim_a*r+c];
                k++;
            }
        h++;
    }
  }
}


/* 
 * determinant restituisce il determinante della matrice presa in input 
 * 
 *  INPUT:	a	(GMP_Integer [])	-> matrice di cui calcolare il determinate;
 *  INPUT	dim	(intero)		-> dimensione della matrice
 *  OUTPUT      ris	(GMP_Integer)		->determinante della matrice
 *
 */
GMP_Integer determinant(GMP_Integer *a,int dim)
{
  if(dim==1) return a[0];
  else 
  {
    GMP_Integer ris=0;
    for (int col=0,row=0;col<dim;col++)
    {
      GMP_Integer *aij=new GMP_Integer[(dim-1)*(dim-1)];
      get_sub_matrix(a,row,col,dim,aij);
      ris+=((row+col)%2?-1:1)*a[dim*row+col]*determinant(aij,dim-1);
      delete[] aij;
    }
    return ris;
  }
}

/* 
 * traspose_assign calcola la matrice trasposta di a 
 * 
 *  INPUT:	a	(GMP_Integer [])	-> matrice da trasporre;
 *  INPUT	m	(intero)		-> numero di righe di a
 *  INPUT	n	(intero)		-> numero di colonne di a
 *  OTPUT	a	(intero)		-> matrice trasposta di a
 *
 */ 
void traspose_assign(GMP_Integer *a,int m, int n)
{
  int i,j;
  int dim=n*m;
  GMP_Integer *trasp = new GMP_Integer[dim];  
  for (i=0;i<n;i++)
    for (j=0;j<m;j++)
      trasp[m*i+j]=a[n*j+i];
        
 for (i=0;i<dim;i++)
      a[i]=trasp[i];
 delete[] trasp;
}


/* 
 * cofactor_assign restituisce la matrice dei cofattori della matrice presa in input 
 * 
 *  INPUT:	num	(GMP_Integer [])	-> matrice a partire dalla quale calcolare i cofattori;
 *  INPUT	f	(intero)		-> dimensione della matrice
 *  OUTPUT      num	(GMP_Integer [])	-> matrice dei cofattori
 *
 */
void cofactor_assign(GMP_Integer *num,int f)
{
  
 GMP_Integer *b=new GMP_Integer[(f-1)*(f-1)];
 GMP_Integer *fac=new GMP_Integer[f*f];
 int p,q,m,n,i,j;

 
 
 if (f==1)
   num[0]=1;
 else
 {
 for (q=0;q<f;q++)
 {
   for (p=0;p<f;p++)
    {
     m=0;
     n=0;
     for (i=0;i<f;i++)
     {
       for (j=0;j<f;j++)
        {
          if (i != q && j != p)
          {
            b[(f-1)*m+n]=num[f*i+j];
            if (n<(f-2))
             n++;
            else
             {
               n=0;
               m++;
               }//end else
            }//end if (i != q && j != p)
        }//end for j 
      }//end for i
      GMP_Integer d=determinant(b,f-1);
      int indice=f*q+p;
      fac[indice]=((q+p)%2?-1:1)*d; 
      //cout<< " determinant(b,f-1);"<<determinant(b,f-1)<<endl;
    }//end for p
  }//end for q
  
traspose_assign(fac,f,f);
for (i=0;i<f;i++)
    {
     for (j=0;j<f;j++)
       {
         num[f*i+j]=fac[f*i+j];
        }
    }
 }
 

 
 
 delete[] fac;
 delete[] b;
}
