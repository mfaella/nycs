#include <ppl.hh>
#include "time_elapse.h"
#include "../../pairwise_reduce.h"

inline Linear_Expression get_linear_expression(const Generator& g) {
  Linear_Expression e;

  for (dimension_type d = g.space_dimension(); d-- > 0; )
    e += g.coefficient(Variable(d)) * Variable(d);

  return e;
}


Poly positive_time_elapse(const Poly& p, const Poly& q) {
  Generator_System::const_iterator i, j;

  Poly p_elapse_q=p;
  p_elapse_q.time_elapse_assign(q);

  Generator_System gs = p_elapse_q.generators();
  Generator_System new_gs = Generator_System();
 // new_gs.set_space_dimension(p.space_dimension());


  // Construct open(p->q)
  for (i=gs.begin(); i!=gs.end(); i++) {
    const Generator &g = *i;
    if (g.is_point()) {
      // cout << "Point: " << g << endl;
      Linear_Expression e = get_linear_expression(g);
      //      for (dimension_type d = g.space_dimension(); d-- > 0; )
      //e += g.coefficient(Variable(d)) * Variable(d);
      Generator closure_g = closure_point(e, g.divisor());
      // DEBUG
      // cout << "Closure point: " << closure_g << endl;
      new_gs.insert(closure_g);
    } else {
      new_gs.insert(g);
    }
    //cout << new_gs << "(END)" << endl;
  }

  Generator_System p_gs = p.generators(), q_gs = q.generators(),
    q_points_gs = Generator_System();

  // Collect real points of q into q_points (performance optimization)
  for (j=q_gs.begin(); j!=q_gs.end(); j++) {
    const Generator &g = *j;
    if (g.is_point())
      q_points_gs.insert(g);
  }

  // Add to new_gs suitable real points
  // For Each real point g1 in p
  for (i=p_gs.begin(); i!=p_gs.end(); i++) {
    const Generator &g1 = *i;
    if (g1.is_point())
      // For Each real point g2 in q
      for (j=q_points_gs.begin(); j!=q_points_gs.end(); j++) {
	const Generator &g2 = *j;
	// add to new_gw the sum of g1 and g2
	new_gs.insert(point(g2.divisor()*get_linear_expression(g1) +
			    g1.divisor()*get_linear_expression(g2),
			    g1.divisor()*g2.divisor()));
      }
  }

  Poly result(new_gs);
  return simplify_coefficients(result);
}


Parma_Polyhedra_Library::Powerset<NNC_Polyhedron> time_elapse(const Poly& p, const Poly& q) {
  Parma_Polyhedra_Library::Powerset<NNC_Polyhedron> result = Parma_Polyhedra_Library::Powerset<NNC_Polyhedron>();
  result.add_disjunct(p);
  result.add_disjunct(positive_time_elapse(p, q));
  return result;
}
