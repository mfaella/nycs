#include "ralib_private.h"
#include "basics.h"
#include "mirror.h"
#include "time_elapse.h"
 
/*
 * mir_cross(ph1,q,ph2) = ( ph1 \/ (cl(ph1) /\ positive_post_flow(ph1)) ) /\ positive_pre_flow(q) /\ 
 *                        ( mirror(ph2,q) \/ mirror(cl(ph2)/\ positive_pre_flow(ph2), q) )
 * 
 *   INPUT:   ph1, ph2, q (NNC_Polyhedron)
 *            f, f_neg    (NNC_Polyhedron) -> flusso positivo e negativo
 *   OUTPUT:  output      (NNC_Polyhedron) -> output=mir_cross(ph1,q,ph2)
 * 
 */
Poly mir_cross_GS(const Poly &ph1, const Poly &q, const Poly &ph2, const Poly &f, const Poly &f_neg);


/*
 * r_mir_cross(ph, ph_hat, ph1) = mir_cross(ph, rint(ph_hat), ph1) \/
 *                                \/_(sui q di [[p_hat\rint(p_hat)]]) (di r_mir_cross(ph,q,ph1))
 * 
 *   INPUT:   ph1, ph_hat, ph2 (NNC_Polyhedron)
 *            f, f_neg         (NNC_Polyhedron) -> flusso positivo e negativo
 *   OUTPUT:  m_cross          (NNC_Polyhedron) -> m_cross=r_mir_cross(ph, ph_hat, ph1)
 * 
 */
Poly r_mir_cross_GS(const Poly &ph1, const Poly &ph_hat, const Poly &ph2,const Poly &f,const Poly &f_neg);


/*
 * cross_plus(ph1,ph_hat,ph2) = ph1 /\ pre_flow(r_mir_cross(ph1,ph_hat,ph2)) =
 * = ph1 /\ (r_mir_cross(ph1,ph_hat,ph2) \/ positive_pre_flow(r_mir_cross(ph1,ph_hat,ph2)))=
 * = (ph1 /\ r_mir_cross(ph1,ph_hat,ph2)) \/ (ph1 /\positive_pre_flow(r_mir_cross(ph1,ph_hat,ph2)))
 * 
 *   INPUT:   ph1, ph_hat, ph2 (NNC_Polyhedron)
 *            f, f_neg         (NNC_Polyhedron) -> flusso positivo e negativo
 *   OUTPUT:  output           (Pointset_Powerset<NNC_Polyhedron<) -> output=cross_plus(ph1,ph_hat,ph2)
 * 
 */
PS cross_plus_GS(const Poly &ph1,const Poly &ph_hat,const Poly &ph2,const Poly &f,const Poly &f_neg);


/*
 * cross_plus(ph1,ph2) = cross_plus(ph1,bndry(ph1,ph2),ph2)
 * 
 *   INPUT:   ph1, ph2  (NNC_Polyhedron)
 *            f, f_neg  (NNC_Polyhedron) -> flusso positivo e negativo
 *   OUTPUT:  cross_p   (Pointset_Powerset<NNC_Polyhedron>) -> cross_p=cross_plus(ph1,bndry(ph1,ph2),ph2)
 * 
 */
PS cross_plus_GS(const Poly &ph1,const Poly &ph2,const Poly &f,const Poly &f_neg);


/*
 * cross_zero(ph1,ph2) = ph1 /\ cl(ph2) /\ post_flow(ph2) =
 *                       ph1 /\ (ph2 \/ (cl(ph2)/\positive_pre_flow(ph2)))
 * 
 *   INPUT:   ph1, ph2  (NNC_Polyhedron)
 *            f, f_neg  (NNC_Polyhedron) -> flusso positivo e negativo
 *   OUTPUT:  output    (NNC_Polyhedron) -> output=
 *                                          ph1 /\ (ph2 \/ (cl(ph2)/\positive_pre_flow(ph2)))
 */
Poly cross_zero(const Poly &ph1,const Poly &ph2,const Poly &f,const Poly &f_neg);


/*
 * cross(ph1,p_hat,ph2) = cross_zero(ph1,ph2)\/cross_plus(ph_1,ph2)\/cross_plus(ph1,ph_hat,ph2)
 * 
 *   INPUT:   ph1, ph_hat, ph2 (NNC_Polyhedron)
 *            f, f_neg         (NNC_Polyhedron) -> flusso positivo e negativo
 *   OUTPUT:  c                (Pointset_Powerset<NNC_Polyhedron>) -> c=cross_zero(ph1,ph2) \/
 *                                                                      cross_plus(ph_1,ph2) \/
 *                                                                      cross_plus(ph1,ph_hat,ph2)
 */
PS cross_GS(const Poly &ph1,const Poly &ph_hat,const Poly &ph2,const Poly &f,const Poly &f_neg);


/*************************************************************************************************************/
/*
 * reach(p,g) = p /\ pre_flow(entry(p,g)) =
 *              p /\ pre_flow(bndry(p,g)/\pre_flow(g)) =
 *              (p /\ bndry(p,g)/\pre_flow(g)) \/ (p /\ positive_pre_flow(bndry(p,g)/\pre_flow(g)))
 * 
 *   INPUT:   p, g     (NNC_Polyhedron)
 *            f, f_neg (NNC_Polyhedron) -> flusso positivo e negativo
 *   OUTPUT:  output   (Pointset_Powerset<NNC_Polyhedron>) -> output=reach(p,g)
 */
PS reach(const Poly &p, const Poly &g, const Poly &f,const Poly &f_neg);
