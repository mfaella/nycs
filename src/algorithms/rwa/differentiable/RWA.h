/*
 * RWA.h
 *
 *  Created on: ??
 *      Author: ??
 *  Modified on: Semptember 07, 2017
 *	Author: Marco Urbano
 */


#ifndef RWA_GS__
#define RWA_GS__




#include "ralib_private.h"

PS RWA_GS(const Poly &g,const PS &a, const Poly &f, const Poly &f_neg, int *conta);


#endif
