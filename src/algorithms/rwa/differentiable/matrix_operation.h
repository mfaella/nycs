#include "ralib_private.h"
/*  
 * exstract_matrix_form_assign estrae i coefficienti e i termini noti 
 * dei vincoli del poliedro preso in input
 * 
 *  INPUT:	ph	(NNC_Polyhedron)	-> poliedro da esprimere in forma matriciale
 *  OUTPUT:	a	(GMP_Integer [])		-> array dei coefficienti;
 *  OUTPUT	b	(GMP_Integer [])	-> array dei termini noti
 *
 *  ph può essere espresso nella forma matricile Ax=B 
 *  (A € R^((n-k)Xn), x € R^(n) e B € R^(n-k)) con A matrice dei coefficienti 
 *  (la riga i-esima contiene i coeff. del vincolo i-esimo), B vettore di termini noti, 
 *  n dim. spaziale di ph e k dim. affine di ph.
 *  Gli array a e b restituiti dalla funzione sono tali per cui A[i][j]=a[n*i+j] e B[i]=b[i]
 *  per i=0,...,n-k-1 e j=0,...,n-1.
 *
 */
void exstract_matrix_form_assign(Poly ph, GMP_Integer *a, GMP_Integer *b);

/* 
 * determinant restituisce il determinante della matrice presa in input 
 * 
 *  INPUT:	a	(GMP_Integer [])	-> matrice di cui calcolare il determinate;
 *  INPUT	dim	(intero)		-> dimensione della matrice
 *  OUTPUT      ris	(GMP_Integer)		->determinante della matrice
 *
 */
GMP_Integer determinant(GMP_Integer *a,int dim);


/* 
 * traspose_assign calcola la matrice trasposta di a 
 * 
 *  INPUT:	a	(GMP_Integer [])	-> matrice da trasporre;
 *  INPUT	m	(intero)		-> numero di righe di a
 *  INPUT	n	(intero)		-> numero di colonne di a
 *  OTPUT	a	(intero)		-> matrice trasposta di a
 *
 */ 
void traspose_assign(GMP_Integer *a,int m, int n);


/* 
 * cofactor_assign restituisce la matrice dei cofattori della matrice presa in input 
 * 
 *  INPUT:	num	(GMP_Integer [])	-> matrice a partire dalla quale calcolare i cofattori;
 *  INPUT	f	(intero)		-> dimensione della matrice
 *  OUTPUT      num	(GMP_Integer [])	-> matrice dei cofattori
 *
 */
void cofactor_assign(GMP_Integer *num, int f);
