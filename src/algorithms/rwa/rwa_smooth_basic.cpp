#include "rwa_smooth_basic.h"
#include "../pairwise_reduce.h"


// Variabili globali commentate da Urbano.
//extern int conta_cross;
//extern int cicli_esterni;
//extern double tempo_reach_cross;
//extern double tempo_cross;

namespace NaPoly{

/*
RWA CHE CONSIDERA TRAIETTORIE DIFFERENZIABILI OVUNQUE
VERSIONE BASE
*/
  std::pair<PS,PS> rwa_smooth_basic(const PS &g,const PS &V, const Poly &f, const Poly &f_neg)
{
  // clock_t start_cross,end_cross;
  // clock_t start_reach,end_reach;
  
  int n = g.space_dimension();
  //insieme universo R^n con n=g.space_dimension()
  PS b(V.space_dimension(),UNIVERSE);

  //b=R^n-not_V => b è il complemento di V
  b.difference_assign(V);
  b.add_space_dimensions_and_embed(n-b.space_dimension());
  //w_old è un PPS inizializzato al vuoto
  PS w_old(n,EMPTY);

  //w_new è un PPS inizializzato al vuoto
  PS w_new(g);

  // start_reach=clock(); // timers

  for (PS::const_iterator it1 = g.begin(), in_end1 = g.end(); it1 != in_end1; ++it1){
	Poly p1=it1->pointset();
 	for (PS::const_iterator it2 = b.begin(), in_end2 = b.end(); it2 != in_end2; ++it2){
	  Poly p2=it2->pointset();
	  PS r(reach(p2,p1,f,f_neg));
	  //se reach(ph,g,f,f_neg) non è vuoto
	  if (!r.is_empty())
	    //w_new unito reach(ph,g,f,f_neg)
	    w_new.least_upper_bound_assign(r);
	}
  }

  // end_reach=clock(); // timers

  // Variabile globale commentata da Urbano.
  //tempo_reach_cross+=((double)(end_reach-start_reach))/CLOCKS_PER_SEC;

  //finché w_old != w_new
  while (!w_old.geometrically_equals(w_new)) {

    // DEBUG
    cout << "." << flush;
    
    //eliminazine da w_new di tutti gli elementi ridondanti
    fast_pairwise_reduce(w_new);
    w_old=w_new;

    //per ogni ph appartenente a [[b]] ovvero [[not V]]
    for (PS::const_iterator i = b.begin(), in_end = b.end(); i != in_end; ++i)
    {
      Poly ph=i->pointset();

      // Variabile globale commentata da Urbano.
      //cicli_esterni++; // statistiche

      //per ogni ph1 appartenente a [[w_old]] ovvero [[w_new]]
      for (PS::const_iterator j = w_old.begin(), in_end = w_old.end(); j != in_end; ++j)
      {
	Poly ph1=j->pointset();

	Poly boundary = bndry(ph,ph1);

	if (!boundary.is_empty()) {
	  // start_cross=clock();

	  // Variabile globale commentata da Urbano.
	  // conta_cross++;

	  PS e = cross_GS(ph, boundary, ph1, f, f_neg);
	  // end_cross=clock();
	  
	  // Variabile globale commentata da Urbano.
	  //tempo_cross+=((double)(end_cross-start_cross))/CLOCKS_PER_SEC;

	  if (!e.is_empty()) {
	    // DEBUG
	    /* cout << endl << "P1: " << ph;
	    cout << endl << "boundary: " << boundary;
	    cout << endl << "P2: " << ph1;
	    cout << endl << "cross: " << e; */

	    w_new.least_upper_bound_assign(e);
	  }
	  continue; // skip the rest of this j-iteration
	}

	//copia di ph
	Poly c(ph);
	//chiusura topologica di ph
	c.topological_closure_assign();
	//copia di ph1
	Poly cl_ph1(ph1);
	//chiusura topologica ph1
	cl_ph1.topological_closure_assign();
	//intersezione tra la chiusura di ph e quella di ph1
	c.intersection_assign(cl_ph1);
	//se c non è vuoto
	if (!c.is_empty()) {
	  //promozione di c da Poly a PS
	  PS d(c);
	  // d è l'intersezione tra c e w
	  d.intersection_assign(w_old);
	  // per ogni ph_hat appartenente a [[d]],
	  // ovvero nell'intersezione tra la chiusura di ph, la chiusura di ph1 e w
	  for (PS::const_iterator k = d.begin(), in_end = d.end(); k != in_end; ++k) {
	    Poly ph_hat(k->pointset());
	      
	    // Variabile globale commentata da Urbano.
	    // conta_cross++;
	    // start_cross=clock();
	    PS e = cross_GS(ph, ph_hat, ph1, f, f_neg);
	    // end_cross=clock();
    
	    // Variabile globale commentata da Urbano.
	    //tempo_cross+=((double)(end_cross-start_cross))/CLOCKS_PER_SEC;

	    //se cross(ph,ph_hat,ph1,f,f_neg) non è vuoto,...
	    if (!e.is_empty())
	    {
	      // DEBUG
	      /* cout << endl << "P1: " << ph;
	      cout << endl << "P^: " << ph_hat;
	      cout << endl << "P2: " << ph1;
	      cout << endl << "cross: " << e; */

	      //...allora ci sono altri elementi da aggiungere a w_new
	      //w_new unito cross(ph,ph_hat,ph1,f,f_neg)
	      w_new.least_upper_bound_assign(e);
	      //cout<<"end_if !e.is_empty"<<endl;
	    }//end if
	   //cout<<"end_for ph_hat"<<endl;
	  }//end for (per ogni ph_hat)
	  //cout<<"end_if c non è vuoto"<<endl;
	}//end if (c non è vuoto)
	//cout<<"end_per ogni ph1"<<endl;
      }//end for (per ogni ph1)
    //cout<<"end_for (per ogni ph)"<<endl;
    }//end for (per ogni ph)
   //cout<<"end_while"<<endl;
   // w_old.clear();
  }//end while

  //cout<<"end_RWA"<<endl;
  fast_pairwise_reduce(w_new);
  // b.clear();
  // w_old.clear();

 //cout<<"n° iterazioni RWA_GS: "<<conta<<endl;

  // We also have to return "(not V) minus W"
  PS internal(b);
  internal.difference_assign(w_new);

  return std::make_pair(w_new, internal);
}
}
