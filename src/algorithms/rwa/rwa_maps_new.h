// This algorithm is not currently used

#include "core/continuous/polyhedra/ppl_polyhedron/extended_ppl.h"
#include "synthesis/continuous/extended_ppl_powerset.h"
#include<ext/hash_map>
#include "rwa_operations.h"
#include "core/hybrid_automata/hybrid_automaton.h"
#include "core/hybrid_automata/location.h"

namespace hybrid_automata {


//INIZIALIZZA LE HASH MAP INTADJ E EXATDJ E LA CODA QUEQUE
void init(PS not_V_ps, PS U_ps, Poly flow_nnc, hash_map_type& extadj, hash_map_type& intadj, list_nnc_type& queue);

void update_ext(PS add_ps, Poly *p1_point, Poly flow_nnc, hash_map_type &extadj, hash_map_type &intadj, list_nnc_type &queue);


// Update external adj of [p2_point] (adjacents to the original P): checks borders among [*p2_point] and the parts removed from original P
void update_ext(PS W, PS add_ps, Poly *p1_point, Poly flow_nnc, hash_map_type &extadj, hash_map_type &intadj, list_nnc_type &queue);


void align_maps(PS under_eff, Poly preflow, hash_map_type& intadj, location_synthesis* loc, list_nnc_type &queue);

// The main loop: Refines internal and external maps.
PS refine_maps(PS U_ps, Poly flow_nnc, hash_map_type& extadj, hash_map_type& intadj, list_nnc_type& queue, int i);


/*
Versione di rwa che utiizza l'approccio locale
*/
PS get_RWA_ce_Plus(PS U_ps, PS V_ps, Poly preflow,location_synthesis* loc);

}
