/*
 * rwa_cd_map.h
 *
 *  Created on: ??
 *      Author: ??
 *  Modified on: Semptember 07, 2017
 *	Author: Marco Urbano
 */


#ifndef RWA_CD_MAP_
#define RWA_CD_MAP_

#include<ext/hash_map>
#include "differentiable/cross.h"
#include "rwa_operations.h"


namespace NaPoly{

/*
    RWA assuming only everywhere-differentiable trajectories.
    Draft version with adjacency maps.

    Warning: this is a work in progress and is NOT correct.

    Output:
    A pair of powersets:
    - first: the result
    - second: the set of cuts (used to optimize the reachability CPre)

*/
std::pair<PS, PS> rwa_smooth_maps(PS U_ps, PS V_ps, Poly flow, Poly preflow);

}


#endif
