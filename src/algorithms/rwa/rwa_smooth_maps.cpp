#include "rwa_smooth_maps.h"
#include "../pairwise_reduce.h"
#include <fstream>

/*
  RWA under everywhere differentiable trajectories.
  Map version.

  Explanation of data structures (maps):

  intadj: weak adjacency between internal polyhedra;
          binary relation among polyhedra;
	  keys and values are (heap) addresses of internal polyhedra
  extadj: adjacency between internal and external polyhedra;
          keys are (heap) addresses of internal polyhedra,
	  these are called "exposed" polyhedra (more precisely, _possibly_ exposed)
	  values are lists of "exposures".
	  An exposure is a pair (B, Q), where Q is an external polyhedron
	  and B is the boundary between P (the key) and Q.

*/

struct maps_t {
  // Internal adjacencies
  hash_map_type intadj;
  // External adjacencies
  hash_map_ps_type extadj;
  // Topological closures of internal
  closure_map_t closure;
  // Internal polyhedra waiting to be processed
  list_nnc_type queue;
};

namespace NaPoly{

static void debug_print_extadj(hash_map_ps_type& extadj)
{
  int i=0;

  cout << "ExtAdj:" << endl;

  for (hash_map_ps_type::iterator p1_it=extadj.begin(); p1_it!=extadj.end(); p1_it++) {
    Poly *p1 = p1_it->first;
    cout << ++i << ": " << *p1 << endl;

    for (list_ps_type::iterator pair_it=extadj[p1].begin(); pair_it!=extadj[p1].end(); pair_it++) {
      pair_poly_ps *pair = *pair_it;
      const Poly &p2 = pair->get_Poly();
      const PS &border = pair->get_PS();
      cout << "\t( " << p2 << ", [" << border << "] )" << endl;
    }
  }
}

static void debug_print_intadj(hash_map_type& intadj)
{
  int i=0;

  cout << "IntAdj:" << endl;

  for (hash_map_type::iterator p1_it=intadj.begin(); p1_it!=intadj.end(); p1_it++) {
    Poly *p1 = p1_it->first;
    cout << ++i << ": " << *p1 << endl << "\t";

    for (list_nnc_type::iterator p2_it=intadj[p1].begin(); p2_it!=intadj[p1].end(); p2_it++) {
      Poly *p2 = *p2_it;
      cout << "{ " << *p2 << "}, ";
    }
    cout << endl;
  }
}

/* Computes an entry of the external adjaceny map (if appropriate),
   which corresponds to a future call to cross(P, P', Q),
   where P' is computed here.

   Assumes that P is in the interior and Q is one of the patches of exterior.

   An entry is returned if P and Q are at least weakly adjacent and,
   either they are strongly adjacent, or their weak boundary (intersection of closures)
   is at least partially in the exterior.

   Returns NULL otherwise.
 */
static bool compute_external_adjacency(Poly &p_nnc, const Poly &q_nnc,
				       const Poly &preflow_nnc, PS &exterior_ps,
				       maps_t &maps)
{
  Poly &p_closed_nnc = *(maps.closure[&p_nnc]);
  Poly q_closed_nnc(q_nnc);
  q_closed_nnc.topological_closure_assign();

  Poly strong_boundary = bndry(p_nnc, q_nnc);
  pair_poly_ps *pair = NULL;

  if (!strong_boundary.is_empty()) {
    // P and Q are strongly adjacent

    // Check if Q is in the direction of flow
    Poly q_preflow_nnc(q_nnc);
    q_preflow_nnc.time_elapse_assign(preflow_nnc);
    q_preflow_nnc.intersection_assign(strong_boundary);
    if (q_preflow_nnc.is_empty()) return false;

    PS B(q_nnc);
    pair = new pair_poly_ps(q_nnc, B, q_closed_nnc);
  } else {
    Poly weak_boundary(p_closed_nnc);
    weak_boundary.intersection_assign(q_closed_nnc);

    // If P is exposed, insert P and its border in extadj
    if (!weak_boundary.is_empty()) {

      // Check if Q is in the direction of flow
      Poly q_preflow_nnc(q_nnc);
      q_preflow_nnc.time_elapse_assign(preflow_nnc);
      q_preflow_nnc.intersection_assign(weak_boundary);
      if (q_preflow_nnc.is_empty()) return false;

      PS B(weak_boundary);
      B.intersection_assign(exterior_ps);
      if (!B.is_empty())
	pair = new pair_poly_ps(q_nnc, B, q_closed_nnc);
    }
  } // end else

  if (pair==NULL) return false;

  // cout << "Adding " << pair->get_Poly() << " as an external adj to " << *p_point << endl;
  hash_map_ps_type& extadj = maps.extadj;
  Poly *p_point = &p_nnc;

  if (extadj.find(p_point)==extadj.end()) {
    list_ps_type empty_list_ps;
    extadj[p_point] = empty_list_ps;
    maps.queue.push_back(p_point);
  }
  extadj[p_point].push_back(pair);
  return true;
}


static void cleanup_maps(maps_t &maps)
{
  for (closure_map_t::iterator it=maps.closure.begin(); it!=maps.closure.end(); it++) {
    delete it->second;
    delete it->first;
  }
}

  // Initialize internal and external adj maps
  // Risky is "internal" stuff (possibly to be cut)
  // notover is "external" (a.k.a. W)
static void init_maps(PS &risky_ps, PS &notover_ps, Poly &preflow_nnc, maps_t &maps)
  {
    hash_map_type::iterator it1_intadj, it2_intadj;
    PS::iterator it_risky, it_notover;
    Poly *p1_point, *p2_point;

    hash_map_type &intadj = maps.intadj;
    closure_map_t &closure = maps.closure;

    // cout << "risky (internal): " << risky_ps << endl;
    // cout << "notover (external): " << notover_ps << endl;

    // Insert all elements of risky in intadj
    for (it_risky=risky_ps.begin(); it_risky!=risky_ps.end(); it_risky++) {
      p1_point = new Poly(it_risky->pointset());
      intadj[p1_point] = list_nnc_type();
      Poly *p1_closed_point = new Poly(*p1_point);
      p1_closed_point->topological_closure_assign();
      closure[p1_point] = p1_closed_point;
    }

    // For each P1 intadj
    for (it1_intadj=intadj.begin(); it1_intadj!=intadj.end(); it1_intadj++) {
      p1_point = it1_intadj->first;

      // For each P2<>P1 in intadj
      it2_intadj=it1_intadj;
      it2_intadj++;
      while (it2_intadj!=intadj.end()) {
	p2_point = it2_intadj->first;
	Poly boundary_nnc = *closure[p2_point];
	boundary_nnc.intersection_assign(*closure[p1_point]);
	// If boundary not empty, P1 and P2 are weakly adjacent
	if (!boundary_nnc.is_empty()) {
	  intadj[p1_point].push_back(p2_point);
	  intadj[p2_point].push_back(p1_point);
	}
	++it2_intadj;
      } // ENDFOR P2

      // External adjacencies

      // For each P' in notover (external)
      for (it_notover=notover_ps.begin(); it_notover!=notover_ps.end(); it_notover++) {
	const Poly &pprime_nnc = it_notover->pointset();
	compute_external_adjacency(*p1_point, pprime_nnc, preflow_nnc, notover_ps, maps);
      } // ENDFOR P'
    } // ENDFOR P1
  }


  /* Initialize internal adjacencies among different patches of Pnew. */
  void init_int_of_pnew(list_nnc_type &pnew_list, maps_t &maps)
  {
    list_nnc_type::iterator it1_list, it2_list;
    Poly *q1_point, *q2_point;

    hash_map_type &intadj = maps.intadj;
    closure_map_t &closure = maps.closure;

    // Compute closures of each Q1 in mylist (Q1 in Pnew)
    for (it1_list=pnew_list.begin(); it1_list!=pnew_list.end(); it1_list++) {
      q1_point = *it1_list;
      // initialize adj list of Q
      intadj[q1_point] = list_nnc_type(); // empty list

      Poly *q1_closed_nnc = new Poly(*q1_point);
      q1_closed_nnc->topological_closure_assign();
      closure[q1_point] = q1_closed_nnc;
    }

    // For each Q1 in mylist (Q1 in Pnew)
    for (it1_list=pnew_list.begin(); it1_list!=pnew_list.end(); it1_list++) {
      q1_point = *it1_list;
      Poly *q1_closed_point = closure[q1_point];

      // For each Q2 different from Q1 in mylist
      it2_list = it1_list;
      it2_list++;
      while (it2_list != pnew_list.end()) {
	q2_point = *it2_list;
	Poly boundary_nnc(*closure[q2_point]);
	boundary_nnc.intersection_assign(*q1_closed_point);
	// If Q1 and Q2 are weakly adjacent, insert Q1 and Q2 in intadj map
	if (!boundary_nnc.is_empty()) {
	  intadj[q1_point].push_back(q2_point);
	  intadj[q2_point].push_back(q1_point);
	}
	it2_list++;
      } // ENDFOR Q2
    } // ENDFOR Q1
  }


/* Second step of the update of the internal adj of P2 (adjacents to the original P):
   checks borders only between (P2, Pnew) [no check for (Pnew, Pnew)]
 */
static void update_int_of_poly_vs_ps(list_nnc_type &pnew_list, Poly *p2_point, maps_t &maps)
{
  hash_map_type &intadj = maps.intadj;

  // For all Q in Pnew
  for (list_nnc_type::iterator q_it=pnew_list.begin(); q_it!=pnew_list.end(); q_it++) {
    Poly *q_point = *q_it;
    Poly boundary(*maps.closure[q_point]);
    boundary.intersection_assign(*maps.closure[p2_point]);

    // SE P2 E Q SONO ADIACENTI INSERISCILI IN INTADJ
    if (!boundary.is_empty()) {
      intadj[q_point].push_back(p2_point);
      intadj[p2_point].push_back(q_point);
    }
  }
}


static void init_ext_of_pnew(PS &w_ps, Poly *p1_point, list_nnc_type &pnew_list, PS &cut_ps, Poly &preflow_nnc, maps_t &maps)
{
  list_ps_type::iterator it_ps_list;
  hash_map_ps_type &extadj = maps.extadj;

  // For each patch Q of Pnew
  for (list_nnc_type::iterator q_it = pnew_list.begin(); q_it != pnew_list.end(); q_it++) {
    Poly *q_point = *q_it;
    // Poly q_closed_nnc(*q_point);
    // q_closed_nnc.topological_closure_assign();

    // cout << "Patch " << *q_point << " of Pnew" << endl;

    // For each external adjacent Q2 of P1 (Pold)
    for (it_ps_list=extadj[p1_point].begin(); it_ps_list!=extadj[p1_point].end(); it_ps_list++) {
      pair_poly_ps *pair = *it_ps_list;
      Poly q2_nnc(pair->get_Poly());
      compute_external_adjacency(*q_point, q2_nnc, preflow_nnc, w_ps, maps);
    }

    // For each patch Q2 of Cut (which is now external)
    for (PS::iterator cut_it=cut_ps.begin(); cut_it!=cut_ps.end(); cut_it++) {
      Poly q2_nnc(cut_it->pointset());
      compute_external_adjacency(*q_point, q2_nnc, preflow_nnc, w_ps, maps);
    }
  }
}


/* Updates the external adjacencies of P2,
   which was adjacent to Pold (the polyhedron being cut).
*/
static void update_cd_external(PS &w_ps, PS &cut_ps, Poly *p2_point, Poly *pold_point, Poly &preflow_nnc, maps_t &maps)
{
  Poly &p2_closed_nnc = *maps.closure[p2_point];
  hash_map_ps_type &extadj = maps.extadj;

  // For each Q in Cut
  for (PS::iterator it_cut=cut_ps.begin(); it_cut!=cut_ps.end(); it_cut++) {
      Poly q_nnc = it_cut->pointset();
      // Check if P2 is exposed by Q
      compute_external_adjacency(*p2_point, q_nnc, preflow_nnc, w_ps, maps);
  } // end for Q

  // Also check whether P2 is exposed via Cut to another external polyhedron Q2
  for (list_ps_type::iterator adj_it=extadj[pold_point].begin();
       adj_it!=extadj[pold_point].end(); adj_it++) {
    Poly &q2_nnc = (*adj_it)->get_Poly(),
         &q2_closed_nnc = (*adj_it)->get_cl();

    Poly p2_q2_weak_boundary_nnc(q2_closed_nnc);
    p2_q2_weak_boundary_nnc.intersection_assign(p2_closed_nnc);
    PS p2_q2_weak_boundary_ps(p2_q2_weak_boundary_nnc);
    p2_q2_weak_boundary_ps.intersection_assign(cut_ps);

    if (!p2_q2_weak_boundary_ps.is_empty())
      compute_external_adjacency(*p2_point, q2_nnc, preflow_nnc, w_ps, maps);
  }
}


  /* Update internal and external maps (when original P1 has internal adjs)
     W contains cut_ps
  */
static void update_int_ext(PS W, PS &pnew_ps, PS &cut_ps, Poly &preflow_nnc, Poly *p1_point,
			   maps_t &maps)
{
  // Dynamically allocate new polyhedra for Pnew,
  // to be used as keys in intadj and extadj
  list_nnc_type pnew_list; // empty list of nnc pointers
  hash_map_type &intadj = maps.intadj;

  // Make a list of dynamically allocated polyhedras for each Q1 in Pnew
  for (PS::iterator it_pnew=pnew_ps.begin(); it_pnew!=pnew_ps.end(); it_pnew++) {
    Poly *q_point = new Poly(it_pnew->pointset());
    pnew_list.push_back(q_point);
  }

  // Populate internal adjacencies among parts of pnew
  init_int_of_pnew(pnew_list, maps);

  // cout << "|" << flush;

  // Populate external adjacencies between parts of pnew and the exterior
  init_ext_of_pnew(W, p1_point, pnew_list, cut_ps, preflow_nnc, maps);

  // cout << "|" << flush;

  // Updates the polyhedra previously adjacent to P1
  // For all P2 adjacent to P1
  for (list_nnc_type::iterator it_list=intadj[p1_point].begin(); it_list!=intadj[p1_point].end(); it_list++) {
    Poly *p2_point = *it_list;

    // Remove P1 from the internal adjacents of P2
    // -> This takes linear time. Would it be better with a (hash)set?
    // (a hashset may be slower on iterating)
    intadj[p2_point].remove(p1_point);
    // do we have to remove the key if the list is empty? Probably not

    // Populate intadj between P2 and all parts of Pnew
    update_int_of_poly_vs_ps(pnew_list, p2_point, maps);
    // Update extadj of P2
    update_cd_external(W, cut_ps, p2_point, p1_point, preflow_nnc, maps);
  }
}

static void debug_save_on_file(PS &ps)
{
  static int i=0;
  char filename[] = "w.output";
  ofstream file;
  file.open(filename, ios_base::app);
  file << "[[" << i++ << "]] " << ps << endl << endl;
  file.close();
}


/* The main loop.
   Refines internal and external maps.
*/
static std::pair<PS, PS> refine_maps(PS &U_ps, Poly &flow, Poly &preflow, maps_t &maps)
{
  const dimension_type dim = flow.space_dimension();
  PS W(U_ps);
  list_ps_type::iterator it_list, it_ps_list;

  hash_map_type& intadj = maps.intadj;
  hash_map_ps_type& extadj = maps.extadj;
  list_nnc_type& queue = maps.queue;
  closure_map_t& closure = maps.closure;

  int i=0;

  // Scan queue
  while (!queue.empty()) {
    PS all_cuts(dim, EMPTY);

    /* if (!(i%10)) {
      debug_save_on_file(W);
      i++;
      } */

    // debug_print_intadj(intadj);
    // debug_print_extadj(extadj);

    // Take first exposed Convex Polyhedron P from the queue
    Poly *p_point = queue.front();
    // Delete first element
    queue.pop_front();
    bool at_least_one_cut = false;

    // COMMENTATO DA Urbano
    //cout << "." << flush;
    // cout << "(p size:" << p_point->total_memory_in_bytes() << ")\t" << flush;

    for(it_ps_list=extadj[p_point].begin(); it_ps_list!=extadj[p_point].end(); it_ps_list++) {
      pair_poly_ps *p=*it_ps_list;
      Poly &p2 = p->get_Poly();
      PS &B = p->get_PS(); // set of borders between P and P2

      for(PS::iterator it_pnew=B.begin(); it_pnew!=B.end(); it_pnew++) {
	Poly p_hat=it_pnew->pointset();
	PS cut = cross_GS(*p_point, p_hat, p2, flow, preflow);

	// cout << "Cross done" << endl;

	// if cut is not empty
	if (!cut.empty()) {
	  at_least_one_cut = true;
	  // cout << "(cuts: " << cut.size() << "->" << flush;
	  // cout << cut.size() << ", " << flush;
	  all_cuts.upper_bound_assign(cut);
	}
      }
    }

    // debug_print_extadj(extadj);

    if (at_least_one_cut) {
      i++; // for debugging

      // Compute Pnew = P \ Cut
      fast_pairwise_reduce(all_cuts);

      // DEBUG
      // cout << "cut size:";
      // for (PS::iterator tmp=all_cuts.begin(); tmp!=all_cuts.end() ;tmp++)
      //cout << tmp->pointset().total_memory_in_bytes() << "\t";

      PS pnew_ps(*p_point);
      pnew_ps.difference_assign(all_cuts);
      // meta_difference_assign(pnew_ps, all_cuts, true);

      // cout << "pnew: " << pnew_ps.size() << "->" << flush;
      fast_pairwise_reduce(pnew_ps);
      // cout << pnew_ps.size() << ")" << flush;

      // cout << "Polyhedron " << p_nnc << " will be replaced by " << pnew_ps << endl;

      // Update the maps
      // merge_and_pairwise_reduce(W, W, all_cuts);
      W.upper_bound_assign(all_cuts);

      //fast_pairwise_reduce(W);
      update_int_ext(W, pnew_ps, all_cuts, preflow, p_point, maps);
    }

    // Now, P has been processed, i.e., its possibly exposed boundaries
    // have produced certain "cuts", but maybe these cuts are empty.

    // In any case, remove P from external map, as it is not exposed anymore
    for (it_list = extadj[p_point].begin(); it_list != extadj[p_point].end(); it_list++)
	delete *it_list;
    extadj.erase(p_point);

    // cout << "Erasing " << *p_point << endl;

    // If a non-empty cut was made, P is replaced by Pnew
    if (at_least_one_cut) { // Obliterate P
      // Remove P from internal map
      intadj.erase(p_point);
      delete closure[p_point];
      closure.erase(p_point);
      delete p_point;

      // COMMENTATO DA Urbano
      //cout << endl << "Intadj: " << intadj.size() << ", Extadj: " << extadj.size() << flush;
      // debug_print_extadj(extadj);
    }
  } // END WHILE

  fast_pairwise_reduce(W);

  PS internal(dim, EMPTY);
  for (hash_map_type::iterator it=intadj.begin(); it!=intadj.end(); it++)
    internal.add_disjunct(*(it->first));
  fast_pairwise_reduce(internal);

  return std::make_pair(W, internal);
}


static std::pair<PS, PS> get_RWA_cd_map_convex(PS &U_ps, PS &V_ps,
					       Poly &flow, Poly &preflow)
{
  dimension_type dim=U_ps.space_dimension();
  PS W_ps(U_ps);
  maps_t maps;

  // cout << "V:" << V_ps << endl;
  //cout << "Computing not-V..." << flush;

  //calcolo not_V
  PS not_V_ps(dim, UNIVERSE);
  V_ps.add_space_dimensions_and_embed(dim - V_ps.space_dimension());
  not_V_ps.difference_assign(V_ps);
  //fast_difference_assign(not_V_ps, V_ps);
  //meta_difference_assign(not_V_ps, V_ps, true);

  //cout << "done." << endl;
  //cout << "Computing Reach..." << flush;

  // per ogni p1 in U
  for (PS::const_iterator it1 = U_ps.begin(); it1 != U_ps.end(); ++it1){
    Poly p1=it1->pointset();
    //per ogni p2 in [|not_V|]
    for (PS::const_iterator it2 = not_V_ps.begin(); it2 != not_V_ps.end(); ++it2){
      Poly p2=it2->pointset();
      PS r(reach(p2, p1, flow, preflow));

      // se reach(ph,g,f,f_neg) non è vuoto
      if (!r.is_empty())
	// w_new unito reach(ph,g,f,f_neg)
	W_ps.least_upper_bound_assign(r);
    }
  }
  fast_pairwise_reduce(W_ps);

  //cout << "done." << endl;

  // COMPUTE "RISKY"
  // Note: ships-3 produces a 25070-205 (polyhedra) difference, which crashes after .
  not_V_ps.difference_assign(W_ps);
  //meta_difference_assign(not_V_ps, W_ps, true);
  fast_pairwise_reduce(not_V_ps);

  // cout << "Initial W: " << W_ps << endl;

  //cout << "Initializing adjacency maps..." << flush;
  init_maps(not_V_ps, W_ps, preflow, maps);
  //cout << "done." << endl;

  pair<PS, PS> result = refine_maps(W_ps, flow, preflow, maps);
  return result;
}



  std::pair<PS, PS> rwa_smooth_maps(PS U_ps, PS V_ps, Poly flow, Poly preflow)
  {
    int n=U_ps.space_dimension();
    PS ret_set(n, EMPTY), ret_cut(n, EMPTY);

    // cout << endl << "U: " << U_ps << endl;
    // cout << "V: " << V_ps << endl;

    // Treats each patch of U separately
    for (PS::const_iterator i = U_ps.begin(), in_end = U_ps.end(); i != in_end; ++i) {
      PS g(i->pointset());
      std::pair<PS, PS> partial = get_RWA_cd_map_convex(g, V_ps, flow, preflow);

      //cout << "Patch of U finished" << endl;

      ret_set.least_upper_bound_assign(partial.first);
      fast_pairwise_reduce(ret_set);
      ret_cut.least_upper_bound_assign(partial.second);
      fast_pairwise_reduce(ret_cut);
    }

    return std::make_pair(ret_set, ret_cut);
  }

}
