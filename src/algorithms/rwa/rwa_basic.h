/*
 * rwa_ce_naive.h
 *
 *  Created on: ??
 *      Author: ??
 *  Modified on: Semptember 07, 2017
 *	Author: Marco Urbano
 */


#ifndef RWA_CE_NAIVE_
#define RWA_CE_NAIVE_



#include <ext/hash_map>
#include "rwa_operations.h"
#include "rwa_map_definition.h"
// #include "rwa_ce_map.h"



namespace NaPoly {

/*
  RWA allowing almost-everywhere differentiable trajectories.
  Basic version (no adjacency maps).
*/
std::pair<PS, PS> rwa_basic(PS U_ps, PS V_ps, Poly preflow);

}



#endif
