/*
 * rwa_cd_naive.h
 *
 *  Created on: ??
 *      Author: ??
 *  Modified on: Semptember 07, 2017
 *	Author: Marco Urbano
 */


#ifndef RWA_CD_NAIVE_
#define RWA_CD_NAIVE_


#include "differentiable/RWA.h"
#include "differentiable/cross.h"
#include "rwa_operations.h"



namespace NaPoly{
  /*
    RWA assuming only everywhere-differentiable trajectories.
    Basic version (no optimizations).
  */
  std::pair<PS,PS> rwa_smooth_basic(const PS &g, const PS &a, const Poly &f, const Poly &f_neg);
}


#endif
