#include "rwa_basic.h"
#include "../pairwise_reduce.h"


namespace NaPoly{

/*
RWA CHE CONSIDERA TRAIETTORIE NON DIFFERENZIABILI IN UN NUMERO FINITO DI PUNTI
VERSIONE BASE
*/
std::pair<PS, PS> rwa_basic(PS U_ps, PS V_ps, Poly preflow)
{
  //cout << "rwa_ce_naive" << endl;

	dimension_type dim=U_ps.space_dimension();
	PS W(dim, EMPTY), tmp(dim, EMPTY), Cut(dim, EMPTY);
	Poly p1_point(dim), p2_point(dim), p1close_nnc(dim), b_nnc(dim);
	PS not_V_ps(dim, UNIVERSE);
	V_ps.add_space_dimensions_and_embed(dim-V_ps.space_dimension());
	not_V_ps.difference_assign(V_ps);

	W = U_ps;
	fast_pairwise_reduce(W);
	//ITERATORI
	PS::iterator it_risky, it_notover;
	bool fixpt=false;

	//FINCHE NON VIENE RAGGIUNTO IL PUNTO FISSO
	while(!fixpt)
	{
		// NOT_V= NOT_V \ W
		not_V_ps.difference_assign(W);
		fast_pairwise_reduce(not_V_ps);
		tmp=W;
		Cut.clear();

		//PER OGNI P1 APP A NOT_V
		for(it_risky=not_V_ps.begin(); it_risky!=not_V_ps.end(); it_risky++)
		{
			p1_point = it_risky->pointset();
			p1close_nnc=p1_point;
			p1close_nnc.topological_closure_assign();
			//PER OGNI P2 APP A W
			for(it_notover=W.begin(); it_notover!=W.end(); it_notover++)
			{
				p2_point=it_notover->pointset();
				//B_NNC= P1 /\ PREFLOW(ENTRY(P1,P2))
				b_nnc = exit_border_opt(p1_point, p1close_nnc, p2_point, preflow);
				b_nnc.time_elapse_assign(preflow);
				b_nnc.intersection_assign(p1_point);
				//AGGIUNGE B_NNC A CUT
				if(!b_nnc.is_empty())
					Cut.add_disjunct(b_nnc);
			}
		}
		//AGGIUNGE CUT A W
		W.upper_bound_assign(Cut);
		fast_pairwise_reduce(W);
		//CONTROLLO PER IL PUNTO FISSO
		if(tmp.geometrically_covers(W))
			fixpt=true;

		// DEBUG
		//cout << "not_V_ps = " << not_V_ps << endl;
	}

	return std::make_pair(W, not_V_ps);
}

}
