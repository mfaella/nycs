// This algorithm is not currently used

#include "rwa_ce_map_new.h"
#include "synthesis/post_operators/rwa/differentiable/cross.h"

namespace hybrid_automata {

void init(PS not_V_ps, PS U_ps, Poly flow_nnc, hash_map_type& extadj, hash_map_type& intadj, list_nnc_type& queue)
{
	dimension_type dim=U_ps.space_dimension();
	hash_map_type::iterator it1_extadj, it2_extadj;
	PS::iterator it_risky, it_notover;
	Poly *p1_point, *p2_point, p1close_nnc(dim), pprime_nnc(dim), b_nnc(dim), *b_point;
	list_nnc_type list_nnc;
	bool first_ext;
	
	// Insert all element of risky in extadj hash_map
	for (it_risky=not_V_ps.begin(); it_risky!=not_V_ps.end(); it_risky++)
    {
		p1_point = new Poly(it_risky->pointset());
		extadj[p1_point]=list_nnc;
    }
	
	// For each P1 extadj (same of risky)
	for (it1_extadj=extadj.begin(); it1_extadj!=extadj.end(); it1_extadj++)   
    {
		p1_point = it1_extadj->first;
		p1close_nnc=*p1_point;
		p1close_nnc.topological_closure_assign();
		
		// For each P2<>P1 in extadj (same of risky)
		it2_extadj=it1_extadj;
		it2_extadj++;
		while (it2_extadj!=extadj.end()) 
		{
			p2_point = it2_extadj->first;
			b_nnc = bndry(*p1_point,*p2_point);
			
			// If boundary exists, P1 and P2 are adjacents
			if (!b_nnc.is_empty()) 
			{
				extadj[p1_point].push_back(p2_point);
				extadj[p2_point].push_back(p1_point);
			}
			++it2_extadj;
		} // ENDFOR P2      
      
		// For each P' in notover
		first_ext=true;
		for (it_notover=U_ps.begin(); it_notover!=U_ps.end(); it_notover++) 
		{
			pprime_nnc = it_notover->pointset();
			b_nnc = exit_border_opt(*p1_point, p1close_nnc, pprime_nnc, flow_nnc);
			
			// If P1 is exposed, insert P1 and its border in intadj
			if (!b_nnc.is_empty())
			{
				if (first_ext)
				{
					intadj[p1_point]=list_nnc;
					queue.push_back(p1_point);
					first_ext=false;
				}
				b_point = new Poly(b_nnc);
				intadj[p1_point].push_back(b_point);
			}
		} // ENDFOR P'
    } // ENDFOR P1
}

void update_ext(PS add_ps, Poly *p1_point, Poly flow_nnc, hash_map_type &extadj, hash_map_type &intadj, list_nnc_type &queue)
{
	dimension_type dim = flow_nnc.space_dimension();
	list_nnc_type list_nnc;
	Poly q1_nnc(dim), *b_point, *p2_point;
	PS *intadj_ps;
	PS:: iterator it_cut;

	// For Each convex P2 adj to P1 (P1 has the part %add_ps% to add to RWAm)
	for (list_nnc_type::iterator it_list=extadj[p1_point].begin(); it_list!=extadj[p1_point].end(); it_list++)
    {
		p2_point=*it_list;
		Poly p2close_nnc(*p2_point);
		p2close_nnc.topological_closure_assign();

		// For each Q1 in ADD, checks exit_borders between Q1 and P2
		for (it_cut=add_ps.begin(); it_cut!=add_ps.end(); it_cut++)
		{
			q1_nnc = it_cut->pointset();
			Poly b_nnc(exit_border_opt(*p2_point, p2close_nnc, q1_nnc, flow_nnc));
	
			// If there exists exit_borders
			if (!b_nnc.is_empty())
			{
				// Add exit_borders to intadj
				b_point = new Poly(b_nnc);
				if (intadj.find(p2_point)==intadj.end())
					intadj[p2_point]=list_nnc;

				intadj[p2_point].push_back(b_point);

				bool find=false;
			
				// Scan queue
				list_nnc_type::iterator it_queue=queue.begin();
				while (it_queue!=queue.end() && (!find))
				{
					if (*it_queue==p2_point)
						find=true;
					it_queue++;
				}
				// Insert P2 in queue if and only if P2 not is already in the queue
				if (!find)
					queue.push_back(p2_point);
			}
		} // ENDFOR ADD
	} // ENDFOR EXTADJ
}


// Update external adj of [p2_point] (adjacents to the original P): checks borders among [*p2_point] and the parts removed from original P
void update_ext(PS W, PS add_ps, Poly *p1_point, Poly flow_nnc, hash_map_type &extadj, hash_map_type &intadj, list_nnc_type &queue)
{
	dimension_type dim = flow_nnc.space_dimension();
	list_nnc_type list_nnc;
	Poly q1_nnc(dim), *b_point, *p2_point;
	PS *intadj_ps;
	PS:: iterator it_cut;

	// For Each convex P2 adj to P1 (P1 has the part %add_ps% to add to RWAm)
	for (list_nnc_type::iterator it_list=extadj[p1_point].begin(); it_list!=extadj[p1_point].end(); it_list++)
    {
		p2_point=*it_list;
		Poly p2close_nnc(*p2_point);
		p2close_nnc.topological_closure_assign();

		// For each Q1 in ADD, checks exit_borders between Q1 and P2
		for (it_cut=add_ps.begin(); it_cut!=add_ps.end(); it_cut++)
		{
			q1_nnc = it_cut->pointset();
			Poly b_nnc(exit_border_opt(*p2_point, p2close_nnc, q1_nnc, flow_nnc));
	
			// If there exists exit_borders
			if (!b_nnc.is_empty())
			{
				bool covered=false;
				PS::iterator it_W=W.begin();
				
				while (!covered && it_W!=W.end())
				{
					Poly W_slice(it_W->pointset());
					PS W_ps(W_slice);
					PS b_ps(b_nnc);
					if (W_ps.geometrically_covers(b_ps))
						covered=true;
					it_W++;
				}

				if (!covered)
				{

					// Add exit_borders to intadj
					b_point = new Poly(b_nnc);
					if (intadj.find(p2_point)==intadj.end())
						intadj[p2_point]=list_nnc;

					intadj[p2_point].push_back(b_point);

					bool found=false;
			
					// Scan queue
					list_nnc_type::iterator it_queue=queue.begin();
					while (it_queue!=queue.end() && (!found))
					{
						if (*it_queue==p2_point)
							found=true;
						it_queue++;
					}
					// Insert P2 in queue if and only if P2 not is already in the queue
					if (!found)
						queue.push_back(p2_point);
				} // ENDIF mainingful exit_border
			} // ENDIF non empty exit_border
		} // ENDFOR ADD
	} // ENDFOR EXTADJ
}


void align_maps(PS under_eff, Poly preflow, hash_map_type& intadj, location_synthesis* loc, list_nnc_type &queue)
{
  dimension_type dim=under_eff.space_dimension();
  hash_map_type::iterator it1_extadj;
  PS::iterator it_under, it_notover;
  Poly *p1_point, *b_point, *p2_point;
  Poly p1close_nnc(dim), b_nnc(dim), flow_nnc(preflow);
  list_nnc_type list_nnc;
  bool first_ext;
  

  PS U_old(loc->get_U_old()->get_powerset());
  //clock_val_set inv(loc.invariant());
  //PS inv_ps=get_polyhedra_powerset(inv, false);
  //U_old.intersection_assign(inv_ps);
  
  // dovrebbe aggiungere la B
  
  //under_eff.poly_difference_assign(U_old);
  
  
  // For Each convex of the new part of not(V)
  for (it_under=under_eff.begin(); it_under!=under_eff.end(); it_under++) {
    p1_point= new Poly(it_under->pointset());
    p1close_nnc=*p1_point;
    p1close_nnc.topological_closure_assign();
    
    // For Each convex of RWA-last-step
    
    list_nnc_type list2_nnc;
    
    // Check bndry(Old-Not(V), New-Part-of-Not(V)) and bndry(New-Part-of-Not(V), New-Part-of-Not(V))
    for (it1_extadj=loc->extadj.begin(); it1_extadj!=loc->extadj.end(); it1_extadj++) {
	p2_point=it1_extadj->first;
	b_nnc = simple_border_opt(*p1_point, p1close_nnc, *p2_point);
	if (!b_nnc.is_empty()) {
	  loc->extadj[p2_point].push_back(p1_point);
	  list2_nnc.push_back(p2_point);
	}
    } // ENDFOR EXTADJ
    
    loc->extadj[p1_point]=list2_nnc;
    
    first_ext=true;
    for (it_notover=U_old.begin(); it_notover!=U_old.end(); it_notover++) {
	Poly u_nnc(it_notover->pointset());
	
	// Check the exit borders
	b_nnc = exit_border_opt(*p1_point, p1close_nnc, u_nnc, flow_nnc);
	// If there exists exit borders 
	if (!b_nnc.is_empty()) {
	  // Add the exit borders in queue and in external adjacenties
	  if (first_ext) {
	    intadj[p1_point]=list_nnc;
	    queue.push_back(p1_point);
	    first_ext=false;
	  }
	  b_point = new Poly(b_nnc);
	  intadj[p1_point].push_back(b_point);
	}
      } // ENDFOR U_old
  } // ENDFOR not(V) \ U
}



// The main loop: Refines internal and external maps.
PS refine_maps(PS U_ps, Poly flow_nnc, hash_map_type& extadj, hash_map_type& intadj, list_nnc_type& queue, int i)
{
	dimension_type dim=U_ps.space_dimension();
	Poly *p_point, p_nnc(dim), *b_point, b_nnc(dim), pprime_nnc(dim), *pprime_point, pprime_close_nnc(dim), q_nnc(dim), q_close_nnc(dim), empty(dim, EMPTY);
	PS *pnew_point, *cut_point, add_ps(dim, EMPTY),  risky_ps(dim);
	PS W(U_ps);
	//PS W(dim, EMPTY);
	PS:: iterator it_cut, it_pnew, it_under;
  
	hash_map_type::iterator it_int, it_ext, it_findint, it_findext;
	list_nnc_type::iterator it_list, it1_list;


	// Scan queue
	while (!queue.empty()) {
	  // Take first exposed Convex Polyhedron P from the queue
	  p_point=queue.front();
	  // Delete first element
	  queue.pop_front();
	  
	  p_nnc=*p_point;
	  
	  // Cut from P the preflow of its exit borders
	  add_ps = cut_polyhedra_with_exit_borders(p_point, flow_nnc, intadj);
	  
	  // if there is some region to add then update the maps accordingly
	  if (!add_ps.empty()) {
	    // Check if P has adjacents
	    if (!extadj[p_point].empty())
	      // Updates external borders (in case P has adjs)
	      if (i==4)
		update_ext(   add_ps, p_point, flow_nnc, extadj, intadj, queue);
	      else
		update_ext(W, add_ps, p_point, flow_nnc, extadj, intadj, queue);
	    
	    // Add the part to RWA
	    W.upper_bound_assign(add_ps);
	    
	    // FREE MEMORY
	    // Delete all external object
	    for (it_list=intadj[p_point].begin(); it_list!=intadj[p_point].end(); it_list++)
	      delete (*it_list);
	    intadj.erase(p_point);
	  } // END IF
	} // END WHILE
	
	fast_pairwise_reduce(W);
	return W;
}

/*
versione di rwa che utilizza l'approccio locale
*/
PS get_RWA_ce_Plus(PS U_ps, PS V_ps, Poly preflow, location_synthesis* loc)
{
	dimension_type dim = U_ps.space_dimension();
	hash_map_type intadj;
	hash_map_type extadj;
	list_nnc_type queue;
	PS notover_ps(dim,UNIVERSE), W(dim, EMPTY);
	PS intermediate_ps(dim);
	int cdebug_info;

	if ((loc->is_location_initialized()) && (!loc->get_Under_eff()->is_empty()))
	{
		
		align_maps(loc->get_Under_eff()->get_powerset(),preflow, intadj, loc, queue);
		W = refine_maps(U_ps, preflow, loc->extadj, intadj, queue, 5);
		
	} else {	
		PS not_V_ps(dim, UNIVERSE);
		not_V_ps.difference_assign(V_ps);
		
		// COMPUTE "RISKY"
		not_V_ps.difference_assign(U_ps);
		not_V_ps.pairwise_reduce();
		std::cout<<loc->extadj.size()<<std::endl;
		init(not_V_ps, U_ps, preflow,loc->extadj, intadj, queue);
		std::cout<<loc->extadj.size()<<std::endl;
		W = refine_maps(U_ps, preflow, loc->extadj, intadj, queue, 4);
	}
	return W;

}
}
