#ifndef RWA_OPERATIONS_H_
#define RWA_OPERATIONS_H_

#include "../../io/polyhedra/ppl_polyhedron/extended_ppl.h"
// #include "extended_ppl_powerset.h"
#include<ext/hash_map>

#include"../../automaton/preprocessor.h"
#include "differentiable/time_elapse.h"

// To include hash_map_type, list_nnc_type and NNC_PolyhedronTraits_loc definitions.
#include "rwa_map_definition.h"

// To include global variables
//#include "../../scenario/global_var_definitions.h"


using namespace Parma_Polyhedra_Library;
using namespace __gnu_cxx;
using namespace std;



//CALCOLA BNDRY(P1,P2)

Poly simple_border_opt(Poly &poly1_nnc, Poly &poly1close_nnc, Poly &poly2_nnc);

//CALCOLA ENTRY(INTERNAL_POLY_NNC, EXTERNAL_POLY_NNC)
Poly exit_border_opt(Poly &internal_poly_nnc, Poly &internal_polyclose_nnc, Poly &external_poly_nnc, Poly &flow_nnc);


//CALCOLA P/\ PREFLOW(B) CON B={b | (P,b) app extadj}

PS cut_polyhedra_with_exit_borders(Poly *p_point, Poly &flow_nnc, hash_map_type& extadj);


/* Returns the "universal preflow" of [r_original].
   This is guaranteed to be an over-approximation to the set
   of points that must reach [r_original] under flow [flow].

   Notice: the universal preflow of P is the intersection,
   over the rays r of the flow, of the existential preflow
   of P w.r.t. r.
 */
PS get_univ_preflow(const PS &g_ps, const Poly &flow);

// RITORNA TRUE SSE [p_nnc] E' BOUNDED RISPETTO [flow_nnc].
bool is_bounded(const Poly &p_nnc, const Poly &flow_nnc);
// RIMUOVE LA PARTE UNBOUNDED DI [a_ps] RISPETTO [postflow_nnc]
void remove_unbounded(PS &a_ps, Poly postflow_nnc, Poly preflow_nnc);
//RITORNA IL POLIEDRO ORIGINE DI DIMENSIONE DIM
Poly get_origin(int dim);


// Tries to simplify each convex polyhedron in [ps].
void simplify(PS &ps);


/*CONTROLLA SE LA LOCAZIONE FORNITA DA DSET HA RAGGIUNTO IL PUNTO FISSO
QUESTO ACCADE QUANDO I POLIEDRI B E C APPENA CALCOLATI SONO UGUALI A QUELLI CALCOLATI AL PASSO PRECEDENTE

bool get_fixpt(const hybrid_automata::automaton_synthesis& aut,PS b_ps, PS c_ps,
		hybrid_automata::location_synthesis* loc);
*/
#endif /*RWA_OPERATIONS_H_*/
