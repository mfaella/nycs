/*
 * rwa_ce_map.h
 *
 *  Created on: ??
 *      Author: ??
 *  Modified on: Semptember 07, 2017
 *	Author: Marco Urbano
 */


#ifndef RWA_CE_MAP_
#define RWA_CE_MAP_

#include <ext/hash_map>
#include "rwa_operations.h"
#include "rwa_map_definition.h"


namespace NaPoly{


// Updates internal adjacencies among pieces of "Pnew".
// Called when the original polyhedron being cut has no adjacents.
void update_int(PS &pnew_ps, hash_map_type &intadj);

/*
  RWA allowing almost-everywhere differentiable trajectories.
  Fast version (with adjacency maps).
*/
 std::pair<PS, PS> rwa_maps(PS U_ps, PS V_ps, Poly preflow);

}

#endif
