#include "pairwise_reduce.h"

using namespace Parma_Polyhedra_Library;
using namespace Parma_Polyhedra_Library::IO_Operators;
typedef NNC_Polyhedron Poly;
typedef Pointset_Powerset<Poly> PS;

using namespace std;

int main(void)
{
  Variable x(0), y(1);

  Poly p1(2), p2(2), p3(2), p4(2); // they start "true"
  p1.add_constraint(x>=0);
  p1.add_constraint(x<1);
  p1.add_constraint(y>=0);
  p1.add_constraint(y<=1);

  p2.add_constraint(x==1);
  p2.add_constraint(y>=0);
  p2.add_constraint(y<=1);

  p3.add_constraint(x>1);
  p3.add_constraint(x<2);
  p3.add_constraint(y>=0);
  p3.add_constraint(y<=1);

  p4.add_constraint(x>1);
  p4.add_constraint(x<3);
  p4.add_constraint(y>=1);
  p4.add_constraint(y<=2);

  // p1,p2,p3 can be merged together; p4 cannot

  PS g(p1);
  g.add_disjunct(p2);
  g.add_disjunct(p3);
  g.add_disjunct(p4);
  cout << "Before: " << g << endl;
  fast_pairwise_reduce(g);
  cout << "After: " << g << endl;

  cout << "affine_image:" << endl;
  Linear_Expression e0 = 24*Variable(0), e1 = 121*Variable(0) + 77*Variable(1);
  p2.affine_image(Variable(0), e0, 12);
  p2.affine_image(Variable(1), e1, 11);
  cout << p2 << endl;
}
