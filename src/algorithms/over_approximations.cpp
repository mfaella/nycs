/*
 *  File: scenario_algo.cpp
 *  Created on: September 10, 2017
 *  Author: Marco Urbano
 */

#include "over_approximations.h"

#include "scenario_utility.h"
#include "rwa/rwa_operations.h"
#include "rwa/differentiable/time_elapse.h"
#include "pairwise_reduce.h"


using namespace std;

namespace NaPoly{


  std::pair<PS, PS> over_small_approx(PS& U, PS& V, const Location& loc)
  {
    LOGGER(DEBUG, "over_small_approx", "Start");
    
    clock_t startover, endover;
    startover = clock();

    dimension_type dim = U.space_dimension();      	
    PS U_ps(U), not_U_ps(U), not_V_ps(V);
    negate(not_V_ps);
    negate(not_U_ps);
    
    PS over(U);
    PS not_over(V);
    
    PS not_ru(dim, Parma_Polyhedra_Library::EMPTY);
    
    Poly flow(loc.get_flow());
    flow.topological_closure_assign();
    Poly preflow(flow);
    get_prepoly(preflow);
    
    // REMOVE_UNBOUNDED( GET_UNIV_PREFLOW(U) \ U )
    PS ru_ps(get_univ_preflow(U, preflow));
    
    // ------ Per not_over ------- //
    //PS not_preflow(dim, UNIVERSE);
    //not_preflow.difference_assign(ru_ps);
    // ------              ------- //
    
    ru_ps.intersection_assign(not_U_ps);
    
    // simplify(ru_ps); // BENE
    // ru_ps.pairwise_reduce();
    PS::iterator it;
    Poly p_nnc(dim), ptime_nnc(dim);
    list_powerset_type to_be_added;
    
    // For each convex P in [ru_ps]
    for (it = ru_ps.begin(); it != ru_ps.end();) {
      p_nnc = it->pointset();
      // If P is not bounded
	if (!is_bounded(p_nnc, flow)) {
	  // cut the positive pre-flow of P from P
	  ptime_nnc = positive_time_elapse(p_nnc, preflow);
	  PS p_ps(p_nnc), ptime_ps(ptime_nnc);
	  
	  // ----- Per not_over ----- //
	  // PS unbounded(p_ps);
	  // unbounded.intersection_assign(ptime_ps);
	  // not_preflow.upper_bound_assign(unbounded);
	  // -----              ----- //
	  
	  p_ps.difference_assign(ptime_ps);
	  it = ru_ps.drop_disjunct(it);
	  to_be_added.push_back(p_ps);
	} else {
	  it++;
	}
      }
      
    for (list_powerset_type::iterator l_it=to_be_added.begin();l_it!=to_be_added.end();l_it++)
      add_powerset(ru_ps, *l_it);
    //Fine calcolo di REMOVE_UNBOUNDED

    ru_ps.intersection_assign(not_V_ps);
    
    // over = (U \/ ru) \ V
    add_powerset(over, ru_ps);
    fast_pairwise_reduce(over);
    
    //std::cout<<"\nover: "<<over<<std::endl;
    
    // Calcolo NOTOVER = V \/ not_univ_preflow \/ not_ru
    
    // BENE DISABLED FOR RWA_must via SOR_may :
    // add_powerset(not_over, not_preflow); // not_preflow contains also not_ru
    // Temporary hack: direct computation of not_over
    not_over = over;
    negate(not_over);
    fast_pairwise_reduce(not_over);

    endover=clock();
    //tempo_over COMMENTATA DA URBANO. LE VARIABILI GLOBALI SARANNO DECOMMENTATE ALLA FINE.
    //tempo_over+=((double)(endover-startover))/CLOCKS_PER_SEC;
    
    return std::make_pair(over, not_over);
}



/*
   Input:
	U - reach region
	V - avoid region
	loc - riferimento alla locazione.
   Output:
	Over(U,V)     = U \/ REMOVE_UNBOUNDED(not_U /\ not_V)
	not_Over(U,V) = V \/ ( union(P /\ positive_time_elapse(P)) [P in not_bounded(not_U/\not_V)]
 */
std::pair<PS, PS> over_large_approx(PS& U, PS& V, const Location& loc)
{
	LOGGER(DEBUG, "over_large_approx", "Start");

	clock_t startover, endover;
	startover = clock();

	PS over(U);
	PS not_over(V);

	// Calcolo di not_U /\ not_V
	PS not_U(U);
	negate(not_U);
	PS not_V(V);
	negate(not_V);

	not_U.intersection_assign(not_V);
	fast_pairwise_reduce(not_U); 

	Poly flow(loc.get_flow());
	flow.topological_closure_assign();
	Poly preflow(flow);	
	get_prepoly(preflow);

	// REMOVE_UNBOUNDED
	PS ru_ps(not_U);

	dimension_type dim = ru_ps.space_dimension();
	PS::iterator it;
	PS not_ru(dim, EMPTY); // will be added to not_over
	list_powerset_type to_be_added;

	// For each convex P in [ru_ps]
	for (it = ru_ps.begin(); it != ru_ps.end();) {
		Poly p_nnc = it->pointset();
		// If P is not bounded
		if (!is_bounded(p_nnc, flow)) {
			// cut the positive pre-flow of P from P
			Poly ptime_nnc = positive_time_elapse(p_nnc, preflow);
			PS p_ps(p_nnc), ptime_ps(ptime_nnc);

			/* ----- Update not_over ----- */
			// PS unbounded(p_ps);
			// unbounded.intersection_assign(ptime_ps);
			// not_ru.upper_bound_assign(unbounded);
			/* -----              ----- */

			p_ps.difference_assign(ptime_ps);
			it = ru_ps.drop_disjunct(it);
			to_be_added.push_back(p_ps);
		}
		else
			it++;
	}

	for (list_powerset_type::iterator l_it=to_be_added.begin();l_it!=to_be_added.end();l_it++)
	  add_powerset(ru_ps, *l_it);
	// End REMOVE_UNBOUNDED

	fast_pairwise_reduce(ru_ps);
	add_powerset(over, ru_ps);
	fast_pairwise_reduce(over);
	//std::cout<<"\nover: "<<over<<std::endl;

	// NOT_Over
	// add_powerset(not_over, not_ru);
	// Temporary hack: direct computation of not_Over
	not_over = over;
	negate(not_over);
	fast_pairwise_reduce(not_over);

	endover=clock();
  // tempo_over COMMENTATO DA URBANO. SARA' DECOMMENTATO ALLA FINE.
  // tempo_over+=((double)(endover-startover))/CLOCKS_PER_SEC;
	return std::make_pair(over, not_over);
}



}
