/*
 *  File: scenario_utility.h
 *  Created on: September 5, 2017
 *  Author: Marco Urbano
*/


#ifndef UTILITIES_H_
#define UTILITIES_H_

// To include alias definitions.
#include "../automaton/preprocessor.h"
#include "rwa/rwa_operations.h"

namespace NaPoly{


  // Funzione che fa le veci della vecchia continuous_set_PPL_Powerset::negate(), serve per negare il PS.
  void negate(PS& to_negate);

  // Funzione che sostituisce la vecchia "continuous_set_PPL_Powerset::simplify()", elimina ridondanze dal PS.
  // E' Già contenuta nel file rwa_operations.h, era già presente nel vecchio codice.
  //void simplify(PS& to_simplify);

  //fa l'inverso rispetto all'origine. Utilizzato dall'operatore SORM.
  void get_prepoly(Poly& my_poly);

  // Negazione scritta da Benerecetti che restituisce il PS.
  // PS negate_bene(PS p);

  /*
    Adds second to first 
  */
  void add_powerset(PS &first, PS &second);

  inline bool are_equal(const PS &first, const PS &second)
  {
    return first.geometrically_covers(second) && second.geometrically_covers(first);
  }


}

#endif
