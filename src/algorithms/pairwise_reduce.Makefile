CPPFLAGS=-ggdb -I/home/spex/local/include

LDFLAGS=-L/home/spex/local/lib
LDLIBS=-lppl

OBJS=pairwise_reduce.o pairwise_reduce_test.o

pairwise_reduce_test: $(OBJS)

clean:
	rm *.o

