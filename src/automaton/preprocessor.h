/*
 *  File: preprocessor.h
 *  Created on: June 20, 2017
 *  Author: Marco Urbano
 */


//Include le librerie ppl installate sul sistema.
#include <ppl.hh>

// Alias for PPL.
using PS = Parma_Polyhedra_Library::Pointset_Powerset<Parma_Polyhedra_Library::NNC_Polyhedron>;
using Poly = Parma_Polyhedra_Library::NNC_Polyhedron;
using Parma_var = Parma_Polyhedra_Library::Variable;
