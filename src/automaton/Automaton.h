/*
 *  File: Automaton.h
 *  Created on: June 15, 2017
 *  Author: Marco Urbano
 */


#ifndef __NEW_AUTOMATON_H__
#define __NEW_AUTOMATON_H__

#include <iostream>
#include <vector>
#include "Location.h"
#include "Variable.h"
#include "State_set.h"

using namespace std;

// Forward declarations.
namespace NaPoly{

  class Variable;
  class Location;
  class StateSet;

  class SafetyScenario;
}

namespace NaPoly{

  class Automaton{
    friend class SafetyScenario;
    //friend class Scenario;
    friend class Parser;
  private:
    string name;
    vector<Location> locations;
    StateSet init_states;
    vector<Variable> variables;


    // Cache to store the StateSet built on the pair <Location, Location.getInvariant()> used from the Scenarios.
    StateSet invariant_cache;

    // Private methods that are used from the Parser class. They can't be used outside of this class because of encapsulation would be violated.
    Location& get_location_ref(int location_id);
    // getLocation by name
    Location& get_location_ref(string location_name);

    void set_name(string my_name);

    void add_variable(const Variable& var);

    void add_location(const Location& loc);

    // Set init states when red from the config file parser.
    void set_init(const StateSet& initially);

  public:


    // Inner class for throwing exception when a Transition has been declared with one or both invalid location id's.
    class NotFoundLocationEx : public std::exception{};
    // Inner class for throwing exception when calling a method that has to operate with locations but the automaton hasn't got any.
    class EmptyLocationsEx : public std::exception{};



    Automaton();
    Automaton(const Automaton& to_copy);
    Automaton(string name);
    // save_to_file(std::string file);
    //~Automaton();

    string get_name();

    // getLocation by id
    const Location& get_location(int location_id) const;
    // getLocation by name
    const Location& get_location(string location_name) const;


    // To iterate over all the Variable(s)
    const vector<Variable>& get_variables() const;
    // To iterate over all the Location(s)
    const vector<Location>& get_locations() const;

    // To get a StateSet that contains a map with <Location, Invariant>
    const StateSet& get_invariant();

    // To get a const reference of the locations.
    const vector<Location>& get_locations();

    // Returns the number of unprimed variables.
    int get_dimension();

    friend ostream& operator<<(ostream& os, Automaton& aut);



    /*  TO BE WRITTEN.
    iterator<Location> getLocationsIter();
    iterator<Variabile> getVariablesIter();
    StateSet& getInit();
    Variabile& getVariabile(string name);
    */
  };

  // Declaration of the overloaded operator<<.
  ostream& operator<<(ostream& os, Automaton& aut);
}


#endif
