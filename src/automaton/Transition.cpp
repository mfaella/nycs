/*
 *  File: Transition.cpp
 *  Created on: July 11, 2017
 *  Author: Marco Urbano
 */

#include "Transition.h"
#include "Location.h"

using namespace std;
using namespace Parma_Polyhedra_Library;

namespace NaPoly{

Transition::Transition(Location& source, Location& destination,
 bool contr_flag, PS my_jump): src{source}, dst{destination}, contr{contr_flag}, jump{my_jump}{};


  ostream& operator<<(ostream& os, Transition& t) {

    using IO_Operators::operator<<;

    os << "Transition " << t.src.get_id() << " --> " << t.dst.get_id() << "." << endl;
    os << " jump : " << "[";
    print_PS_regex(os, t.jump);
    os << "]";

    // Stampa con variabili PPL.
    //os<<"["<<t.jump<<"]"<<endl;

    return os;
  }

  
const PS& Transition::get_jump() const{
  return jump;
}

bool Transition::is_contr() const{
  return contr;
}


PS Transition::compute_preimage(PS& p){

  PS p2(p); // a copy of p
  ppl_polyhedron::convert_to_primed(p2);
  p2.intersection_assign(jump);
  p2.remove_higher_space_dimensions(p.space_dimension());

  return p2;
}


const Location& Transition::get_target() const{
  return dst;
}

const Location& Transition::get_source() const{
  return src;
}

}
