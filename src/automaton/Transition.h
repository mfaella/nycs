/*
 *  File: Transition.h
 *  Created on: June 20, 2017
 *  Author: Marco Urbano
 */

#ifndef __NEW_TRANSITION_H__
#define __NEW_TRANSITION_H__


#include <iostream>
#include "preprocessor.h"
//#include "Location.h"
#include "../io/predicates/convert_to_ppl_powerset.h"
#include "../io/output_operators.h"




using namespace std;


namespace NaPoly{

  /* Forward declaration */
  class Location; 

  class Transition{

   private:
    Location& src;
    Location& dst;
    bool contr;
    PS jump;


  public:
    Transition(Location& source, Location& destination, bool contr_flag, PS my_jump);

    const PS& get_jump() const;
    bool is_contr() const;
    friend ostream& operator<<(ostream& os, Transition& t);
    //~Transition();

    const Location& get_target() const;
    const Location& get_source() const;
    // Scenario's methods
    PS compute_preimage(PS& p);


  };

  ostream& operator<<(ostream& os, Transition& t);

}

#endif
