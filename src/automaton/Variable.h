/*
 *  File: Variable.h
 *  Created on: June 20, 2017
 *  Author: Marco Urbano
 */


#ifndef __NEW_VARIABLE_H__
#define __NEW_VARIABLE_H__


#include <iostream>
#include <unordered_map>
#include "preprocessor.h" //Include gli alias e le librerie utili.


using namespace std;


namespace NaPoly{

 class Variable{
   friend class Parser;
 private:
   int id;
   string name;
   Parma_var ppl_var;
   bool _is_primed;
   Variable* dual_variable; // se "is_primed" è true allora restituisce la unprimed, altrimenti restituisce la primed variable.

   static unordered_map<int, Variable> old_id_to_variable;

   // Static counter to assign the dimensions while building the variables.
   static int current_dim;

   // Used to reset the map for every new automaton to be read.
   static void reset_var_map();

 public:

   // This constructor builds an object of the class Variable with a Parma_var object created within.
   Variable(string my_name, bool primed_flag);

   // copy constructor.
   Variable(const Variable& var);

   //virtual ~Variable();

   void set_id(int my_id);
   int get_id();
   const Parma_var& get_PPL() const;
   string get_name() const;
   void set_dual(Variable* var);
   Variable* get_dual() const;
   bool is_primed() const;

   static int get_dimension();
   static void add_to_map(int id, Variable& var);
   static const Variable& get_var_from_id(int id);
   static const unordered_map<int, Variable>& get_map();

   // Overloaded operator << to output this object.
   friend ostream& operator<<(ostream& os, Variable var);
 };

 // Declaration of the overloaded operator <<. (will be defined later into the same namespace).
 ostream& operator<<(ostream& os, Variable var);

}

#endif
