/*
 *  File: Location.cpp
 *  Created on: July 11, 2017
 *  Author: Marco Urbano
 */


#include "Location.h"


using namespace std;



namespace NaPoly{


ostream& operator<<(ostream& os, Location& loc){
  // Used to output Poly and PS.
  using IO_Operators::operator<<;
  
  os << "Location Id=" << loc.id << ", name=" << loc.name << endl ;
  os << "Flow: [";
  print_poly(os, loc.flow);
  os << "]" << endl << "Invariant: [";
  print_PS_regex(os, loc.invariant);
  os << "]" << endl;
  
  return os;
}

/* Costruisce un oggetto Location ed inizializza tutti gli attributi.
   Le unordered_set sono inizializzate dal costruttore di default.
*/

  Location::Location(string my_name, unsigned int my_id, PS my_invariant, Poly my_flow) :
    id{my_id},
    name{my_name},
    invariant{my_invariant},
    flow{my_flow}
{}

void Location::set_id(int my_id){
  id=my_id;
}

int Location::get_id() const{
  return id;
}


string Location::get_name() const {
  return name;
}


const PS& Location::get_invariant() const{
  return invariant;
}


Poly Location::get_flow() const {
  return flow;
}

/*                   NEVER USED
std::vector<Transition>::iterator Location::getInContrTransitionIter(){
  return incoming_contr.begin();
}

std::vector<Transition>::iterator Location::getInUncontrTransitionIter(){
  return incoming_uncontr.begin();
}

std::vector<Transition>::iterator Location::getOutContrTransitionIter(){
  return outgoing_contr.begin();
}

std::vector<Transition>::iterator Location::getOutUncontrTransitionIter(){
  return outgoing_uncontr.begin();
}
*/

const vector<Transition>& Location::get_out_contr_transition(){
  return outgoing_contr;
}

const vector<Transition>& Location::get_out_uncontr_transition(){
  return outgoing_uncontr;
}

const vector<Transition>& Location::get_in_contr_transition(){
  return incoming_contr;
}

const vector<Transition>& Location::get_in_uncontr_transition(){
  return incoming_uncontr;
}

void Location::add_transition(Transition trans, bool contr, bool outgoing){
     if(contr){ // controlled
       if(outgoing){ // outgoing transition because verse == 1
          outgoing_contr.push_back(trans);
       }
       else incoming_contr.push_back(trans);
     }
     else  // uncontrolled
     {
       if(outgoing){ //outgoing transition because verse == 1
         outgoing_uncontr.push_back(trans);
       }
       else incoming_uncontr.push_back(trans);
     }
}



}
