/*
 *  File: State_set.cpp
 *  Created on: July 11, 2017
 *  Author: Marco Urbano
 */

#include "State_set.h"

using namespace std;

namespace NaPoly{

// Inizializza automaticamente l'attributo "states" come una unordered_map vuota.
StateSet::StateSet(){};


// Copy constructor: it copies the unordered_map.
StateSet::StateSet(const StateSet& to_copy): states{to_copy.states}{}

/* Restituisce un riferimento all'oggetto associato alla locazione loc.
   Utilizzato l'operatore "at" della struttura dati "unordered_map".
   Se non lo trova il metodo "at" lancia l'eccezione out_of_range. */
  PS& StateSet::get_region(const Location& loc) {
    return states.at(loc);
  }

  const PS& StateSet::get_region(const Location& loc) const {
    return states.at(loc);
  }

  // Inserisce la "pair" <loc,region> all'interno dell'unordered_map.
  void StateSet::add(Location loc, PS region) {
    states.insert({loc, region});
    return;
  }

//Distruttore.
StateSet::~StateSet(){}

// Returns the const unordered_map to iterate without modifying anything.
const unordered_map<Location, PS>& StateSet::get_map(){
return states;
}


// May throw an out_of_range exception.
void StateSet::intersection_assign(const StateSet& y){

 /* Intersects the PS obtained from y if the symb_state.first
   is present into the StateSet y */
 for(auto& symb_state : states)
  symb_state.second.intersection_assign(y.get_region(symb_state.first));

return;
}


// Overloaded output operator
ostream& operator<<(ostream& os, const StateSet& stateset){

 /* Uncomment to print using << operator of PPL
    using IO_Operators::operator<<;           */

 for(auto it = stateset.states.begin(); it!=stateset.states.end(); it++)
 {
    stringstream PPL_region;
    os << "Location: " << it->first.get_name() << " ";
    print_PS_regex(os, it->second);
    os << endl;
 }

return os;
}

  // print function.
  void StateSet::print() {

    // using IO_Operators::operator<<;
    cout << *this;
  }

}
