/*
 *  File: Automaton.cpp
 *  Created on: July 11, 2017
 *  Author: Marco Urbano
 */


#include "Automaton.h"

using namespace std;



namespace NaPoly{

// Default constructor.
Automaton::Automaton(){
    name = "default";
};

Automaton::Automaton(string my_name):name{my_name}{}


void Automaton::add_variable(const Variable& var){
 variables.push_back(var);
}

void Automaton::set_name(string my_name){
 name = my_name;
}

string Automaton::get_name(){
 return name;
}

void Automaton::add_location(const Location& loc){
 locations.push_back(loc);
}

// Throws not existent location exception.
const Location& Automaton::get_location(int location_id) const{

 for(auto& loc : locations)
 {
   if(loc.get_id()==location_id)
    return loc;
 }

/* If doesn't find the location then throw the exception.
   It has to be handled from the caller. */

throw NotFoundLocationEx{};


}


// Throws not existent location exception.
const Location& Automaton::get_location(string location_name) const{
  for(auto& loc : locations) {
    if(loc.get_name()==location_name)
      return loc;
  }

/* If doesn't find the location then throw the exception.
   It has to be handled from the caller. */

  throw NotFoundLocationEx{};
}

  
//Throws not existent location exception. Private method to build the automaton.
Location& Automaton::get_location_ref(string location_name){
 for(auto& loc : locations)
 {
   if(loc.get_name()==location_name) return loc;
 }

/* If doesn't find the location then throw the exception.
   It has to be handled from the caller.*/

throw NotFoundLocationEx{};

}

// Throws not existent location exception.
Location& Automaton::get_location_ref(int location_id){
  for(auto& loc : locations)
  {
    if(loc.get_id()==location_id) return loc;
  }

/* If doesn't find the location then throw the exception.
   It has to be handled from the caller. */

throw NotFoundLocationEx{};


}

const vector<Variable>& Automaton::get_variables() const{
  return variables;
}

const vector<Location>& Automaton::get_locations() const{
  return locations;
}


void Automaton::set_init(const StateSet& initially){
  init_states = initially;
}

// Throws not existent location exception.
const StateSet& Automaton::get_invariant(){
  return invariant_cache;
}

int Automaton::get_dimension(){
  return Variable::get_dimension()/2;
}

const vector<Location>& Automaton::get_locations(){
 return locations;
}

Automaton::Automaton(const Automaton& to_copy): name{to_copy.name},
                                     locations{to_copy.locations},
                                     init_states{to_copy.init_states},
                                     variables{to_copy.variables},
                                     invariant_cache{to_copy.invariant_cache}{};


// Overloaded output operator.
ostream& operator<<(ostream& os, Automaton& aut){

  os<<"Automaton :"<<aut.name<<endl;
  os<<"Variables: "<<endl;
  for (auto var: aut.variables) os<<var;

  os<<"______________________________________________"<<endl;
  for(auto& loc: aut.locations) {
    os<< loc << endl;
    os<<"**********************************************"<<endl;
    os<<"Controllable outgoing transitions: "<<endl;
    os<<"______________________________________________"<<endl;
    for(auto trans: loc.get_out_contr_transition()) os<<trans<<endl;
    os<<"______________________________________________"<<endl;

    os<< "Uncontrollable outgoing transitions: "<<endl;
    os<<"______________________________________________"<<endl;
    for(auto trans: loc.get_out_uncontr_transition()) os<<trans<<endl;
    os<<"______________________________________________"<<endl;
    
  }

  return os;
}

}
