/*
 *  File: StateSet.h
 *  Created on: June 20, 2017
 *  Author: Marco Urbano
 */

#ifndef __NEW_STATESET_H__
#define __NEW_STATESET_H__

#include <iostream>
#include <unordered_map>
#include "preprocessor.h"
#include "Location.h"

// Per stampare i PS con le variabili originali e non PPL VAR.
#include "../io/output_operators.h"


using namespace std;

namespace NaPoly{

  class StateSet{
  private:
    unordered_map<Location, PS> states;

  public:
    // Makes an empty StateSet
    StateSet();

    // Copy constructor
    StateSet(const StateSet& to_copy);

    // Returns the region corresponding to location "loc"
    PS& get_region(const Location& loc);
    const PS& get_region(const Location& loc) const;

    // Inserisce la coppia <locazione, regione> all'interno della mappa.
    void add(Location loc, PS region);
    // Destructor.
    ~StateSet();

    // print procedure.
    void print();

    /* Intersects all the elements with the argument. (The argument must be the same type of states->second)
       the function throws an out_of_range exception. */
    void intersection_assign(const StateSet& y);

    // Returns the const reference to the unordered_map to iterate.
    const unordered_map<Location, PS>& get_map();

    // Output overload operator.
    friend ostream& operator<<(ostream& os, const StateSet& stateset);


  };

    // Declaration of the overloaded << operator with StateSet as input.
    ostream& operator<<(ostream& os, const StateSet& stateset);
}


#endif
