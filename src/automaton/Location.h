/*
 *  File: Location.h
 *  Created on: June 15, 2017
 *  Author: Marco Urbano
 */

#ifndef __NEW_LOCATION_H__
#define __NEW_LOCATION_H__

#include <iostream>
#include <string>
#include <vector>
#include "Transition.h"
#include "preprocessor.h"
#include "../io/output_operators.h"


using namespace std;



namespace NaPoly{

  // Forward declaration.
  class SafetyScenario;

  class Location{
    friend class SafetyScenario;

  private:
    unsigned id;
    string name;
    vector<Transition> outgoing_contr, outgoing_uncontr,
                       incoming_contr, incoming_uncontr;
    PS invariant;
    Poly flow;

  public:
    Location(string my_name, unsigned int my_id, PS my_invariant, Poly my_flow);
    //~Location();
    void set_id(int my_id);
    int get_id() const;
    string get_name() const;
    const PS& get_invariant() const;
    Poly get_flow() const;

    /* The third argument "verse" means "incoming" or "outgoing"
       with the rispective "false" and "true" values.    */
    void add_transition(Transition trans, bool contr, bool outgoing);

    /* Returns the reference to the entire vector.
       (not very expensive because it's just a Reference).
       This have been written because sometimes you don't want to use the
       plain old for but the enhanced for.  */
    const vector<Transition>& get_out_contr_transition();
    const vector<Transition>& get_out_uncontr_transition();
    const vector<Transition>& get_in_contr_transition();
    const vector<Transition>& get_in_uncontr_transition();


    // Returns the BEGIN iterator.
    //std::vector<Transition>::iterator get_in_contr_transition_iter();
    //std::vector<Transition>::iterator getInUncontrTransitionIter();
    //std::vector<Transition>::iterator getOutContrTransitionIter();
    //std::vector<Transition>::iterator getOutUncontrTransitionIter();

    // Overloaded << operator written as friend function.
    friend ostream& operator<<(ostream& os, Location& loc);

    /*     TO BE WRITTEN.
       iterator<Transition> getInTransitionIter(bool contr);
       iterator<Transition> getOutTransitionIter(bool contr);
   */
 };

 /* Declaration of the overloaded operator <<.
    (will be defined later into the same namespace). */
 ostream& operator<<(ostream& os, Location& loc);

}



namespace std {


 /* -- Metodi "hash" e "equal_to" per la classe Location --
    l'operatore operator() di hashing ritorna come hash code
    l'id di ogni locazione. Il confronto "equal_to" è basato
    alla stessa maniera sull'id.
 */

 template<>
 struct hash<NaPoly::Location> {
   size_t operator()(const NaPoly::Location& loc) const{
     return loc.get_id();
   }
 };

 template<>
 struct equal_to<NaPoly::Location> {
   bool operator()(const NaPoly::Location& loc1, const NaPoly::Location& loc2) const{
     return loc1.get_id()==loc2.get_id();
   }
 };

}


#endif
