/*
 *  File: Variable.cpp
 *  Created on: July 10, 2017
 *  Author: Marco Urbano
 */


#include "Variable.h"
#include <iostream>

using namespace std;


namespace NaPoly{


  int Variable::current_dim = 0;
  unordered_map<int, Variable> Variable::old_id_to_variable{};

Variable::Variable(const Variable& var): name{var.name}, ppl_var{var.ppl_var}, id{var.id}, _is_primed{var._is_primed}, dual_variable{var.dual_variable}{}

Variable::Variable(string my_name, bool primed_flag): ppl_var{Parma_var(current_dim)}, id{0}, _is_primed{primed_flag}, name{my_name}, dual_variable{nullptr}{
  //name=my_name;
  current_dim++;
}


void Variable::set_id(int my_id){
  id=my_id;
}

int Variable::get_id(){
  return id;
}

const Parma_var& Variable::get_PPL() const{
  return ppl_var;
}

string Variable::get_name() const {
  return name;
}

void Variable::set_dual(Variable* var){
  dual_variable = var;
}

Variable* Variable::get_dual() const {
  return dual_variable;
}

bool Variable::is_primed() const {
  return _is_primed;
}


// Member functions to access the old_id_to_variable unordered_map.
void Variable::add_to_map(int id, Variable& var){
  //std::pair<int, Variable&> var_pair{id, var};
  //std::cout<<"Adding <"<<id<<", "<<var.getName()<<">"<<std::endl;
  old_id_to_variable.insert({id, var});
}


const Variable& Variable::get_var_from_id(int id){
  return old_id_to_variable.at(id);
}

int Variable::get_dimension(){
  return current_dim;
}


const unordered_map<int, Variable>& Variable::get_map(){
  return old_id_to_variable;
}

void Variable::reset_var_map(){
  // Reset counter
  Variable::current_dim = 0;
  // Reset map
  Variable::old_id_to_variable = {};

 return;
}

// TO BE FINISHED.
ostream& operator<<(ostream& os, Variable var){
  using IO_Operators::operator<<;
  os<< "________________________" << endl;
  os<< "ID: "<< var.id <<"        |"<< endl;
  os<< "MY DIMENSION: " << var.ppl_var.id() << endl;
  os<< "NAME: "<< var.name <<"       |" << endl;
  os<< "POLY_VAR: "<< var.ppl_var << endl;
  os<<"IS_PRIMED: "<< var._is_primed << endl;
  if(var.get_dual()) os << "DUAL : " << var.get_dual()->get_name() << endl;

  // TO OUTPUT OTHER ATTRIBUTES HERE!

return os;
}

}
