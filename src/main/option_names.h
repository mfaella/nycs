#define VERBOSITY_DEBUG "debug"

// Scenarios
#define SCENARIO_SAFETY "safety control"
#define SCENARIO_REACHABILITY "reachability control"

// Outer operators for safety
#define OUTER_RWA "rwa"
#define OUTER_SOR "sor"
// Outer operators for reachability
#define OUTER_RWA_RWA "rwa via rwa"
#define OUTER_RWA_SOR "rwa via sor"
#define OUTER_SOR_RWA "sor via rwa"
#define OUTER_SOR_SOR "sor via sor"

// Inner operators
#define INNER_BASIC "basic"
#define INNER_MAPS  "maps"
#define INNER_SMOOTH "smooth"
#define INNER_SMOOTH_BASIC "smooth-basic"

#define INNER_GLOBAL "global"
#define INNER_LOCAL  "local"
#define INNER_LOCAL_RECYCLE  "local-recycle"

// Over-approximations
#define OVER_LARGE "large"
#define OVER_SMALL "small"
