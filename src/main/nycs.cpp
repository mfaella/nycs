//============================================================================
// Name          : nycs.cpp
// Author        : Marco Urbano
// Version       : 0.1
// Description   : NYCS main.
// Creation Date : June 22, 2017
// Last update   : December 06 2017
//============================================================================

#include <iostream>
#include <fstream>
#include <glpk.h>
#include <ppl.hh>
#include <ctime>

// Following 3 includes are needed for the logger.
#include "../utility/logger.h"
#include "../utility/logger_stopwatch.h"
#include "../utility/basic_warning.h"

#include "version_info.h"
#include "config_options.h"
#include "../global/global_types.h"

/* NYCS */
#include "../io/Parser.h"
#include "../scenario/OptionsBundle.h"
#include "../scenario/Scenario.h"
#include "../automaton/State_set.h"

/* Scenarios extras. */
#include "../algorithms/scenario_utility.h"
#include "../scenario/safety_scenario.h"
#include "../scenario/reach_scenario.h"

#include "option_names.h"

#include "signal_handlers.h"

using namespace std;
using namespace NaPoly;


int main(int argc, char* argv[]) {

  try {
    logger::set_stream(std::cerr);

    /* Controllo che gli argomenti siano 1 oppure 2. Se ho un argomento può
       essere -version o -help. Se ho 3 argomenti mi sono stati forniti sia
       il model file che il config file. */
    if (argc!=2 && argc!=3) {
      cerr << "Invalid arguments." << endl << endl
	   << NaPoly::get_help_message() << endl;
      return 1;
    }

    if(argc==2) { /* -help or -version */
      if ( std::string(argv[1]) == "-help" )
	cout << NaPoly::get_help_message() << endl;
      else if (strcmp(argv[1], "-version")==0) {
	cout << NaPoly::get_name()
	     << ", v" << NaPoly::get_version() << ", compiled on "
	     << NaPoly::get_compilation_time() << endl;
      } else {
	cerr << "Invalid arguments." << endl << endl
	     << NaPoly::get_help_message() << endl;
	return 1;
      }
      return 0;
    }

    LOGGER(DEBUG, "main", "Parsing automaton and config file");
    cout << " Parsing..." << endl;
    
    Parser Xml_parser(argv[1], argv[2]);
    Automaton aut = Xml_parser.get_automaton();
    OptionsBundle options = Xml_parser.get_options_bundle();

    // Apply verbosity, a.k.a. Logging level
    string verbosity = options.get_verbosity();
    if (verbosity == VERBOSITY_DEBUG) 
      logger::set_active_level(logger_level::DEBUG);
    else
      logger::set_active_level(logger_level::OFF);

    install_signal_handlers();
    
    // -------------------- AUTOMATA PRINT ------------------------ //
    // *logger::get_stream() << aut;
    // *logger::get_stream() << options;
    // -------------------- AUTOMATA PRINT ------------------------ //
    
    // ------------------------------------------------- //
    // EXECUTING THE SCENARIO.
    //------------------------------------------------
    string scenario = options.get_scenario();

    if ( scenario == SCENARIO_SAFETY ) {
      SafetyScenario safety(aut, options);
      safety.launch();
      
      StateSet controllable = safety.get_safe_region();
      cout << "[RESULT] Controllable region: " << endl << controllable;
      
      Automaton controlled = safety.get_controlled_automaton();
      
      // cout << "CONTROLLED : " << endl << controlled << endl;
      // cout << "NOT CONTROLLED : " << endl << aut << endl;
      
    } else if ( scenario == SCENARIO_REACHABILITY ) {
      ReachScenario reach(aut, options);
      reach.launch();
      
      StateSet controllable = reach.get_reached_states();
      cout <<  "[RESULT] Controllable region:" << endl << controllable;

    } else
      cerr << "The scenario that you have chosen does not exist." << endl;

    //basic_warning::output_statistics();
    LOGGER(DEBUG, "main", "Done.");
    
    logger_stopwatch::report_all(logger_level::DEBUG);
    logger::terminate();

    uninstall_signal_handlers();
    
  } catch (exception& e) {
    cerr << endl << "ERROR: "<< e.what() << "\n";
    return 1;
  }
  
  return 0;
}
