/*
 * version_info.cpp
 *      Author: M. Faella
 */

#include "version_info.h"

namespace NaPoly {

  std::string get_version() {
    return "0.90";
  }
  
  std::string get_compilation_time() {
    return std::string(__DATE__) + ", " + std::string(__TIME__);
  }
  
  std::string get_name() {
    return "Naples hYbrid Controller Synthesis";
  }
  
  
  std::string get_help_message() {
    return "This is " + get_name() + " " + get_version() + "\n\nUsage: nycs <model file> <config file>\nFor more information, check out http://wpage.unina.it/m.faella/nycs\n";
  }

}
