/*
 * version_info.h
 *
 *      Author: M. Faella
 */

#ifndef VERSION_INFO_H_
#define VERSION_INFO_H_

#include <string>

namespace NaPoly {

  std::string get_version();
  std::string get_compilation_time();
  std::string get_name();
  std::string get_help_message();

}

#endif /* VERSION_INFO_H_ */
