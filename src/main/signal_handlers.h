/*
 *  File: signal_handlers.h
 *  Created on: October 23, 2017
 *  Author: Marco Urbano
*/

#ifndef __MY_SIGNALS_H__
#define __MY_SIGNALS_H__

#include "../automaton/State_set.h"

void set_current_stateset(const NaPoly::StateSet *cur);
void install_signal_handlers(void);
void uninstall_signal_handlers(void);
  
#endif
