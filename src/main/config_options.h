/*
 * config_options.h
 *
 *  Created on: 27/apr/2014
 *      Author: donatella
 */

#ifndef CONFIG_OPTIONS_H_
#define CONFIG_OPTIONS_H_

#define SCENARIO_SAFETY_CONTROL "safety_control"
#define SCENARIO_REACH_CONTROL "reach_control"
#define SCENARIO_NON_CONVEX "nonconvex"
#define SCENARIO_NON_BLOCKING "nonblocking"

#endif /* CONFIG_OPTIONS_H_ */
