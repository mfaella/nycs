/*
 * base_options.cpp
 *
 *  Created on: Dec 30, 2009
 *      Author: frehse
 */

#include "base_options.h"

// Sottocartella creata nel nuovo repo con lo stesso nome.
#include "../global/global_types.h"
#include "../utility/logger.h"
#include "../utility/basic_exception.h"


/* Le inclusioni di questo commento non sembrano servire più dopo
   aver rimosso determinate opzioni.

	 #include "math/numeric/approx_comparator.h"
	 #include "math/scalar_types/scalar_with_infinity.h"
	 #include "math/ode_solving/ode_solver.h"
	 #include "math/scalar_types/rational.h"
	 #include "core/scenarios/reachability_scenario.h"
	 #include "core/scenarios/scenario_chooser.h"

*/




namespace options {

void add_base_options() {
	// Richiama il metodo add_options sull'attributo statico "config" che restituisce un oggetto "boost::program_options::options_description_easy_init"
	// e dopo richiama l'operatore () della classe "options_description_easy_init" che prende in input (nome_opzione, come_trattare_opzione, descrizione).
	// Se nel parametro nome_opzione non appare "," allora si specifica solo la versione lunga del nome dell'opzione. Se appare anche "," quello che segue è il nome abbreviato.

	options::options_processor::config.add_options()("verbosity,v",
			boost::program_options::value<std::string>(),
			"Set verbosity level (0,l,m,h,d,D1,D2,D3,D4,D5,D6,D7).");
  //std::cout<<"verbosity option added.\n";
  options::options_processor::config.add_options()("iter-max",
    	boost::program_options::value<std::string>(),
    	"Set the maximum number of iterations for the reachability algorithm.");
  //std::cout<<"iter-max option added.\n";
}

void set_verbosity(options::options_processor::variables_map& vmap) {
	std::string vlevel;
	if (options::options_processor::get_string_option(vmap,"verbosity",vlevel)) {
		if (vlevel.size() == 0)
			throw basic_exception("Unrecognized verbosity level \"" + vlevel + "\"");

		char c = vlevel[0];
		switch (c) {
		case 'l':
			logger::set_active_level(logger_level::LOW);
			break;
		case 'm':
			logger::set_active_level(logger_level::MEDIUM);
			break;
		case 'h':
			logger::set_active_level(logger_level::HIGH);
			break;
		case 'd':
			logger::set_active_level(logger_level::DEBUG);
			break;
		case 'D':
			if (vlevel == "D")
				logger::set_active_level(logger_level::DEBUG);
			if (vlevel == "D1")
				logger::set_active_level(logger_level::DEBUG1);
			else if (vlevel == "D2")
				logger::set_active_level(logger_level::DEBUG2);
			else if (vlevel == "D3")
				logger::set_active_level(logger_level::DEBUG3);
			else if (vlevel == "D4")
				logger::set_active_level(logger_level::DEBUG4);
			else if (vlevel == "D5")
				logger::set_active_level(logger_level::DEBUG5);
			else if (vlevel == "D6")
				logger::set_active_level(logger_level::DEBUG6);
			else if (vlevel == "D7")
				logger::set_active_level(logger_level::DEBUG7);
			break;
		case '0':
			logger::set_active_level(logger_level::OFF);
			break;
		default:
			throw basic_exception("Unrecognized verbosity level \"" + vlevel + "\"");
		}
	}
}

bool check_base_options(options::options_processor::variables_map& vmap) {
	return true;
}


bool apply_base_options(options::options_processor::variables_map& vmap) {
	return true;
}

}
