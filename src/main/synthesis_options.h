/*
 * synthesis_options.h
 *
 */

#ifndef SYNTHESIS_OPTIONS_H_
#define SYNTHESIS_OPTIONS_H_

#include <iostream>
#include <boost/algorithm/string/trim.hpp>
#include "application/options.h"
#include "io/common_output/output_formatter.h"
#include "core/analysis_algorithms/reachability_options.h"
#include "utility/stl_helper_functions.h"
#include "utility/logger_stopwatch.h"
#include "core/hybrid_automata/automaton_cache.h"
#include "core/hybrid_automata/hybrid_automaton.h"
#include "synthesis/hybrid_automata/hybrid_automaton_utility_powerset.h"
#include "synthesis/hybrid_automata/hybrid_automaton_powerset.h"
#include "core/symbolic_states/symbolic_state_collection.h"
#include "core/scenarios/reachability_scenario.h"
#include "core/scenarios/scenario_chooser.h"
#include "core/scenarios/parse_with_scenario.h"
#include "core/analysis_algorithms/reachability_algorithm.h"
#include "core/pwl/passed_and_waiting_list.h"

#include "application/options.h"
#include "io/common_output/output_options.h"

/** Forward declarations */

namespace hybrid_automata {
  
  class hybrid_automaton; // Dichiarazione dell'automa che è definito nel file hybrid_automaton.h
  typedef boost::shared_ptr<hybrid_automaton> hybrid_automaton_ptr; // Librerie boost da evitare ( shared_ptr è standard C++11) 
  class symbolic_state_collection; // Classe symbolic_state_collection utilizzata dalle funzioni membro di synthesis_options che è definita nel file core/symbolic_states/symbolic_state_collection.h
  typedef boost::shared_ptr<symbolic_state_collection> symbolic_state_collection_ptr; // Puntatore condiviso a symbolic_state_collection. UTILIZZARE SHARED POINTERS C++11.
  typedef boost::shared_ptr<const symbolic_state_collection> symbolic_state_collection_const_ptr; // Puntatore condiviso a symbolic_state_collection. UTILIZZARE SHARED POINTERS C++11.
}

using std::string;  // Dice al compilatore che quando si utilizza un oggetto "string" ci si riferisce a quello definito nello standard namespace. 

namespace options {

  const string AEDIFF="almost-everywhere-differentiable",
               SMOOTH="smooth",
               SMALL="small",
               LARGE="large",
               RWA="rwa",
               SOR="sor";

  /* (*) means default

     Scenarios: safe_control, reach_control

     Options:
        - semantics: almost-everywhere-differentiable*, smooth
   */


class synthesis_options {
public:
  synthesis_options(options::options_processor::variables_map& vmap, hybrid_automata::hybrid_automaton::ptr aut_net, hybrid_automata::symbolic_state_collection::ptr init_states){
    
    options::options_processor::get_string_option(vmap, "scenario", scenario);
    
    // The Defaults
    if (!options::options_processor::get_string_option(vmap,"over",over_approx)) 
      over_approx = SMALL;
    if (!options::options_processor::get_string_option(vmap,"semantics", semantics)) 
      semantics = AEDIFF;
    string basic_op_default, reach_op_default;
    if (semantics == AEDIFF) {
      reach_op_default = "rwa_must";
      basic_op_default = "sorm_local";
    } else {
      reach_op_default = "rwa_must";
      basic_op_default = "rwa_cd_map";
    }
    if (!options::options_processor::get_string_option(vmap,"basic-operator",operator_version)) 
      operator_version = basic_op_default;
    if (!options::options_processor::get_string_option(vmap,"reachability-operator",operator_choice))
      operator_choice = reach_op_default;
    if (!options::options_processor::get_string_option(vmap,"opt-version",opt_version))
      opt_version = "true";
    reach_states = get_states(vmap, aut_net, "reach", init_states);
    safe_states = get_states(vmap, aut_net, "safe", init_states);
  }
  
  std::string get_operator_version(){
    return operator_version;
  }
  
  std::string get_scenario(){
    return scenario;
  }

  std::string get_over_approx(){
    return over_approx;
  }

  std::string get_operator_choice(){
    return operator_choice;
  }

  std::string get_operator_computation(){
    return operator_computation;
  }

  std::string get_opt_version(){
    return opt_version;
  }

	hybrid_automata::symbolic_state_collection::ptr get_reach_states(){
		if (reach_states->is_empty())
			throw std::runtime_error("Reach states not defined!");
		return reach_states;
	}

	hybrid_automata::symbolic_state_collection::ptr get_safe_states(){
		if (safe_states->is_empty())
			throw std::runtime_error("Safe states not defined!");
		return safe_states;
	}

private:
	hybrid_automata::symbolic_state_collection::ptr get_states(options::options_processor::variables_map& vmap,
			hybrid_automata::hybrid_automaton::ptr aut_net, const std::string& opt_name,
			hybrid_automata::symbolic_state_collection::ptr init_states) {
		hybrid_automata::symbolic_state_collection::ptr states = hybrid_automata::symbolic_state_collection::ptr(
				init_states->create());
		std::string statestring;
		const hybrid_automata::reachability_scenario& scen = hybrid_automata::scenario_chooser::get_scenario();
		options::options_processor::get_string_option(vmap,opt_name,statestring);
		if (!options::options_processor::get_string_option(vmap,opt_name,statestring)) return states;
		else{
			parser::parse_policy ppol = parser::parse_policy::SX_policy();
			// accept unknown vars so that subcomponent variables can be specified too
			ppol.add_unknown_vars = true;
			try {
				states = hybrid_automata::parse_symbolic_state_collection_with_scenario(
						scen, statestring, aut_net->get_name(), ppol);
			} catch (std::exception& e) {
				throw basic_exception("Failed to define " + opt_name + " states.", e);
			}
			return states;
		}
	}

	std::string operator_version;
	std::string scenario;
	std::string over_approx;
	std::string operator_choice;
	std::string operator_computation;
	std::string opt_version;
	std::string semantics;
	hybrid_automata::symbolic_state_collection::ptr reach_states;
	hybrid_automata::symbolic_state_collection::ptr safe_states;

};


#endif /* SYNTHESIS_OPTIONS_H_ */
