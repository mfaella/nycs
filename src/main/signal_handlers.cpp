/*
 *  File: signal_handlers.h
 *  Created on: October 23, 2017
 *  Author: Marco Urbano
*/

#include <csignal>
#include "signal_handlers.h"

using namespace std;
using namespace NaPoly;

/* Global module variable to obtain the information about the
     states obtained during the scenario */

static StateSet *current_stateset;
static bool atLeastOneIterationDone = false;
static struct sigaction old_action[2];

void signal_handler(int signum);

void install_signal_handlers(void) {
    struct sigaction act;
    act.sa_handler = signal_handler;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;

    sigaction(SIGALRM, &act, &old_action[0]);
    sigaction(SIGINT,  &act, &old_action[1]);
}

// Restores old handlers
void uninstall_signal_handlers(void) {
  sigaction(SIGALRM, &old_action[0], NULL);
  sigaction(SIGINT,  &old_action[1], NULL);
}


void set_current_stateset(const StateSet *cur) {
  /* START UNINTERRUPTABLE ISTRUCTIONS MANAGEMENT:
     here we BLOCK the SIGALRM signal during this operations.
     When all these operations have been done,
     then we UNBLOCK SIGALRM. */
  /* Declaration of the sigsets to build the mask to BLOCK/UNBLOCK signals.*/
  sigset_t oldmask, newmask;
  // Build the empty set of signals where to block the SIGALRM.
  sigemptyset(&newmask);
  // Adds the SIGARLM to the set.
  sigaddset(&newmask, SIGALRM);
  
  /* Set SIGALRM as BLOCKED during these istructions.
     oldmask contains the old signals mask to be restored after
     the execution of uninterruptable istructions.
  */
  // if (sigprocmask(SIG_SETMASK, &newmask, &oldmask) < 0)
  //  throw exception();
  
  current_stateset = cur;
  atLeastOneIterationDone = true;
  
    // Set SIGALRM as UNBLOCKED from this istruction.
    // if (sigprocmask(SIG_SETMASK, &oldmask, NULL) < 0)
    //    throw exception();
}


static void signal_handler(int signum) {

  if (signum == SIGALRM)
    cout << "Time budget has expired." << endl;
  else
    cout << "Forced interruption by SIGINT." << endl;

  if(atLeastOneIterationDone){
     StateSet last = *current_stateset;

     cout << "Last symbolic states obtained from the execution of the scenario are :" << endl;
     cout << last << endl;
  } else
    cout << "There was not enough time to compute a partial solution." << endl;

   // cleanup and close up stuff here
   // terminate program

   /* quick_exit is the best choice because "exit()" can cause deadlocks
      when some resource is locked.
   */
   quick_exit(0);
}
