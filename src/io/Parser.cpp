/*
*  File: Parser.cpp
*  Created on: June 27, 2017
*  Author: Marco Urbano
*/


#include "Parser.h"


using namespace std;

namespace NaPoly{

// Currently unused
// string encode_lt_gt(string filename) {
//     ifstream istream;
//     ofstream ostream;
//     string line;
//     regex tags("((</)|(<nycs)|(<spaceex)|(<guard)|(<transition)|(<location)|(<contr)|(<flow)|(<param)|(<component))[^>]*>"),
//       lt("([^<])<([^<])"), gt("([^>])>([^>])"),
//       modified_open_tag("<<<"), modified_close_tag(">>>"),
//       amperstand("&");

//     int testNum = 0;
    
//     list<string> l;

//     istream.open(filename);
//     if (!istream.good()) return "";
//     string output_filename(tmpnam(nullptr));
//     ostream.open(output_filename);
    
//     while (getline(istream, line)) {
//       line = regex_replace(line, tags, "<<$&>>");
//       line = regex_replace(line, amperstand, "&amp;");
//       line = regex_replace(line, lt, "$1&lt;$2");
//       line = regex_replace(line, gt, "$1&gt;$2");
//       line = regex_replace(line, modified_open_tag, "<");
//       line = regex_replace(line, modified_close_tag, ">");
//       ostream << line << endl;
//     }

//     istream.close();
//     ostream.close();
//     return output_filename;
//   }
  

  
  Parser::Parser(string model_file, string config_file) {

    TiXmlDocument model, config;

    //First, load the xml file in a  TiXmlDocument
    model = model_file.c_str();
    config = config_file.c_str();

    parser::parse_policy parse_pol = parser::parse_policy::SX_policy();
    // symb_table contains all the variables data with the old version structure.
    // If you're looking for the new Variable(s) it's better look to Variable class.

    // If the static field "dimension" is not zero then reset the varmap.
    if (Variable::get_dimension()) Variable::reset_var_map();

    TiXmlBase::SetCondenseWhiteSpace(true);

    //LoadFile carica tutto il file xml all'interno di un oggetto TiXmlDocument
    bool load = model.LoadFile();

    if (!load) {
      std::stringstream ss;
      ss << "Error loading model XML file \"" << model_file << "\""<< std::endl;
      ss << "TinyXML error #" << model.ErrorId() << " : "
	 << model.ErrorDesc();
      throw basic_exception(ss.str());
    }

    load = config.LoadFile();

    if (!load) {
      std::stringstream ss;
      ss << "Error loading config XML file \"" << config_file << "\""<< std::endl;
      ss << "TinyXML error #" << config.ErrorId() << " : "
	 << config.ErrorDesc();
      throw basic_exception(ss.str());
    }

    /* Now Construct the automaton and return the symbol_table
       to be used in the Options_bundle construction.   */
    parser::symbol_table symb_table = build_automaton(model, parse_pol);
    build_options_bundle(config, symb_table, parse_pol);
  }


  parser::symbol_table Parser::build_automaton(TiXmlDocument& model,
					       parser::parse_policy& ppol){

    // This boolean will be true after the creation of the primed variables.
    bool primed_build = false;

    TiXmlElement* root = model.RootElement();

    //Next, verify that the root of the xml file is sspaceex or nycs.
    if (strcmp(root->Value(), "sspaceex") != 0 &&
	strcmp(root->Value(), "nycs") != 0)
      throw basic_exception("Error in XML file: XML root incorrect, "
			    "it should be nycs or spaceex (all lowercase).");

    TiXmlElement* DocChild = root->FirstChildElement();

    /* Verifica che si inizi con <component id="qualcosa">.
       Attribute("nomeattributo") restituisce null se non è stato dichiarato
       attributo con nomeattributo, altrimenti restituisce il valore come char*. */
    if (strcmp(DocChild->Value(), "component") != 0 || !DocChild->Attribute("id"))
      throw basic_exception("Incorrect syntax in file: component "
			    "should contain automata name.");


    // Put component name with id="name" into component_name.
    std::string component_name = std::string(DocChild->Attribute("id"));
    /* Inizializza l'oggetto symbol_table che conterrà
       le variabili dell'automa con ID="component_name".  */
    parser::symbol_table s_table(component_name);

    TiXmlElement* Temp=DocChild;

    //Inizio qui la lettura delle informazioni dall'oggetto TinyXML.
    try {
      aut.set_name(component_name);
      
      //Read the template element, and for each child element:
      for (TiXmlElement* TempChild = Temp->FirstChildElement();
	   TempChild; TempChild = TempChild->NextSiblingElement())
	{
	  /* If it's a param, instantiate the symbol table of
	     the component and add variable to automaton  */
	  if (strcmp(TempChild->Value(), "param") == 0)
	    {
	      parser::symbol unv_symbol =
		get_unvalued_symbol_synthesis(TempChild,s_table);
	      parser::symbol res_symbol =
		instantiate_symbol(unv_symbol,s_table,false);
	      
	      /* Inserisco qui la Variabile nell'automa originale e
		 nella symbol table solo se non è una Label. */
	      if(res_symbol.my_symbol_type != parser::symbol::LABEL)
		{
		  /* The last argument is a boolean that indicates if
		     the variable is a primed one, this case every variable
		     parsered will be unprimed */
		  Variable var= build_variable(res_symbol.my_name, false);
		  
		  /* add_symbol adds the variable to the symbol_table and sets
		     the variable id to the Variable object. */
		  add_symbol(var, res_symbol);
		  aut.add_variable(var);
		}
	      
	    } else {
	    // Creates the primed variables.
	    if(!primed_build)
	      {
		int numVar = NaPoly::Variable::get_dimension();
		vector<Variable>& var_vec = aut.variables;
		
		for (int i=0; i<numVar; i++) {
		  Variable& var = var_vec[i];
		  Variable prim_var = build_variable(var.get_name()+"'", true);
		  
		  // Set the dual variables to the primed and unprimed ones.
		  parser::symbol res_symbol = parser::symbol(prim_var.get_name(),
							     parser::symbol::VARIABLE,
							     parser::symbol::REAL,
							     parser::symbol::ANY, 1, 0, math::matrix<boost::any>(),
							     false, true);
		  
		  add_symbol(prim_var, res_symbol);
		  aut.add_variable(prim_var);
		}
		
		for(int i=0; i<numVar; i++) {
		  var_vec[i].set_dual(&var_vec[i+numVar]);
		  var_vec[i+numVar].set_dual(&var_vec[i]);
		  
		  Variable::add_to_map(var_vec[i+numVar].get_id(), var_vec[i+numVar]);
		  Variable::add_to_map(var_vec[i].get_id(), var_vec[i]);
		}
		
		
		
		/* After the primed variables have been built,
		   primed_build is set to true to not repeat the operation. */
		primed_build = true;
	      }
	    
	    /* lock the symbol table because it's supposed to not find any variables
	       anymore.    */
	    s_table.set_locked();
	    
	    // If it's a location, do:
	    if (strcmp(TempChild->Value(), "location") == 0) {
	      std::string loc_id(TempChild->Attribute("id"));
	      std::string loc_name(TempChild->Attribute("name"));
	      std::string invariant("");
	      std::string flow("");
	      
	      for (TiXmlElement* CurrentElement =
		     TempChild->FirstChildElement(); CurrentElement; CurrentElement
		     = CurrentElement->NextSiblingElement())
		{
		  //Next, extract the invariant and flow, where they exist.
		  if (strcmp(CurrentElement->Value(), "invariant") == 0 &&
		      CurrentElement->FirstChild())
		    {
		      invariant = CurrentElement->FirstChild()->Value();
		    }
		  
		  if (strcmp(CurrentElement->Value(), "flow") == 0 &&
		      CurrentElement->FirstChild())
		    {
		      flow = CurrentElement->FirstChild()->Value();
		    }
		}
	      
	      //In the end, create the location and add it to the automaton
	      Location loc=build_location(stoi(loc_id), loc_name,
					  flow, invariant, s_table, ppol);
	      aut.add_location(loc);
	      
	      // This adds all the pair <Location, PS> to the invariant_cache.
	      aut.invariant_cache.add(loc, loc.get_invariant());
	    }
	    	    
	    //If it's a transition:
	    else if (strcmp(TempChild->Value(), "transition") == 0)
	      {
		
		/* Copy the names of the source location and of thec target location,
		   by using the map id_to_name  */
		
		std::string source_id(TempChild->Attribute("source"));
		std::string target_id(TempChild->Attribute("target"));
		std::string label("");
		std::string guard(""), contr(""), assignment("");
		
		//Extract the label, the guard and the assignment, where they exist
		for (TiXmlElement* CurrentElement =
		       TempChild->FirstChildElement(); CurrentElement; CurrentElement
		       = CurrentElement->NextSiblingElement())
		  {
		    if (strcmp(CurrentElement->Value(), "label") == 0 &&
			CurrentElement->FirstChild())
		      label = CurrentElement->FirstChild()->Value();
		    
		    else if (strcmp(CurrentElement->Value(), "assignment") == 0 &&
			     CurrentElement->FirstChild())
		      assignment = CurrentElement->FirstChild()->Value();
		    
		    else if (strcmp(CurrentElement->Value(), "guard") == 0 &&
			     CurrentElement->FirstChild())
		      guard = CurrentElement->FirstChild()->Value();
		    
		    else if (strcmp(CurrentElement->Value(), "contr") == 0 &&
			     CurrentElement->FirstChild())
		      contr = CurrentElement->FirstChild()->Value();
		  }
		
		/* Here we're creating the transition using buildTransition()
		   and passing it a pointer to the Automaton. */
		build_transition(guard, assignment, stoi(source_id), stoi(target_id),
				 contr, s_table, ppol);
		
	      }
	    
	  }
	  
	  
	}
      
      // According to the policy, add symbol table to the cache.
      if (ppol.use_symbol_table_cache)
	parser::symbol_table_cache::get_symbol_table(component_name)=s_table;
      
      // Return the symbol_table
      return s_table;
    } catch (std::exception& e) {
      throw basic_exception("Could not parse First XML file named:  "
			    + component_name + ".", e);
    }
  }
  
  Automaton& Parser::get_automaton(){
    return aut;
  }


  //Create a symbol or value from a TiXmlElement* param.
  //The symbol table is used to look up the dimensions, which can
  //be symbols.
  //The value of the symbol is yet undetermined.

  using namespace parser;

  symbol Parser::get_unvalued_symbol_synthesis(TiXmlElement* param,
					       const symbol_table& s_table) {

    symbol res;
    bool local, is_label;
    std::string symbol_name(param->Attribute("name"));

    if(symbol_name.compare("") == 0)
      throw basic_exception("Empty param name found in XML model file! \n"
			    " the program will terminate now. \n", std::exception{});

    if(param->Attribute("local"))
      local = !(strcmp(param->Attribute("local"), "false")==0);
    else
      local = true;

    if(param->Attribute("type"))
      is_label =(strcmp(param->Attribute("type"), "label")==0);
    else
      is_label = false;

    symbol::symbol_type s_type = symbol::LABEL;
    symbol::dynamics_type dyn_type = symbol::ANY;
    symbol::data_type dat_type = symbol::REAL;
    bool controlled = true;
    unsigned int dim1 = 0;
    unsigned int dim2 = 0;

    if (!is_label)
      {

	/* Determinate dynamics_type, data_type and
	   dimensions of parameter, if it is controlled. */

	// If it's not a label, then it can only be a variable.
	// (constants are defined in map tags, not in param tags)
	s_type = symbol::VARIABLE;

	dyn_type = symbol::ANY;

	if(param->Attribute("dynamics")){
	  if (strcmp(param->Attribute("dynamics"), "const") == 0)
	    dyn_type = symbol::CONSTANT;
	  if (strcmp(param->Attribute("dynamics"), "explicit") == 0)
	    dyn_type = symbol::EXPLICIT;
	}

	if(param->Attribute("type")){
	  if (strcmp(param->Attribute("type"), "int") == 0)
	    dat_type = symbol::INT;
	}

	if(param->Attribute("d1"))
	  dim1 = instantiate_dimension(param->Attribute("d1"), s_table);
	else
	  dim1 = instantiate_dimension("1", s_table);

	if(param->Attribute("d2"))
	  dim2 = instantiate_dimension(param->Attribute("d2"), s_table);
	else
	  dim2 = instantiate_dimension("1", s_table);


	if (param->Attribute("controlled") && strcmp(param->Attribute(
								      "controlled"), "false") == 0)
	  controlled = false;
      }

    res = symbol(symbol_name, s_type, dat_type, dyn_type, dim1, dim2,
		 math::matrix<boost::any>(), local, controlled);

    return res;
  }



  void Parser::add_variable(Variable& var, std::string name, bool is_input,
			    bool is_const, unsigned int dim) {

    variable_id id;
    /**
     * If variable is a vector, add all element in automaton
     */
    if (dim <= 1)
      {
	/** Instantiate scalars as vectors of dimension 1 */
	id = variable::get_or_add_variable_id(name, 1);
	/** Aggiungo qui l'id alla variabile del nuovo automa  */
	var.set_id((int)id);
      }
    else
      {
	/** Instantiate the vector */
	id = variable::get_or_add_variable_id(name, dim);
	for (int i = 1; i <= dim; ++i)
	  {
	    id = variable::get_or_add_variable_id(name + "(" + int2string(i) + ")");
	    /** Aggiungo qui l'id alla variabile del nuovo automa  */
	    var.set_id((int)id);
	  }
      }
  }

  void Parser::add_symbol(Variable& var, const symbol& symb) {

    if (symb.my_symbol_type != symbol::CONST_VALUE)
      {
	if(symb.my_symbol_type != symbol::LABEL)
	  {
	    bool is_constant = (symb.my_dynamics_type == symbol::CONSTANT);
	    std::string name = symb.my_name;
	    bool controlled = symb.is_controlled;
	    unsigned int dim1 = symb.dim1;
	    unsigned int dim2 = symb.dim2;
	    unsigned int var_dim = 0;

	    if (dim1 > 1 || dim2 > 1) var_dim = dim1 * dim2;

	    add_variable(var, name, !controlled, is_constant, var_dim);
	  }
      }
  }

  static Variable Parser::build_variable(string name, bool is_primed){

    Variable var(name, is_primed);

    return var;
  }

  Location Parser::build_location(int id, string name, string flow,
				  string invariant,
				  const symbol_table& symbol_table,
				  const parse_policy& ppol){

    tree::node::ptr inv;
    tree::node::ptr flo;



    try
      {
	inv = predicate_parser::parse_predicate(invariant, symbol_table, ppol);
      }
    catch (std::exception& e)
      {
	throw basic_exception("Failed to parse invariant of location " +
			      name + ".", e);
      }

    try
      {
	flo = predicate_parser::parse_predicate(flow, symbol_table, ppol);
      }
    catch (std::exception& e)
      {
	throw basic_exception("Failed to parse flow of location " + name
			      + ".", e);
      }

    try
      {
	// check that invariant don't contain constraints on primed variables
	variable_id_set invariant_mod_vars =
	  valuation_functions::get_primed_variable_ids(inv, 1);

	if (!invariant_mod_vars.empty())
	  {
	    std::stringstream s;
	    logger::copyfmt_to(s);
	    print_variable_id_set(s,invariant_mod_vars);

	    throw basic_exception("The derivatives of variable(s) "+s.str()+
				  " are being assigned a value inside an invariant."
				  + " They should be in the flow.");
	  }

	// check that flow doesn't contain constraints without primed variables
	std::pair<tree::node_ptr, tree::node_ptr> flow_parts=divide_tree(flo);
	if (!valuation_functions::get_variable_ids(flow_parts.first).empty())
	  {
	    std::stringstream s;
	    logger::copyfmt_to(s);
	    print_as_predicate(s,flow_parts.first);
	    throw basic_exception("The following constraints should be put "
				  "inside an invariant instead of a flow: \n"
				  +s.str());
	  }



	/* Here I have the direct conversion from tree::node to Poly or PS.
	   The function ppl_polyhedron::convert_to_POLY takes as third argument a
	   boolean to know if is a flow or not. This is needed because flows contains
	   constraints on primed variables that can be not interpreted as we want.
	   (e.g. if we have x'==1 in flow, it does not mean
	   x-primed == 1, but simply x==1).                                    */
	Poly ppl_flow=ppl_polyhedron::convert_to_POLY(flo,
						      valuation_functions::get_variable_ids(flo), true);

	/* Tutti i poliedri vengono costruiti contando sia variabili
	   primate che non primate. Solo la condizione di Jump viene mantenuta
	   alla dimensione originale. L'istruzione seguente ridimensiona
	   correttamente tutti i PS che non siano Jump.  */
	ppl_flow.remove_higher_space_dimensions(ppl_flow.space_dimension()/2);


	PS ppl_invariant=ppl_polyhedron::convert_to_PS(inv);
	/* Tutti i poliedri vengono costruiti contando sia variabili
	   primate che non primate. Solo la condizione di Jump viene mantenuta alla
	   dimensione originale. L'istruzione seguente ridimensiona
	   correttamente tutti i PS che non siano Jump. */
	ppl_invariant.remove_higher_space_dimensions(
						     ppl_invariant.space_dimension()/2);


	Location loc(name, id, ppl_invariant, ppl_flow);

	return loc;

      }
    catch (std::exception& e)
      {
	throw basic_exception("Failed to create location " + name + ".", e);
      }

  }



  void Parser::build_transition(string guard, string assignment,
				int source_id, int target_id, string contr,
				const parser::symbol_table& symbol_table,
				const parser::parse_policy& ppol){

    tree::node::ptr guard_tree;

    try
      {
	guard_tree = predicate_parser::parse_predicate(guard, symbol_table, ppol);
      }
    catch (std::exception& e)
      {
	throw basic_exception("Failed to parse transition guard.", e);
      }

    tree::node::ptr assignment_tree;

    try
      {
	assignment_tree =
	  predicate_parser::parse_predicate(assignment,symbol_table, ppol);
      }
    catch (std::exception& e)
      {
	throw basic_exception("Failed to parse transition assignment.", e);
      }

    try
      {
	// check that guards don't contain constraints on primed variables
	variable_id_set guard_mod_vars =
	  valuation_functions::get_primed_variable_ids(guard_tree, 1);

	if (!guard_mod_vars.empty())
	  {
	    std::stringstream s;
	    logger::copyfmt_to(s);
	    print_variable_id_set(s,guard_mod_vars);
	    throw basic_exception("The variables "+s.str()+" are being assigned a" +
				  "value inside a guard. Primed variables can only occur in the assignment field.");
	  }


	// check that assignments don't contain constraints without primed variables
	std::pair<tree::node_ptr, tree::node_ptr> assignment_parts=
	  divide_tree(assignment_tree);

	if (!valuation_functions::get_variable_ids(assignment_parts.first).empty())
	  {
	    std::stringstream s;
	    logger::copyfmt_to(s);
	    print_as_predicate(s,assignment_parts.first);
	    throw basic_exception("The following constraints should be put "
				  "inside a guard instead of an assignment:\n"+s.str());
	  }

	PS poly_guard = ppl_polyhedron::convert_to_PS(guard_tree);
	Poly poly_assignment = ppl_polyhedron::convert_to_POLY(assignment_tree,
							       valuation_functions::get_variable_ids(assignment_tree), false);

	/* Controlla se ci sono tutti i constraints sulle variabili primate.
	   Se non ci sono metti x'=x.                          */

	for (auto& pair: Variable::get_map()) // for all variables
	  {
	    const Variable& var = pair.second;
	    if (var.is_primed())
	      {
		regex to_find(var.get_name());

		if(!(regex_search(assignment, to_find)))
		  poly_assignment.add_constraint(var.get_PPL() == var.get_dual()->get_PPL());
	      }

	  }


	/* Aggiungo il Poly(edro) convesso poly_assignment al
	   poliedro non necessariamente convesso poly_guard */

	// INIZIO DELLE OPERAZIONI SUL PS.
	ppl_polyhedron::expand_to_primed(poly_guard);

	PS::iterator it;

	for (it=poly_guard.begin(); it!=poly_guard.end(); it++)
	  it->pointset().intersection_assign(poly_assignment);

	// FINE DELLE OPERAZIONI SUL PS.

	/* Uses Automaton class private methods to get a reference
	   instead obtain a const reference.                     */
	Location& source_loc=aut.get_location_ref(source_id);
	Location& target_loc=aut.get_location_ref(target_id);

	// -- INTERSEZIONE DI JUMP CON INVARIANT DELLA LOC SORGENTE -- //

	/* Dopo aver creato il Poly per il Jump (sfrutto sempre poly_guard),
	   lo interseco l'invariant della locazione di partenza.
	   Prima di intersecarlo però, visto che la dimensione di invariant è
	   la metà di quella del jump, aggiungo le dimensioni che mancano.  */

	PS temp_invariant(source_loc.get_invariant());

	// Aggiunge le dimensioni mancanti.
	ppl_polyhedron::expand_to_primed(temp_invariant);

	poly_guard.intersection_assign(temp_invariant);

	// -- INTERSEZIONE DI JUMP CON INVARIANT DELLA LOC SORGENTE -- //
	// -- INTERSEZIONE DI JUMP CON INVARIANT DELLA LOC TARGET   -- //
	PS target_invariant(target_loc.get_invariant());
	ppl_polyhedron::convert_to_primed(target_invariant);
	poly_guard.intersection_assign(target_invariant);

	// -- INTERSEZIONE DI JUMP CON INVARIANT DELLA LOC TARGET -- //


	// Controllo se la stringa contr è true o false e setto la flag.
	bool contr_flag;
	if(contr.compare("true")==0) contr_flag=true;
	else contr_flag=false;


	/* The boolean flag "contr" has to be defined in XML.
	   poly_guard is now the jump because has been built
	   from the union of assigment plus guard.         */
	Transition built_transition(source_loc, target_loc, true, poly_guard);
	/* The addTransition() member function of the class Location uses the last
	   argument to know if is incoming or outgoing. Adds the transition to
	   the suorce_loc as outgoing_contr or outgoing_uncontr. */
	source_loc.add_transition(built_transition, contr_flag, true);
	//Add to the target_loc as incoming_contr or incoming_uncontr.
	target_loc.add_transition(built_transition, contr_flag, false);

	return;
      }
    catch (std::exception& e)
      {
	std::stringstream s;
	logger::copyfmt_to(s);
	throw basic_exception("Failed create transition.", e);
      }
  }



  //OptionsBundle
  void Parser::build_options_bundle(TiXmlDocument& config,
				    parser::symbol_table& symb_table,
				    parser::parse_policy& parse_pol){

    std::string scenario, rwa_or_sor, operator_version, over;
    std::string init_loc, init_region, verbosity, goal;
    std::string goal_loc, goal_region;
    StateSet initially, goal_set;
    std::string iter_max("");
    string timer("");
    TiXmlElement* root = config.RootElement();

    //Next, verify that the root of the xml file is sspaceex
    if (strcmp(root->Value(), "sspaceex") != 0 && strcmp(root->Value(), "nycs") != 0)
      throw basic_exception("Error in XML file: XML root incorrect");

    TiXmlElement* DocChild = root->FirstChildElement();

    /* Verifica che si inizi con <component id="qualcosa">.
       Attribute("nomeattributo") restituisce null se non è stato dichiarato
       attributo con nomeattributo, altrimenti restituisce il valore come char* */
    if (strcmp(DocChild->Value(), "component") != 0 || !DocChild->Attribute("id"))
      throw basic_exception("Incorrect syntax in file");

    // Salva il nome del componente con id="nomecomponente" nella stringa component_name.
    std::string component_name = std::string(DocChild->Attribute("id"));

    TiXmlElement* Temp = DocChild;

    // Inizia a leggere il documento XML parsered with TiXmlDocument.
    for (TiXmlElement* TempChild = Temp->FirstChildElement(); TempChild; TempChild
	   = TempChild->NextSiblingElement())
      {
	// <scenario name="" /> TAG
	if (strcmp(TempChild->Value(), "scenario") == 0)
	  scenario = std::string(TempChild->Attribute("name"));

	// inner-operator TAG
	if (strcmp(TempChild->Value(), "inner-operator") == 0)
	  operator_version = std::string(TempChild->Attribute("name"));

	if (strcmp(TempChild->Value(), "max") == 0)
	  iter_max = std::string(TempChild->Attribute("iterations"));

	if (strcmp(TempChild->Value(), "timeout") == 0)
	  timer = std::string(TempChild->Attribute("seconds"));

	// <verbosity level=""/> TAG
	if (strcmp(TempChild->Value(), "verbosity") == 0)
	  verbosity = std::string(TempChild->Attribute("level"));

	// <over></over> TAG
	if (strcmp(TempChild->Value(), "over") == 0)
	  over = std::string(TempChild->Attribute("level"));

	// operator TAG
	if (strcmp(TempChild->Value(), "outer-operator") == 0)
	  {
	    rwa_or_sor = std::string(TempChild->Attribute("name"));

	    /*
	      if(!(rwa_or_sor.compare("RWA") == 0)
	      && !(rwa_or_sor.compare("SOR") == 0)
	      && !(rwa_or_sor.compare("sor") == 0)
	      && !(rwa_or_sor.compare("rwa") == 0))
	      throw basic_exception("Error! There are errors in the 'operator' TAG! "
	      "This must be 'RWA' or 'SOR', all uppercase or lowercase!");
	    */
	  }

	// <initially></initially> TAG
	if (strcmp(TempChild->Value(), "initially") == 0)
	  {
	    for (TiXmlElement* CurrentElement = TempChild->FirstChildElement();
		 CurrentElement; CurrentElement = CurrentElement->NextSiblingElement())
	      {
		/* Verify if there's any location. if statement needed to avoid
		   segmentation fault caused by nullptr returned from function call*/
		if(CurrentElement->Attribute("loc"))
		  init_loc = CurrentElement->Attribute("loc");
		else init_loc = "";

		// Get the region string.
		init_region = CurrentElement->FirstChild()->Value();

		// Get the region PS.
		tree::node::ptr region;

		try
		  {
		    region = predicate_parser::parse_predicate(init_region,
							       symb_table, parse_pol);
		  }
		catch (std::exception& e)
		  {
		    throw basic_exception("Failed to parse region of the state "
					  "set with location "+ init_loc + ".", e);
		  }

		/* This is used if the pair <loc,region> doesn't
		   exists in the state set */
		PS poly_region = ppl_polyhedron::convert_to_PS(region);
		/* Tutti i poliedri vengono costruiti contando sia
		   variabili primate che non primate. Solo la condizione di Jump
		   viene mantenuta alla dimensione originale. L'istruzione seguente
		   ridimensiona correttamente tutti i PS che non siano Jump. */
		poly_region.remove_higher_space_dimensions(
							   poly_region.space_dimension()/2);
		/* This is used if the pair <loc,region> already exists in the state
		   set and a "add_disjunct" operation will be performed on
		   the PS using the Poly(s).  */
		vector<Poly> region_vector= ppl_polyhedron::getPolyArray(region);

		/* Here there is the analysis of the string to
		   get the locations ( if more than one or zero). */
		if(init_loc.empty())
		  {
		    vector<Location> locations = aut.get_locations();
		    for(auto loc : locations)
		      {
			try
			  {
			    PS& Region = initially.get_region(loc);

			    /* Here all the Poly in the region vector
			       will be assigned to the existent PS.  */

			    for(Poly tmp : region_vector)
			      {
				dimension_type dim=tmp.space_dimension();
				if(Region.space_dimension()<dim)
				  Region.add_space_dimensions_and_embed(
									dim-Region.space_dimension());
				Region.add_disjunct(tmp);
			      }
			  }
			catch(std::out_of_range& out_excpt)
			  {
			    initially.add(loc, poly_region);
			  }
		      }
		  }
		// If user specified any location. (one or more than one)
		else
		  {
		    /* Build a vector with the names of the locations.
		       (because the string is built with commas).  */
		    stringstream ss(init_loc);
		    vector<string> loc_vect;

		    while( ss.good() )
		      {
			string substr;
			getline( ss, substr, ',' );
			loc_vect.push_back( substr );
		      }

		    try
		      {
			for(string loc_name : loc_vect)
			  {
			    Location loc = aut.get_location(loc_name);

			    try
			      {
				PS& Region = initially.get_region(loc);
				// If exists concatenate the new polyhedron to the existent.

				/* Here all the Poly in the region vector will
				   be assigned to the existent PS. */
				for(Poly tmp : region_vector)
				  {
				    dimension_type dim=tmp.space_dimension();
				    if(Region.space_dimension()<dim)
				      Region.add_space_dimensions_and_embed(
									    dim-Region.space_dimension());
				    Region.add_disjunct(tmp);
				  }


			      }
			    catch(std::out_of_range& out_excpt)
			      {
				initially.add(loc, poly_region);
			      }
			  }
		      }
		    catch(NaPoly::Automaton::NotFoundLocationEx& notfoundloc)
		      {
			throw basic_exception("Error, not existent Location in"
					      "XML config file. Wrong location name"
					      "in 'Initially' TAG: "
					      + init_loc, notfoundloc);
		      }
		  }
	      }
	  }

	// <safe></safe> or <reach></reach> TAGS.
	if ((strcmp(TempChild->Value(), "safe") == 0)
	    || (strcmp(TempChild->Value(), "reach") == 0))
	  {
	    for (TiXmlElement* CurrentElement = TempChild->FirstChildElement();
		 CurrentElement; CurrentElement = CurrentElement->NextSiblingElement())
	      {
		/* Verify if there's any location. if statement needed to avoid
		   segmentation fault caused by nullptr returned from
		   function call. */
		if(CurrentElement->Attribute("loc"))
		  goal_loc = CurrentElement->Attribute("loc");
		else goal_loc = "";

		// Get the region string.
		goal_region = CurrentElement->FirstChild()->Value();
		// Get the region PS.
		tree::node::ptr region;

		try
		  {
		    region = predicate_parser::parse_predicate(goal_region,
							       symb_table, parse_pol);
		  }
		catch (std::exception& e)
		  {
		    throw basic_exception("Failed to parse region of"
					  "the state set with location "+ goal_loc + ".", e);
		  }

		/* This is used if the pair <loc,region> doesn't
		   exists in the state set. */
		PS poly_region = ppl_polyhedron::convert_to_PS(region);
		/* Tutti i poliedri vengono costruiti contando sia variabili
		   primate che non primate. Solo la condizione di Jump viene
		   mantenuta alla dimensione originale. L'istruzione seguente
		   ridimensiona correttamente tutti i PS che non siano Jump. */
		poly_region.remove_higher_space_dimensions(
							   poly_region.space_dimension()/2);

		/* This is used if the pair <loc,region> already exists in the
		   state set and a "add_disjunct" operation will be performed
		   on the PS using the Poly(s) */
		vector<Poly> region_vector= ppl_polyhedron::getPolyArray(region);

		/* Here there is the analysis of the string to get
		   the locations ( if more than one or zero). */
		if(goal_loc.empty())
		  {
		    vector<Location> locations = aut.get_locations();
		    for(auto loc : locations)
		      {
			try
			  {
			    PS& Region = goal_set.get_region(loc);

			    /* Here all the Poly in the region vector will
			       be assigned to the existent PS. */
			    for(Poly tmp : region_vector)
			      {
				dimension_type dim=tmp.space_dimension();

				if(Region.space_dimension()<dim)
				  Region.add_space_dimensions_and_embed(
									dim-Region.space_dimension());
				Region.add_disjunct(tmp);
			      }
			  }
			catch(std::out_of_range& out_excpt)
			  {
			    goal_set.add(loc, poly_region);
			  }

		      }
		  }
		else // If user specified any location. (one or more than one)
		  {
		    /* Build a vector with the names of the locations.
		       (because the string is built with commas). */
		    stringstream ss(goal_loc);
		    vector<string> loc_vect;

		    while( ss.good() )
		      {
			string substr;
			getline( ss, substr, ',' );
			loc_vect.push_back( substr );
		      }

		    try
		      {
			for(string loc_name : loc_vect)
			  {
			    Location loc = aut.get_location(loc_name);

			    try
			      {
				PS& Region = goal_set.get_region(loc);

				/* If exists concatenate the
				   new polyhedron to the existent. */

				/* Here all the Poly in the region vector will
				   be assigned to the existent PS. */
				for(Poly tmp : region_vector)
				  {
				    dimension_type dim=tmp.space_dimension();
				    if(Region.space_dimension()<dim)
				      Region.add_space_dimensions_and_embed(
									    dim-Region.space_dimension());
				    Region.add_disjunct(tmp);
				  }
			      }
			    catch(std::out_of_range& out_excpt)
			      {
				goal_set.add(loc, poly_region);
			      }
			  }
		      }
		    catch(NaPoly::Automaton::NotFoundLocationEx& notfoundloc)
		      {
			throw basic_exception("Error, not existent Location in XML"
					      "config file. Wrong location name in"
					      "'Initially' TAG: "
					      + goal_loc, notfoundloc);
		      }
		  }
	      }
	  }
      }
    // Set the OptionsBundle class attributes directly. (FRIEND CLASSES)
    options.aut_name = component_name;
    options.scenario = scenario;
    options.outer_operator = rwa_or_sor;
    options.inner_operator = operator_version;

    // Checks if the user inserted iter_max or left it as a blank string.
    if (iter_max.compare("") == 0) options.iter_max = 0;
    else options.iter_max = stoi(iter_max, nullptr);

    if (timer.compare("") == 0) options.timer = 0;
    else options.timer = stoi(timer, nullptr);

    options.verbosity = verbosity;
    options.initially = initially;
    options.goal = goal_set;
    options.over = over;
 
    // In the end adds the Initial states to the Automaton member object "aut".
    aut.set_init(initially);
  }

  OptionsBundle& Parser::get_options_bundle(){
    return options;
  }

}
