/*
 *  File: output_operators.cpp
 *  Created on: Semptember 22, 2017
 *  Author: Marco Urbano
 */


#include "output_operators.h"

#include <regex>

using namespace std;
using namespace NaPoly;



void NaPoly::print_poly(ostream& os, const Poly& poliedro_nnc){

   // buffer usato per ottenere il nome della variabile ppl.
   stringstream poly_stream;
   regex comma_pattern(",");
   regex equals_pattern(" =");

   IO_Operators::operator<<(poly_stream, poliedro_nnc);

   string poly_string(poly_stream.str());

   // Sfrutto la mappa statica di Variable per ottenere il nome delle
   // variabili e di quelle PPL.
   for(auto& var: Variable::get_map())
   {
     stringstream ppl_name_stream;

     // Ottieni il nome della variabile corrente.
     Parma_Polyhedra_Library::Variable::default_output_function(ppl_name_stream, var.second.get_PPL());
     string ppl_var_name(ppl_name_stream.str());

     // Effettua la sostituzione.
     regex var_name(ppl_var_name);
     poly_string = regex_replace(poly_string, var_name, var.second.get_name());

     //os << "Sostituisco " << ppl_var_name << " con " << var.second.getName() << endl;
   }

   poly_string = regex_replace(poly_string, comma_pattern, " &");
   poly_string = regex_replace(poly_string, equals_pattern, " ==");
   os << poly_string;

return;

}


void NaPoly::print_PS(ostream& os, const PS& poliedro){

  //PS::iterator it;
string poly_string;

 for (auto it=poliedro.begin(); it!=poliedro.end(); it++)
  {
    stringstream poly_stream;
    print_poly(poly_stream, it->pointset());
    poly_string = poly_stream.str().substr(0, poly_stream.str().length()-1);

    os << "{ " << poly_string << " } ";

  }

  os << endl;

return;

}


void NaPoly::print_PS_regex(ostream& os, const PS& poliedro){
  using IO_Operators::operator<<;

  regex first_pattern("},");
  regex second_pattern(",");
  regex third_pattern(" =");
  stringstream ps_stream;

  ps_stream << poliedro;
  string ps_string(ps_stream.str());

  for(auto& var: Variable::get_map())
  {
    stringstream ppl_name_stream;

    // Ottieni il nome della variabile corrente.
    Parma_Polyhedra_Library::Variable::default_output_function(ppl_name_stream, var.second.get_PPL());
    string ppl_var_name(ppl_name_stream.str());

    // Effettua la sostituzione.
    regex var_name(ppl_var_name);
    ps_string = regex_replace(ps_string, var_name, var.second.get_name());
    //ReplaceStringInPlace(poly_string, ppl_var_name, var.second.getName());

    //os << "Sostituisco " << ppl_var_name << " con " << var.second.getName() << endl;
  }


  ps_string = regex_replace(ps_string, first_pattern, "}");
  ps_string = regex_replace(ps_string, second_pattern, " &");
  ps_string = regex_replace(ps_string, third_pattern, " ==");

  os << ps_string;

  return;
}
