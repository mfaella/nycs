/*
 * convert_to_ppl.cpp
 *
 *  Created on: July 21, 2017
 *      Author: Marco Urbano
 */

// QUESTA LIBRERIA NON SERVE PIU' POICHE' NON SI USANO PIU CONTINUOUS_SET
//#include "../polyhedra/ppl_polyhedron/continuous_set_PPL_NNC.h"

// La libreria convert_to_ppl è stata modificata per restituire direttamente PPL e non continuous_set_PPL_NNC.
#include "../polyhedra/ppl_polyhedron/convert_to_ppl.h"

#include "convert_to_ppl_powerset.h"
#include "../../utility/stl_helper_functions.h"
#include "valuation_function_tree_utility.h"
#include "../../utility/shared_ptr_output.h"
#include "node_print_visitor.h"
#include "../common_input/bool_node_creation.h"
#include "convert_tree_to_DNF.h"

using namespace Parma_Polyhedra_Library;

namespace ppl_polyhedron {
//CONVERTE UN INSIEME CONVESSO RAPPRESENTATO TRAMITE PREDICATO IN UN POLIEDRO CONVESSO
// The function ppl_polyhedron::convert_to_POLY takes as third argument a boolean to know if is a flow or not.
// This is needed because flows contains constraints on primed variables that can be not interpreted as we want.
// (e.g. if we have x'==1 in flow, it does not mean x-primed == 1, but simply x==1).
Parma_Polyhedra_Library::NNC_Polyhedron convert_to_POLY(const tree::node::ptr& p, variable_id_set vis, bool is_flow) {
	PPL_NNC_generator g;
	index_to_variable_id_map_ptr iimap(index_to_variable_id_map::empty_map());
	iimap = iimap->get_map_with_ids_added(vis);
	return g.convert(p, iimap, is_flow);

	// Il vecchio metodo convertiva prima in continuous_set_PPL_NNC e poi restituiva il poliedro tramite get_poly.
	// Adesso il metodo "convert" restituisce direttamente il poliedro.
	//return g.convert(p,iimap)->get_poly();
}


/**
 * Share continuous and discrete trees and store them in a vector of pairs.
 * For all OR node, call itself for each child.
 * Else, make a continuous tree and a discrete tree with make_continuous_or_discrete_tree,
 * then compose a pair with them and store it in the vector.
 */
void share_continuous_and_discrete_tree_recursion(
		const tree::node::ptr& p,
		std::vector<tree::node::ptr> & list_tree) {
	bool is_disjunction = false;
	if (valuation_functions::boolean_node* b = dynamic_cast<valuation_functions::boolean_node*>(p.get())) {
		if (b->my_op == OR) {
			share_continuous_and_discrete_tree_recursion(b->child1, list_tree);
			share_continuous_and_discrete_tree_recursion(b->child2, list_tree);
			is_disjunction = true;
		}
	}
	if (!is_disjunction) {
		list_tree.push_back(p);
	}
}
/*
DATO UN PREDICATO P LO CONVERTE IN DNF E RESTITUISCE LA LISTA DI DISGIUNTI IN LIST_TREE
*/
void share_continuous_and_discrete_tree(const tree::node::ptr& p,
		std::vector<tree::node::ptr> & list_tree) {
	tree::node::ptr new_p = valuation_functions::convert_tree_to_DNF(p);
	share_continuous_and_discrete_tree_recursion(new_p, list_tree);
}

Parma_Polyhedra_Library::Pointset_Powerset<NNC_Polyhedron> convert_to_PS(const tree::node::ptr& p) {

	// tree_vector res è utilizzato per conservare un vettore di nodi che rappresentano ognuno un poliedro.
  typedef std::vector<tree::node::ptr> tree_vector;
  tree_vector res;

	//recupera l' index_to_variable_id_map_ptr del predicato
	variable_id_set vis = get_variable_ids(p);

	//trasforma il predicato in un insieme di disgiunti
	share_continuous_and_discrete_tree(p, res);
	using ::operator<<;
	Pointset_Powerset<NNC_Polyhedron> nnc_pol;
	nnc_pol.clear();
	//converte ogni disgiunto in PPL_NNC ed effettua l'unione dei poliedri così ottenuti
	for (tree_vector::iterator it = res.begin(); it != res.end(); ++it) {

			 //std::cout<<" predicato: "<< *it<<std::endl;
			 // Here the is_flow boolean is set to false because a flow can only be a POLY not a PS.
	     NNC_Polyhedron tmp=convert_to_POLY(*it, vis, false);
	     dimension_type dim=tmp.space_dimension();

			 if(nnc_pol.space_dimension()<dim)
			        nnc_pol.add_space_dimensions_and_embed(dim-nnc_pol.space_dimension());

			 nnc_pol.add_disjunct(tmp);
	 }

return nnc_pol;

}

// This function is used to get a vector of NNC_Polyhedron to be used when building the States_set.
// It does the same task of convert_to_PS function but doesn't build any PS.
vector<NNC_Polyhedron> getPolyArray(const tree::node::ptr& p){

	typedef std::vector<tree::node::ptr> tree_vector;
  tree_vector res;

	//recupera l' index_to_variable_id_map_ptr del predicato
	variable_id_set vis = get_variable_ids(p);

	//trasforma il predicato in un insieme di disgiunti
	share_continuous_and_discrete_tree(p, res);
	using ::operator<<;
	vector<NNC_Polyhedron> nnc_pol_vector;
	//converte ogni disgiunto in PPL_NNC ed aggiunge il poliedro al vettore.
	for (tree_vector::iterator it = res.begin(); it != res.end(); ++it) {

			 //std::cout<<" predicato: "<< *it<<std::endl;
			 // Here the is_flow boolean is set to false because a flow can only be a POLY not a PS.
	     NNC_Polyhedron tmp=convert_to_POLY(*it, vis, false);
       // Tutti i poliedri vengono costruiti contando sia variabili primate che non primate.
			 // Solo la condizione di Jump viene mantenuta alla dimensione originale.
			 // L'istruzione seguente ridimensiona correttamente tutti i PS che non siano Jump.
			 tmp.remove_higher_space_dimensions(tmp.space_dimension()/2);
	     nnc_pol_vector.push_back(tmp);
	 }

return nnc_pol_vector;

}

void convert_to_primed(Pointset_Powerset<NNC_Polyhedron>& p){
	// The partial function assigns which dimension to exchange with.
	// E.G. if dimension 1 has to be exchanged with dimension n
	// It must be called the function my_partial_function.insert(1, n);

	// Expands the "basic" poly to "extended" poly.
	expand_to_primed(p);
	// creates empty Partial_Function.
	Parma_Polyhedra_Library::Partial_Function my_partial_function;

	// Start from dimension 0 to n, considering only the unprimed variables.
	// Adds the int n, indicating "number of unprimed vars", to every dimension to
	// link the dimension k with n+k. This way we will obtain the exchange among
	// primed variables with their unprimed ones.

	dimension_type unprimed_dimension = NaPoly::Variable::get_dimension()/2;

  for(dimension_type i = 0 ; i < unprimed_dimension; i++){
		my_partial_function.insert(i, i+unprimed_dimension);
    my_partial_function.insert(i+unprimed_dimension, i);
	}


		// Make the exchange following the Partial_Function rules.
		p.map_space_dimensions(my_partial_function);


}

// makes all the primed vars become unprimed and removes the unprimed dimensions.
void convert_to_unprimed(PS& p){

	// The partial function assigns which dimension to exchange with.
	// E.G. if dimension 1 has to be exchanged with dimension n
	// It must be called the function my_partial_function.insert(1, n);

	// creates empty Partial_Function.
	Parma_Polyhedra_Library::Partial_Function my_partial_function;

	// Start from dimension 0 to n, considering only the unprimed variables.
	// Adds the int n, indicating "number of unprimed vars", to every dimension to
	// link the dimension k with n+k. This way we will obtain the exchange among
	// primed variables with their unprimed ones.

	dimension_type unprimed_dimension = NaPoly::Variable::get_dimension()/2;

	 for(dimension_type i = 0 ; i < unprimed_dimension; i++){
		 my_partial_function.insert(i, i+unprimed_dimension);
     my_partial_function.insert(i+unprimed_dimension, i);
	}


		// Make the exchange following the Partial_Function rules.
		p.map_space_dimensions(my_partial_function);

		// Removes the n last dimensions.
    p.remove_higher_space_dimensions(unprimed_dimension);

}




}
