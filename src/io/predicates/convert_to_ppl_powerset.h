#ifndef CONVERT_TO_PPL_NEW
#define CONVERT_TO_PPL_NEW

#include <list>
#include <iostream>

#include "../polyhedra/ppl_polyhedron/rat_linexpression.h"
#include "valuation_function_tree.h"
#include "share_continuous_and_discrete_tree.h"
#include "../../automaton/Variable.h"
// Commentato da Marco per evitare di usare i continuous_set.
//#include "synthesis/continuous/continuous_set_PPL_Powerset.h"
//#include "continuous_set_PPL_NNC.h"


/** Forward declarations */
class index_to_variable_id_map;
typedef boost::shared_ptr<const index_to_variable_id_map> index_to_variable_id_map_ptr;
/*
namespace ppl_polyhedron {
class continuous_set_PPL_NNC;
typedef boost::shared_ptr<continuous_set_PPL_NNC> continuous_set_PPL_NNC_ptr;
typedef boost::shared_ptr<const continuous_set_PPL_NNC> continuous_set_PPL_NNC_const_ptr;
class continuous_set_PPL_Powerset;
typedef boost::shared_ptr<continuous_set_PPL_Powerset> continuous_set_PPL_Powerset_ptr;
typedef boost::shared_ptr<const continuous_set_PPL_Powerset> continuous_set_PPL_Powerset_const_ptr;
}
*/


namespace ppl_polyhedron {
using namespace Parma_Polyhedra_Library;
using namespace valuation_functions;

	//CONVERTE UN INSIEME CONVESSO RAPPRESENTATO TRAMITE PREDICATO IN UN POLIEDRO CONVESSO
	// The function ppl_polyhedron::convert_to_POLY takes as third argument a boolean to know if is a flow or not.
	// This is needed because flows contains constraints on primed variables that can be not interpreted as we want.
	// (e.g. if we have x'==1 in flow, it does not mean x-primed == 1, but simply x==1).
	NNC_Polyhedron convert_to_POLY(const tree::node::ptr& p, variable_id_set vis, bool is_flow);

	//CONVERTE UN INSIEME NON CONVESSO RAPPRESENTATO TRAMITE PREDICATO IN UN INSIEME DI POLIEDRI CONVESSI
	Pointset_Powerset<NNC_Polyhedron> convert_to_PS(const tree::node::ptr& p);

	//CONVERTE UN INSIEME NON CONVESSO RAPPRESENTATO TRAMITE PREDICATO IN UN VETTORE DI POLIEDRI CONVESSI.
	vector<NNC_Polyhedron> getPolyArray(const tree::node::ptr& p);

	// CONVERTE UN POLIEDRO DEFINITO SULLE VARIABILI NON PRIMATE SOSTITUENDO OGNI VARIABILE CON LA SUA PRIMATA:
	void convert_to_primed(Pointset_Powerset<NNC_Polyhedron>& p);

	// AGGIUNGE LE DIMENSIONI MANCANTI AL POLIEDRO COSTRUITO SULLE DIMENSIONI UNPRIMED.
	inline void expand_to_primed(PS& p){
		unsigned current_dim = NaPoly::Variable::get_dimension();

		if(current_dim > p.space_dimension())
			 p.add_space_dimensions_and_embed(current_dim - p.space_dimension());
	}

	// CONVERTE TUTTE LE VARIABILI PRIMATE IN NON PRIMATE E INFINE ELIMINA LE DIMENSIONI DELLE VARIABILI NON PRIMATE.
	void convert_to_unprimed(PS& p);
}

#endif /*CONVERT_TO_PPL_*/
