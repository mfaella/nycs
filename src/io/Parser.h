/*
 *  File: Parser.h
 *  Created on: June 15, 2017
 *  Author: Marco Urbano
 */


#ifndef __NEW_PARSER_H__
#define __NEW_PARSER_H__

#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <string>
#include <boost/variant.hpp>
#include "../automaton/preprocessor.h"
/* TinyXML has been used to parse XML model file.*/
#include "tinyxml/tinyxml.h"
#include "common_input/parser_basics.h"
#include "common_input/symbol_table.h"
#include "common_input/symbol_table_operators.h"
#include "common_input/symbol_table_cache.h"
#include "common_input/parse_policy.h"
#include "../math/vdom/variable.h"
#include "../automaton/Variable.h"
#include "../automaton/Automaton.h"
#include "../automaton/Location.h"
#include "../automaton/State_set.h"
#include "../automaton/Transition.h"
#include "../scenario/OptionsBundle.h"

// Librerie per il parsing e la costruzione dell'albero AST
#include "common_input/predicate_parser.h"
#include "common_input/predicate_tree_operations.h"
#include "predicates/node_print_visitor.h"
#include "../utility/basic_exception.h"
#include "common_input/parse_policy.h"
#include "predicates/valuation_function_tree_utility.h"
// Librerie per il parsing del file di configurazione.
#include "common_input/state_parser.h"
// Libreria per la conversione da Albero a PPL.
#include "predicates/convert_to_ppl_powerset.h"

#include <regex>

/*  La libreria automaton_creation ha i metodi add_symbol, add_variable
    che sono stati inclusi nella classe Parser come metodi statici.
    Il motivo è che contribuiscono alla creazione dell'automa
    quando si fa parsing.


    #include "common_input/automaton_creation.h"
*/

using namespace std;


namespace NaPoly{

class Parser{
  private:
    Automaton aut;
    OptionsBundle options;


    parser::symbol_table build_automaton(TiXmlDocument& model,
                                         parser::parse_policy& ppol);

    void build_options_bundle(TiXmlDocument& config,
                              parser::symbol_table& symb_table,
                              parser::parse_policy& parse_pol);

    static Location build_location(int id, string name,
                                   string flow, string invariant,
                                   const parser::symbol_table& symbol_table,
                                   const parser::parse_policy& ppol);

    static Variable build_variable(string name, bool is_primed);

    void build_transition(string guard, string assignment, int source_id,
                          int target_id, string contr,
                          const parser::symbol_table& symbol_table,
                          const parser::parse_policy& ppol);

    /* Vecchi metodi di "automaton_creation" che crea la variabile
       e compie le operazioni per aggiungerla alla collezioni di
       variabili della vecchia classe variable. */
    static void add_variable(Variable& var, std::string name,
                             bool is_input, bool is_const, unsigned int dim);

    static void add_symbol(Variable& var, const parser::symbol& symb);

    static parser::symbol get_unvalued_symbol_synthesis(TiXmlElement* param,
                                          const parser::symbol_table& s_table);


  public:
    Parser(string model_file, string config_file);
    Automaton& get_automaton();
    OptionsBundle& get_options_bundle();
  };

}

#endif
