/*
 * input_options.cpp
 *
 *  Created on: Sep 28, 2009
 *      Author: frehse
 */

#include "input_options.h"

#include <iostream>
#include <fstream>
#include <sstream>

#include "../../main/options.h"
#include "../../utility/logger.h"
#include "../../utility/stl_helper_functions.h"
#include "../../utility/basic_exception.h"


/*
    Le seguenti dipendenze vanno sostituite con le nuove.

#include "core/hybrid_automata/location.h"
#include "core/hybrid_automata/hybrid_automaton.h"
#include "core/hybrid_automata/hybrid_automaton_utility.h"
#include "core/hybrid_automata/hybrid_automaton_network.h"
#include "core/hybrid_automata/automaton_cache.h"
#include "core/hybrid_automata/adapt_automaton_visitor.h"
#include "core/hybrid_automata/automaton_name_formatter.h"
#include "core/symbolic_states/symbolic_state_collection.h"

#include "core/scenarios/parse_with_scenario.h"
#include "core/scenarios/scenario_chooser.h"
#include "math/vdom/context_variable_formatter.h"
#include "io/TXT_format/TXT_formatter.h"

#include "core/hybrid_automata/adaptors/dae_to_ode_adapter.h"
#include "synthesis/config_options.h"

*/

namespace options {

void add_input_options() {
		options::options_processor::config.add_options()("system",
				boost::program_options::value<std::string>(),
				"name of the component to be analyzed");
	  //std::cout<<"system option added.\n";
		options::options_processor::config.add_options()("model-file,m",
				boost::program_options::value<std::vector<std::string> >(),
				"model file");
		//std::cout<<"model-file option added.\n";
		options::options_processor::config.add_options()("initially,i",
				boost::program_options::value<std::string>(),
				"Set the initial states.");
		//std::cout<<"initially option added.\n";
		options::options_processor::config.add_options()("safe,sf",
				boost::program_options::value<std::string>(),
				"Set the safe states.");
		//std::cout<<"safe option added.\n";
		options::options_processor::config.add_options()("reach,rc",
				boost::program_options::value<std::string>(),
				"Set the reach states.");
		//std::cout<<"reach option added.\n";
	}


	/* Nel nuovo programma potrebbe essere utile un metodo del genere che verifica
	   se il file passato come primo argomento che è stato inserito nella mappa
		 sotto il nome di "model-file".
	*/
	bool check_input_options(options::options_processor::variables_map& vmap) {
		std::string model_file;
    if (options::options_processor::get_string_option(vmap,"model-file",model_file)) {
       std::cout << "Model file is: " << model_file << "\n";

        std::ifstream ifs(model_file.c_str(), std::ifstream::in);
        if (!ifs.good()) {
          throw basic_exception("Error opening model file "
              + model_file + " for reading.");
        }
        ifs.close();

    } else {
      throw basic_exception("No model file specified.");
    }
    return true;
}


bool check_input_options_mod(std::string model_file){
	   std::cout << "Model file is: " << model_file << "\n";
     std::ifstream ifs(model_file.c_str(), std::ifstream::in);
		 if (!ifs.good()) {
			 throw basic_exception("Error opening model file "
					 + model_file + " for reading.");
		 }
		 ifs.close();

return true;
}



std::string get_system_name(options::options_processor::variables_map& vmap) {
	std::string sys_name = "system";
	options::options_processor::get_string_option(vmap,"system",sys_name);
	return sys_name;
}


/*

hybrid_automata::symbolic_state_collection_ptr define_initial_states(
		options::options_processor::variables_map& vmap) {
			/*
	hybrid_automata::symbolic_state_collection_ptr ini =
			hybrid_automata::symbolic_state_collection_ptr();
	hybrid_automata::symbolic_state_collection_ptr model_ini =
			sys->get_initial_states();
	if (model_ini) {
		hybrid_automata::canonicalize_location_constraints(*model_ini);
	}


	std::string ini_states_string;
	if (options::options_processor::get_string_option(vmap,"initially",ini_states_string)) {
		const hybrid_automata::reachability_scenario& scen =
				hybrid_automata::scenario_chooser::get_scenario();

		std::string sys_name = get_system_name(vmap);

		parser::parse_policy ppol = parser::parse_policy::SX_policy();
		// accept unknown vars so that subcomponent variables can be specified too
		ppol.add_unknown_vars = true;
		try {
		ini = hybrid_automata::parse_symbolic_state_collection_with_scenario(
				scen, ini_states_string, sys_name,ppol);
		} catch ( std::exception& e ) {
			throw basic_exception("Failed to define initial states.",e);
		}

		if (ini) {
			hybrid_automata::canonicalize_location_constraints(*ini);
			std::string s;
			bool override = options::options_processor::get_string_option(vmap,
					"override-model-initial-states", s) && (s == "no");
			if (model_ini && override) {
				ini->intersection_assign(model_ini);
			}
		}
	} else
		ini = model_ini;
	if (!ini) {
		throw basic_exception(
				"No initial states specified. Please specify in options or in model file.");
	}

	// Check for variables
	variable_id_set vis = ini->get_variable_ids();
	throw_if_not_contains(variable_id_set(), get_primed_variables(vis),
			"Primed variable(s) not permitted in initial states, but found ",
			".");
	throw_if_not_contains(sys->get_variable_ids(), vis, "Unknown variable(s) ",
			" in initial states.");

	return ini;
}
*/

/*

std::string format_ident(const std::string& ident, const std::string& h) {
	std::string new_str = dot_context::in_context_name(ident,h);
	return new_str;
}

std::string format_var(const std::string& ident, const std::string& h) {
	return context_variable_formatter(h).format(ident);
}

std::string format_aut(const std::string& ident, const std::string& h) {
	std::string new_str = dot_context::in_context_name(ident, h);
	//	replace_first(new_str, h + ".", "");
	if (new_str.find(".") != std::string::npos) {
		std::set<std::string> known =
				hybrid_automata::hybrid_automaton_cache::get_automaton_names();
		new_str = dot_context::smallest_unique_name(new_str, h,
				known.begin(), known.end());
	}
	return new_str;
}
*/

}
