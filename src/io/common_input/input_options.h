/*
 * input_options.h
 *
 *  Created on: Sep 28, 2009
 *      Author: frehse
 */

#ifndef input_options_H_
#define input_options_H_

#include "../../main/options.h"

/** Forward declarations */
namespace hybrid_automata {
class hybrid_automaton;
typedef boost::shared_ptr<hybrid_automaton> hybrid_automaton_ptr;
class symbolic_state_collection;
typedef boost::shared_ptr<symbolic_state_collection> symbolic_state_collection_ptr;
}

namespace options {

void add_input_options();
bool check_input_options(options::options_processor::variables_map& vmap);
bool check_input_options_mod(std::string model_file);
std::string get_system_name(options::options_processor::variables_map& vmap);

/*
    I seguenti metodi sono commentati in attesa di essere riadattati per il nuovo tool.
    Alcuni metodi le cui firme non compaiono qui sono ancora presenti nel file .cpp


hybrid_automata::symbolic_state_collection_ptr define_initial_states(
		options::options_processor::variables_map& vmap,
		hybrid_automata::hybrid_automaton_ptr sys);

*/

}

#endif /* input_options_H_ */
