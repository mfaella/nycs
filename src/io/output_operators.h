/*
 *  File: output_operators.h
 *  Created on: Semptember 22, 2017
 *  Author: Marco Urbano
 */


#include "../automaton/preprocessor.h"
#include "../automaton/Variable.h"
#include <regex>

using namespace std;




// Sono qui definiti gli operatori di output
namespace NaPoly{

  void print_poly(ostream& os, const  Poly& poliedro_nnc);
  void print_PS(ostream& os, const PS& poliedro);
  void print_PS_regex(ostream& os, const PS& poliedro);
}
