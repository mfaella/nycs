/*
*  File: SafetyScenario.cpp
*  Created on: August 13, 2017
*  Author: Marco Urbano
*/


#include <csignal>
#include <unordered_map>

#include "../io/output_operators.h"

/* To include SIGALRM_Handler function */
#include "../main/signal_handlers.h"

/* To include functions that will be used
   to build the new automaton          */
#include "../io/predicates/convert_to_ppl_powerset.h"

#include "../algorithms/pairwise_reduce.h"
#include "../main/option_names.h"

#include "safety_scenario.h"


using namespace std;


namespace NaPoly{


// Constructor that simply calls the superclass one and instantiate the bundle attribute.
SafetyScenario::SafetyScenario(Automaton my_aut, OptionsBundle my_bundle): Scenario(my_aut, my_bundle){}

// Returns the Safe_region.
StateSet SafetyScenario::get_safe_region(){
 return safe_region;
}



Automaton SafetyScenario::get_controlled_automaton(){

// Copy the automata to perform the last operation that will return a "safe automata"
Automaton controlled(aut);

for(pair<Location, PS> p : safe_region.get_map())
 {
   Location& source = controlled.get_location_ref(p.first.get_name());
   source.invariant.intersection_assign(p.second);

   // Get the safe source region
   PS safe_source_region = p.second;

   // set to empty the incoming_contr vector.
   source.incoming_contr.clear();

   while(!source.outgoing_contr.empty())
   {
    // Pop back all the outgoing controllable transitions from the new automaton.
    Transition out_contr = source.outgoing_contr.back();
    source.outgoing_contr.pop_back();

    // Get the reference to the new automaton's target location.
    Location& target = controlled.get_location_ref(out_contr.get_target().get_name());

    // Get the new jump
    PS jump = out_contr.get_jump();
    // Convert to prime the safe target region
    PS safe_target_region = safe_region.get_region(out_contr.get_target());
    ppl_polyhedron::convert_to_primed(safe_target_region);
    ppl_polyhedron::expand_to_primed(safe_source_region);

    // Build the new jump as
    //  (old_jump ∩ primed(safe_target_region) ∩ safe_source_region)
    jump.intersection_assign(safe_target_region);
    jump.intersection_assign(safe_source_region);

    // Build the new transition with the new jump
    Transition controlled_transition(source, target, false, jump);

    // Add the new transition to both source and target.
    source.add_transition(controlled_transition, false, true);
    target.add_transition(controlled_transition, false, false);
    }
  }
return controlled;
}


// The algorithm used in spaceex+ modified with the new classes.
void SafetyScenario::launch() {

  int max_seconds = options.get_timer();
  int max_it = options.get_iter_max();
  StateSet safe_states = options.get_goal();
  StateSet invariants_set = aut.get_invariant();
  safe_states.intersection_assign(invariants_set);
  
  bool fixpoint=false;
 
  ScenarioBundle scen_bundle(aut);

  // If the user has set a timeout then set an alarm.
  if (max_seconds > 0) 
    alarm(max_seconds);

  int j = 0;

  // Exit loop at fixpoint or if max_it has been reached
  StateSet old_safe, new_safe(safe_states);
  while (!fixpoint && (max_it==0 || j<max_it)) {

    cout << endl << " Iteration " << ++j << flush;

    old_safe = new_safe;    
    new_safe = cpre_safety(old_safe, scen_bundle, fixpoint);
        
    // Set current State_set to be visible to the interruption handler
    set_current_stateset(&new_safe);
  }

  cout << endl;
  
  /* Set Safe_region field */
  safe_region = new_safe;

  return;
}


StateSet SafetyScenario::cpre_safety(StateSet& safe_states, ScenarioBundle& scen_bundle, bool& fixpoint){

PS U, Z, V, Good, A;
StateSet result_set;
//std::string op_version = options.get_op_version();
fixpoint = true;

LOGGER(DEBUG, "cpre_safety", "Inizio iterazioni");


 for (auto& symb_state : safe_states.get_map()) {

  using IO_Operators::operator<<;
  const Location& loc = symb_state.first;
  A = symb_state.second; // good region of current location

  // bundle for current location
  ScenarioLocationBundle& loc_bundle = scen_bundle.get_location_bundle(loc);

  PS A1(A);

  // calcolo dei parametri c e b
  bool change_c = scen_bundle.update_controllable_preimage(safe_states, loc);
  bool change_b = scen_bundle.update_uncontrollable_preimage(safe_states, loc);

  fast_pairwise_reduce(loc_bundle.my_Under_old);
  fast_pairwise_reduce(loc_bundle.My_b_old);

  if (change_c || change_b) {

    fixpoint = false;
    V = scen_bundle.avoid_region(A1, loc);
    string outer_op = options.get_outer_operator();
    
    if (outer_op == OUTER_SOR || outer_op == "") { /* default */
      Z    = scen_bundle.stay_region(A1, loc);
      Good = scen_bundle.SOR_must(Z, V, loc, options);
	
      // se il risultato del SORM (Good) è non vuoto, allora lo interseca con la vecchia regione safe (A1)
      if(!Good.is_empty()) {
        fast_pairwise_reduce(A1);
        A1.intersection_assign(Good);
        fast_pairwise_reduce(A1);
      }
    } else if (outer_op == OUTER_RWA) {
      U                 = scen_bundle.reach_region(A1, loc);
      pair<PS, PS> pair = scen_bundle.RWA_may(U, V, loc, options);
      
      // se il risultato dell'RWA (Cut) è non vuoto, allora lo sottrae alla vecchia regione safe (A1)
      if(!pair.first.is_empty()) {
	fast_pairwise_reduce(A1);
	PS safe_part = pair.second;
	add_powerset(safe_part, V);
	A1.intersection_assign(safe_part);
	fast_pairwise_reduce(A1);
      }
    } else {
      throw basic_exception("Unknown outer operator for safety control scenario.");
    }    
  }
  
  PS cset(A1);
  
  //aggiunge il nuovo stato simbolico al risultato
  result_set.add(loc, cset);
 }
 
 return result_set;
}




}
