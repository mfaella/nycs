/*
 *  File: OptionsBundle.cpp
 *  Created on: July 30, 2017
 *  Author: Marco Urbano
 */


#include "OptionsBundle.h"

using namespace std;

namespace NaPoly{

  OptionsBundle::OptionsBundle(){
    aut_name = "default";
  }

  OptionsBundle::~OptionsBundle(){}

  OptionsBundle::OptionsBundle(const OptionsBundle& bundle): aut_name{bundle.aut_name}, scenario{bundle.scenario}, outer_operator{bundle.outer_operator}
                                                , inner_operator{bundle.inner_operator}, verbosity{bundle.verbosity}
                                                , over{bundle.over}, goal{bundle.goal}, initially{bundle.initially}, iter_max{bundle.iter_max}
                                                , timer{bundle.timer}{}


  OptionsBundle::OptionsBundle(string name, string my_scenario,
			       string my_inner_op, string my_outer_op, string verbosity_lev,
			       string my_over, int my_iter_max, int my_timer,
			       StateSet my_goal, StateSet init_states): aut_name{name},
									scenario{my_scenario}, outer_operator{my_outer_op}, inner_operator{my_outer_op},
									verbosity{verbosity_lev}, over{my_over},
									iter_max{my_iter_max}, timer{my_timer},
									goal{my_goal}, initially{init_states}
{}

  string OptionsBundle::get_aut_name(){
    return aut_name;
  }

  string OptionsBundle::get_scenario(){
    return scenario;
  }

  string OptionsBundle::get_outer_operator(){
    return outer_operator;
  }

  string OptionsBundle::get_inner_operator(){
    return inner_operator;
  }

 string OptionsBundle::get_over(){
   return over;
 }

  string OptionsBundle::get_verbosity(){
   return verbosity;
 }

 StateSet OptionsBundle::get_goal(){
   return goal;
 }

 StateSet OptionsBundle::get_init(){
   return initially;
 }

 int OptionsBundle::get_iter_max(){
   return iter_max;
 }

 int OptionsBundle::get_timer(){
   return timer;
 }


 ostream& operator<<(ostream& os, OptionsBundle opt){
   os << "Automaton : " << opt.aut_name << endl;
   os << "Scenario : " << opt.scenario << endl;
   os << "Outer Operator : " << opt.outer_operator << endl;
   os << "Inner Operator : " << opt.inner_operator << endl;
   os << "Max iterations : " << opt.iter_max << endl;
   os << "Seconds to end the scenario: " << opt.timer << endl;

   os << "Verbosity : " << opt.verbosity << endl;
   os << "Over : " << opt.over << endl;
   os << "Init states : " << opt.initially << endl;
   os << "Goal : " << opt.goal << endl;

   return os;

 }

}
