/*
 *  scenario_options.cpp
 *
 *  Created on: Jun 27, 2017
 *      Author: Marco Urbano
 */



#include <iostream>
#include <boost/algorithm/string/trim.hpp>
#include "../utility/stl_helper_functions.h"
#include "../main/options.h"
#include "scenario_options.h"


/*  I seguenti header inclusi sono stati commentati poichè non utili ai fini dei
    metodi rimasti.


Serve per la selezione dello scenario. "scenario_chooser" è una classe che
mantiene le informazioni per l'esecuzione dello scenario.
La classe "reachability_scenario" è utilizzata come attributo della classe
"scenario_chooser"

#include "core/scenarios/scenario_chooser.h"
#include "core/scenarios/reachability_scenario.h"


#include "core/hybrid_automata/automaton_cache.h"
#include "core/hybrid_automata/hybrid_automaton.h"
#include "core/hybrid_automata/hybrid_automaton_utility.h"
#include "core/symbolic_states/symbolic_state_collection.h"
#include "core/scenarios/parse_with_scenario.h"
#include "core/analysis_algorithms/reachability_algorithm.h"
*/


namespace options {

void add_scenario_options() {

	       options::options_processor::config.add_options()(
			       "scenario,c",
			       boost::program_options::value<std::string>(),
			       "Set the active scenario:\n- reach_control : \n- safety_control :\n- nonblocking_check : \n- nonconvex_check : DEVELOPMENT SUSPENDED ");
        //std::cout<<"scenario option added.\n";

			/*
          Le opzioni:
					 - operator-version
					 - operator-choice
					 - operator-computation

					sono state ridotte a:

					- rwa_or_sor (rappresenta l'unione del vecchio operator_version + operator-choice)
					- operator-version (rappresenta il vecchio computation).
			*/

      options::options_processor::config.add_options()(
					"operator-version",
					boost::program_options::value<std::string>(),
					"version of operator (RWA/SOR)");

			//std::cout<<"operator-version option added.\n";

			options::options_processor::config.add_options()(
					"rwa_or_sor",
					boost::program_options::value<std::string>(),
					"operator (rwa/sor).");

			//std::cout<<"rwa_or_sor option added.\n";

			options::options_processor::config.add_options()(
					"over",
					boost::program_options::value<std::string>(),
					"overapproximation(small,large) for contr_reach_scenario.");

			//std::cout<<"over option added.\n";
}

bool check_scenario_options(options::options_processor::variables_map& vmap) {
	return true;
}

/*
bool apply_scenario_options_wo_system(options::options_processor::variables_map& vmap) {

	 Sembra che questo metodo non abbia più motivo di esistere visto che lo scenario non fa
     parte della classe automa ed i metodi sono quelli utilizzati per il vecchio automa.


	std::string s;
	if (options::options_processor::get_string_option(vmap,"scenario",s)) {
		hybrid_automata::scenario_chooser::set_scenario(s);
	}
	// @todo Check why this is here twice
	if (options::options_processor::get_string_option(vmap,"scenario",s)) {
			hybrid_automata::scenario_chooser::set_scenario(s);
	}

	return true;
}

bool apply_scenario_options_with_system(options::options_processor::variables_map& vmap) {

	// Hand the rest of the options over to the scenario itself
	hybrid_automata::reachability_scenario scen =
			hybrid_automata::scenario_chooser::get_scenario();
	scen.apply_options(vmap);

	hybrid_automata::scenario_chooser::set_scenario(scen);

	return true;
}
*/

}
