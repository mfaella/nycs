/*
 * phaver_scenario.cpp
 *
 *  Created on: Aug 31, 2009
 *      Author: frehse
 */

#include "core/scenarios/phaver_scenario.h"
#include "nonconvex.h"
#include "core/post_operators/pcd_post/constant_bound_time_elapse.h"
#include "synthesis/post_operators/constant_bound_time_elapse_powerset.h"
#include "synthesis/post_operators/pre_operator.h"
#include "synthesis/post_operators/direct_discrete_post_powerset.h"
#include "core/pwl/PLWL_sstate_collection_stl_list.h"
#include "core/symbolic_states/symbolic_state_collection_stl_list.h"
#include "core/hybrid_automata/explicit_automaton.h"
#include "core/hybrid_automata/pairwise_hybrid_automaton_network.h"
#include "core/hybrid_automata/adaptors/ppl_adaptors.h"
#include "core/hybrid_automata/adaptors/constr_poly_adaptors.h"
#include "synthesis/hybrid_automata/adaptors/ppl_adaptors_powerset.h"
#include "synthesis/hybrid_automata/automaton_synthesis.h"
#include "synthesis/plwl/PLWL_sstate_collection_stl_list_powerset.h"

namespace hybrid_automata {

/*
LO SCENARIO NON CONVEX HA L'OPZIONE PER SPECIFICARE LA VERSIONE DI RWA UTILIZZATA
*/
void nonconvex_scenario_apply_options(reachability_scenario* sp,
		options::options_processor::variables_map& vmap) {
	std::string tmp;
	options::options_processor::get_string_option(vmap,"rwa",tmp);
	pre_operator::rwa_type=tmp;
}

/*
DEFINISCE LO SCENARIO CHE GESTISCE INSIEMI CONTINUI NON CONVESSI
DEFINISCE POST OPERATOR, TIPO DI PASSED AND WAITING LIST, STATI SIMBOLICI, TIPO DI AUTOMA, L'ADAPTOR PER CONVERTIRE GLI INSIEMI CONTINUI
*/
reachability_scenario get_nonconvex_scenario() {
	reachability_scenario s;
	s.set_name("nonconvex");
	s.set_continuous_post_operator(continuous_post::ptr(new pre_operator));
	s.set_discrete_post_operator(discrete_post::ptr(new direct_discrete_post_powerset));
	s.set_passed_and_waiting_list(passed_and_waiting_list::ptr(new PLWL_sstate_collection_stl_list_Powerset));
	s.set_symbolic_state_collection(symbolic_state_collection::ptr(
			new symbolic_state_collection_stl_list));
	s.set_hybrid_automaton(hybrid_automaton::ptr(new automaton_synthesis));
	s.set_hybrid_automaton_network(hybrid_automaton_network::ptr(
			new pairwise_hybrid_automaton_network));
	automaton_to_PPL_adaptor_ptr aut_ad=automaton_to_PPL_adaptor_ptr(new automaton_to_PPL_Powerset_adaptor());
//	automaton_to_constr_poly_adaptor_ptr aut_ad = automaton_to_constr_poly_adaptor_ptr(
//			new automaton_to_constr_poly_adaptor(global_types::STD_BOOL,global_types::GMP_RATIONAL));
	aut_ad->init();
	s.set_adapt_automaton_visitor(aut_ad);

	s.set_option_handler(&nonconvex_scenario_apply_options);

	return s;
}

}
