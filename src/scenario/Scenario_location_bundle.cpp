/*
 *  File: ScenarioLocationBundle.cpp
 *  Created on: August 16, 2017
 *  Author: Marco Urbano
*/

#include "Scenario_location_bundle.h"

using namespace std;


namespace NaPoly{

  // Empty arguments constructor.
  ScenarioLocationBundle::ScenarioLocationBundle(){
    // If you need to obtain the dimension without counting the primed variables, use Variable.getDimension()/2.
    int unprimed_count = Variable::get_dimension()/2;



    my_U_old = PS(unprimed_count, Parma_Polyhedra_Library::EMPTY);
    my_Under_old = PS(unprimed_count, Parma_Polyhedra_Library::EMPTY);
    My_b_old = PS(unprimed_count, Parma_Polyhedra_Library::EMPTY);
    my_Under_eff = PS(unprimed_count, Parma_Polyhedra_Library::EMPTY);
    my_not_inv = PS(unprimed_count, Parma_Polyhedra_Library::EMPTY);

    my_location_initialized = false;
    my_initialized_map_sorm = false;

  }

  // Copy constructor.
  ScenarioLocationBundle::ScenarioLocationBundle(const ScenarioLocationBundle& to_copy): my_U_old{to_copy.my_U_old}, my_Under_old{to_copy.my_Under_old}
                                                                                         , My_b_old{to_copy.My_b_old}, my_Under_eff{to_copy.my_Under_eff}
                                                                                         , my_not_inv{to_copy.my_not_inv}
                                                                                         , my_location_initialized{to_copy.my_location_initialized}
                                                                                         , my_initialized_map_sorm{to_copy.my_initialized_map_sorm}
                                                                                         , extadj{to_copy.extadj}, ruadj{to_copy.ruadj}, rradj{to_copy.rradj}{}










     PS& ScenarioLocationBundle::get_my_U_old(){
       return my_U_old;
     }

     PS& ScenarioLocationBundle::get_my_Under_old(){
       return my_Under_old;
     }

     PS& ScenarioLocationBundle::get_My_b_old(){
       return My_b_old;
     }

     PS& ScenarioLocationBundle::get_my_Under_eff(){
       return my_Under_eff;
     }

     PS& ScenarioLocationBundle::get_my_not_inv(){
       return my_not_inv;
     }

     bool& ScenarioLocationBundle::get_my_location_initialized(){
       return my_location_initialized;
     }

     bool& ScenarioLocationBundle::get_my_initialized_map_sorm(){
       return my_initialized_map_sorm;
     }

     hash_map_type& ScenarioLocationBundle::get_extadj(){
       return extadj;
     }

     hash_map_type& ScenarioLocationBundle::get_rradj(){
       return rradj;
     }

     hash_map_u_type& ScenarioLocationBundle::get_ruadj(){
       return ruadj;
     }


     void ScenarioLocationBundle::set_my_U_old(PS U_old){
       my_U_old = U_old;
     }

     void ScenarioLocationBundle::set_my_Under_old(PS Under_old){
       my_Under_old = Under_old;
     }

     void ScenarioLocationBundle::set_My_b_old(PS b_old){
       My_b_old = b_old;
     }

     void ScenarioLocationBundle::set_my_Under_eff(PS Under_eff){
       my_Under_eff = Under_eff;
     }

     void ScenarioLocationBundle::set_my_not_inv(PS not_inv){
       my_not_inv = not_inv;
     }

     void ScenarioLocationBundle::set_my_location_initialized(bool location_initialized){
       my_location_initialized = location_initialized;
     }

     void ScenarioLocationBundle::set_my_initialized_map_sorm(bool initialized_map_sorm){
       my_initialized_map_sorm = initialized_map_sorm;
     }

     void ScenarioLocationBundle::set_extadj(hash_map_type new_extadj){
       extadj = new_extadj;
     }

     void ScenarioLocationBundle::set_rradj(hash_map_type new_rradj){
       rradj = new_rradj;
     }

     void ScenarioLocationBundle::set_ruadj(hash_map_u_type new_ruadj){
       ruadj = new_ruadj;
     }



}
