/*
 *  File: ReachScenario.h
 *  Created on: September 08, 2017
 *  Author: Marco Urbano
 */

#ifndef _REACH_SCENARIO_NAPOLY__
#define _REACH_SCENARIO_NAPOLY__

#include "Scenario.h"
#include "scenario_bundle.h"
#include "../automaton/Automaton.h"
#include "../automaton/State_set.h"
#include "../algorithms/pairwise_reduce.h"
#include "Scenario_location_bundle.h"

using namespace std;

namespace NaPoly{

   class ReachScenario : public Scenario{

   private:

     /* Winning region obtained from launch*/
     StateSet reached_states;

     StateSet cpre_reach(StateSet& reach_states,
                         ScenarioBundle& scen_bundle, bool& fixpoint);



   public:
     ReachScenario(Automaton my_aut, OptionsBundle my_bundle);
     StateSet get_reached_states();
     void launch();

   };

}

#endif
