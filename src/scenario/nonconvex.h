/*
 * phaver_scenario.h
 *
 *  Created on: Jun 21, 2009
 *      Author: frehse
 */

#ifndef NONCONVEX_H_
#define NONCONVEX_H_

#include "core/scenarios/reachability_scenario.h"

namespace hybrid_automata {
/*
DEFINISCE LO SCENARIO CHE GESTISCE INSIEMI CONTINUI NON CONVESSI
DEFINISCE POST OPERATOR, TIPO DI PASSED AND WAITING LIST, STATI SIMBOLICI, TIPO DI AUTOMA, L'ADAPTOR PER CONVERTIRE GLI INSIEMI CONTINUI
*/
reachability_scenario get_nonconvex_scenario();

}

#endif /* NONCONVEX_H_ */
