/*
*  File: Scenario_bundle.cpp
*  Created on: August 13, 2017
*  Author: Marco Urbano
*/

#include "scenario_bundle.h"

#include "../main/option_names.h"

// RWA versions
#include "../algorithms/rwa/rwa_smooth_basic.h"
#include "../algorithms/rwa/rwa_smooth_maps.h"
#include "../algorithms/rwa/rwa_basic.h"
#include "../algorithms/rwa/rwa_maps.h"

// SOR versions
#include "../algorithms/sor/sor_basic.h"
#include "../algorithms/sor/sor_global.h"
#include "../algorithms/sor/sor_local.h"
#include "../algorithms/sor/sor_local_recycle.h"

#include "../algorithms/over_approximations.h"

#include "../io/output_operators.h"

#include "../algorithms/pairwise_reduce.h"

using namespace std;


namespace NaPoly{


  using IO_Operators::operator<<;

  // Default constructor (do nothing).
  //Scenario_bundle::Scenario_bundle(){}

  // Gets the Automaton to build the unordered_map with all the locations: each location will be inserted in the map with the default Safety_location_bundle.
  ScenarioBundle::ScenarioBundle(Automaton& aut){
    vector<Location> locations = aut.get_locations();
    
    for(auto loc : locations){
      map.insert({loc, ScenarioLocationBundle()});
    }
  }


  // Copy constructor.
  ScenarioBundle::ScenarioBundle(const ScenarioBundle& to_copy): map{to_copy.map}{
    throw ScenarioBundleCopyException{};
  }

  ScenarioLocationBundle& ScenarioBundle::get_location_bundle(const Location& loc){
    try
    {
      return map.at(loc);
    }

    catch(const std::out_of_range& oor)
    {
      throw ScenarioBundleNotFoundLoc();
    }
  }

  std::pair<PS, PS> rwa_may_dispatcher(PS& U, PS& V, Poly& flow, Poly& preflow, OptionsBundle& options)
  {
    std::string op_version = options.get_inner_operator();

    if (op_version == INNER_BASIC) {
      return rwa_basic(U, V, preflow);
    } else if (op_version == INNER_MAPS || op_version == "") { /* default */
      return rwa_maps(U, V, preflow);
    }  else if (op_version == INNER_SMOOTH_BASIC) {
      return rwa_smooth_basic(U, V, flow, preflow);
    } else if (op_version == INNER_SMOOTH) {
      return rwa_smooth_maps(U, V, flow, preflow);
    } else {
      throw basic_exception("Unknown inner operator for outer operator " + options.get_outer_operator() + ".");
    }
  }

 
  std::pair<PS, PS> sor_must_dispatcher(PS& Z, PS& V, const Location& loc, ScenarioLocationBundle& loc_bundle, OptionsBundle& options)
  {
    std::string op_version = options.get_inner_operator(),
                scenario   = options.get_scenario();
    
    if (op_version == INNER_BASIC) {
      return sor_basic(Z, V, loc.get_flow());
    } else if (op_version == INNER_GLOBAL) {
      return sor_global(Z, V, loc.get_flow());
    } else if (op_version == INNER_LOCAL ||
	       (op_version == "" && scenario == SCENARIO_REACHABILITY)) {
      // Default for reachability
      return sor_local(Z, V, loc.get_flow());
    } else if (op_version == INNER_LOCAL_RECYCLE ||
	       (op_version == "" && scenario == SCENARIO_SAFETY)) {
      // Default for safety, but incompatible with reachability
      if (scenario == SCENARIO_REACHABILITY)
	throw basic_exception("Cannot use local-recycle inner operator in reachability control scenario.");
      return sor_local_recycle(Z, V, loc, loc_bundle);
    } else {
      throw basic_exception("Unknown inner operator for outer operator " + options.get_outer_operator() + ".");
    }
  }


  /*
  Input:
  symb_safety - regione safe;
  loc - locazione.
  Output:
  Aggiorna C (Under_old) = PRE_c(symb_safety|loc) per la locazione  e restituisce true se è cambiato,
  false altrimenti.
  */

  bool ScenarioBundle::update_controllable_preimage(const StateSet& symbolic_states, const Location& loc)
  {
    ScenarioLocationBundle& bundle = get_location_bundle(loc);
    PS under_new(bundle.my_Under_old.space_dimension(), Parma_Polyhedra_Library::EMPTY);

    //cout << "---------------START UPDATE_CONTROLLABLE_PREIMAGE DEBUGGING-------------" << endl;


    //cout << "update_controllable_preimage, prima dell'iterazione" << endl;
    // itero su tutte le transizioni uscenti controllate della locazione
    for (auto& trans : loc.get_out_contr_transition())
    {

      // id della locazione target di questa trans
      const Location& target_loc = trans.get_target();
      // safe region della locazione target di questa transizione
      PS safe_target_loc = symbolic_states.get_region(target_loc);

      //cout << "current safe = ";
      //print_PS_regex(cout, safe_target_loc);
      //cout << endl;

      // PS safe_tloc = safe_states.get_region(t_id)
      //	std::cout<<"transizione controllata di "<<loc->get_name()<<std::endl;
      //ottengo la regione PRE(A)
      //questa funzione aggiunge il secondo PS al primo.
      PS to_be_added = trans.compute_preimage(safe_target_loc);

      //cout << "current to_be_added = ";
      //print_PS_regex(cout, to_be_added);
      //cout << endl;

      //using IO_Operators::operator<<;

      //cout << "in update_uncontrollable_preimage, to_be_added = " << to_be_added << endl << "under_new = " << under_new << endl;

      add_powerset(under_new, to_be_added);
    }

    //cout << "under_new = ";
    //print_PS_regex(cout, under_new);
    //cout << endl;
    //cout << "Dimensione di to_be_added = " << under_new.space_dimension() << endl;

    if (!are_equal(bundle.my_Under_old, under_new)) {
      bundle.my_Under_old = under_new;
      //cout << "my_Under_old = ";
      //print_PS_regex(cout, bundle.my_Under_old);
      //cout << endl;
      //cout << "---------------END UPDATE_CONTROLLABLE_PREIMAGE DEBUGGING-------------" << endl;

      return true;
    }

    //cout << "Non modifico my_Under_old." << endl;
    //cout << "---------------END UPDATE_CONTROLLABLE_PREIMAGE DEBUGGING-------------" << endl;
    return false;
  }


  /*
  Input:
  symb_safety - regione safe;
  loc - locazione.
  Output:
  Aggiorna B (B_old) = PRE_u(not(symb_safety|loc)) per la locazione e restituisce true se è cambiato,
  false altrimenti.
  */

  bool ScenarioBundle::update_uncontrollable_preimage(const StateSet& symbolic_states, const Location& loc)
  {
    ScenarioLocationBundle& bundle = get_location_bundle(loc);
    PS b_new(bundle.My_b_old.space_dimension(), Parma_Polyhedra_Library::EMPTY);

    // itero su tutte le transizioni uscenti incontrollate della locazione
    for (const Transition& trans: loc.get_out_uncontr_transition())
    {

      // locazione target di questa trans
      const Location& target_loc = trans.get_target();
      // regione safe della locazione target.
      PS safe_target_loc = symbolic_states.get_region(target_loc);

      // Nego la regione.
      negate(safe_target_loc);

      //	std::cout<<"transizione incontrollata di "<<loc->get_name()<<std::endl;

      //ottengo la regione b=PRE(not A)
      PS to_be_added = trans.compute_preimage(safe_target_loc);
      add_powerset(b_new, to_be_added);
    }

    if (!are_equal(bundle.My_b_old, b_new)) {
      bundle.My_b_old = b_new;
      return true;
    }
    return false;
  }


  /*
  Input:
  A - (parte continua di uno stato simbolico della regione safe)
  loc - locazione.
  Output:
  V = (c \/ not_inv) \ U = (c /\ A\b) \/ not_inv - Avoid region per l'operatore RWA_may.
  (è calcolata in modo che sia disgiunta dalla reach region)
  */
  PS ScenarioBundle::avoid_region(const PS& A, const Location& loc){

    //cset_cptr_t V(A->create_empty());
    PS V(A.space_dimension(), Parma_Polyhedra_Library::EMPTY);

    //cout << "---------------START AVOID_REGION DEBUGGING-------------" << endl;

    ScenarioLocationBundle& bundle = get_location_bundle(loc);
    //cset_cptr_t b(loc->get_b_old()->clone());
    PS b = bundle.My_b_old;

    //cset_cptr_t c(loc->get_Under_old()->clone());
    PS c = bundle.my_Under_old;

    //cout << "Inizialmente ho: "<< endl;
    //cout << "V = ";
    //print_PS_regex(cout, V);
    //cout << endl << "b = ";
    //print_PS_regex(cout, b);
    //cout << endl << "c = ";
    //print_PS_regex(cout, c);
    //cout << endl;


    //Calcolo A\b
    //cset_cptr_t A1(A->clone());
    PS A1(A);

    // Riga successiva al commento identica all'originale, le funzioni utilizzate sono le stesse.
    // Sostituito solo l'operatore "->" con "." .
    if(!b.is_empty()) A1.difference_assign(b);

    //c /\ A\b
    c.intersection_assign(A1);

    //NOT_inv

    //cset_cptr_t not_inv(A->create_empty());
    PS not_inv(A.space_dimension(), Parma_Polyhedra_Library::EMPTY);


    //if(loc->get_not_inv()->is_empty())
    if(bundle.my_not_inv.is_empty())
    {
      PS loc_inv =  loc.get_invariant();
      not_inv = loc_inv;
      negate(not_inv);
      bundle.my_not_inv = not_inv;
    }
    else not_inv = bundle.my_not_inv;


    LOGGER(DEBUG, "avoid_region", "calcolo V");
    //V= (c /\ A\b) \/ not_inv
    // COMMENTATO PER IL DEBUGGING POICHE' L'HO CREATO VUOTO SOPRA.
    //PS V(c);
    V = c;

    add_powerset(V, not_inv);
    fast_pairwise_reduce(V);

    return V;
  }


  /*
  Input:
  A - (parte continua di uno stato simbolico della regione safe)
  loc - locazione.
  Output:
  Z = not_U = not_inv \/ (A \ b) - Stay region per l'operatore SOR_must.
  (è il complemento della reach_region)
  */
  PS ScenarioBundle::stay_region(const PS& A, const Location& loc){

    LOGGER(DEBUG, "stay_region", "calcolo Z per sor_must");

    PS Z(A.space_dimension(), Parma_Polyhedra_Library::EMPTY);

    ScenarioLocationBundle& bundle = get_location_bundle(loc);
    PS b = bundle.My_b_old;


    if(!bundle.my_location_initialized){

      //Z= not_inv \/ (a \ b)
      //NOT_INV
      PS not_inv(A.space_dimension(), Parma_Polyhedra_Library::EMPTY);

      if(bundle.my_not_inv.is_empty()){

        PS loc_inv = loc.get_invariant();
        not_inv = loc_inv;
        negate(not_inv);
        bundle.my_not_inv = not_inv;
      }
      else not_inv = bundle.my_not_inv;

      Z = A;
      if(!b.is_empty()) Z.difference_assign(b);

      add_powerset(Z, not_inv);

      bundle.my_location_initialized = true;
    } else {
      //Z= loc.get_U_old \ b
      Z = bundle.my_U_old;

      if(!b.is_empty()) Z.difference_assign(b);
    }

    fast_pairwise_reduce(Z);
    return Z;
  }


  /*
  Input:
     Z   - stay region
     V   - reach region
     loc - location
  Output:
     SOR_must(Z,V)
  */
  PS ScenarioBundle::SOR_must(PS& Z, PS& V, const Location& loc, OptionsBundle& options)
  {
    LOGGER(DEBUG, "SOR_must", "SOR_must");

    //clock per cacolare il tempo impiegato da SORMt nell'algoritmo
    clock_t start,end;
    start=clock();

    // Utilizzato per modificare ScenarioLocationBundle della locazione loc.
    ScenarioLocationBundle& loc_bundle = get_location_bundle(loc);

    PS result(Z.space_dimension(), Parma_Polyhedra_Library::EMPTY);

    //riduzione di vincoli di Z e V
    simplify(Z);
    simplify(V);

    std::pair<PS, PS> ret_set_map = sor_must_dispatcher(Z, V, loc, loc_bundle, options);

    //imposta il risultato calcolato nell'attributo U_old della locazione e nel powerset result
    loc_bundle.set_my_U_old(ret_set_map.first);
    result = ret_set_map.first;

    end=clock();
    // Variabili globali commentate da Urbano. Verranno decommentate in fase di esecuzione.
    //tempo_SORmust+=((double)(end-start))/CLOCKS_PER_SEC;

    return result;
  }


  

  /*
  Input:
     A - (parte continua di uno stato simbolico della regione safe)
     loc - locazione.
  Output:
     U = ((not_A \/ b) /\ inv) - Reach region per l'operatore RWA_may.
  */
  PS ScenarioBundle::reach_region(const PS& A, const Location& loc)
  {
    //cset_cptr_t U(A->create_empty());
    PS U(A.space_dimension(), Parma_Polyhedra_Library::EMPTY);

    ScenarioLocationBundle& loc_bundle = get_location_bundle(loc);

    //cset_cptr_t b(loc->get_b_old()->clone());
    PS b(loc_bundle.My_b_old);


    LOGGER(DEBUG, "reach_region", "calcolo U per rwa");


    //if(!loc->is_location_initialized())
    if(!loc_bundle.my_location_initialized)
    {
      //U = (not_A \/ b) /\ inv
      //INV

      PS invariant(loc.get_invariant());

      //not_A
      PS not_A(A);
      negate(not_A);

      //U
      U = not_A;

      if(!b.is_empty()) add_powerset(U, b);
      U.intersection_assign(invariant);

      loc_bundle.my_location_initialized = true;
    }
    else {
      //U= loc.get_U_old \/ b
      U = loc_bundle.my_U_old;
      if(!b.is_empty()) add_powerset(U, b);
    }

    fast_pairwise_reduce(U);
    return U;
  }




  /*
  Input:
  U - reach region;
  V - avoid region;
  loc - location;
  op_version - desired version of RWA
  Note:   U e V should be disjoint.
  Output:
  RWA_may(U,V).
  */
  std::pair<PS, PS> ScenarioBundle::RWA_may(PS& U, PS& V, const Location& loc, OptionsBundle& options)
  {
    LOGGER(DEBUG, "RWA_may", "RWA_may");
      
    // ScenarioLocationBundle& utilizzato per la modifica degli attributi.
    ScenarioLocationBundle& loc_bundle = get_location_bundle(loc);

    Poly flow(loc.get_flow());
    Poly preflow(flow);
    get_prepoly(preflow);

    //riduzione di vincoli di U e V
    simplify(U);
    simplify(V);
    fast_pairwise_reduce(U);
    fast_pairwise_reduce(V);

    PS result_ps;

    //richiamo la versione di rwa in base al parametro passato in input
    //cout << "operator-version: " << synth_opt->get_operator_version() << endl;
    /*
    cout << "U before invoking rwa operators = ";
    print_PS_regex(cout, U);
    cout << endl;
    cout << "V before invoking rwa operators = ";
    print_PS_regex(cout, V);
    cout << endl;
    cout << "preflow before invoking rwa operators = ";
    print_poly(cout, preflow);
    cout << endl;
    cout << "flow before invoking rwa operators = ";
    print_poly(cout, flow);
    cout << endl;
    */
    
    std::pair<PS, PS> rwa_res = rwa_may_dispatcher(U, V, flow, preflow, options);

    // DEBUGGING
    /*
    cout << "rwa_res.first = ";
    print_PS_regex(cout, rwa_res.first);
    cout << endl;
    cout << "rwa_res.second = ";

    cout << rwa_res.second << endl;
    print_PS_regex(cout, rwa_res.second);
    cout << endl; */

    //using IO_Operators::operator<<;
    //cout << "rwa_res.first = " << rwa_res.first << endl << "rwa_res.second = " << rwa_res.second << endl;

    result_ps = rwa_res.first;

    //imposta il risultato calcolato nell'attributo U_old della locazione e nel powerset result
    loc_bundle.my_U_old = result_ps;

    PS result(U.space_dimension(), Parma_Polyhedra_Library::EMPTY);
    PS complement(U.space_dimension(), Parma_Polyhedra_Library::EMPTY);

    result = result_ps;
    complement = rwa_res.second;

    // cout << "---------------------- END RWA_MAY DEBUGGING -----------------------------" << endl;

    return std::make_pair(result, complement);
  }



  /*
  Input:
  A - (insieme continuo di uno stato simbolico della regione safe)
  loc - riferimento a locazione
  Output:
  V = B \ A - Avoid region
  */
  PS ScenarioBundle::reachability_avoid_region(PS& A, const Location& loc){

    LOGGER(DEBUG, "reachability_avoid_region", "Start");

    ScenarioLocationBundle& scen_bundle = get_location_bundle(loc);

    // V = B \ A
    PS V(scen_bundle.get_My_b_old());
    V.difference_assign(A);

    simplify(V);
    fast_pairwise_reduce(V);

    return V;
  }




  /*
  Input:
    A   - (insieme continuo di uno stato simbolico della regione safe)
    loc - riferimento alla locazione.
  Output:
    U = A \/ (C \ B) \/ not_inv - First argument for RWA
  */
  PS ScenarioBundle::reachability_reach_region(PS& A, const Location& loc)
  {
    LOGGER(DEBUG, "reachability_reach_region", "Start");

    ScenarioLocationBundle& loc_bundle(get_location_bundle(loc));

    PS not_inv;
    PS U(A);

    //C \ B
    PS c(loc_bundle.my_Under_old);
    PS b(loc_bundle.My_b_old);
    c.difference_assign(b);

    simplify(c);   // URBANO
    fast_pairwise_reduce(c);

    //((not_inv)
    if(!loc_bundle.my_not_inv.is_empty())
      not_inv = loc_bundle.my_not_inv;
    else {
      PS invariant(loc.get_invariant());
      not_inv = invariant;
      negate(not_inv);
      loc_bundle.my_not_inv = not_inv;
    }

    // A \/ (C \ B) \/ not_inv
    add_powerset(U, c);
    add_powerset(U, not_inv);
    simplify(U);   // URBANO
    fast_pairwise_reduce(U);

    return U;
  }


  /*
    Computes RWA^Must(U, V). Useful for reachability control.

    Input:
      U   - reach region;
      V   - avoid region;
      loc - location;
      synth_opt - synthesis options

    Precondition: U and V must be disjoint

    Output:
      - via SOR_must: Over(U,V) /\ SOR_must(Over(U,V),U)
      - via RWA_may:
         - versione non ottimizzata: Over(U,V) \ RWA_may(not_Over(U,V),U)
         - versione ottimizzata:     Over(U,V) /\ (U \/ Cut)
  */
  PS ScenarioBundle::RWA_must(PS& U, PS& V, const Location& loc, OptionsBundle& options)
  {
    //clock per cacolare il tempo impiegato da RWA_must nell'algoritmo
    clock_t start,end,s_over,e_over;
    start=clock();

    std::pair<PS, PS> ret_set_map;
    std::pair<PS, PS> over_pair;

    PS good(U.space_dimension(), Parma_Polyhedra_Library::EMPTY);

    // Compute Over and not_Over
    if (options.get_over() == OVER_LARGE)
      over_pair = over_large_approx(U, V, loc);
    else /* default */
      over_pair = over_small_approx(U, V, loc);

    PS over(over_pair.first), not_over(over_pair.second);
    PS result(over);

    if (options.get_outer_operator() == OUTER_RWA_RWA) {
      // rwa_must via rwa_may
      LOGGER(DEBUG, "RWA_must", "RWA_must via RWA_may");
	
      PS cut(U.space_dimension(), Parma_Polyhedra_Library::EMPTY);
      PS U1(U);

      Poly flow(loc.get_flow());
      Poly preflow(flow);
      get_prepoly(preflow);

      simplify(not_over);
      simplify(U);

      // Nota: Per la versione smooth_basic di rwa non è disponibile il calcolo ottimizzato di RWA_must
      ret_set_map = rwa_may_dispatcher(not_over, U, flow, preflow, options);

      //Per la versione cd_naive non è disponibile il calcolo ottimizzato di rwa_must
      //if(synth_opt->get_opt_version() == "true" && op_computation != "rwa_cd_naive")
      // CONSIDERA L'AGGIUNTA DI OPT_VERSION CHE SEGNALA LA VERSIONE OTTIMIZZATA.
      if (options.get_inner_operator() != INNER_SMOOTH_BASIC) {
        cut = ret_set_map.second;
        if (!cut.is_empty()) add_powerset(U1, cut);
        simplify(U1);
        result.intersection_assign(U1);
      } else {
        good = ret_set_map.first;
        //std::cout<<"\ngood: "<<good<<std::endl;

        // ISTRUZIONE COMMENTATA DA URBANO DA CONTROLLARE.
        //if(good->is_universe()) good->embed_variables(result->get_variable_ids());

        simplify(good);
        result.difference_assign(good);

        //std::cout<<"\nresult: "<<result<<std::endl;
        simplify(result);
      }

    } else {
      // rwa_must via sor_must (default)
      LOGGER(DEBUG, "RWA_must", "RWA_must via SOR_must");
      
      simplify(over);
      simplify(U);

      ret_set_map = sor_must_dispatcher(over, U, loc, get_location_bundle(loc), options);
      
      // L'INVOCAZIONE A get_sorM_LocalPlus ERA GIA' COMMENTATA NEL CODICE "ORIGINALE".
      //else if(op_computation == "sorm_localplus") {
      //ret_set_map=get_sorM_LocalPlus(over->get_powerset(),U->get_powerset(),loc);
      //}

      good = ret_set_map.first;
      result.intersection_assign(good);
    }

    simplify(result); // URBANO.
    fast_pairwise_reduce(result);

    return result;
  }



  /*
  Input:
  A - (insieme continuo di uno stato simbolico della regione safe)
  loc - locazione.
  Output:
  Z = not_U = not_A /\ (not_C \/ B) /\ inv - Stay region per l'operatore SOR.
  */
  PS ScenarioBundle::reachability_stay_region(PS& A, const Location& loc)
  {
    LOGGER(DEBUG, "reachability_stay_region", "Start");

    //ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr Z, not_c, inv;
    // DICHIARATI QUANDO UTILIZZATI.

    ScenarioLocationBundle& loc_bundle(get_location_bundle(loc));

    //not_A
    PS Z(A);
    negate(Z);

    //(not_C \/ B)
    PS not_c(loc_bundle.my_Under_old);
    negate(not_c);
    add_powerset(not_c, loc_bundle.My_b_old);

    PS invariant(loc.get_invariant());

    //not_A /\ (not_C \/ B) /\ inv
    Z.intersection_assign(not_c);
    Z.intersection_assign(invariant);

    return Z;
  }


  /*
    Input:
       Z   - stay region;
       V   - reach region;
       loc - location;
       synth_opt - opzioni di sintesi.
    Output:
    SOR_may(Z,V):
    - tramite RWA_may: not_over(not_Z,V) \/ RWA_may(not_over(not_Z,V),not_Z)
    [versione ottimizzata e non ottimizzata differiscono per il calcolo di not_over]
    - tramite SOR_must:
       - versione non ottimizzata: not_over(not_Z,V) \/ not_SOR_must(over(not_Z,V),not_Z)
       - versione ottimizzata:     not_over(not_Z,V) \/ Cut
  */
  PS ScenarioBundle::SOR_may(PS& Z, PS& V, const Location& loc, OptionsBundle& options)
  {
    //clock per calcolare il tempo impiegato da SOR_may nell'algoritmo
    clock_t start,end,s_over,e_over;
    start=clock();

    std::pair<PS, PS> ret_set_map;

    pair<PS, PS> over_pair;
    PS result;

    PS good(Z.space_dimension(), Parma_Polyhedra_Library::EMPTY);

    PS not_Z = Z;
    negate(not_Z);
    fast_pairwise_reduce(not_Z);
    
    // Compute Over and not_Over
    if (options.get_over() == OVER_LARGE)
      over_pair = over_large_approx(not_Z, V, loc);
    else /* default */
      over_pair = over_small_approx(not_Z, V, loc);

    // Warning: There is an error in computation of not_over
    PS over     = over_pair.first,
       not_over = over_pair.second;

    // not_over = over;
    // negate(not_over);

    if(options.get_outer_operator() == OUTER_SOR_SOR) {
      LOGGER(DEBUG, "SOR_may", "sor_may via sor_must");
      // result = not_over \/ cut (see Eramo's thesis, page 72)
      
      result = not_over;
      ret_set_map = sor_must_dispatcher(over, not_Z, loc, get_location_bundle(loc), options);
      PS cut = ret_set_map.second;
      add_powerset(result, cut);     

    } else {
      LOGGER(DEBUG, "SOR_may", "sor_may via rwa_may");
      // result = not_over \/ rwa_may(not_over, not_Z) (see Eramo's thesis, page 69)
      
      result = not_over;      
      Poly flow(loc.get_flow());
      Poly preflow(flow);
      get_prepoly(preflow);
      ret_set_map = rwa_may_dispatcher(not_over, not_Z, flow, preflow, options);
      PS good = ret_set_map.first;
      //std::cout<<"\ngood: "<<good<<std::endl;
      
      // ----------------------- ANALIZZA LA RIGA DI CODICE TRA QUESTO PATTERN --------------- //
      //if(good.is_universe()) good->embed_variables(result->get_variable_ids());
      // ----------------------- ANALIZZA LA RIGA DI CODICE TRA QUESTO PATTERN --------------- //
      
      add_powerset(result, good);
      
      //std::cout<<"\n result: "<<result<<std::endl;
    }
    
    fast_pairwise_reduce(result);
    // std::cout<<"\n result dopo il pairwise: "<<result<<std::endl;
    
    end=clock();
    //tempo_SORmay COMMENTATA DA URBANO. SARA' DECOMMENTATA ALLA FINE.
    //tempo_SORmay+=((double)(end-start))/CLOCKS_PER_SEC;
    
    return result;
  }
  
  


}
