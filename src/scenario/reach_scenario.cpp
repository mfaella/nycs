/*
 *  File: ReachScenario.cpp
 *  Created on: September 08, 2017
 *  Author: Marco Urbano
 */

#include "reach_scenario.h"
#include "../main/option_names.h"
#include "../algorithms/over_approximations.h"
#include "../main/signal_handlers.h"

using namespace std;

namespace NaPoly{

/* Constructor that simply calls the superclass one and
   instantiate the bundle attribute. */
ReachScenario::ReachScenario(Automaton my_aut, OptionsBundle my_bundle)
                            : Scenario(my_aut, my_bundle){}

StateSet ReachScenario::get_reached_states(){
  return reached_states;
}

void ReachScenario::launch() {

  int max_seconds = options.get_timer();
  int max_it = options.get_iter_max();
  StateSet reach_states = options.get_goal();

/* hybrid_automata::canonicalize_location_constraints(*reach_states);
   reach_states = hybrid_automata::intersect_with_invariants_powerset(
                                                       *reach_states, *aut_net);
*/

//     ---------------------- DEBUG ----------------------             //
//	  std::cout << "reach_states:" << *reach_states << std::endl;
//		std::cout << "init_states:" << *init_states << std::endl;
//     ---------------------- DEBUG ----------------------             //

  StateSet invariants_set = aut.get_invariant();
  reach_states.intersection_assign(invariants_set);
  bool fixpoint = false;
  StateSet final_reach_states, new_reach(reach_states);  
  ScenarioBundle bundle(aut);

  // If the user has set a timeout then set an alarm.
  if (max_seconds > 0) 
    alarm(max_seconds);
  
  int j=0;
  clock_t start,end;
  start=clock();
  
  while(!fixpoint && (max_it==0 || j<max_it)) {
    final_reach_states = new_reach;
    std::cout << endl << " Iteration " << ++j << flush;
    new_reach = cpre_reach(final_reach_states, bundle, fixpoint);
    // cpre->union_assign(final_reach_states);

    // Set current State_set to be visible to the interruption handler
    set_current_stateset(&new_reach);
  }
  
  cout << endl;
  // end=clock();
  // tempo_reach+=((double)(end-start))/CLOCKS_PER_SEC;
  
  reached_states = final_reach_states;
  
  /*
    std::cout << "\n Total time for computation of RWAmust: "
    << tempo_RWAmust << std::endl;
std::cout << "Total time for computation of SORmay: "
         << tempo_SORmay << std::endl;
std::cout << "Total time for computation of Reach: "
         << tempo_reach << std::endl;
std::cout << "Total time for cross: "
         << tempo_cross << std::endl;
std::cout << "Total time for Reach_Cross: "
         << tempo_reach_cross <<std::endl;
std::cout << "Total time for computation of Over: "
         << tempo_over << std::endl;
std::cout << "final reach_states:" << *final_reach_states
         << std::endl;

comp_reach_states=complement_states(final_reach_states);
std::cout << "final reach_states:" << *comp_reach_states
         << std::endl;
final_reach_states->intersection_assign(init_states);
std::cout << "final reach_states:" << *final_reach_states
         << std::endl;
comp_reach_states=adapt_to_NNC(comp_reach_states);
std::cout << "final reach_states:" << *final_reach_states
         << std::endl;

LOGGERSW(LOW, "execute_reach", "Output of reach states");

for (unsigned int i = 0; i< of.size(); ++i)
of[i]->output(*final_reach_states);
*/

}

  /* Notice: fixpoint is an output parameter */
StateSet ReachScenario::cpre_reach(StateSet& reach_states,
                                   ScenarioBundle& scen_bundle, bool& fixpoint) {

  PS add;
  string outer_op = options.get_outer_operator();
  StateSet result;
  fixpoint = true;

  LOGGER(DEBUG, "cpre_reach", "Inizio iterazioni");

  // for each pair (location, set of valuations) in reach_states
  for (auto symb_state : reach_states.get_map()) {
      PS A = symb_state.second;
      const Location& loc = symb_state.first;
      ScenarioLocationBundle& loc_bundle =
	scen_bundle.get_location_bundle(loc);
      PS A1(A);

      //CALCOLO DEI PARAMETRI c, b
      bool change_c = scen_bundle.update_controllable_preimage(reach_states, loc);
      bool change_b = scen_bundle.update_uncontrollable_preimage(reach_states, loc);
      fast_pairwise_reduce(loc_bundle.my_Under_old);
      fast_pairwise_reduce(loc_bundle.My_b_old);

      // std::cout<<"\ncalcolati c e b per la locazione: "<<loc->get_name()<<std::endl;
      // std::cout<<"c: "<<loc->get_Under_old()<<std::endl;
      // std::cout<<"b: "<<loc->get_b_old()<<std::endl;

      if (change_c || change_b) {
	fixpoint = false;

	PS V = scen_bundle.reachability_avoid_region(A1, loc);

	if (outer_op == OUTER_SOR_SOR || outer_op == OUTER_SOR_RWA) {
	  PS Z = scen_bundle.reachability_stay_region(A1, loc);
	  add = scen_bundle.SOR_may(Z, V, loc, options);
	  negate(add);
	} else { /* default */
	  PS U = scen_bundle.reachability_reach_region(A1, loc);
	  add = scen_bundle.RWA_must(U, V, loc, options);
	}

	//SE add NON E' VUOTO L'AGGIUNGE ALLA VECCHIA REGIONE A
	if(!add.is_empty()) {
	  add.intersection_assign(loc.get_invariant());
	  add_powerset(A1, add);
	  simplify(A1); // URBANO.
	  fast_pairwise_reduce(A1);
	}
      }

      PS cset(A1);
      result.add(loc, cset);
    }

  return result;
}


}
