/*
 *  File: Scenario.cpp
 *  Created on: August 13, 2017
 *  Author: Marco Urbano
 */


#include "Scenario.h"

using namespace std;

namespace NaPoly{

Scenario::Scenario(Automaton the_aut, OptionsBundle the_options): aut{the_aut}, options{the_options}{};

void Scenario::print_info(){
  cout << "Safety scenario con le seguenti componenti" << endl;
  cout << "Automa: " << endl;
  cout << aut << endl;
  cout << "OptionsBundle: " << endl;
  cout << options << endl;
}

}
