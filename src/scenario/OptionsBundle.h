/*
*  File: OptionsBundle.h
*  Created on: July 30, 2017
*  Author: Marco Urbano
*/

#ifndef __OptionsBundle_H__
#define __OptionsBundle_H__



#include "../automaton/preprocessor.h"
#include "../automaton/State_set.h"
#include <string>


using namespace std;


namespace NaPoly{


  class OptionsBundle{
    friend class Parser;
  private:
    string aut_name, scenario, outer_operator, inner_operator, over, verbosity;
    StateSet goal; // The goal means Reach if scenario is "reach_control" and Safe if scenario is "safety_control".
    StateSet initially;
    int iter_max;
    int timer;

  public:

    // Constructors
    OptionsBundle();
    OptionsBundle(string name, string my_scenario, string op_version, string rwa_sor, string verbosity_lev, string my_over, int my_iter_max, int my_timer, StateSet my_goal, StateSet init_states);

    // Destructor
    ~OptionsBundle();

    // Copy constructor (if needed)
    OptionsBundle(const OptionsBundle& bundle);

    // Getters
    string get_aut_name();
    string get_scenario();
    string get_outer_operator();
    string get_inner_operator();
    string get_over();
    StateSet get_goal();
    StateSet get_init();
    string get_verbosity();
    int get_iter_max();
    int get_timer();


    /* Setters has not been written because all the write operations on an OptionsBundle
    are done only in the construction phase. The OptionsBundle is built only from the parser
    so the parser has been declarated as FRIEND class of OptionsBundle class.
    */

    // Overloaded << operator.
    friend ostream& operator<<(ostream& os, OptionsBundle opt);
  };

  // Forward declaration ( look for the .cpp file).
  ostream& operator<<(ostream& os, OptionsBundle opt);


}

#endif
