/*
 * nonblocking_option.cpp
 *
 *  Created on: 04/ago/2014
 *      Author: donatella
 */

#include "nonblocking_check.h"

double tempo_check=0;

namespace options {

bool execute_nonblocking_check(hybrid_automata::hybrid_automaton::ptr aut_net,
		options::synthesis_options* synth_opt) {

	LOGGER(DEBUG, "execute_nonblocking_check", "Start");
	clock_t start,end;
	start=clock();

	hybrid_automata::automaton_synthesis* aut = dynamic_cast<hybrid_automata::hybrid_automaton*>(aut_net.get());
	std::pair<hybrid_automata::automaton_synthesis::location_const_iterator,
	hybrid_automata::automaton_synthesis::location_const_iterator> ploc = aut->get_locations();
	std::pair<hybrid_automata::automaton_synthesis::transition_const_iterator,
	hybrid_automata::automaton_synthesis::transition_const_iterator> ptrans;
	hash_map<hybrid_automata::location_id, ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr> blocking_states;

	//Iterazione su tutte le locazioni dell'automa
	for (hybrid_automata::automaton_synthesis::location_const_iterator loc_id = ploc.first;
			loc_id != ploc.second; loc_id++) {

		const hybrid_automata::location_synthesis* loc = aut->get_location_synthesis(*loc_id);
		loc->initialize();
		hybrid_automata::time_constraints tcons = loc->get_time_constraints();
//		std::cout<<"\nlocazione: "<<loc->get_name()<<std::endl;

		//Recupero l'invariante e il flow della locazione
		const ppl_polyhedron::continuous_set_PPL_Powerset* tmp =
				dynamic_cast<const ppl_polyhedron::continuous_set_PPL_Powerset*>(tcons.get_invariant().get());
		const ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr inv(tmp->clone());
		inv->embed_variables(aut->get_variable_ids());
		continuous::continuous_set::ptr invcset(inv->clone());
//		std::cout<<"\tinvariante: "<<inv<<std::endl;

		const continuous::relation_dynamics* dp =
				dynamic_cast<const continuous::relation_dynamics*>(tcons.get_dynamics().get());
		const ppl_polyhedron::continuous_set_PPL_Powerset* tmp1 =
				dynamic_cast<const ppl_polyhedron::continuous_set_PPL_Powerset*> (dp->get_relation().get());
		const ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr flow(tmp1->clone());

		//Se l'invariante della locazione è "true", essa è sicuramente non bloccante.
		if(!inv->is_universe()){

			//Calcolo la chiusura topologica del flow
			PS flow_ps = flow->get_powerset();
			//		std::cout<<"flow_ps - dimensione:  "<<flow_ps.space_dimension()<<std::endl;
			Poly flow_poly = flow_ps.begin()->pointset();
			flow_poly.topological_closure_assign();

			//Calcolo il flow negato, per il calcolo del "positive pre-flow"
			ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr not_flow(flow->clone());
			not_flow->decrease_primedness();
			not_flow->get_prepoly();
			PS not_flow_ps = not_flow->get_powerset();
			//		std::cout<<"not_flow_ps - dimensione:  "<<not_flow_ps.space_dimension()<<std::endl;
			Poly not_flow_poly = not_flow_ps.begin()->pointset();

			ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr x_one =
					ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr(inv->create_empty());

			Poly p_nnc;
			PS x1(x_one->get_dim(),EMPTY);
			PS inv_ps(inv->get_powerset());
			//Iterazione su ogni poliedro convesso dell'invariante
			for (PS::const_iterator it = inv_ps.begin(); it != inv_ps.end(); it++) {
				p_nnc = it->pointset();
				if (!is_bounded(p_nnc, flow_poly)){
					Poly pos_preflow = positive_time_elapse(p_nnc, not_flow_poly);
					pos_preflow.intersection_assign(p_nnc);
					x1.add_disjunct(pos_preflow);
				}
			}
			x1.pairwise_reduce();
			x_one->set_powerset(x1);
			//		std::cout<<"\ncalcolato x_one - dimensione: "<<x_one->get_dim()<<std::endl;

			ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr x_two =
					ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr(inv->create_empty());
			//		std::cout<<"x_two - dimensione:  "<<x_two->get_dim()<<std::endl;

			ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr guard =
					ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr(inv->create_empty());;
			//Iterazione su tutte le transizioni non controllabili uscenti
			hybrid_automata::label_id_set lab_set = aut->get_labels();
			for (hybrid_automata::label_id_set::const_iterator lab_it = lab_set.begin();
					lab_it != lab_set.end(); ++lab_it) {
				ptrans = aut->get_outgoing_transitions(*loc_id, *lab_it);
				for (hybrid_automata::automaton_synthesis::transition_const_iterator trans_id = ptrans.first;
						trans_id != ptrans.second; trans_id++) {
					hybrid_automata::transition_ptr trans = aut->get_transition(*trans_id);
					guard = ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr(
							trans->get_jump_constraints().get_guard().get());
					guard->intersection_assign(inv);
					x_two->union_assign(guard);
				}
			}
			//		std::cout<<"\ncalcolato x_two - dimensione: "<<x_two->get_dim()<<std::endl;
			x_one->union_assign(x_two);
			//		std::cout<<"\ncalcolato x_one unito x_two - dimensione: "<<x_one->get_dim()<<std::endl;

			ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr not_inv =
					ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr(inv->clone());
			not_inv->negate();
			ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr result =
					ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr(inv->create_empty());

			if(synth_opt->get_operator_version() == "rwa_cd_naive" ||
			   synth_opt->get_operator_version() == "rwa_cd_map" ||
			   synth_opt->get_operator_version() == "rwa_ce_naive" ||
			   synth_opt->get_operator_version() == "rwa_ce_map"){
			  result = RWA_may(x_one,not_inv,loc,synth_opt).first;
			} else {
				ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr not_xone =
						ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr(x_one->clone());
				not_xone->negate();
				ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr cut =
						ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr(inv->create_empty());
				std::pair<PS, PS> ret_set_map;

				//richiamo la versione di sorm in base al parametro passato in input
				if(synth_opt->get_operator_version()=="sorm_basic"){
					ret_set_map=get_sorM_Basic(not_xone->get_powerset(),not_inv->get_powerset(),loc);
				} else if(synth_opt->get_operator_version()=="sorm_global"){
					ret_set_map=get_sorM_Global(not_xone->get_powerset(),not_inv->get_powerset(),loc);
				} else if(synth_opt->get_operator_version()=="sorm_local"){
					ret_set_map=get_sorM_Local(not_xone->get_powerset(),not_inv->get_powerset(),loc);
				} else if(synth_opt->get_operator_version()=="sorm_localplus"){
					ret_set_map=get_sorM_LocalPlus(not_xone->get_powerset(),not_inv->get_powerset(),loc);
				}

				if(synth_opt->get_opt_version() == "false") {
					result->set_powerset(ret_set_map.first);
					result->negate();
				} else {
					cut->set_powerset(ret_set_map.second);
					result = x_one;
					result->intersection_assign(inv);
					result->union_assign(cut);
				}
			}

			ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr cset =
					ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr(inv->clone());
			cset->difference_assign(result);
			if (!cset->is_empty()) blocking_states[(aut->get_location_id(loc->get_name()))] = cset;

		}
	}

	end=clock();
	tempo_check+=((double)(end-start))/CLOCKS_PER_SEC;
	std::cout<<"\ntempo impiegato: "<<tempo_check<<std::endl;


	if(!blocking_states.empty()){
		std::cout<<"\nAutoma bloccante!"<<std::endl;
		std::cout<<"\nGli stati bloccanti sono i seguenti: "<<std::endl;
		for(hash_map<hybrid_automata::location_id,
				ppl_polyhedron::continuous_set_PPL_Powerset::const_ptr>::const_iterator it = blocking_states.begin();
				it != blocking_states.end(); ++it){
			std::cout<<"\n{ "<<aut->get_location(it->first)->get_name()<< ", "<<it->second<< " }"<<std::endl;
		}
		return false;
	} else std::cout<<"\nL'automa è non bloccante!"<<std::endl;
	return true;

}

}
