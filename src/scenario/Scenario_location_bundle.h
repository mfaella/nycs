/*
 *  File: ScenarioLocationBundle.h
 *  Created on: August 13, 2017
 *  Author: Marco Urbano
*/


#ifndef _SCENARIO_LOC_BUNDLE_H__
#define _SCENARIO_LOC_BUNDLE_H__

#include <ext/hash_map>
#include "../algorithms/rwa/rwa_map_definition.h" // To include hash_map_type, list_nnc_type and NNC_PolyhedronTraits_loc definitions.
#include "../automaton/preprocessor.h"
#include "../automaton/Variable.h" // To invoke Variable.getDimension() static method, used to build empty PS.
#include "../automaton/Automaton.h"


using namespace std;

namespace NaPoly{

  class ScenarioLocationBundle{

    /* Integration testing classes, to be removed before release*/
     friend class SafetyScenarioTest;
    /* Integration testing classes, to be removed before release*/

    friend class ScenarioBundle;
    friend class SafetyScenario;
    friend class ReachScenario;
  private:
    PS my_U_old;
    PS my_Under_old;
    PS My_b_old;
    PS my_Under_eff;
    PS my_not_inv;
    bool my_location_initialized;
    bool my_initialized_map_sorm;

  public:
    // hash_map_type is defined as : typedef hash_map<NNC_Polyhedron*, list_nnc_type, NNC_PolyhedronTraits_loc, NNC_PolyhedronTraits_loc> hash_map_type;
    // list_nnc_type is defined as : typedef list<NNC_Polyhedron*> list_nnc_type.

    hash_map_type extadj;
    hash_map_type rradj;
    hash_map_u_type ruadj;


    // Constructor builds empty ScenarioLocationBundle.
    ScenarioLocationBundle();
    // Copy constructor.
    ScenarioLocationBundle(const ScenarioLocationBundle& to_copy);



    //bool update_controllable_preimage(const Automaton& aut, const StateSet& symb_safety);


    // Getters and Setters
    PS& get_my_U_old();
    PS& get_my_Under_old();
    PS& get_My_b_old();
    PS& get_my_Under_eff();
    PS& get_my_not_inv();

    bool& get_my_location_initialized();
    bool& get_my_initialized_map_sorm();

    hash_map_type& get_extadj();
    hash_map_type& get_rradj();
    hash_map_u_type& get_ruadj();


    void set_my_U_old(PS U_old);
    void set_my_Under_old(PS Under_old);
    void set_My_b_old(PS b_old);
    void set_my_Under_eff(PS Under_eff);
    void set_my_not_inv(PS not_inv);

    void set_my_location_initialized(bool location_initialized);
    void set_my_initialized_map_sorm(bool initialized_map_sorm);

    void set_extadj(hash_map_type new_extadj);
    void set_rradj(hash_map_type new_rradj);
    void set_ruadj(hash_map_u_type new_ruadj);



  };



}






#endif
