/*
 *  File: Scenario.h
 *  Created on: August 13, 2017
 *  Author: Marco Urbano
 */

#include "../automaton/preprocessor.h"
#include "../automaton/Automaton.h"
#include "../scenario/OptionsBundle.h"
#include "../io/Parser.h"


#ifndef _SCENARIO_NAPOLY__
#define _SCENARIO_NAPOLY__


using namespace std;

namespace NaPoly{

 /* Forward declarations */
 /* Forward declarations */

 class Scenario {
 protected:
   Automaton aut;
   OptionsBundle options;
   
 public:
   Scenario(Automaton the_automaton, OptionsBundle the_options);
   
   void print_info();
   // This virtual method makes this class abstract.
   virtual void launch() = 0;
 };





}


















#endif /* _SCENARIO_NAPOLY__ */
