/*
 * nonblocking_option.h
 *
 *  Created on: 04/ago/2014
 *      Author: donatella
 */

#ifndef NONBLOCKING_CHECK_H_
#define NONBLOCKING_CHECK_H_

#include "synthesis_options.h"
#include "synthesis/hybrid_automata/automaton_synthesis.h"
#include "synthesis/post_operators/rwa/rwa_operations.h"
#include "synthesis/post_operators/rwa/differentiable/time_elapse.h"
//#include "core/continuous/polyhedra/ppl_polyhedron/continuous_set_PPL_NNC.h"
#include "synthesis/continuous/continuous_set_PPL_Powerset.h"
#include "synthesis/post_operators/synthesis/cpre_safety.h"
#include "core/hybrid_automata/explicit_transition.h"
#include "core/discrete/discrete_set_stl_set.h"

using namespace Parma_Polyhedra_Library;

// Lo spazio di nomi options è da eliminare.
namespace options {

/* Prende in input l'automa
 * Restituisce true se l'automa è non-blocking, false altrimenti.
 */
bool execute_nonblocking_check(hybrid_automata::hybrid_automaton::ptr aut_net,
		options::synthesis_options* synth_opt);

}

#endif /* NONBLOCKING_CHECK_H_ */
