/*
*  File: Safety_scenario.h
*  Created on: August 13, 2017
*  Author: Marco Urbano
*/


#ifndef _SAFETY_SCENARIO_NAPOLY__
#define _SAFETY_SCENARIO_NAPOLY__


#include "Scenario.h"
#include "scenario_bundle.h"
#include "../automaton/Automaton.h"
#include "../automaton/Location.h"
#include "../automaton/State_set.h"

using namespace std;

namespace NaPoly{

  /* Forward declaration */
  class Automaton;

  
  class SafetyScenario : public Scenario {
  private:

    // Winning region obtained from launch
    StateSet safe_region;

    // Solves the one-step controllability problem
    StateSet cpre_safety(StateSet& symb_safety, ScenarioBundle& scen_bundle, bool& fixpoint);

  public:
    SafetyScenario(Automaton my_aut, OptionsBundle my_bundle);

    // Solves the safety controllability problem
    void launch();

    // To be called after launch
    StateSet get_safe_region();

    /*
      Returns the automaton with winning control applied.
      In detail, the new automaton is equal to the old one, except for:
        1) The new invariant is the intersection of the old invariant and
           the safe region.
        2) Each controllable transition is modified as follows:
           a) The guard is intersected with the safe region.
           b) The transition is converted to uncontrollable.
    */
    Automaton get_controlled_automaton();

  };
}


#endif  /* _SAFETY_SCENARIO_NAPOLY__ */
