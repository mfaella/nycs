/*
 *  File: Safety_bundle.h
 *  Created on: August 13, 2017
 *  Author: Marco Urbano
*/


#ifndef _SCENARIO_BUNDLE_H__
#define _SCENARIO_BUNDLE_H__

#include <unordered_map>
#include "Scenario_location_bundle.h"
#include "OptionsBundle.h"
#include "../automaton/Location.h"
#include "../automaton/Automaton.h"

// To include all utility functions.
#include "../algorithms/scenario_utility.h"

// Per utilizzare i metodi sui PS. (aggiunta di un PS ad un altro).
#include "../algorithms/rwa/rwa_operations.h"


/*     OPERATORI RWA  da analizzare.
#include "synthesis/post_operators/rwa/rwa_ce_map_new.h"
*/

using namespace std;


namespace NaPoly{

  class ScenarioBundle{

  private:
    unordered_map<Location, ScenarioLocationBundle> map;

    // This exception is thrown when the copy constructor is used. You must use copy by reference instead using a copy constructor.
    class ScenarioBundleCopyException : public std::exception{};

    // This exception is thrown wnen the location is not found.
    class ScenarioBundleNotFoundLoc : public std::exception{};


  public:

    ScenarioBundle(){};
    // Constructor.
    ScenarioBundle(Automaton& aut);
    // Copy constructor.
    ScenarioBundle(const ScenarioBundle& to_copy);

    // Setters and getters to be written.
    ScenarioLocationBundle& get_location_bundle(const Location& loc);

    // Scenario methods.
    bool update_controllable_preimage(const StateSet& symbolic_states, const Location& loc);
    bool update_uncontrollable_preimage(const StateSet& symbolic_states, const Location& loc);

    // Safety scenario methods.
    PS avoid_region(const PS& A, const Location& loc);
    PS stay_region(const PS& A, const Location& loc);
    PS reach_region(const PS& A, const Location& loc);

    // Reachability scenario methods.
    PS reachability_avoid_region(PS& A, const Location& loc);
    PS reachability_reach_region(PS& A, const Location& loc);
    PS reachability_stay_region(PS& A, const Location& loc);

    PS RWA_must(PS& U, PS& V, const Location& loc, OptionsBundle& options);
    PS SOR_must(PS& Z, PS& V, const Location& loc, OptionsBundle& options);
    pair<PS, PS> RWA_may(PS& U, PS& V, const Location& loc, OptionsBundle& options);
    PS SOR_may(PS& Z, PS& V, const Location& loc, OptionsBundle& options);
  };

}

#endif
