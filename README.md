# Naples hYbrid Controller Synthesis

A tool for automatic controller synthesis for Linear Hybrid Games with safety or reachability goals.

Homepage: http://wpage.unina.it/m.faella/nycs

## Dependencies:

* g++ (>= 4.9)

* Boost (>= 1.64.0, package libboost-dev)
* glpk (>= 4.62, package libglpk-dev)
* PPL (>= 1.2, it is recommended to activate Java support to use Polyview)
    - GMP (>= 6.1.2, package libgmp-dev)
    - m4 (package m4)
  
## To compile:

- If your libraries are in a non-standard path, rename Makefile.local.template to Makefile.local and edit it
- Run `make`

Notice: version 1.64 of BOOST may suffer from a linking issue,
solved by adding an "#include" as explained here:

https://github.com/boostorg/serialization/commit/1d86261581230e2dc5d617a9b16287d326f3e229
