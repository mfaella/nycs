#include "test.h"
#include <fstream>

/* Reads a Powerset (set of polyhedra) from [file], having variables [vars].
*/
PS read_powerset(std::fstream& file, Varmap vars);
