#include "test_support.h"

#include <fstream>

PS read_powerset(string filename, const Varmap& varmap)
{
  std::fstream file;

  file.open(filename, ios_base::in);
  if (!file) throw string("Error opening file " + filename);
  PS ps = read_powerset(file, varmap);
  file.close();
  
  return ps;
}

Poly read_poly(string filename, const Varmap& varmap)
{
  PS ps = read_powerset(filename, varmap);
  return ps.begin()->pointset();
}
