//============================================================================
// Name        : test.cpp
// Author      : Marco Urbano, Marco Faella
// Version     : 0.1
// Description : NYCS Unit Testing
// Date        : October 30, 2017
//============================================================================

#include <iostream>
#include <ppl.hh>
#include "unit_testing.h"
#include "integration_testing.h"

using namespace std;



/* Struct to launch n tests */
struct test_case {
  string name;
  bool (*test_function)(void);
};


int main(void){

  vector<test_case> all_tests = {
    
    {"rwa_ce_naive_1", &test_rwa_ce_naive_1},
    {"rwa_ce_map_1", &test_rwa_ce_map_1},
    {"rwa_cd_map_1", &test_rwa_cd_map_1},
    {"rwa_cd_naive_1", &test_rwa_cd_naive_1},
    {"sorm_basic_1", &test_sorm_basic_1},
    {"sorm_global_1", &test_sorm_global_1},
    {"sorm_local_1", &test_sorm_local_1},
    {"rwa_cd_naive_2", &test_rwa_cd_naive_2},
    {"rwa_cd_naive_3", &test_rwa_cd_naive_3},
    {"rwa_cd_naive_4", &test_rwa_cd_naive_4},

    {"safety rwa basic (no maps)", &safety_scenario_TNC3det_rwacenaive},
    {"safety rwa smooth basic", &safety_scenario_TNC3det_rwacdnaive},
    {"safety rwa with maps", &safety_scenario_TNC3det_rwacemap},
    {"safety rwa smooth with maps", &safety_scenario_TNC3det_rwacdmap},
    {"safety sor basic", &safety_scenario_TNC3det_sormbasic},
    {"safety sor local", &safety_scenario_TNC3det_sormlocal},
    {"safety sor local with map recycle", &safety_scenario_TNC3det_sormlocal_recycle},
    {"safety sor global", &safety_scenario_TNC3det_sormglobal}, 
    {"reachability maze rwa via rwa maps small-over", &reachability_maze_rwa_via_rwa_maps_small},
    {"reachability maze rwa via rwa maps large-over", &reachability_maze_rwa_via_rwa_maps_large},
    {"reachability maze rwa via sor local small-over", &reachability_maze_rwa_via_sor_local_small},
    {"reachability maze sor via sor local small-over", &reachability_maze_sor_via_sor_local_small},
    {"reachability maze sor via rwa maps small-over", &reachability_maze_sor_via_rwa_maps_small}

  };

  init_unit_tests();
  init_integration_tests();

  for (auto test: all_tests) {

    cout << "Test '" << test.name << "', result : " << flush;

    if (test.test_function()) cout << "PASSED" << endl;
    else cout << "FAILED" << endl;
  }

  return 0;
}
