/*
 *  File: unit_testing.cpp
 *  Created on: October 30, 2017
 *  Author: Marco Urbano
 */

#ifndef _NYCS_UNIT_TESTING_
#define _NYCS_UNIT_TESTING_

void init_unit_tests();

bool test_rwa_ce_naive_1();

bool test_rwa_cd_naive_1();
bool test_rwa_cd_naive_2();
bool test_rwa_cd_naive_3();

bool test_rwa_cd_naive_4();

bool test_rwa_ce_map_1();
bool test_rwa_cd_map_1();

bool test_sorm_basic_1();
bool test_sorm_global_1();
bool test_sorm_local_1();


#endif
