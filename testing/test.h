/*
 *  File: unit_testing.cpp
 *  Created on: October 31, 2017
 *  Author: Marco Urbano
 */

#ifndef _TEST_H_
#define _TEST_H_


#include <ppl.hh>

using namespace Parma_Polyhedra_Library;
using namespace Parma_Polyhedra_Library::IO_Operators;
using namespace std;

typedef NNC_Polyhedron Poly;
typedef Pointset_Powerset<NNC_Polyhedron> PS;
typedef map<string,int> Varmap;
typedef map<int,float> Sectionmap;


#endif
