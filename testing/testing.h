/*
 *  File: testing.h
 *  Created on: Semptember 08, 2017
 *  Author: Marco Urbano
 */


#include "../src/automaton/preprocessor.h"
#include "../src/automaton/Automaton.h"
#include "../src/io/output_operators.h"

// To test add_powerset.
#include "../src/algorithms/rwa/rwa_operations.h"
#include "../src/algorithms/scenario_utility.h"


using namespace std;

using namespace NaPoly;

namespace NaPoly_test{

   // Test function for "add_powerset".
   void test_add_powerset(PS& A, PS& B);

   // Test function for "negate".
   void test_negate();

   // Test function for get_prepoly;
   void test_get_prepoly();

   // Test what appens if there are 25+ PPL variables
   void test_n_variables(int n);

   // Test output_operators's Poly output.
   void test_Poly_output(Poly& poliedro_nnc);

   // Test output_operators's PS output.
   void test_PS_output(PS& poliedro);

}
