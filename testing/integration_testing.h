/*
*  File: integration_testing.h
*  Created on: November 18, 2017
*  Author: Marco Urbano
*/


#ifndef _NYCS_INTEGRATION_TESTING_
#define _NYCS_INTEGRATION_TESTING_

#include "../src/scenario/safety_scenario.h"
#include "../src/scenario/reach_scenario.h"

/* NYCS CLASSES */
#include "../src/automaton/Automaton.h"
#include "../src/io/Parser.h"

using namespace std;

void init_integration_tests();

/* SAFETY INTEGRATION TESTING TNC3_det*/
bool safety_scenario_TNC3det_rwacenaive();
bool safety_scenario_TNC3det_rwacdnaive();
bool safety_scenario_TNC3det_rwacdmap();
bool safety_scenario_TNC3det_rwacemap();
bool safety_scenario_TNC3det_sormbasic();
bool safety_scenario_TNC3det_sormlocal();
bool safety_scenario_TNC3det_sormlocal_recycle();
bool safety_scenario_TNC3det_sormglobal();

/* REACHABILITY INTEGRATION TESTING MAZE_3DET */
bool reachability_maze_rwa_via_rwa_maps_small();
bool reachability_maze_rwa_via_rwa_maps_large();
bool reachability_maze_rwa_via_sor_local_small();
bool reachability_maze_sor_via_sor_local_small();
bool reachability_maze_sor_via_rwa_maps_small();

#endif
