#include "test.h"
#include "polyread.h"

PS read_powerset(string filename, const Varmap& varmap);
Poly read_poly(string filename, const Varmap& varmap);
