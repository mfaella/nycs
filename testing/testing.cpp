/*
 *  File: testing.cpp
 *  Created on: Semptember 08, 2017
 *  Author: Marco Urbano
 */

#include "testing.h"

using namespace std;

using namespace NaPoly;

namespace NaPoly_test{

void test_add_powerset(PS& A, PS& B){

       try
       {
          add_powerset(A, B);
       }
       catch(std::invalid_argument& e)
       {
          cout << "Errore nella dimensione di uno dei due powerset" << endl;
          cout << "Dimensione A: " << A.space_dimension() << endl;
          cout << "Dimensione B: " << B.space_dimension() << endl;

       }
return;

}




void test_negate(){

  using IO_Operators::operator<<;
  cout << "Test negate, start." << endl;

  Parma_var x(0), y(1);
  Poly p(2);

  p.add_constraint(x>=3);
  p.add_constraint(x<=4);
  p.add_constraint(y<=6);
  p.add_constraint(y>=5);

  //PS Ps_1(2, Parma_Polyhedra_Library::UNIVERSE);
  //Ps_1.add_disjunct(p);

  PS Ps_1(p);
  cout << "Il PS è :" << Ps_1 << endl;

  NaPoly::negate(Ps_1);

  cout << "Il risultato della negazione è : " << Ps_1 << endl;
  cout << "Test negate, end." << endl;

return;
}



void test_get_prepoly(){

  using IO_Operators::operator<<;
  cout << "Test get_prepoly, start." << endl;

  Parma_var x(0), y(1);
  Poly p(2);

  p.add_constraint(x>=3);
  p.add_constraint(x<=4);
  p.add_constraint(y<=6);
  p.add_constraint(y>=5);

  //PS Ps_1(2, Parma_Polyhedra_Library::UNIVERSE);
  //Ps_1.add_disjunct(p);

  //PS Ps_1(p);
  cout << "Il Poly è :" << p << endl;

  NaPoly::get_prepoly(p);

  cout << "Il risultato di prepoly è : " << p << endl;
  cout << "Test get_prepoly, end." << endl;

}



void test_n_variables(int n){
   //using IO_Operators::operator<<;

   Parma_var* parma_vector[n];

   for(int i = 0; i < n; i++){
     parma_vector[i] = new Parma_var(i);
   }

   Poly p(n);

   for(int i = 0; i < n; i++)
   {
     p.add_constraint(*parma_vector[i]<0);
   }

   cout << p << endl;


}



void test_Poly_output(Poly& poliedro_nnc){

  // Stampa con l'operatore << del namespace corrente.
  cout << "TEST POLY OUTPUT" << endl;
  print_poly(cout, poliedro_nnc);

  //cout << poliedro_nnc << endl;
  cout << "TEST POLY OUTPUT" << endl;
return;
}


void test_PS_output(PS& poliedro){
cout << "TEST PS OUTPUT" << endl;
print_PS(cout, poliedro);
cout << "TEST PS OUTPUT" << endl;

return;
}



}
