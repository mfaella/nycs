/*
 *  File: unit_testing.cpp
 *  Created on: October 30, 2017
 *  Author: Marco Urbano, Marco Faella
 */

#include "unit_testing.h"

/* To include polyread features used to parse PS */
#include "test_support.h"

/* Extras */
#include "../src/algorithms/scenario_utility.h"

/* get_RWA_*_* testing */
#include "../src/algorithms/rwa/rwa_basic.h"
#include "../src/algorithms/rwa/rwa_smooth_basic.h"
#include "../src/algorithms/rwa/rwa_maps.h"
#include "../src/algorithms/rwa/rwa_smooth_maps.h"

/* get_sorM_*_* testing */
#include "../src/algorithms/sor/sor_basic.h"
#include "../src/algorithms/sor/sor_global.h"
#include "../src/algorithms/sor/sor_local.h"

// DEBUG
#include "../src/io/output_operators.h"


/* Based on SE loc of TNC_3_det, 1st iteration ( ovvero prendo in input il risultato già ottenuto della
    macchina virtuale per la locazione che risulta errata. UNIT TESTING).
*/
using namespace std;
using namespace NaPoly;

static Varmap xyt_map, xy_map, AB_map;
static string path = "tests/unit/";

void init_unit_tests() {
  xyt_map["x"] = 0;
  xyt_map["y"] = 1;
  xyt_map["t"] = 2;

  xy_map["x"] = 0;
  xy_map["y"] = 1;

  AB_map["A"] = 0;
  AB_map["B"] = 1;
}


bool test_rwa_ce_naive_1(){
  Poly preflow           = read_poly(path + "rwa_naive_1_preflow.poly", xyt_map);
  PS   U                 = read_powerset(path + "rwa_naive_1_U.poly",   xyt_map);
  PS   V                 = read_powerset(path + "rwa_naive_1_V.poly",   xyt_map);
  PS   ref_result_first  = read_powerset(path + "rwa_naive_1_result_first.poly",  xyt_map);
  PS   ref_result_second = read_powerset(path + "rwa_naive_1_result_second.poly", xyt_map);

  pair<PS,PS> result_new = rwa_basic(U, V, preflow);

  return (result_new.first.geometrically_covers(ref_result_first) &&
          ref_result_first.geometrically_covers(result_new.first) &&
          ref_result_second.geometrically_covers(result_new.second) &&
          result_new.second.geometrically_covers(ref_result_second));
}


bool test_rwa_ce_map_1(){

  Poly preflow = read_poly(path + "rwa_ce_map_1_preflow.poly", xyt_map);
  PS U = read_powerset(path + "rwa_ce_map_1_U.poly", xyt_map);
  PS V = read_powerset(path + "rwa_ce_map_1_V.poly", xyt_map);
  PS ref_result_first = read_powerset(path + "rwa_ce_map_1_result_first.poly", xyt_map);
  PS ref_result_second = read_powerset(path + "rwa_ce_map_1_result_second.poly", xyt_map);

  pair<PS,PS> result_new = rwa_maps(U, V, preflow);
  return (result_new.first.geometrically_covers(ref_result_first) &&
          ref_result_first.geometrically_covers(result_new.first) &&
          ref_result_second.geometrically_covers(result_new.second) &&
          result_new.second.geometrically_covers(ref_result_second));
}


bool test_rwa_cd_map_1(){

  std::fstream file;

  file.open(path + "rwa_cd_map_1_preflow.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_map_1_preflow.poly>.");
  PS preflow_ps = read_powerset(file, xyt_map);
  file.close();

  Poly preflow = preflow_ps.begin()->pointset();

  file.open(path + "rwa_cd_map_1_flow.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_map_1_flow.poly>.");
  PS flow_ps = read_powerset(file, xyt_map);
  file.close();

  Poly flow = flow_ps.begin()->pointset();

  file.open(path + "rwa_cd_map_1_U.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_map_1_U.poly>.");
  PS U = read_powerset(file, xyt_map);
  file.close();

  file.open(path + "rwa_cd_map_1_V.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_map_1_V.poly>.");
  PS V = read_powerset(file, xyt_map);
  file.close();

  file.open(path + "rwa_cd_map_1_result_first.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_map_1_result_first.poly>.");
  PS ref_result_first = read_powerset(file, xyt_map);
  file.close();

  file.open(path + "rwa_cd_map_1_result_second.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_map_1_result_second.poly>.");
  PS ref_result_second = read_powerset(file, xyt_map);
  file.close();

  pair<PS,PS> result_new = rwa_smooth_maps(U, V, flow, preflow);

  return (result_new.first.geometrically_covers(ref_result_first) &&
          ref_result_first.geometrically_covers(result_new.first) &&
          ref_result_second.geometrically_covers(result_new.second) &&
          result_new.second.geometrically_covers(ref_result_second));
}


bool test_rwa_cd_naive_1(){

  std::fstream file;

  file.open(path + "rwa_cd_naive_1_preflow.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_naive_1_preflow.poly>.");
  PS preflow_ps = read_powerset(file, xyt_map);
  file.close();

  Poly preflow = preflow_ps.begin()->pointset();

  file.open(path + "rwa_cd_naive_1_flow.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_naive_1_flow.poly>.");
  PS flow_ps = read_powerset(file, xyt_map);
  file.close();

  Poly flow = flow_ps.begin()->pointset();

  file.open(path + "rwa_cd_naive_1_U.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_naive_1_U.poly>.");
  PS U = read_powerset(file, xyt_map);
  file.close();

  file.open(path + "rwa_cd_naive_1_V.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_naive_1_V.poly>.");
  PS V = read_powerset(file, xyt_map);
  file.close();

  file.open(path + "rwa_cd_naive_1_result_first.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_naive_1_result_first.poly>.");
  PS ref_result_first = read_powerset(file, xyt_map);
  file.close();

  file.open(path + "rwa_cd_naive_1_result_second.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_naive_1_result_second.poly>.");
  PS ref_result_second = read_powerset(file, xyt_map);
  file.close();

  pair<PS,PS> result_new = rwa_smooth_basic(U, V, flow, preflow);

  return (result_new.first.geometrically_covers(ref_result_first) &&
          ref_result_first.geometrically_covers(result_new.first) &&
          ref_result_second.geometrically_covers(result_new.second) &&
          result_new.second.geometrically_covers(ref_result_second));
}


bool test_sorm_basic_1(){

  std::fstream file;

  file.open(path + "sorm_basic_1_flow.poly", ios_base::in);
  if (!file) throw string("Error opening file <sorm_basic_1_flow.poly>.");
  PS flow_ps = read_powerset(file, xyt_map);
  file.close();

  Poly flow = flow_ps.begin()->pointset();

  file.open(path + "sorm_basic_1_underapprox.poly", ios_base::in);
  if (!file) throw string("Error opening file <sorm_basic_1_underapprox.poly>.");
  PS underapprox = read_powerset(file, xyt_map);
  file.close();

  file.open(path + "sorm_basic_1_overapprox.poly", ios_base::in);
  if (!file) throw string("Error opening file <sorm_basic_1_overapprox.poly>.");
  PS overapprox = read_powerset(file, xyt_map);
  file.close();

  file.open(path + "sorm_basic_1_result_first.poly", ios_base::in);
  if (!file) throw string("Error opening file <sorm_basic_1_result_first.poly>.");
  PS ref_result_first = read_powerset(file, xyt_map);
  file.close();

  file.open(path + "sorm_basic_1_result_second.poly", ios_base::in);
  if (!file) throw string("Error opening file <sorm_basic_1_result_second.poly>.");
  PS ref_result_second = read_powerset(file, xyt_map);
  file.close();

  pair<PS,PS> result_new = sor_basic(overapprox, underapprox, flow);

  return (result_new.first.geometrically_covers(ref_result_first) &&
          ref_result_first.geometrically_covers(result_new.first) &&
          ref_result_second.geometrically_covers(result_new.second) &&
          result_new.second.geometrically_covers(ref_result_second));
}


bool test_sorm_global_1(){

  std::fstream file;

  file.open(path + "sorm_global_1_flow.poly", ios_base::in);
  if (!file) throw string("Error opening file <sorm_global_1_flow.poly>.");
  PS flow_ps = read_powerset(file, xyt_map);
  file.close();

  Poly flow = flow_ps.begin()->pointset();

  file.open(path + "sorm_global_1_underapprox.poly", ios_base::in);
  if (!file) throw string("Error opening file <sorm_global_1_underapprox.poly>.");
  PS underapprox = read_powerset(file, xyt_map);
  file.close();

  file.open(path + "sorm_global_1_overapprox.poly", ios_base::in);
  if (!file) throw string("Error opening file <sorm_global_1_overapprox.poly>.");
  PS overapprox = read_powerset(file, xyt_map);
  file.close();

  file.open(path + "sorm_global_1_result_first.poly", ios_base::in);
  if (!file) throw string("Error opening file <sorm_global_1_result_first.poly>.");
  PS ref_result_first = read_powerset(file, xyt_map);
  file.close();

  file.open(path + "sorm_global_1_result_second.poly", ios_base::in);
  if (!file) throw string("Error opening file <sorm_global_1_result_second.poly>.");
  PS ref_result_second = read_powerset(file, xyt_map);
  file.close();

  pair<PS,PS> result_new = sor_global(overapprox, underapprox, flow);

  return (result_new.first.geometrically_covers(ref_result_first) &&
          ref_result_first.geometrically_covers(result_new.first) &&
          ref_result_second.geometrically_covers(result_new.second) &&
          result_new.second.geometrically_covers(ref_result_second));
}


bool test_sorm_local_1(){

  std::fstream file;

  file.open(path + "sorm_local_1_flow.poly", ios_base::in);
  if (!file) throw string("Error opening file <sorm_local_1_flow.poly>.");
  PS flow_ps = read_powerset(file, xyt_map);
  file.close();

  Poly flow = flow_ps.begin()->pointset();

  file.open(path + "sorm_local_1_underapprox.poly", ios_base::in);
  if (!file) throw string("Error opening file <sorm_local_1_underapprox.poly>.");
  PS underapprox = read_powerset(file, xyt_map);
  file.close();

  file.open(path + "sorm_local_1_overapprox.poly", ios_base::in);
  if (!file) throw string("Error opening file <sorm_local_1_overapprox.poly>.");
  PS overapprox = read_powerset(file, xyt_map);
  file.close();

  file.open(path + "sorm_local_1_result_first.poly", ios_base::in);
  if (!file) throw string("Error opening file <sorm_local_1_result_first.poly>.");
  PS ref_result_first = read_powerset(file, xyt_map);
  file.close();

  file.open(path + "sorm_local_1_result_second.poly", ios_base::in);
  if (!file) throw string("Error opening file <sorm_local_1_result_second.poly>.");
  PS ref_result_second = read_powerset(file, xyt_map);
  file.close();

  pair<PS,PS> result_new = sor_local(overapprox, underapprox, flow);

  return (result_new.first.geometrically_covers(ref_result_first) &&
          ref_result_first.geometrically_covers(result_new.first) &&
          ref_result_second.geometrically_covers(result_new.second) &&
          result_new.second.geometrically_covers(ref_result_second));
}


/* Input files from launching spaceex+ on TNC_3_det, location NE first iteration */
bool test_rwa_cd_naive_2(){

  std::fstream file;

  file.open(path + "rwa_cd_naive_2_preflow.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_naive_2_preflow.poly>.");
  PS preflow_ps = read_powerset(file, xyt_map);
  file.close();

  Poly preflow = preflow_ps.begin()->pointset();

  file.open(path + "rwa_cd_naive_2_flow.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_naive_2_flow.poly>.");
  PS flow_ps = read_powerset(file, xyt_map);
  file.close();

  Poly flow = flow_ps.begin()->pointset();

  file.open(path + "rwa_cd_naive_2_U.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_naive_2_U.poly>.");
  PS U = read_powerset(file, xyt_map);
  file.close();

  file.open(path + "rwa_cd_naive_2_V.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_naive_2_V.poly>.");
  PS V = read_powerset(file, xyt_map);
  file.close();

  file.open(path + "rwa_cd_naive_2_result_first.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_naive_2_result_first.poly>.");
  PS ref_result_first = read_powerset(file, xyt_map);
  file.close();

  file.open(path + "rwa_cd_naive_2_result_second.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_naive_2_result_second.poly>.");
  PS ref_result_second = read_powerset(file, xyt_map);
  file.close();

  pair<PS,PS> result_new = rwa_smooth_basic(U, V, flow, preflow);

  return (result_new.first.geometrically_covers(ref_result_first) &&
          ref_result_first.geometrically_covers(result_new.first) &&
          ref_result_second.geometrically_covers(result_new.second) &&
          result_new.second.geometrically_covers(ref_result_second));
}



/* Input files from launching spaceex+ on TNC_3_det, location NE second iteration */
bool test_rwa_cd_naive_3(){
  std::fstream file;

  file.open(path + "rwa_cd_naive_3_preflow.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_naive_3_preflow.poly>.");
  PS preflow_ps = read_powerset(file, xyt_map);
  file.close();

  Poly preflow = preflow_ps.begin()->pointset();

  file.open(path + "rwa_cd_naive_3_flow.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_naive_3_flow.poly>.");
  PS flow_ps = read_powerset(file, xyt_map);
  file.close();

  Poly flow = flow_ps.begin()->pointset();

  file.open(path + "rwa_cd_naive_3_U.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_naive_3_U.poly>.");
  PS U = read_powerset(file, xyt_map);

  //cout << "U = " << U << endl;
  // cout << "U = ";
  // print_PS_regex(cout, U);

  file.close();

  file.open(path + "rwa_cd_naive_3_V.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_naive_3_V.poly>.");
  PS V = read_powerset(file, xyt_map);
  file.close();

  //cout << "V = " << V << endl;

  file.open(path + "rwa_cd_naive_3_result_first.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_naive_3_result_first.poly>.");
  PS ref_result_first = read_powerset(file, xyt_map);
  file.close();

  file.open(path + "rwa_cd_naive_3_result_second.poly", ios_base::in);
  if (!file) throw string("Error opening file <rwa_cd_naive_3_result_second.poly>.");
  PS ref_result_second = read_powerset(file, xyt_map);
  file.close();

  pair<PS,PS> result_new = rwa_smooth_basic(U, V, flow, preflow);

  //cout << "first = " << result_new.first << endl;
  //cout << "second = " << result_new.second << endl;

  bool check_first = result_new.first.geometrically_covers(ref_result_first) &&
    ref_result_first.geometrically_covers(result_new.first),
    check_second = ref_result_second.geometrically_covers(result_new.second) &&
    result_new.second.geometrically_covers(ref_result_second);

  // cout << check_first << ", " << check_second << endl;

  return (check_first && check_second);
}


// Custom 2D inputs
bool test_rwa_cd_naive_4()
{
  Poly flow              = read_poly(path + "rwa_cd_naive_4_flow.poly",    AB_map);
  Poly preflow           = read_poly(path + "rwa_cd_naive_4_preflow.poly", AB_map);
  PS   U                 = read_powerset(path + "rwa_cd_naive_4_U.poly",   AB_map);
  PS   V                 = read_powerset(path + "rwa_cd_naive_4_V.poly",   AB_map);
  PS   ref_result_first  = read_powerset(path + "rwa_cd_naive_4_result_first.poly",  AB_map);

  pair<PS,PS> result_new = rwa_smooth_basic(U, V, flow, preflow);

  // first union second should be equal to "not V"
  PS result = result_new.first;
  add_powerset(result, result_new.second);
  NaPoly::negate(V);
  bool check_second = V.geometrically_covers(result) &&
                      result.geometrically_covers(V);

  bool check_first = result_new.first.geometrically_covers(ref_result_first) &&
    ref_result_first.geometrically_covers(result_new.first);

  return check_first && check_second;
}


bool test_over_small()
{
  Poly flow              = read_poly(path + "rwa_cd_naive_4_flow.poly",    AB_map);
  Poly preflow           = read_poly(path + "rwa_cd_naive_4_preflow.poly", AB_map);
  PS   U                 = read_powerset(path + "rwa_cd_naive_4_U.poly",   AB_map);
  PS   V                 = read_powerset(path + "rwa_cd_naive_4_V.poly",   AB_map);
  PS   ref_result_first  = read_powerset(path + "rwa_cd_naive_4_result_first.poly",  AB_map);

  pair<PS,PS> result_new = rwa_smooth_basic(U, V, flow, preflow);

  // first union second should be equal to "not V"
  PS result = result_new.first;
  add_powerset(result, result_new.second);
  NaPoly::negate(V);
  bool check_second = V.geometrically_covers(result) &&
                      result.geometrically_covers(V);

  bool check_first = result_new.first.geometrically_covers(ref_result_first) &&
    ref_result_first.geometrically_covers(result_new.first);

  return check_first && check_second;
}
