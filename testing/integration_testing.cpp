/*
*  File: integration_testing.cpp
*  Created on: November 18, 2017
*  Author: Marco Urbano
*/


#include "integration_testing.h"
#include "test_support.h"

static Varmap xyt_map;
static const string path = "tests/integration/";
static const string TNC_MODEL = path + "safety/TNC-model.xml";

void init_integration_tests() {
  xyt_map["x"] = 0;
  xyt_map["y"] = 1;
  xyt_map["t"] = 2;
}


using namespace std;
using namespace NaPoly;



bool safety_scenario_TNC3det_rwacenaive(){

  using namespace NaPoly;

  /* Following istructions are used to build the automaton and to
  make the integration testing of Safety_scenario           */

  Parser t_parser(TNC_MODEL, path + "safety/TNC_3_rwa_basic_config.xml");
  Automaton t_automata = t_parser.get_automaton();
  OptionsBundle t_optbundle = t_parser.get_options_bundle();

  Location loc_NW = t_automata.get_location("NW");
  Location loc_NE = t_automata.get_location("NE");
  Location loc_SW = t_automata.get_location("SW");
  Location loc_SE = t_automata.get_location("SE");

  SafetyScenario test_object(t_automata, t_optbundle);

  PS NW_old = read_powerset(path + "safety/TNC3det_rwacenaive_NW.poly", xyt_map);
  PS NE_old = read_powerset(path + "safety/TNC3det_rwacenaive_NE.poly", xyt_map);
  PS SW_old = read_powerset(path + "safety/TNC3det_rwacenaive_SW.poly", xyt_map);
  PS SE_old = read_powerset(path + "safety/TNC3det_rwacenaive_SE.poly", xyt_map);

  /* Used to avoid debugging messages during "launch_modded". */
  // std::cout.setstate(std::ios_base::failbit);

  // test_object.launch();
  // StateSet result = test_object.getSafeRegion();
  //   StateSet result_old(filename);
  // oppure:
  //   filestream >> result_old;
  //
  // cout << result;

  test_object.launch();
  StateSet result_new = test_object.get_safe_region();
  
  /* Used to clear cout to print any new string before this statement.*/
  // std::cout.clear();

  PS NW_new = result_new.get_region(loc_NW);
  PS NE_new = result_new.get_region(loc_NE);
  PS SW_new = result_new.get_region(loc_SW);
  PS SE_new = result_new.get_region(loc_SE);

  bool NW_flag = (NW_new.geometrically_covers(NW_old) && NW_old.geometrically_covers(NW_new));
  bool NE_flag = (NE_new.geometrically_covers(NE_old) && NE_old.geometrically_covers(NE_new));
  bool SW_flag = (SW_new.geometrically_covers(SW_old) && SW_old.geometrically_covers(SW_new));
  bool SE_flag = (SE_new.geometrically_covers(SE_old) && SE_old.geometrically_covers(SE_new));

 return (NW_flag && NE_flag && SW_flag && SE_flag);
}


bool safety_scenario_TNC3det_rwacdnaive(){

  using namespace NaPoly;

  /* Following istructions are used to build the automaton and to
  make the integration testing of Safety_scenario           */

  Parser t_parser(TNC_MODEL, path + "safety/TNC_3_smooth_basic_config.xml");
  Automaton t_automata = t_parser.get_automaton();
  OptionsBundle t_optbundle = t_parser.get_options_bundle();

  Location loc_NW = t_automata.get_location("NW");
  Location loc_NE = t_automata.get_location("NE");
  Location loc_SW = t_automata.get_location("SW");
  Location loc_SE = t_automata.get_location("SE");

  SafetyScenario test_object(t_automata, t_optbundle);

  PS NW_old = read_powerset(path + "safety/TNC3det_rwacdnaive_NW.poly", xyt_map);
  PS NE_old = read_powerset(path + "safety/TNC3det_rwacdnaive_NE.poly", xyt_map);
  PS SW_old = read_powerset(path + "safety/TNC3det_rwacdnaive_SW.poly", xyt_map);
  PS SE_old = read_powerset(path + "safety/TNC3det_rwacdnaive_SE.poly", xyt_map);

  /* PS NW_old = read_powerset(path + "safety/TNC3det_rwacdmap_NW.poly", xyt_map);
  PS NE_old = read_powerset(path + "safety/TNC3det_rwacdmap_NE.poly", xyt_map);
  PS SW_old = read_powerset(path + "safety/TNC3det_rwacdmap_SW.poly", xyt_map);
  PS SE_old = read_powerset(path + "safety/TNC3det_rwacdmap_SE.poly", xyt_map); */
  
  test_object.launch();
  StateSet result_new = test_object.get_safe_region();
  
  PS NW_new = result_new.get_region(loc_NW);
  PS NE_new = result_new.get_region(loc_NE);
  PS SW_new = result_new.get_region(loc_SW);
  PS SE_new = result_new.get_region(loc_SE);

  bool NW_flag = (NW_new.geometrically_covers(NW_old) && NW_old.geometrically_covers(NW_new));
  bool NE_flag = (NE_new.geometrically_covers(NE_old) && NE_old.geometrically_covers(NE_new));
  bool SW_flag = (SW_new.geometrically_covers(SW_old) && SW_old.geometrically_covers(SW_new));
  bool SE_flag = (SE_new.geometrically_covers(SE_old) && SE_old.geometrically_covers(SE_new));
  
  return (NW_flag && NE_flag && SW_flag && SE_flag);
}


bool safety_scenario_TNC3det_rwacdmap(){

  using namespace NaPoly;

  /* Following istructions are used to build the automaton and to
  make the integration testing of Safety_scenario           */

  Parser t_parser(TNC_MODEL, path + "safety/TNC_3_smooth_maps_config.xml");
  Automaton t_automata = t_parser.get_automaton();
  OptionsBundle t_optbundle = t_parser.get_options_bundle();

  Location loc_NW = t_automata.get_location("NW");
  Location loc_NE = t_automata.get_location("NE");
  Location loc_SW = t_automata.get_location("SW");
  Location loc_SE = t_automata.get_location("SE");

  SafetyScenario test_object(t_automata, t_optbundle);

  PS NW_old = read_powerset(path + "safety/TNC3det_rwacdmap_NW.poly", xyt_map);
  PS NE_old = read_powerset(path + "safety/TNC3det_rwacdmap_NE.poly", xyt_map);
  PS SW_old = read_powerset(path + "safety/TNC3det_rwacdmap_SW.poly", xyt_map);
  PS SE_old = read_powerset(path + "safety/TNC3det_rwacdmap_SE.poly", xyt_map);

  /* Used to avoid debugging messages during "launch_modded". */
  // std::cout.setstate(std::ios_base::failbit);

  test_object.launch();
  StateSet result_new = test_object.get_safe_region();
  
  /* Used to clear cout to print any new string before this statement.*/
  // std::cout.clear();

  PS NW_new = result_new.get_region(loc_NW);
  PS NE_new = result_new.get_region(loc_NE);
  PS SW_new = result_new.get_region(loc_SW);
  PS SE_new = result_new.get_region(loc_SE);

  bool NW_flag = (NW_new.geometrically_covers(NW_old) && NW_old.geometrically_covers(NW_new));
  bool NE_flag = (NE_new.geometrically_covers(NE_old) && NE_old.geometrically_covers(NE_new));
  bool SW_flag = (SW_new.geometrically_covers(SW_old) && SW_old.geometrically_covers(SW_new));
  bool SE_flag = (SE_new.geometrically_covers(SE_old) && SE_old.geometrically_covers(SE_new));


 return (NW_flag && NE_flag && SW_flag && SE_flag);
}



bool safety_scenario_TNC3det_rwacemap(){

  using namespace NaPoly;

  /* Following istructions are used to build the automaton and to
  make the integration testing of Safety_scenario           */

  Parser t_parser(TNC_MODEL, path + "safety/TNC_3_rwa_maps_config.xml");
  Automaton t_automata = t_parser.get_automaton();
  OptionsBundle t_optbundle = t_parser.get_options_bundle();

  Location loc_NW = t_automata.get_location("NW");
  Location loc_NE = t_automata.get_location("NE");
  Location loc_SW = t_automata.get_location("SW");
  Location loc_SE = t_automata.get_location("SE");

  SafetyScenario test_object(t_automata, t_optbundle);

  PS NW_old = read_powerset(path + "safety/TNC3det_rwacemap_NW.poly", xyt_map);
  PS NE_old = read_powerset(path + "safety/TNC3det_rwacemap_NE.poly", xyt_map);
  PS SW_old = read_powerset(path + "safety/TNC3det_rwacemap_SW.poly", xyt_map);
  PS SE_old = read_powerset(path + "safety/TNC3det_rwacemap_SE.poly", xyt_map);

  /* avoid debugging messages during "launch_modded". */
  // std::cout.setstate(std::ios_base::failbit);

  test_object.launch();
  StateSet result_new = test_object.get_safe_region();

  /* Used to clear cout to print any new string before this statement.*/
  // std::cout.clear();

  PS NW_new = result_new.get_region(loc_NW);
  PS NE_new = result_new.get_region(loc_NE);
  PS SW_new = result_new.get_region(loc_SW);
  PS SE_new = result_new.get_region(loc_SE);

  bool NW_flag = (NW_new.geometrically_covers(NW_old) && NW_old.geometrically_covers(NW_new));
  bool NE_flag = (NE_new.geometrically_covers(NE_old) && NE_old.geometrically_covers(NE_new));
  bool SW_flag = (SW_new.geometrically_covers(SW_old) && SW_old.geometrically_covers(SW_new));
  bool SE_flag = (SE_new.geometrically_covers(SE_old) && SE_old.geometrically_covers(SE_new));

  return (NW_flag && NE_flag && SW_flag && SE_flag);
}


bool safety_scenario_TNC3det_sormbasic(){

  using namespace NaPoly;

  /* Following istructions are used to build the automaton and to
  make the integration testing of Safety_scenario           */

  Parser t_parser(TNC_MODEL, path + "safety/TNC_3_sor_basic_config.xml");
  Automaton t_automata = t_parser.get_automaton();
  OptionsBundle t_optbundle = t_parser.get_options_bundle();

  Location loc_NW = t_automata.get_location("NW");
  Location loc_NE = t_automata.get_location("NE");
  Location loc_SW = t_automata.get_location("SW");
  Location loc_SE = t_automata.get_location("SE");

  SafetyScenario test_object(t_automata, t_optbundle);

  PS NW_old = read_powerset(path + "safety/TNC3det_sormbasic_NW.poly", xyt_map);
  PS NE_old = read_powerset(path + "safety/TNC3det_sormbasic_NE.poly", xyt_map);
  PS SW_old = read_powerset(path + "safety/TNC3det_sormbasic_SW.poly", xyt_map);
  PS SE_old = read_powerset(path + "safety/TNC3det_sormbasic_SE.poly", xyt_map);

  /* Used to avoid debugging messages during "launch_modded". */
  // std::cout.setstate(std::ios_base::failbit);

  test_object.launch();
  StateSet result_new = test_object.get_safe_region();
  
  /* Used to clear cout to print any new string before this statement.*/
  // std::cout.clear();

  PS NW_new = result_new.get_region(loc_NW);
  PS NE_new = result_new.get_region(loc_NE);
  PS SW_new = result_new.get_region(loc_SW);
  PS SE_new = result_new.get_region(loc_SE);

  bool NW_flag = (NW_new.geometrically_covers(NW_old) && NW_old.geometrically_covers(NW_new));
  bool NE_flag = (NE_new.geometrically_covers(NE_old) && NE_old.geometrically_covers(NE_new));
  bool SW_flag = (SW_new.geometrically_covers(SW_old) && SW_old.geometrically_covers(SW_new));
  bool SE_flag = (SE_new.geometrically_covers(SE_old) && SE_old.geometrically_covers(SE_new));

  return (NW_flag && NE_flag && SW_flag && SE_flag);
}

bool safety_scenario_TNC3det_sormlocal(){

  using namespace NaPoly;

  /* Following istructions are used to build the automaton and to
  make the integration testing of Safety_scenario           */

  Parser t_parser(TNC_MODEL, path + "safety/TNC_3_sor_local_config.xml");
  Automaton t_automata = t_parser.get_automaton();
  OptionsBundle t_optbundle = t_parser.get_options_bundle();

  Location loc_NW = t_automata.get_location("NW");
  Location loc_NE = t_automata.get_location("NE");
  Location loc_SW = t_automata.get_location("SW");
  Location loc_SE = t_automata.get_location("SE");

  SafetyScenario test_object(t_automata, t_optbundle);

  PS NW_old = read_powerset(path + "safety/TNC3det_sormlocal_NW.poly", xyt_map);
  PS NE_old = read_powerset(path + "safety/TNC3det_sormlocal_NE.poly", xyt_map);
  PS SW_old = read_powerset(path + "safety/TNC3det_sormlocal_SW.poly", xyt_map);
  PS SE_old = read_powerset(path + "safety/TNC3det_sormlocal_SE.poly", xyt_map);

  /* Used to avoid debugging messages during "launch_modded". */
  // std::cout.setstate(std::ios_base::failbit);

  test_object.launch();
  StateSet result_new = test_object.get_safe_region();
  
  /* Used to clear cout to print any new string before this statement.*/
  // std::cout.clear();

  PS NW_new = result_new.get_region(loc_NW);
  PS NE_new = result_new.get_region(loc_NE);
  PS SW_new = result_new.get_region(loc_SW);
  PS SE_new = result_new.get_region(loc_SE);

  bool NW_flag = (NW_new.geometrically_covers(NW_old) && NW_old.geometrically_covers(NW_new));
  bool NE_flag = (NE_new.geometrically_covers(NE_old) && NE_old.geometrically_covers(NE_new));
  bool SW_flag = (SW_new.geometrically_covers(SW_old) && SW_old.geometrically_covers(SW_new));
  bool SE_flag = (SE_new.geometrically_covers(SE_old) && SE_old.geometrically_covers(SE_new));

  return (NW_flag && NE_flag && SW_flag && SE_flag);
}


bool safety_scenario_TNC3det_sormlocal_recycle(){

  using namespace NaPoly;

  /* Following istructions are used to build the automaton and to
  make the integration testing of Safety_scenario           */

  Parser t_parser(TNC_MODEL, path + "safety/TNC_3_sor_local_recycle_config.xml");
  Automaton t_automata = t_parser.get_automaton();
  OptionsBundle t_optbundle = t_parser.get_options_bundle();

  Location loc_NW = t_automata.get_location("NW");
  Location loc_NE = t_automata.get_location("NE");
  Location loc_SW = t_automata.get_location("SW");
  Location loc_SE = t_automata.get_location("SE");

  SafetyScenario test_object(t_automata, t_optbundle);

  PS NW_old = read_powerset(path + "safety/TNC3det_sormlocal_NW.poly", xyt_map);
  PS NE_old = read_powerset(path + "safety/TNC3det_sormlocal_NE.poly", xyt_map);
  PS SW_old = read_powerset(path + "safety/TNC3det_sormlocal_SW.poly", xyt_map);
  PS SE_old = read_powerset(path + "safety/TNC3det_sormlocal_SE.poly", xyt_map);

  /* Used to avoid debugging messages during "launch_modded". */
  // std::cout.setstate(std::ios_base::failbit);

  test_object.launch();
  StateSet result_new = test_object.get_safe_region();
  
  /* Used to clear cout to print any new string before this statement.*/
  // std::cout.clear();

  PS NW_new = result_new.get_region(loc_NW);
  PS NE_new = result_new.get_region(loc_NE);
  PS SW_new = result_new.get_region(loc_SW);
  PS SE_new = result_new.get_region(loc_SE);

  bool NW_flag = (NW_new.geometrically_covers(NW_old) && NW_old.geometrically_covers(NW_new));
  bool NE_flag = (NE_new.geometrically_covers(NE_old) && NE_old.geometrically_covers(NE_new));
  bool SW_flag = (SW_new.geometrically_covers(SW_old) && SW_old.geometrically_covers(SW_new));
  bool SE_flag = (SE_new.geometrically_covers(SE_old) && SE_old.geometrically_covers(SE_new));

  return (NW_flag && NE_flag && SW_flag && SE_flag);
}


bool safety_scenario_TNC3det_sormglobal(){

  using namespace NaPoly;

  Parser t_parser(TNC_MODEL, path + "safety/TNC_3_sor_global_config.xml");
  Automaton t_automata = t_parser.get_automaton();
  OptionsBundle t_optbundle = t_parser.get_options_bundle();

  Location loc_NW = t_automata.get_location("NW");
  Location loc_NE = t_automata.get_location("NE");
  Location loc_SW = t_automata.get_location("SW");
  Location loc_SE = t_automata.get_location("SE");

  SafetyScenario test_object(t_automata, t_optbundle);

  PS NW_old = read_powerset(path + "safety/TNC3det_sormglobal_NW.poly", xyt_map);
  PS NE_old = read_powerset(path + "safety/TNC3det_sormglobal_NE.poly", xyt_map);
  PS SW_old = read_powerset(path + "safety/TNC3det_sormglobal_SW.poly", xyt_map);
  PS SE_old = read_powerset(path + "safety/TNC3det_sormglobal_SE.poly", xyt_map);

  /* Used to avoid debugging messages during "launch_modded". */
  // std::cout.setstate(std::ios_base::failbit);

  test_object.launch();
  StateSet result_new = test_object.get_safe_region();
  
  /* Used to clear cout to print any new string before this statement.*/
  // std::cout.clear();

  PS NW_new = result_new.get_region(loc_NW);
  PS NE_new = result_new.get_region(loc_NE);
  PS SW_new = result_new.get_region(loc_SW);
  PS SE_new = result_new.get_region(loc_SE);

  bool NW_flag = (NW_new.geometrically_covers(NW_old) && NW_old.geometrically_covers(NW_new));
  bool NE_flag = (NE_new.geometrically_covers(NE_old) && NE_old.geometrically_covers(NE_new));
  bool SW_flag = (SW_new.geometrically_covers(SW_old) && SW_old.geometrically_covers(SW_new));
  bool SE_flag = (SE_new.geometrically_covers(SE_old) && SE_old.geometrically_covers(SE_new));

  return (NW_flag && NE_flag && SW_flag && SE_flag);
}


bool reachability_maze_rwa_via_rwa_maps_small(){
  Parser t_parser(path + "reachability/maze_3det.xml",
     path + "reachability/maze_rwa_rwa_maps_small_config.xml");

  Automaton t_automata = t_parser.get_automaton();
  OptionsBundle t_optbundle = t_parser.get_options_bundle();

  Location loc_N = t_automata.get_location("N");
  Location loc_E = t_automata.get_location("E");
  Location loc_S = t_automata.get_location("S");
  Location loc_W = t_automata.get_location("W");
  Location loc_Abort = t_automata.get_location("Abort");

  ReachScenario test_object(t_automata, t_optbundle);

  PS W_old = read_powerset(path + "reachability/maze3det_rwacemap_small_W.poly", xyt_map);
  PS N_old = read_powerset(path + "reachability/maze3det_rwacemap_small_N.poly", xyt_map);
  PS E_old = read_powerset(path + "reachability/maze3det_rwacemap_small_E.poly", xyt_map);
  PS S_old = read_powerset(path + "reachability/maze3det_rwacemap_small_S.poly", xyt_map);
  PS Abort_old = read_powerset(path + "reachability/maze3det_rwacemap_small_Abort.poly", xyt_map);

  test_object.launch();
  StateSet result_new = test_object.get_reached_states();

  PS N_new = result_new.get_region(loc_N);
  PS E_new = result_new.get_region(loc_E);
  PS S_new = result_new.get_region(loc_S);
  PS W_new = result_new.get_region(loc_W);
  PS Abort_new = result_new.get_region(loc_Abort);

  bool N_flag = (N_new.geometrically_covers(N_old) && N_old.geometrically_covers(N_new));
  bool E_flag = (E_new.geometrically_covers(E_old) && E_old.geometrically_covers(E_new));
  bool S_flag = (S_new.geometrically_covers(S_old) && S_old.geometrically_covers(S_new));
  bool W_flag = (W_new.geometrically_covers(W_old) && W_old.geometrically_covers(W_new));
  bool Abort_flag = (Abort_new.geometrically_covers(Abort_old) && Abort_old.geometrically_covers(Abort_new));

  // cout << "\nN:" << N_flag << "\nE:" << E_flag << "\nS:" << S_flag << "\nW:" << W_flag << "\nAbort:" << Abort_flag << endl;

  return (N_flag && E_flag && S_flag && W_flag && Abort_flag);
}


bool reachability_maze_rwa_via_rwa_maps_large(){
  Parser t_parser(path + "reachability/maze_3det.xml",
     path + "reachability/maze_rwa_rwa_maps_large_config.xml");

  Automaton t_automata = t_parser.get_automaton();
  OptionsBundle t_optbundle = t_parser.get_options_bundle();

  Location loc_N = t_automata.get_location("N");
  Location loc_E = t_automata.get_location("E");
  Location loc_S = t_automata.get_location("S");
  Location loc_W = t_automata.get_location("W");
  Location loc_Abort = t_automata.get_location("Abort");

  ReachScenario test_object(t_automata, t_optbundle);

  PS W_old = read_powerset(path + "reachability/maze3det_rwacemap_small_W.poly", xyt_map);
  PS N_old = read_powerset(path + "reachability/maze3det_rwacemap_small_N.poly", xyt_map);
  PS E_old = read_powerset(path + "reachability/maze3det_rwacemap_small_E.poly", xyt_map);
  PS S_old = read_powerset(path + "reachability/maze3det_rwacemap_small_S.poly", xyt_map);
  PS Abort_old = read_powerset(path + "reachability/maze3det_rwacemap_small_Abort.poly", xyt_map);

  test_object.launch();
  StateSet result_new = test_object.get_reached_states();

  PS N_new = result_new.get_region(loc_N);
  PS E_new = result_new.get_region(loc_E);
  PS S_new = result_new.get_region(loc_S);
  PS W_new = result_new.get_region(loc_W);
  PS Abort_new = result_new.get_region(loc_Abort);

  bool N_flag = (N_new.geometrically_covers(N_old) && N_old.geometrically_covers(N_new));
  bool E_flag = (E_new.geometrically_covers(E_old) && E_old.geometrically_covers(E_new));
  bool S_flag = (S_new.geometrically_covers(S_old) && S_old.geometrically_covers(S_new));
  bool W_flag = (W_new.geometrically_covers(W_old) && W_old.geometrically_covers(W_new));
  bool Abort_flag = (Abort_new.geometrically_covers(Abort_old) && Abort_old.geometrically_covers(Abort_new));

  // cout << "\nN:" << N_flag << "\nE:" << E_flag << "\nS:" << S_flag << "\nW:" << W_flag << "\nAbort:" << Abort_flag << endl;

  return (N_flag && E_flag && S_flag && W_flag && Abort_flag);
}


bool reachability_maze_rwa_via_sor_local_small(){
  Parser t_parser(path + "reachability/maze_3det.xml",
     path + "reachability/maze_rwa_sor_local_small_config.xml");

  Automaton t_automata = t_parser.get_automaton();
  OptionsBundle t_optbundle = t_parser.get_options_bundle();

  Location loc_N = t_automata.get_location("N");
  Location loc_E = t_automata.get_location("E");
  Location loc_S = t_automata.get_location("S");
  Location loc_W = t_automata.get_location("W");
  Location loc_Abort = t_automata.get_location("Abort");

  ReachScenario test_object(t_automata, t_optbundle);

  PS W_old = read_powerset(path + "reachability/maze3det_rwacemap_small_W.poly", xyt_map);
  PS N_old = read_powerset(path + "reachability/maze3det_rwacemap_small_N.poly", xyt_map);
  PS E_old = read_powerset(path + "reachability/maze3det_rwacemap_small_E.poly", xyt_map);
  PS S_old = read_powerset(path + "reachability/maze3det_rwacemap_small_S.poly", xyt_map);
  PS Abort_old = read_powerset(path + "reachability/maze3det_rwacemap_small_Abort.poly", xyt_map);

  test_object.launch();
  StateSet result_new = test_object.get_reached_states();

  PS N_new = result_new.get_region(loc_N);
  PS E_new = result_new.get_region(loc_E);
  PS S_new = result_new.get_region(loc_S);
  PS W_new = result_new.get_region(loc_W);
  PS Abort_new = result_new.get_region(loc_Abort);

  bool N_flag = (N_new.geometrically_covers(N_old) && N_old.geometrically_covers(N_new));
  bool E_flag = (E_new.geometrically_covers(E_old) && E_old.geometrically_covers(E_new));
  bool S_flag = (S_new.geometrically_covers(S_old) && S_old.geometrically_covers(S_new));
  bool W_flag = (W_new.geometrically_covers(W_old) && W_old.geometrically_covers(W_new));
  bool Abort_flag = (Abort_new.geometrically_covers(Abort_old) && Abort_old.geometrically_covers(Abort_new));

  // cout << "\nN:" << N_flag << "\nE:" << E_flag << "\nS:" << S_flag << "\nW:" << W_flag << "\nAbort:" << Abort_flag << endl;

  return (N_flag && E_flag && S_flag && W_flag && Abort_flag);
}


bool reachability_maze_sor_via_sor_local_small(){
  Parser t_parser(path + "reachability/maze_3det.xml",
     path + "reachability/maze_sor_sor_local_small_config.xml");

  Automaton t_automata = t_parser.get_automaton();
  OptionsBundle t_optbundle = t_parser.get_options_bundle();

  Location loc_N = t_automata.get_location("N");
  Location loc_E = t_automata.get_location("E");
  Location loc_S = t_automata.get_location("S");
  Location loc_W = t_automata.get_location("W");
  Location loc_Abort = t_automata.get_location("Abort");

  ReachScenario test_object(t_automata, t_optbundle);

  PS W_old = read_powerset(path + "reachability/maze3det_rwacemap_small_W.poly", xyt_map);
  PS N_old = read_powerset(path + "reachability/maze3det_rwacemap_small_N.poly", xyt_map);
  PS E_old = read_powerset(path + "reachability/maze3det_rwacemap_small_E.poly", xyt_map);
  PS S_old = read_powerset(path + "reachability/maze3det_rwacemap_small_S.poly", xyt_map);
  PS Abort_old = read_powerset(path + "reachability/maze3det_rwacemap_small_Abort.poly", xyt_map);

  test_object.launch();
  StateSet result_new = test_object.get_reached_states();

  PS N_new = result_new.get_region(loc_N);
  PS E_new = result_new.get_region(loc_E);
  PS S_new = result_new.get_region(loc_S);
  PS W_new = result_new.get_region(loc_W);
  PS Abort_new = result_new.get_region(loc_Abort);

  bool N_flag = (N_new.geometrically_covers(N_old) && N_old.geometrically_covers(N_new));
  bool E_flag = (E_new.geometrically_covers(E_old) && E_old.geometrically_covers(E_new));
  bool S_flag = (S_new.geometrically_covers(S_old) && S_old.geometrically_covers(S_new));
  bool W_flag = (W_new.geometrically_covers(W_old) && W_old.geometrically_covers(W_new));
  bool Abort_flag = (Abort_new.geometrically_covers(Abort_old) && Abort_old.geometrically_covers(Abort_new));

  // cout << "\nN:" << N_flag << "\nE:" << E_flag << "\nS:" << S_flag << "\nW:" << W_flag << "\nAbort:" << Abort_flag << endl;

  return (N_flag && E_flag && S_flag && W_flag && Abort_flag);
}


bool reachability_maze_sor_via_rwa_maps_small(){
  Parser t_parser(path + "reachability/maze_3det.xml",
     path + "reachability/maze_sor_rwa_maps_small_config.xml");

  Automaton t_automata = t_parser.get_automaton();
  OptionsBundle t_optbundle = t_parser.get_options_bundle();

  Location loc_N = t_automata.get_location("N");
  Location loc_E = t_automata.get_location("E");
  Location loc_S = t_automata.get_location("S");
  Location loc_W = t_automata.get_location("W");
  Location loc_Abort = t_automata.get_location("Abort");

  ReachScenario test_object(t_automata, t_optbundle);

  PS W_old = read_powerset(path + "reachability/maze3det_rwacemap_small_W.poly", xyt_map);
  PS N_old = read_powerset(path + "reachability/maze3det_rwacemap_small_N.poly", xyt_map);
  PS E_old = read_powerset(path + "reachability/maze3det_rwacemap_small_E.poly", xyt_map);
  PS S_old = read_powerset(path + "reachability/maze3det_rwacemap_small_S.poly", xyt_map);
  PS Abort_old = read_powerset(path + "reachability/maze3det_rwacemap_small_Abort.poly", xyt_map);

  test_object.launch();
  StateSet result_new = test_object.get_reached_states();

  PS N_new = result_new.get_region(loc_N);
  PS E_new = result_new.get_region(loc_E);
  PS S_new = result_new.get_region(loc_S);
  PS W_new = result_new.get_region(loc_W);
  PS Abort_new = result_new.get_region(loc_Abort);

  bool N_flag = (N_new.geometrically_covers(N_old) && N_old.geometrically_covers(N_new));
  bool E_flag = (E_new.geometrically_covers(E_old) && E_old.geometrically_covers(E_new));
  bool S_flag = (S_new.geometrically_covers(S_old) && S_old.geometrically_covers(S_new));
  bool W_flag = (W_new.geometrically_covers(W_old) && W_old.geometrically_covers(W_new));
  bool Abort_flag = (Abort_new.geometrically_covers(Abort_old) && Abort_old.geometrically_covers(Abort_new));

  // cout << "\nN:" << N_flag << "\nE:" << E_flag << "\nS:" << S_flag << "\nW:" << W_flag << "\nAbort:" << Abort_flag << endl;

  return (N_flag && E_flag && S_flag && W_flag && Abort_flag);
}
